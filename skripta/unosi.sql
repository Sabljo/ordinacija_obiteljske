--ZAPOSLENICI---
call unos_zaposlenika('Luka', 'Mari�', 'Ru�ina 10, Osijek', '0913603214',10000.00 ,'doktor',48735974);
call unos_zaposlenika('Antonija', 'Leki�', 'Vratni�ka 6, Osijek','0985742316',11000.00,'doktor',60725170);
call unos_zaposlenika('Zvonimir', 'Jelkovi�', 'Vukovarska 128, Osijek','0995892317',11000.00,'doktor',88705971);
call unos_zaposlenika('Sandra', 'Matkovi�', '�upanijska 32, Osijek','0956237831',10500.00,'doktor',90731979);

call unos_zaposlenika('Petra', 'Sabljo', 'Vratni�ka 6,�epin', '0996778520',6800,'med.sestra',123455);
call unos_zaposlenika('Marko', 'Markovi�', 'Kolodvorska 24, Osijek','0918527852',7100, 'med.sestra',223344 );
call unos_zaposlenika('Klara', 'Prgi�', 'Osje�ka 76, Tenja', '0986435352',6000,'med.sestra',145678);
call unos_zaposlenika('Klaudija', 'Maro�evi�', 'Ul. Stjepana Radi�a 67, Vuka ', '0916632752',7000, 'med.sestra',220909);

--RADNO VRIJEME--
ALTER TABLE radno_vrijeme
ADD CONSTRAINT rad_vri_ck
CHECK(radna_subota IN ('1.','2.','3.','4.'));

insert into radno_vrijeme  values (100,'poslijepodne','prijepodne','4.');
insert into radno_vrijeme  values (200,'poslijepodne','prijepodne','1.');
insert into radno_vrijeme  values (300,'prijepodne','poslijepodne','3.');
insert into radno_vrijeme  values (400,'prijepodne','poslijepodne','2.');

--ORDINACIJA---
insert into ordinacija values(1,'Osijek,Prolaz Snje�ne Gospe 1B','031/501-090',100,1,5);
insert into ordinacija values(2,'Osijek, Bi�evska 32','031/333-018',200,2,6);
insert into ordinacija values(3,'Ivanovac, Duga 33','031/507-377',300,3,7);
insert into ordinacija values(4,'�epin, Grada Vukovara 6b','031/302-651',400,4,8);

--PACIJENTI---
--ordinacija4---
INSERT INTO pacijent VALUES(1,'Ivo', 'Filipovi�', 'M', '07/10/1937', 'Psunjska 3,�epin', '0912414367',4);
INSERT INTO pacijent VALUES(2,'Ana', '�aravanja', 'F', '07/12/2000', 'Osje�ka 7,�epin', '099677851',4);
INSERT INTO pacijent VALUES(3,'Matea', 'Filipovi�', 'F', '03/08/1943', 'Vratni�ka 3,�epin', '0918414367',4);
INSERT INTO pacijent VALUES(4,'Anita', 'Bari�i�', 'F', '11/11/2018', 'Svilajska 33,�epin','0913214367',4);
INSERT INTO pacijent VALUES(5,'Sara', 'Filipovi�', 'F', '31/12/1999', 'Psunjska 26,�epin', '0915614237',4);
INSERT INTO pacijent VALUES(6,'Tea', 'Bakovi�', 'F', '09/03/1968', 'Ul.Zrinske gore 16,�epin', '0958956234',4);
INSERT INTO pacijent VALUES(7,'Lea', 'Vuki�', 'F', '17/04/1956', 'Osje�ka 120,�epin','0950552341',4);
INSERT INTO pacijent VALUES(8,'Lucija', 'Pospi�il', 'F', '05/05/1945', 'Kalni�ka 13,�epin', '0951234567',4);
INSERT INTO pacijent VALUES(9,'Ante', 'Kne�evi�', 'M', '04/06/1998', 'Psunjska 120,�epin', '0951122333',4);
INSERT INTO pacijent VALUES(10,'Josip', 'Varga', 'M', '13/07/1999', 'Kalni�ka 44,�epin', '0992244555',4);
INSERT INTO pacijent VALUES(11,'Filip', 'Sari�', 'M', '02/08/1996', 'Ul.Petrove gore 3,�epin', '0992323828',4);
INSERT INTO pacijent VALUES(12,'Mirko', 'Stanojlovi�', 'M', '01/09/1982', 'Ul.Petrove gore 92,�epin', '0990994367',4);
INSERT INTO pacijent VALUES(13,'David', 'Lovri�', 'M', '28/12/2006', 'Drinska 43,�epin', '0992466553',4);
INSERT INTO pacijent VALUES(14,'David', 'Stipi�', 'M', '31/01/1944', 'Drinska 2,�epin', '0992414123',4);
INSERT INTO pacijent VALUES(15,'Ivan', 'Maro�evi�', 'M', '18/02/1978', 'Vratni�ka 81,�epin', '0982424457',4);
INSERT INTO pacijent VALUES(31,'Sandra', 'Mati�', 'F', '01/10/1937', 'Ulica kralja Zvonimira 3,�epin', '0913314367',4);
INSERT INTO pacijent VALUES(32,'Ana', 'Vrdoljak', 'F', '07/10/2000', 'Osje�ka 7,�epin', '099371821',4);
INSERT INTO pacijent VALUES(33,'Matija', 'Stani�', 'F', '03/08/1999', 'Ulica kralja Zvonimira 7,�epin', '0912404267',4);
INSERT INTO pacijent VALUES(34,'Antonela', 'Bari�i�', 'F', '11/04/2012', 'Bilogorska 28,�epin', '0911204367',4);
INSERT INTO pacijent VALUES(35,'Sanja', 'Mitrovi�', 'F', '30/11/1999', 'Bilogorska 91,�epin', '09152142070',4);
INSERT INTO pacijent VALUES(36,'Teuta', 'Bari�i�', 'F', '02/03/1969', 'Ul.Eugena Kvaternika 16,�epin', '0958116234',4);
INSERT INTO pacijent VALUES(37,'Lana', 'Klari�', 'F', '01/04/1959', 'Ul.Eugena Kvaternika 100,�epin', '0950522341',4);
INSERT INTO pacijent VALUES(38,'Petra', 'Buconji�', 'F', '05/05/1949', 'Kalni�ka 13,�epin', '0951232267',4);
INSERT INTO pacijent VALUES(39,'Stipe', 'Kne�evi�', 'M', '04/06/1998', 'Savska 1,�epin', '0951112313',4);
INSERT INTO pacijent VALUES(40,'Josip', 'Buconji�', 'M', '13/07/1992', 'Svaska 4,�epin', '0992244585',4);

--ordinacija2--
INSERT INTO pacijent VALUES(56,'Goran', 'Bilo�', 'M', '02/08/1992', 'Diljska 31,Osijek', '0992123001',2);
INSERT INTO pacijent VALUES(57,'Mislav', 'Stani�', 'M', '10/09/1980', 'Diljska 1,Osijek', '0990914003',2);
INSERT INTO pacijent VALUES(58,'Slaven', 'Brajdi�', 'M', '24/12/2001', 'Diljska 1B,Osijek', '0992486004',2);
INSERT INTO pacijent VALUES(59,'Davor', 'Biskup', 'M', '04/01/1948', 'Dalmatinska 18,Osijek', '0992412101',2);
INSERT INTO pacijent VALUES(60,'Ivan', 'Magdi�', 'M', '12/02/1971', 'Dalmatinska 10,Osijek', '0982424090',2);
INSERT INTO pacijent VALUES(61,'Fran', 'Leventi�', 'M', '02/04/1990', 'Mlinska 6B,Osijek', '0951202070',2);
INSERT INTO pacijent VALUES(62,'Lovro', 'Adamovi�', 'M', '05/09/1958', 'Mlinska 120,Osijek', '0951105600',2);
INSERT INTO pacijent VALUES(63,'Petar', 'Agi�', 'M', '21/12/2009', 'Zve�evska 41,Osijek', '0951002009',2);
INSERT INTO pacijent VALUES(64,'Dinko', 'Vuleti�', 'M', '29/01/1952', 'Dunavska 56,Osijek', '0951230901',2);
INSERT INTO pacijent VALUES(65,'Leon', 'Peri�', 'M', '18/02/1970', 'Dunavska 11,Osijek', '0951200055',2);
INSERT INTO pacijent VALUES(66,'Karla', 'Bajkovi�', 'F', '08/08/1996', 'Dunavska 31,Osijek', '0992129122',2);
INSERT INTO pacijent VALUES(67,'Renata', 'Kalini�', 'F', '10/09/1967', 'Dalmatinska 20,Osijek', '0990914262',2);
INSERT INTO pacijent VALUES(68,'Zdravka', 'Brlek', 'F', '19/12/2018', 'Ul.Ivana Gunduli�a 41,Osijek', '0992406053',2);
INSERT INTO pacijent VALUES(69,'Dragica', '�trlek', 'F', '04/10/1944', 'Ul.Ivana Gunduli�a 80,Osijek', '0992410101',2);
INSERT INTO pacijent VALUES(70,'Ivona', 'Mili�', 'F', '12/01/1988', 'Reisnerova 22,Osijek', '0982420050',2);
INSERT INTO pacijent VALUES(71,'Filipa', 'Rebi�', 'F', '02/04/1953', 'Reisnerova 32,Osijek', '0951200323',2);
INSERT INTO pacijent VALUES(72,'Petra', 'Karlovi�', 'F', '05/09/1982', 'Ru�ina 90,Osijek', '0951105070',2);
INSERT INTO pacijent VALUES(73,'Patricija', 'Ami�', 'F', '21/12/2007', 'Ru�ina 41,Osijek', '0951002101',2);
INSERT INTO pacijent VALUES(74,'Danica', 'Dali�', 'F', '20/01/1959', 'Zve�evska 56,Osijek', '0951230909',2);
INSERT INTO pacijent VALUES(75,'Leona', 'Pranji�', 'F', '18/01/1978', 'Vratni�ka 11,Osijek', '0951204350',2);
INSERT INTO pacijent VALUES(76,'Milka', '�ivkovi�', 'F', '10/04/1981', 'Ul.Stjepana Radi�a 23,Osijek', '0990914764',2);
INSERT INTO pacijent VALUES(77,'Slaven', 'Svalina', 'M', '24/10/2002', 'Ul.Stjepana Radi�a 42,Osijek', '0992486753',2);
INSERT INTO pacijent VALUES(78,'Danijel', 'Rogina', 'M', '04/09/1940', 'Ul.Stjepana Radi�a 8,Osijek', '0992410021',2);
INSERT INTO pacijent VALUES(79,'Ivica', 'Markovi�', 'M', '11/02/1975', 'Vratni�ka 22,Osijek', '0982420050',2);
INSERT INTO pacijent VALUES(80,'Predrag', 'Prpi�', 'M', '02/08/1992', 'Na�i�ka 32,Osijek', '0951202113',2);

--ordinacija3--
INSERT INTO pacijent VALUES(81,'Milo�', 'Bali�', 'M', '02/08/1997', 'Duga 30,Ivanovac', '0992123128',3);
INSERT INTO pacijent VALUES(82,'Stanko', 'Pavi�', 'M', '10/09/1981', 'Duga 1,Ivanovac', '0990914267',3);
INSERT INTO pacijent VALUES(83,'Ante', 'Bilo�', 'M', '20/12/2018', 'Duga 15,Ivanovac', '0992486753',3);
INSERT INTO pacijent VALUES(84,'Dragan', 'Pavi�', 'M', '04/09/1944', 'Dr�ani�ka 15,Ivanovac', '0992412121',3);
INSERT INTO pacijent VALUES(85,'Tomislav', 'Ivi�', 'M', '17/02/1978', 'Dr�ani�ka 10,Ivanovac', '0982424050',3);
INSERT INTO pacijent VALUES(86,'Franjo', 'Kraljevi�', 'M', '02/04/1992', 'Dr�ani�ka 1,Ivanovac', '0951202323',3);
INSERT INTO pacijent VALUES(87,'Leon', 'Oberman', 'M', '05/09/1982', 'Grobni�ka 67,Ivanovac', '0951105670',3);
INSERT INTO pacijent VALUES(88,'Perislav', 'Milo�evi�', 'M', '21/12/2007', 'Grobni�ka 53,Ivanovac', '0951002131',3);
INSERT INTO pacijent VALUES(89,'Ladislav', 'Matijevi�', 'M', '29/01/1959', '�epinska 56,Ivanovac', '0951230900',3);
INSERT INTO pacijent VALUES(90,'Kre�imir', 'Pa�anin', 'M', '18/02/1978', '�epinska 11,Ivanovac', '0951204355',3);
INSERT INTO pacijent VALUES(91,'Marin', 'Pa�anin', 'M', '02/08/1990', 'Mala 1,Ivanovac', '0992123120',3);
INSERT INTO pacijent VALUES(92,'Milica', 'Kraljevi�', 'F', '10/02/1986', 'Crkvena 5,Ivanovac', '0990914007',3);
INSERT INTO pacijent VALUES(93,'Sandra', 'Oberman', 'F', '22/11/2000', 'Crkvena 10,Ivanovac', '0992486703',3);
INSERT INTO pacijent VALUES(94,'Lucija', 'Butkovi�', 'F', '04/01/1949', 'Crkvena 34,Ivanovac', '0992812921',3);
INSERT INTO pacijent VALUES(95,'Ivana', 'Stani�', 'F', '12/01/1958', 'Crkvena 19,Ivanovac', '0980424950',3);
INSERT INTO pacijent VALUES(96,'Franka', 'Babi�', 'F', '08/04/1982', 'Mala 26,Ivanovac', '0951202993',3);
INSERT INTO pacijent VALUES(97,'Korina', 'Adam�evi�', 'F', '05/09/1972', 'Mala 12,Ivanovac', '0951105990',3);
INSERT INTO pacijent VALUES(98,'Pera', 'Agati�', 'F', '02/12/2009', '�epinska 19,Ivanovac', '0951000101',3);
INSERT INTO pacijent VALUES(99,'Dajana', '�eravica', 'F', '29/01/1951', 'Grobni�ka 4,Ivanovac', '0951210910',3);
INSERT INTO pacijent VALUES(100,'Laura', 'Peri�', 'F', '18/01/1965', 'Grobni�ka 15B,Ivanovac', '0951204388',3);
INSERT INTO pacijent VALUES(41,'Mirka', 'Batrnek', 'F', '10/09/1951', 'Dr�ani�ka 56,Ivanovac', '0990900267',3);
INSERT INTO pacijent VALUES(42,'Tea', 'Batrnek', 'F', '24/11/2002', 'Dr�ani�ka 23,Ivanovac', '0992580753',3);
INSERT INTO pacijent VALUES(43,'Aleksandra', '�eravica', 'F', '09/02/1933', 'Duga 100,Ivanovac', '0992456721',3);
INSERT INTO pacijent VALUES(44,'Dragana', 'Volmut', 'F', '10/02/1973', 'Duga 98,Ivanovac', '0982499850',3);
INSERT INTO pacijent VALUES(45,'Filipa', 'Babi�', 'F', '02/05/1983', 'Duga 45,Ivanovac', '0951204563',3);

--ordinacija 1---
INSERT INTO pacijent VALUES(16,'Aleksandar', 'Dugand�i�', 'M', '07/10/1968', 'Psunjska 3,Osijek', '0996770851',1);
INSERT INTO pacijent VALUES(17,'Antonela', 'Duvnjak', 'F', '09/11/2000', 'Osje�ka 7,Osijek', '0996228510',1);
INSERT INTO pacijent VALUES(18,'Matija', 'Filipovi�', 'F', '03/08/1991', 'Vratni�ka 3,Osijek', '0996724810',1);
INSERT INTO pacijent VALUES(19,'Anita', 'Bari�i�', 'F', '11/11/2018', '�upanijska 33,Osijek', '0996770009',1);
INSERT INTO pacijent VALUES(20,'Tara', 'Mesar', 'F', '21/12/1995', 'Istarska 26,Osijek', '0996701010',1);
INSERT INTO pacijent VALUES(21,'Teuta', 'Bakovi�', 'F', '02/03/1963', '�upanijska 16,Osijek', '0912417777',1);
INSERT INTO pacijent VALUES(22,'Lea', 'Vu�kovi�', 'F', '11/04/1958', 'Osje�ka 120,Osijek', '0912422367',1);
INSERT INTO pacijent VALUES(23,'Lucija', 'Torbica', 'F', '05/05/1945', 'Kozja�ka 13,Osijek', '0912400207',1);
INSERT INTO pacijent VALUES(24,'Antonio', 'Karlovi�', 'M', '04/06/1998', 'Istarska 120,Osijek', '0910024367',1);
INSERT INTO pacijent VALUES(25,'Juraj', 'Varga', 'M', '13/07/1999', 'Kozja�ka 44,Osijek', '0911100367',1);
INSERT INTO pacijent VALUES(26,'Filip', 'Klari�', 'M', '02/06/1992', 'Kolodvorska 3,Osijek', '0951222333',1);
INSERT INTO pacijent VALUES(27,'Stipe', 'Lovri�', 'M', '02/09/1982', 'Kolodvorska 92,Osijek', '0951145673',1);
INSERT INTO pacijent VALUES(28,'David', 'Lovkovi��', 'M', '22/12/2008', 'Drinska 49,Osijek', '0951002233',1);
INSERT INTO pacijent VALUES(29,'Dalibor', 'Vu�ak', 'M', '31/01/1951', 'Drinska 21,Osijek', '0951231100',1);
INSERT INTO pacijent VALUES(30,'Ivano', 'Matijevi�', 'M', '23/02/1975', 'Vratni�ka 81,Osijek', '0951224455',1);
INSERT INTO pacijent VALUES(46,'Antonio', 'Datulja', 'M', '11/11/1931', 'Vukovarska 22,Osijek', '0996118510',1);
INSERT INTO pacijent VALUES(47,'Antonia', 'Duki�', 'F', '02/10/2006', 'Ilirska 7,Osijek', '0996028017',1);
INSERT INTO pacijent VALUES(48,'Marija', 'Jak�i�', 'F', '03/08/1988', 'Vukovarska 3,Osijek', '0996224315',1);
INSERT INTO pacijent VALUES(49,'An�elka', 'Bari�i�', 'F', '11/09/2014', 'Diljska 25,Osijek', '0996770402',1);
INSERT INTO pacijent VALUES(50,'Tatjana', 'Kotromanovi�', 'F', '20/12/1992', 'Diljska 45,Osijek', '0996708810',1);
INSERT INTO pacijent VALUES(51,'Tea', 'Toli�', 'F', '02/04/1967', 'Dalmatinska 11,Osijek', '0912410717',1);
INSERT INTO pacijent VALUES(52,'Lea', 'Lon�ar', 'F', '12/04/1951', 'Dalmatinska 101,Osijek', '0912022367',1);
INSERT INTO pacijent VALUES(53,'Lucija', 'Vinceti�', 'F', '02/05/1955', 'Mlinska 13,Osijek', '0911100207',1);
INSERT INTO pacijent VALUES(54,'Antonio', 'Toma�i�', 'm', '04/07/1994', 'Mlinska 120,Osijek', '0910124337',1);
INSERT INTO pacijent VALUES(55,'Nikola', 'Vargi�', 'M', '11/07/1999', 'Mlinska 41,Osijek', '0911180397',1);

--osnovno zdravstveno
INSERT INTO osnovno_zdravstveno VALUES (170090305,'nositelj','Hrvatska',1);
INSERT INTO osig_pac VALUES(1, '07/10/1990', '07/10/2025',170090305);
INSERT INTO osnovno_zdravstveno VALUES (182090305,'nositelj','Hrvatska',2);
INSERT INTO osig_pac VALUES(2, '07/10/2018', '07/10/2028',182090305);
INSERT INTO osnovno_zdravstveno VALUES (170090388,'nositelj','Hrvatska',3);
INSERT INTO osig_pac VALUES(3, '07/10/1992', '07/10/2026',170090388);
INSERT INTO osnovno_zdravstveno VALUES (111190305,'�lan obitelji','Hrvatska',4);
INSERT INTO osig_pac VALUES(4, '07/12/2018', '07/12/2036',111190305);
INSERT INTO osnovno_zdravstveno VALUES (150050305,'nositelj','Hrvatska',5);
INSERT INTO osig_pac VALUES(5, '12/10/2018', '12/10/2028',150050305);
INSERT INTO osnovno_zdravstveno VALUES (270090305,'nositelj','Hrvatska',6);
INSERT INTO osig_pac VALUES(6, '07/09/1992', '07/09/2035',270090305);
INSERT INTO osnovno_zdravstveno VALUES (440090305,'nositelj','Hrvatska',7);
INSERT INTO osig_pac VALUES(7, '01/01/2020', '01/01/2045',440090305);
INSERT INTO osnovno_zdravstveno VALUES (530090305,'nositelj','Hrvatska',8);
INSERT INTO osig_pac VALUES(8, '12/12/2008', '12/12/2030',530090305);
INSERT INTO osnovno_zdravstveno VALUES (171230305,'nositelj','Hrvatska',9);
INSERT INTO osig_pac VALUES(9, '11/07/2016', '11/07/2026',171230305);
INSERT INTO osnovno_zdravstveno VALUES (770088805,'nositelj','Hrvatska',10);
INSERT INTO osig_pac VALUES(10, '31/07/2017', '31/07/2027',770088805);
INSERT INTO osnovno_zdravstveno VALUES (220543305,'nositelj','Hrvatska',11);
INSERT INTO osig_pac VALUES(11, '03/05/1990', '03/05/2025',220543305);
INSERT INTO osnovno_zdravstveno VALUES (330190305,'nositelj','Hrvatska',12);
INSERT INTO osig_pac VALUES(12, '02/11/2008', '02/11/2048',330190305);
INSERT INTO osnovno_zdravstveno VALUES (110391305,'�lan obitelji','Hrvatska',13);
INSERT INTO osig_pac VALUES(13, '30/12/2006', '30/12/2024',110391305);
INSERT INTO osnovno_zdravstveno VALUES (170596385,'nositelj','Hrvatska',14);
INSERT INTO osig_pac VALUES(14, '07/05/2009', '07/05/2039',170596385);
INSERT INTO osnovno_zdravstveno VALUES (170092255,'nositelj','Hrvatska',15);
INSERT INTO osig_pac VALUES(15, '01/10/1997', '01/10/2047',170092255);
INSERT INTO osnovno_zdravstveno VALUES (220543105,'nositelj','Hrvatska',31);
INSERT INTO osig_pac VALUES(16, '02/11/2000', '02/11/2030',220543105);
INSERT INTO osnovno_zdravstveno VALUES (330190306,'nositelj','Hrvatska',32);
INSERT INTO osig_pac VALUES(17, '07/10/2018', '07/10/2028',330190306);
INSERT INTO osnovno_zdravstveno VALUES (110391302,'nositelj','Hrvatska',33);
INSERT INTO osig_pac VALUES(18, '20/08/2017', '20/08/2027',110391302);
INSERT INTO osnovno_zdravstveno VALUES (170516385,'�lan obitelji','Hrvatska',34);
INSERT INTO osig_pac VALUES(19, '11/04/2012', '11/04/2030',170516385);
INSERT INTO osnovno_zdravstveno VALUES (170092245,'nositelj','Hrvatska',35);
INSERT INTO osig_pac VALUES(20, '07/12/2017', '07/12/2027',170092245);
INSERT INTO osnovno_zdravstveno VALUES (342090405,'nositelj','Hrvatska',36);
INSERT INTO osig_pac VALUES(21, '01/10/1992', '01/10/2032',342090405);
INSERT INTO osnovno_zdravstveno VALUES (129090300,'nositelj','Hrvatska',37);
INSERT INTO osig_pac VALUES(22, '07/02/1985', '07/02/2022',129090300);
INSERT INTO osnovno_zdravstveno VALUES (170011000,'nositelj','Njema�ka',38);
INSERT INTO osig_pac VALUES(23, '20/10/2001', '20/10/2041',170011000);
INSERT INTO osnovno_zdravstveno VALUES (103450305,'nositelj','Hrvatska',39);
INSERT INTO osig_pac VALUES(24, '07/10/2017', '07/10/2027',103450305);
INSERT INTO osnovno_zdravstveno VALUES (187590005,'nositelj','Hrvatska',40);
INSERT INTO osig_pac VALUES(25, '05/11/2019', '05/11/2059',187590005);


--nova ordinacija---
INSERT INTO osnovno_zdravstveno VALUES (340090305,'nositelj','Hrvatska',56);
INSERT INTO osig_pac VALUES(26, '11/10/2015', '11/10/2055',340090305);
INSERT INTO osnovno_zdravstveno VALUES (729090305,'nositelj','Hrvatska',57);
INSERT INTO osig_pac VALUES(27, '07/09/2007', '07/09/2047',729090305);
INSERT INTO osnovno_zdravstveno VALUES (174411005,'nositelj','Hrvatska',58);
INSERT INTO osig_pac VALUES(28, '07/01/2019', '07/01/2029',174411005);
INSERT INTO osnovno_zdravstveno VALUES (123459905,'nositelj','Hrvatska',59);
INSERT INTO osig_pac VALUES(29, '09/10/2004', '09/10/2044',123459905);
INSERT INTO osnovno_zdravstveno VALUES (187330305,'nositelj','Hrvatska',60);
INSERT INTO osig_pac VALUES(30, '07/06/1995', '07/06/2045',187330305);
INSERT INTO osnovno_zdravstveno VALUES (211543305,'nositelj','Hrvatska',61);
INSERT INTO osig_pac VALUES(31, '05/07/2017', '05/07/2057',211543305);
INSERT INTO osnovno_zdravstveno VALUES (312190305,'nositelj','Hrvatska',62);
INSERT INTO osig_pac VALUES(32, '03/11/2011', '03/11/2041',312190305);
INSERT INTO osnovno_zdravstveno VALUES (155391305,'�lan obitelji','Hrvatska',63);
INSERT INTO osig_pac VALUES(33, '30/12/2009', '30/12/2028',155391305);
INSERT INTO osnovno_zdravstveno VALUES (170096385,'nositelj','Hrvatska',64);
INSERT INTO osig_pac VALUES(34, '19/10/2003', '19/10/2043',170096385);
INSERT INTO osnovno_zdravstveno VALUES (170091155,'nositelj','Hrvatska',65);
INSERT INTO osig_pac VALUES(35, '17/03/1998', '17/03/2048',170091155);
INSERT INTO osnovno_zdravstveno VALUES (342023305,'nositelj','Hrvatska',66);
INSERT INTO osig_pac VALUES(36, '02/03/2020', '02/03/2070',342023305);
INSERT INTO osnovno_zdravstveno VALUES (129090125,'nositelj','Hrvatska',67);
INSERT INTO osig_pac VALUES(37, '29/10/2005', '29/10/2035',129090125);
INSERT INTO osnovno_zdravstveno VALUES (170811005,'�lan obitelji','Hrvatska',68);
INSERT INTO osig_pac VALUES(38, '07/01/2019', '07/01/2039',170811005);
INSERT INTO osnovno_zdravstveno VALUES (123890305,'nositelj','Hrvatska',69);
INSERT INTO osig_pac VALUES(39, '06/09/2010', '06/09/2045',123890305);
INSERT INTO osnovno_zdravstveno VALUES (187090305,'nositelj','Hrvatska',70);
INSERT INTO osig_pac VALUES(40, '02/11/2014', '02/11/2054',187090305);
INSERT INTO osnovno_zdravstveno VALUES (299543305,'nositelj','Hrvatska',71);
INSERT INTO osig_pac VALUES(41, '06/10/2010', '06/10/2035',299543305);
INSERT INTO osnovno_zdravstveno VALUES (388190305,'nositelj','Hrvatska',72);
INSERT INTO osig_pac VALUES(42, '02/11/2014', '02/11/2054',388190305);
INSERT INTO osnovno_zdravstveno VALUES (910391305,'�lan obitelji','Hrvatska',73);
INSERT INTO osig_pac VALUES(43, '07/01/2008', '07/01/2026',910391305);
INSERT INTO osnovno_zdravstveno VALUES (870596385,'nositelj','Hrvatska',74);
INSERT INTO osig_pac VALUES(44, '07/10/1990', '07/10/2024',870596385);
INSERT INTO osnovno_zdravstveno VALUES (670092255,'nositelj','Hrvatska',75);
INSERT INTO osig_pac VALUES(45, '02/11/2010', '02/11/2032',670092255);
INSERT INTO osnovno_zdravstveno VALUES (345590305,'nositelj','Hrvatska',76);
INSERT INTO osig_pac VALUES(46, '02/11/2014', '02/11/2054',345590305);
INSERT INTO osnovno_zdravstveno VALUES (111090305,'nositelj','Hrvatska',77);
INSERT INTO osig_pac VALUES(47, '07/10/1990', '07/10/2025',111090305);
INSERT INTO osnovno_zdravstveno VALUES (150011005,'nositelj','Hrvatska',78);
INSERT INTO osig_pac VALUES(48, '22/03/2005', '22/03/2035',150011005);
INSERT INTO osnovno_zdravstveno VALUES (150450305,'nositelj','Hrvatska',79);
INSERT INTO osig_pac VALUES(49, '07/12/2003', '07/12/2043',150450305);
INSERT INTO osnovno_zdravstveno VALUES (187590301,'nositelj','Hrvatska',80);
INSERT INTO osig_pac VALUES(50, '12/11/2014', '12/11/2059',187590301);


--ordinacija---
INSERT INTO osnovno_zdravstveno VALUES (300090305,'nositelj','Hrvatska',81);
INSERT INTO osig_pac VALUES(51, '01/02/2020', '01/02/2060',300090305);
INSERT INTO osnovno_zdravstveno VALUES (119090305,'nositelj','Hrvatska',82);
INSERT INTO osig_pac VALUES(52, '12/08/1999', '12/08/2039',119090305);
INSERT INTO osnovno_zdravstveno VALUES (190011005,'�lan obitelji','Hrvatska',83);
INSERT INTO osig_pac VALUES(53, '30/12/1990', '30/12/2030',190011005);
INSERT INTO osnovno_zdravstveno VALUES (100450305,'nositelj','Hrvatska',84);
INSERT INTO osig_pac VALUES(54, '30/10/2014', '30/10/2044',100450305);
INSERT INTO osnovno_zdravstveno VALUES (180190305,'nositelj','Hrvatska',85);
INSERT INTO osig_pac VALUES(55, '07/07/2006', '07/07/2046',180190305);
INSERT INTO osnovno_zdravstveno VALUES (280543305,'nositelj','Hrvatska',86);
INSERT INTO osig_pac VALUES(56, '01/05/2019', '01/05/2059',280543305);
INSERT INTO osnovno_zdravstveno VALUES (380190305,'nositelj','Hrvatska',87);
INSERT INTO osig_pac VALUES(57, '01/05/2015', '01/05/2055',380190305);
INSERT INTO osnovno_zdravstveno VALUES (190391305,'�lan obitelji','Hrvatska',88);
INSERT INTO osig_pac VALUES(58, '04/01/2007', '04/01/2025',190391305);
INSERT INTO osnovno_zdravstveno VALUES (120596385,'nositelj','Hrvatska',89);
INSERT INTO osig_pac VALUES(59, '12/10/2016', '12/10/2056',120596385);
INSERT INTO osnovno_zdravstveno VALUES (120092255,'nositelj','Hrvatska',90);
INSERT INTO osig_pac VALUES(60, '07/05/2006', '07/05/2046',120092255);
INSERT INTO osnovno_zdravstveno VALUES (366090305,'nositelj','Hrvatska',91);
INSERT INTO osig_pac VALUES(61, '08/12/2008', '08/12/2058',366090305);
INSERT INTO osnovno_zdravstveno VALUES (125090305,'nositelj','Hrvatska',92);
INSERT INTO osig_pac VALUES(62, '07/03/2011', '07/03/2051',125090305);
INSERT INTO osnovno_zdravstveno VALUES (170012405,'nositelj','Hrvatska',93);
INSERT INTO osig_pac VALUES(63, '07/12/2018', '07/12/2028',170012405);
INSERT INTO osnovno_zdravstveno VALUES (123110305,'nositelj','Hrvatska',94);
INSERT INTO osig_pac VALUES(64, '17/05/2010', '17/05/2040',123110305);
INSERT INTO osnovno_zdravstveno VALUES (187070305,'nositelj','Hrvatska',95);
INSERT INTO osig_pac VALUES(65, '05/10/2009', '05/10/2020',187070305);
INSERT INTO osnovno_zdravstveno VALUES (652543305,'nositelj','Hrvatska',96);
INSERT INTO osig_pac VALUES(66, '02/09/2019', '02/09/2025',652543305);
INSERT INTO osnovno_zdravstveno VALUES (960190305,'nositelj','Hrvatska',97);
INSERT INTO osig_pac VALUES(67, '07/11/2019', '07/11/2022',960190305);
INSERT INTO osnovno_zdravstveno VALUES (530391305,'�lan obitelji','Hrvatska',98);
INSERT INTO osig_pac VALUES(68, '07/12/2009', '07/12/2027',530391305);
INSERT INTO osnovno_zdravstveno VALUES (170526385,'nositelj','Hrvatska',99);
INSERT INTO osig_pac VALUES(69, '22/05/2017', '22/05/2047',170526385);
INSERT INTO osnovno_zdravstveno VALUES (170092005,'nositelj','Hrvatska',100);
INSERT INTO osig_pac VALUES(70, '07/10/2018', '07/10/2021',170092005);
INSERT INTO osnovno_zdravstveno VALUES (942090305,'nositelj','Hrvatska',41);
INSERT INTO osig_pac VALUES(71, '07/10/1990', '07/10/2015',942090305);
INSERT INTO osnovno_zdravstveno VALUES (809090305,'�lan obitelji','Hrvatska',42);
INSERT INTO osig_pac VALUES(72, '07/12/2002', '07/12/2020',809090305);
INSERT INTO osnovno_zdravstveno VALUES (170911085,'nositelj','Hrvatska',43);
INSERT INTO osig_pac VALUES(73, '14/09/1997', '14/09/2027',170911085);
INSERT INTO osnovno_zdravstveno VALUES (120950305,'nositelj','Hrvatska',44);
INSERT INTO osig_pac VALUES(74, '07/06/1999', '07/06/2030',120950305);
INSERT INTO osnovno_zdravstveno VALUES (107590309,'nositelj','Hrvatska',45);
INSERT INTO osig_pac VALUES(75, '11/10/2003', '11/10/2043',107590309);

--ordinacija---
INSERT INTO osnovno_zdravstveno VALUES (388092305,'nositelj','Hrvatska',16);
INSERT INTO osig_pac VALUES(76, '11/03/2000', '11/03/2035',388092305);
INSERT INTO osnovno_zdravstveno VALUES (929091305,'nositelj','Hrvatska',17);
INSERT INTO osig_pac VALUES(77, '22/10/2018', '22/10/2028',929091305);
INSERT INTO osnovno_zdravstveno VALUES (165031005,'nositelj','Hrvatska',18);
INSERT INTO osig_pac VALUES(78, '07/05/2018', '07/05/2058',165031005);
INSERT INTO osnovno_zdravstveno VALUES (903450305,'�lan obitelji','Hrvatska',19);
INSERT INTO osig_pac VALUES(79, '23/11/2018', '23/11/2036',903450305);
INSERT INTO osnovno_zdravstveno VALUES (187598302,'nositelj','Hrvatska',20);
INSERT INTO osig_pac VALUES(80, '18/02/2020', '18/02/2060',187598302);
INSERT INTO osnovno_zdravstveno VALUES (290543302,'nositelj','Hrvatska',21);
INSERT INTO osig_pac VALUES(81, '07/10/1986', '07/10/2036',290543302);
INSERT INTO osnovno_zdravstveno VALUES (330197108,'nositelj','Hrvatska',23);
INSERT INTO osig_pac VALUES(82, '05/08/2010', '05/08/2040',330197108);
INSERT INTO osnovno_zdravstveno VALUES (190301308,'nositelj','Hrvatska',22);
INSERT INTO osig_pac VALUES(83, '01/11/2020', '01/11/2050',190301308);
INSERT INTO osnovno_zdravstveno VALUES (570522384,'nositelj','Hrvatska',24);
INSERT INTO osig_pac VALUES(84, '23/10/2019', '23/10/2059',570522384);
INSERT INTO osnovno_zdravstveno VALUES (500092254,'nositelj','Hrvatska',25);
INSERT INTO osig_pac VALUES(85, '07/09/2018', '07/09/2028',500092254);
INSERT INTO osnovno_zdravstveno VALUES (345590306,'nositelj','Hrvatska',26);
INSERT INTO osig_pac VALUES(86, '15/11/2018', '15/11/2058',345590306);
INSERT INTO osnovno_zdravstveno VALUES (129790386,'nositelj','Hrvatska',27);
INSERT INTO osig_pac VALUES(87, '02/08/2005', '02/08/2045',129790386);
INSERT INTO osnovno_zdravstveno VALUES (950011000,'�lan obitelji','Hrvatska',28);
INSERT INTO osig_pac VALUES(88, '05/01/2008', '05/01/2026',950011000);
INSERT INTO osnovno_zdravstveno VALUES (100450300,'nositelj','Hrvatska',29);
INSERT INTO osig_pac VALUES(89, '11/12/2006', '11/12/2036',100450300);
INSERT INTO osnovno_zdravstveno VALUES (587590391,'nositelj','Hrvatska',30);
INSERT INTO osig_pac VALUES(90, '19/04/2000', '19/04/2040',587590391);
INSERT INTO osnovno_zdravstveno VALUES (820543303,'nositelj','Hrvatska',46);
INSERT INTO osig_pac VALUES(91, '15/09/1998', '15/09/2028',820543303);
INSERT INTO osnovno_zdravstveno VALUES (350198303,'�lan obitelji','Hrvatska',47);
INSERT INTO osig_pac VALUES(92, '11/10/2006', '11/10/2024',350198303);
INSERT INTO osnovno_zdravstveno VALUES (190301303,'nositelj','Hrvatska',48);
INSERT INTO osig_pac VALUES(93, '23/12/2010', '23/12/2050',190301303);
INSERT INTO osnovno_zdravstveno VALUES (250596384,'�lan obitelji','Hrvatska',49);
INSERT INTO osig_pac VALUES(94, '29/09/2014', '29/09/2038',250596384);
INSERT INTO osnovno_zdravstveno VALUES (456092254,'nositelj','Hrvatska',50);
INSERT INTO osig_pac VALUES(95, '12/09/2012', '12/09/2052',456092254);
INSERT INTO osnovno_zdravstveno VALUES (302050309,'nositelj','Hrvatska',51);
INSERT INTO osig_pac VALUES(96, '23/11/1990', '23/11/2030',302050309);
INSERT INTO osnovno_zdravstveno VALUES (129880309,'nositelj','Hrvatska',52);
INSERT INTO osig_pac VALUES(97, '26/12/2012', '26/12/2042',129880309);
INSERT INTO osnovno_zdravstveno VALUES (178811008,'nositelj','Hrvatska',53);
INSERT INTO osig_pac VALUES(98, '16/04/2019', '16/04/2049',178811008);
INSERT INTO osnovno_zdravstveno VALUES (543450328,'nositelj','Hrvatska',54);
INSERT INTO osig_pac VALUES(99, '07/09/2015', '07/09/2055',543450328);
INSERT INTO osnovno_zdravstveno VALUES (297590308,'nositelj','Hrvatska',55);
INSERT INTO osig_pac VALUES(100, '16/10/2019', '16/10/2059',297590308);

--ordinacija--
INSERT INTO dopunsko_osiguranje VALUES (1700903,'HZZO');
INSERT INTO dop_pac VALUES(1, '06/01/2020', '06/01/2021',1,1700903);
INSERT INTO dopunsko_osiguranje VALUES (1500901,'HZZO');
INSERT INTO dop_pac VALUES(2, '07/01/2020', '07/01/2021',2,1500901);
INSERT INTO dopunsko_osiguranje VALUES (170090343,'Allianz');
INSERT INTO dop_pac VALUES(3, '12/04/2020', '12/04/2021',3,170090343);
INSERT INTO dopunsko_osiguranje VALUES (1260903,'HZZO');
INSERT INTO dop_pac VALUES(4, '23/05/2020', '23/05/2021',4,1260903);
INSERT INTO dopunsko_osiguranje VALUES (1700923,'HZZO');
INSERT INTO dop_pac VALUES(5, '11/01/2020', '11/01/2021',5,1700923);
INSERT INTO dopunsko_osiguranje VALUES (170090300,'Generali');
INSERT INTO dop_pac VALUES(6, '22/02/2020', '22/02/2021',6,170090300);
INSERT INTO dopunsko_osiguranje VALUES (170122333,'Croatia');
INSERT INTO dop_pac VALUES(7, '12/01/2019', '12/01/2020',7,170122333);
INSERT INTO dopunsko_osiguranje VALUES (1705223,'HZZO');
INSERT INTO dop_pac VALUES(8, '12/03/2019', '12/03/2020',8,1705223);
INSERT INTO dopunsko_osiguranje VALUES (2500903,'HZZO');
INSERT INTO dop_pac VALUES(9, '06/03/2020', '05/03/2021',9,2500903);
INSERT INTO dopunsko_osiguranje VALUES (150090100,'Allianz');
INSERT INTO dop_pac VALUES(10, '15/09/2019', '13/09/2020',10,150090100);
INSERT INTO dopunsko_osiguranje VALUES (170090340,'Allianz');
INSERT INTO dop_pac VALUES(11, '12/08/2020', '11/08/2021',11,170090340);
INSERT INTO dopunsko_osiguranje VALUES (1223903,'HZZO');
INSERT INTO dop_pac VALUES(12, '23/04/2020', '22/04/2021',12,1223903);
INSERT INTO dopunsko_osiguranje VALUES (2200923,'HZZO');
INSERT INTO dop_pac VALUES(13, '25/01/2020', '24/01/2021',13,2200923);
INSERT INTO dopunsko_osiguranje VALUES (170090305,'Generali');
INSERT INTO dop_pac VALUES(14, '21/02/2020', '20/02/2021',14,170090305);
INSERT INTO dopunsko_osiguranje VALUES (170122353,'Croatia');
INSERT INTO dop_pac VALUES(15, '12/07/2019', '11/07/2020',15,170122353);
INSERT INTO dopunsko_osiguranje VALUES (9905223,'HZZO');
INSERT INTO dop_pac VALUES(16, '12/03/2019', '11/03/2020',31,9905223);
INSERT INTO dopunsko_osiguranje VALUES (1724903,'HZZO');
INSERT INTO dop_pac VALUES(17, '06/01/2020', '06/01/2021',32,1724903);
INSERT INTO dopunsko_osiguranje VALUES (4400901,'HZZO');
INSERT INTO dop_pac VALUES(18, '07/03/2020', '07/03/2021',33,4400901);
INSERT INTO dopunsko_osiguranje VALUES (122090343,'Allianz');
INSERT INTO dop_pac VALUES(19, '05/04/2020', '04/04/2021',34,122090343);
INSERT INTO dopunsko_osiguranje VALUES (1254903,'HZZO');
INSERT INTO dop_pac VALUES(20, '23/05/2020', '22/05/2021',35,1254903);
INSERT INTO dopunsko_osiguranje VALUES (8900923,'HZZO');
INSERT INTO dop_pac VALUES(21, '11/01/2019', '10/01/2020',36,8900923);
INSERT INTO dopunsko_osiguranje VALUES (430090300,'Generali');
INSERT INTO dop_pac VALUES(22, '22/02/2020', '21/02/2021',37,430090300);
INSERT INTO dopunsko_osiguranje VALUES (174422333,'Croatia');
INSERT INTO dop_pac VALUES(23, '12/07/2019', '11/07/2020',38,174422333);
INSERT INTO dopunsko_osiguranje VALUES (1705993,'HZZO');
INSERT INTO dop_pac VALUES(24, '15/03/2020', '14/03/2021',39,1705993);
INSERT INTO dopunsko_osiguranje VALUES (170095400,'Generali');
INSERT INTO dop_pac VALUES(25, '20/02/2020', '19/02/2021',40,170095400);

--ordinacija--
INSERT INTO dopunsko_osiguranje VALUES (2806901,'HZZO');
INSERT INTO dop_pac VALUES(51, '14/01/2020', '13/01/2021',81,2806901);
INSERT INTO dopunsko_osiguranje VALUES (172210343,'Allianz');
INSERT INTO dop_pac VALUES(52, '12/05/2020', '11/05/2021',82,172210343);
INSERT INTO dopunsko_osiguranje VALUES (1262909,'HZZO');
INSERT INTO dop_pac VALUES(53, '30/05/2020', '29/05/2021',83,1262909);
INSERT INTO dopunsko_osiguranje VALUES (1702013,'HZZO');
INSERT INTO dop_pac VALUES(54, '11/03/2020', '10/03/2021',84,1702013);
INSERT INTO dopunsko_osiguranje VALUES (870090355,'Generali');
INSERT INTO dop_pac VALUES(55, '19/06/2020', '18/06/2021',85,870090355);
INSERT INTO dopunsko_osiguranje VALUES (170122032,'Croatia');
INSERT INTO dop_pac VALUES(56, '12/01/2019', '11/01/2020',86,170122032);
INSERT INTO dopunsko_osiguranje VALUES (1740003,'HZZO');
INSERT INTO dop_pac VALUES(57, '09/03/2019', '08/03/2020',87,1740003);
INSERT INTO dopunsko_osiguranje VALUES (8580903,'HZZO');
INSERT INTO dop_pac VALUES(58, '06/01/2020', '05/01/2021',88,8580903);
INSERT INTO dopunsko_osiguranje VALUES (150990190,'Allianz');
INSERT INTO dop_pac VALUES(59, '15/01/2019', '14/01/2020',89,150990190);
INSERT INTO dopunsko_osiguranje VALUES (170092255,'Allianz');
INSERT INTO dop_pac VALUES(60, '12/02/2019', '11/02/2020',90,170092255);
INSERT INTO dopunsko_osiguranje VALUES (1223243,'HZZO');
INSERT INTO dop_pac VALUES(61, '25/04/2020', '24/04/2021',91,1223243);
INSERT INTO dopunsko_osiguranje VALUES (228592300,'Generali');
INSERT INTO dop_pac VALUES(62, '25/01/2020', '24/01/2021',92,228592300);
INSERT INTO dopunsko_osiguranje VALUES (6757903,'HZZO');
INSERT INTO dop_pac VALUES(63, '27/02/2020', '26/02/2021',93,6757903);
INSERT INTO dopunsko_osiguranje VALUES (559122353,'Croatia');
INSERT INTO dop_pac VALUES(64, '16/01/2019', '15/01/2020',94,559122353);
INSERT INTO dopunsko_osiguranje VALUES (9877223,'HZZO');
INSERT INTO dop_pac VALUES(65, '12/03/2020', '11/03/2021',95,9877223);
INSERT INTO dopunsko_osiguranje VALUES (1720553,'HZZO');
INSERT INTO dop_pac VALUES(66, '31/01/2020', '30/01/2021',96,1720553);
INSERT INTO dopunsko_osiguranje VALUES (4562321,'Allianz');
INSERT INTO dop_pac VALUES(67, '07/06/2020', '06/06/2021',97,4562321);
INSERT INTO dopunsko_osiguranje VALUES (1223223,'HZZO');
INSERT INTO dop_pac VALUES(68, '05/04/2020', '04/04/2021',98,1223223);


INSERT INTO dopunsko_osiguranje VALUES (1252555,'HZZO');
INSERT INTO dop_pac VALUES(69, '23/05/2020', '22/05/2021',99,1252555);
INSERT INTO dopunsko_osiguranje VALUES (8920911,'HZZO');
INSERT INTO dop_pac VALUES(70, '11/01/2019', '10/01/2020',100,8920911);
INSERT INTO dopunsko_osiguranje VALUES (430090322,'Generali');
INSERT INTO dop_pac VALUES(71, '22/02/2020', '21/02/2021',41,430090322);
INSERT INTO dopunsko_osiguranje VALUES (1744223,'HZZO');
INSERT INTO dop_pac VALUES(72, '12/06/2020', '11/06/2021',42,1744223);
INSERT INTO dopunsko_osiguranje VALUES (173567300,'Croatia');
INSERT INTO dop_pac VALUES(73, '15/03/2020', '14/03/2021',43,173567300);
INSERT INTO dopunsko_osiguranje VALUES (122295490,'Generali');
INSERT INTO dop_pac VALUES(74, '19/02/2020', '18/02/2021',44,122295490);
INSERT INTO dopunsko_osiguranje VALUES (6522903,'HZZO');
INSERT INTO dop_pac VALUES(75, '20/05/2020', '19/05/2021',45,6522903);

---ordinacija---
INSERT INTO dopunsko_osiguranje VALUES (2800901,'HZZO');
INSERT INTO dop_pac VALUES(26, '07/01/2020', '06/01/2021',56,2800901);
INSERT INTO dopunsko_osiguranje VALUES (172290343,'Allianz');
INSERT INTO dop_pac VALUES(27, '12/05/2020', '11/05/2021',57,172290343);
INSERT INTO dopunsko_osiguranje VALUES (1262903,'HZZO');
INSERT INTO dop_pac VALUES(28, '31/05/2020', '30/05/2021',58,1262903);
INSERT INTO dopunsko_osiguranje VALUES (1702523,'HZZO');
INSERT INTO dop_pac VALUES(29, '11/02/2020', '10/02/2021',59,1702523);
INSERT INTO dopunsko_osiguranje VALUES (870090300,'Generali');
INSERT INTO dop_pac VALUES(30, '12/06/2020', '11/06/2021',60,870090300);
INSERT INTO dopunsko_osiguranje VALUES (170122033,'Croatia');
INSERT INTO dop_pac VALUES(31, '12/01/2019', '11/01/2020',61,170122033);
INSERT INTO dopunsko_osiguranje VALUES (1745223,'HZZO');
INSERT INTO dop_pac VALUES(32, '12/03/2019', '11/03/2020',62,1745223);
INSERT INTO dopunsko_osiguranje VALUES (2580903,'HZZO');
INSERT INTO dop_pac VALUES(33, '06/01/2020', '05/01/2021',63,2580903);
INSERT INTO dopunsko_osiguranje VALUES (150990100,'Allianz');
INSERT INTO dop_pac VALUES(34, '15/01/2019', '14/01/2020',64,150990100);
INSERT INTO dopunsko_osiguranje VALUES (170090355,'Allianz');
INSERT INTO dop_pac VALUES(35, '12/02/2019', '11/02/2020',65,170090355);
INSERT INTO dopunsko_osiguranje VALUES (1223503,'HZZO');
INSERT INTO dop_pac VALUES(36, '25/04/2020', '24/04/2021',66,1223503);
INSERT INTO dopunsko_osiguranje VALUES (2205923,'HZZO');
INSERT INTO dop_pac VALUES(37, '25/01/2020', '24/01/2021',67,2205923);
INSERT INTO dopunsko_osiguranje VALUES (670090305,'Generali');
INSERT INTO dop_pac VALUES(38, '21/02/2020', '20/02/2021',68,670090305);
INSERT INTO dopunsko_osiguranje VALUES (550122353,'Croatia');
INSERT INTO dop_pac VALUES(39, '12/01/2019', '11/01/2020',69,550122353);
INSERT INTO dopunsko_osiguranje VALUES (9805223,'HZZO');
INSERT INTO dop_pac VALUES(40, '12/03/2019', '11/03/2020',70,9805223);
INSERT INTO dopunsko_osiguranje VALUES (1720903,'HZZO');
INSERT INTO dop_pac VALUES(41, '31/01/2020', '30/01/2021',71,1720903);
INSERT INTO dopunsko_osiguranje VALUES (4560901,'HZZO');
INSERT INTO dop_pac VALUES(42, '07/06/2020', '06/06/2021',72,4560901);
INSERT INTO dopunsko_osiguranje VALUES (122330343,'Allianz');
INSERT INTO dop_pac VALUES(43, '05/04/2020', '04/04/2021',73,122330343);
INSERT INTO dopunsko_osiguranje VALUES (1254905,'HZZO');
INSERT INTO dop_pac VALUES(44, '23/05/2020', '22/05/2021',74,1254905);
INSERT INTO dopunsko_osiguranje VALUES (8920923,'HZZO');
INSERT INTO dop_pac VALUES(45, '11/01/2019', '10/01/2020',75,8920923);
INSERT INTO dopunsko_osiguranje VALUES (430090308,'Generali');
INSERT INTO dop_pac VALUES(46, '22/02/2020', '21/02/2021',76,430090308);
INSERT INTO dopunsko_osiguranje VALUES (174400333,'Croatia');
INSERT INTO dop_pac VALUES(47, '12/07/2019', '11/07/2020',77,174400333);
INSERT INTO dopunsko_osiguranje VALUES (1734993,'HZZO');
INSERT INTO dop_pac VALUES(48, '15/03/2020', '14/03/2021',78,1734993);
INSERT INTO dopunsko_osiguranje VALUES (122295400,'Generali');
INSERT INTO dop_pac VALUES(49, '19/02/2020', '18/02/2021',79,122295400);
INSERT INTO dopunsko_osiguranje VALUES (6500903,'HZZO');
INSERT INTO dop_pac VALUES(50, '20/05/2020', '19/05/2021',80,6500903);

SELECT *
FROM dopunsko_osiguranje;
select*
from dop_pac;
Commit;
--ordinacija--
INSERT INTO dopunsko_osiguranje VALUES (3900901,'HZZO');
INSERT INTO dop_pac VALUES(76, '09/01/2020', '08/01/2021',17,3900901);
INSERT INTO dopunsko_osiguranje VALUES (134590343,'Allianz');
INSERT INTO dop_pac VALUES(77, '12/05/2019', '11/05/2020',16,134590343);
INSERT INTO dopunsko_osiguranje VALUES (1265673,'HZZO');
INSERT INTO dop_pac VALUES(78, '31/05/2020', '30/05/2021',18,1265673);
INSERT INTO dopunsko_osiguranje VALUES (1702883,'HZZO');
INSERT INTO dop_pac VALUES(79, '11/02/2020', '10/02/2021',19,1702883);
INSERT INTO dopunsko_osiguranje VALUES (870090377,'Generali');
INSERT INTO dop_pac VALUES(80, '12/06/2020', '11/06/2021',20,870090377);
INSERT INTO dopunsko_osiguranje VALUES (171234033,'Croatia');
INSERT INTO dop_pac VALUES(81, '12/01/2019', '11/01/2020',21,171234033);
INSERT INTO dopunsko_osiguranje VALUES (1740223,'HZZO');
INSERT INTO dop_pac VALUES(82, '12/03/2019', '11/03/2020',22,1740223);
INSERT INTO dopunsko_osiguranje VALUES (2582343,'HZZO');
INSERT INTO dop_pac VALUES(83, '21/05/2020', '20/05/2021',23,2582343);
INSERT INTO dopunsko_osiguranje VALUES (150992340,'Allianz');
INSERT INTO dop_pac VALUES(84, '15/01/2019', '14/01/2020',24,150992340);
INSERT INTO dopunsko_osiguranje VALUES (170567355,'Allianz');
INSERT INTO dop_pac VALUES(85, '12/02/2020', '11/02/2021',25,170567355);
INSERT INTO dopunsko_osiguranje VALUES (9923503,'HZZO');
INSERT INTO dop_pac VALUES(86, '25/04/2020', '24/04/2021',26,9923503);
INSERT INTO dopunsko_osiguranje VALUES (2895923,'HZZO');
INSERT INTO dop_pac VALUES(87, '25/01/2020', '24/01/2021',28,2895923);
INSERT INTO dopunsko_osiguranje VALUES (891090305,'Generali');
INSERT INTO dop_pac VALUES(88, '21/03/2020', '20/03/2021',27,891090305);
INSERT INTO dopunsko_osiguranje VALUES (534522353,'Croatia');
INSERT INTO dop_pac VALUES(89, '05/01/2019', '04/01/2020',29,534522353);
INSERT INTO dopunsko_osiguranje VALUES (9565223,'HZZO');
INSERT INTO dop_pac VALUES(90, '30/03/2019', '29/03/2020',30,9565223);
INSERT INTO dopunsko_osiguranje VALUES (1767803,'HZZO');
INSERT INTO dop_pac VALUES(91, '31/03/2020', '30/03/2021',46,1767803);
INSERT INTO dopunsko_osiguranje VALUES (4880901,'HZZO');
INSERT INTO dop_pac VALUES(92, '07/04/2020', '06/04/2021',47,4880901);
INSERT INTO dopunsko_osiguranje VALUES (122398343,'Allianz');
INSERT INTO dop_pac VALUES(93, '11/04/2020', '10/04/2021',48,122398343);
INSERT INTO dopunsko_osiguranje VALUES (1254215,'HZZO');
INSERT INTO dop_pac VALUES(94, '27/05/2020', '26/05/2021',49,1254215);
INSERT INTO dopunsko_osiguranje VALUES (8543923,'HZZO');
INSERT INTO dop_pac VALUES(95, '11/01/2020', '10/01/2021',50,8543923);
INSERT INTO dopunsko_osiguranje VALUES (432290308,'Generali');
INSERT INTO dop_pac VALUES(96, '22/02/2019', '21/02/2020',51,432290308);
INSERT INTO dopunsko_osiguranje VALUES (174405533,'Croatia');
INSERT INTO dop_pac VALUES(97, '12/04/2020', '11/04/2021',52,174405533);
INSERT INTO dopunsko_osiguranje VALUES (1730990,'HZZO');
INSERT INTO dop_pac VALUES(98, '18/03/2020', '17/03/2021',53,1730990);
INSERT INTO dopunsko_osiguranje VALUES (122295409,'Generali');
INSERT INTO dop_pac VALUES(99, '28/02/2020', '27/02/2021',54,122295409);
INSERT INTO dopunsko_osiguranje VALUES (6566903,'HZZO');
INSERT INTO dop_pac VALUES(100, '19/05/2020', '18/05/2021',55,6566903);
--dijagnoze---
INSERT INTO dijagnoza VALUES('J45.0','Alergijska astma');
INSERT INTO dijagnoza VALUES('B02','Zonski-pojasasti herpes');
INSERT INTO dijagnoza VALUES('F41.2','Mje�ovit anksiozni i depresivni poreme�aj');
INSERT INTO dijagnoza VALUES('A09','Dijareja i gastroenteritis, za koje se pretpostavlja da su infektivnog podrijetla');
INSERT INTO dijagnoza VALUES('Z04.2','Pregled za izdavanje voza�ke dozvole');
INSERT INTO dijagnoza VALUES('Z23.5','Potreba za cijepljenjem protiv tetanusa');
INSERT INTO dijagnoza VALUES('J03.9','Akutni tonzilitis');
INSERT INTO dijagnoza VALUES('J03.0','Streptokokni tonzilitis');
INSERT INTO dijagnoza VALUES('J01','Akutna upala sinusa');
INSERT INTO dijagnoza VALUES('J02','Akutna upala �drijela');
INSERT INTO dijagnoza VALUES('D50','Anemija zbog manjka �eljeza');
INSERT INTO dijagnoza VALUES('Z00.1','Sistematski pregled dojen�adi i djece');
INSERT INTO dijagnoza VALUES('Z27.2','Potreba za cijepljenjem protiv difterije-tetanusa-pertusisa');
INSERT INTO dijagnoza VALUES('Z24.0','Potreba za cijepljenjem protiv poliomijelitisa');
INSERT INTO dijagnoza VALUES('Z24.6','Potreba za cijepljenjem protiv hepatitisa B');
INSERT INTO dijagnoza VALUES('Z23.7','Potreba za cijepljenjem protiv hripavca');
INSERT INTO dijagnoza VALUES('R30','Bol pri mokrenju');
INSERT INTO dijagnoza VALUES('M75','O�te�enja ramena');
INSERT INTO dijagnoza VALUES('I10','Esencijalna hipertenzija');
INSERT INTO dijagnoza VALUES('R51','Glavobolja');
INSERT INTO dijagnoza VALUES('K40','Hernija');
INSERT INTO dijagnoza VALUES('B98','Helicobacter pylori');
INSERT INTO dijagnoza VALUES('B07','Virusne bradavice');
INSERT INTO dijagnoza VALUES('B01','Vodene kozice');
INSERT INTO dijagnoza VALUES('C47.1','Sr� nadbubre�ne �lijezde(neoplazma)');
INSERT INTO dijagnoza VALUES('L20','Atopijski dermatitis');
INSERT INTO dijagnoza VALUES('H40.0','Glaukom');
INSERT INTO dijagnoza VALUES('H25','Katarakta senilna');
INSERT INTO dijagnoza VALUES('T1.1','Otvorena rana ruke');
INSERT INTO dijagnoza VALUES('R16.1','Splenomegalija');
INSERT INTO dijagnoza VALUES('N30','Upala mokra�nog mjehura');
INSERT INTO dijagnoza VALUES('W54','Ugriz ili udarac psa');
INSERT INTO dijagnoza VALUES('W14','Pad sa stabla');
INSERT INTO dijagnoza VALUES('C50','Zlo�udna novotvorina dojke');
INSERT INTO dijagnoza VALUES('F32','Depresija');
INSERT INTO dijagnoza VALUES('H10','Konjuktivitis');
INSERT INTO dijagnoza VALUES('A54','Kapavac(gonoreja)');
INSERT INTO dijagnoza VALUES('G43.8','ostale migrene');
INSERT INTO dijagnoza VALUES('J15.1','Pneumonija uzrokvana streptokokima');
INSERT INTO dijagnoza VALUES('S10','Bol u koljenu');
INSERT INTO dijagnoza VALUES('S91.3','Otvorena rana stopala');
INSERT INTO dijagnoza VALUES('A37.0','Hripavac koji uzrokuje Bordetella pertussis');
INSERT INTO dijagnoza VALUES('J01.2',' Akutni etmoidni sinusitis');
INSERT INTO dijagnoza VALUES('J06.0','Akutni laringofaringitis');
INSERT INTO dijagnoza VALUES('J10.0','Influenca s pneumonijom');
INSERT INTO dijagnoza VALUES('K35.9','Akutna upala crvuljka');
INSERT INTO dijagnoza VALUES('K50.0','Chronova bolest tankog crijeva');
INSERT INTO dijagnoza VALUES('L08.0','Piodermija');
INSERT INTO dijagnoza VALUES('L21.0','Seboreja glave');
INSERT INTO dijagnoza VALUES('M00.0','Stafilokokni artritis');
INSERT INTO dijagnoza VALUES('M34.0','Progresivna sistemna skleroza');
INSERT INTO dijagnoza VALUES('N15.1','Bubre�ni i perinefriti�ki apsces');
INSERT INTO dijagnoza VALUES('N21.0','Kamenac u mjehuru');
INSERT INTO dijagnoza VALUES('C22.0','Rak jetrenih stanica');
INSERT INTO dijagnoza VALUES('C64.0','Zlo�udna novotvorina bubrega');
--cjepiva---
INSERT INTO cjepivo VALUES (1,'ANA-TE');
INSERT INTO cjepivo VALUES (2,'DTP');
INSERT INTO cjepivo VALUES (3,'Polio');
INSERT INTO cjepivo VALUES (4,'Hepatitis B');
INSERT INTO cjepivo VALUES (9,'Hripavac');

--lijekovi--
INSERT INTO lijek(sifra_lijeka,naziv,proizvodjac,cijena) VALUES(1001,'Dasselta tbl film obl', 'Krka' ,21.45);
INSERT INTO lijek VALUES(1002,'Herplexim tbl film obl', 'Belupo' ,135.60,8.50);
INSERT INTO lijek(sifra_lijeka,naziv,proizvodjac,cijena) VALUES(1003,'Klavocin tbl film obl', 'Pliva' ,34.40);
INSERT INTO lijek VALUES (1004,'Medazol tbl film obl', 'Pliva' ,34.40,5.80);
INSERT INTO lijek VALUES (1005,'Velafax XL kapsule', 'Pliva' ,66.80, 10.00);
INSERT INTO lijek VALUES (1006,'Amoksiklav tbl film obl', 'Sandoz' ,56.40,8.80);
INSERT INTO lijek (sifra_lijeka,naziv,proizvodjac,cijena) VALUES (1007,'Nolicin tbl film obl', 'Krka' ,32.40);
INSERT INTO lijek VALUES (1008,'Brufen sumece granule', 'Mylan' ,30.40,10.00);
INSERT INTO lijek (sifra_lijeka,naziv,proizvodjac,cijena) VALUES (1009,'Nebilet tbl film obl', 'Sandoz' ,40.45);
INSERT INTO lijek VALUES (1010,'Nomigren', 'Krka' ,54.40,10.00);
INSERT INTO lijek (sifra_lijeka,naziv,proizvodjac,cijena) VALUES (1011,'Panatus tbl film obl', 'Pliva' ,59.89);
INSERT INTO lijek(sifra_lijeka,naziv,proizvodjac,cijena) VALUES (1012,'Sumamed', 'Krka' ,64.40);
INSERT INTO lijek(sifra_lijeka,naziv,proizvodjac,cijena) VALUES (1013,'Betaklav', 'Pliva' ,64.40);
INSERT INTO lijek(sifra_lijeka,naziv,proizvodjac,cijena) VALUES (1014,'Ketonal', 'Pliva' ,76.29);
INSERT INTO lijek(sifra_lijeka,naziv,proizvodjac,cijena) VALUES (1015,'Lexoderm', 'Sandoz' ,52.29);
INSERT INTO lijek(sifra_lijeka,naziv,proizvodjac,cijena) VALUES (1016,'Belogent krema', 'Belupo' ,16.45);
INSERT INTO lijek VALUES (1017,'Amoksicilin', 'Belupo' ,54.40,3.40);
INSERT INTO lijek VALUES (1018,'Triplixam', 'Belupo' ,67.59,5.89);
INSERT INTO lijek VALUES (1020,'Ninur', 'Belupo' ,54.49,4.89);
INSERT INTO lijek (sifra_lijeka,naziv,proizvodjac,cijena) VALUES (1021,'Misar', 'Krka' ,35.49);
INSERT INTO lijek VALUES (1022,'Maxitrol mast', 'Belupo' ,27.59,6.89);
INSERT INTO lijek VALUES (1023,'Maxitrol kapi', 'Belupo' ,62.99,4.89);
INSERT INTO lijek (sifra_lijeka,naziv,proizvodjac,cijena) VALUES (1024,'Azitomicin', 'Krka' ,58.49);
INSERT INTO lijek VALUES (1025,'Imigran', 'Pliva' ,45.89,10.00);
INSERT INTO lijek (sifra_lijeka,naziv,proizvodjac,cijena) VALUES (1026,'Ramed', 'Krka' ,45.89);

INSERT INTO pacijent VALUES(102,'Matej','Prpi�','M',TO_DATE('2001-01-13','YYYY-MM-DD'), 'Vratni�ka 21,Osijek','0994029366',4);
INSERT INTO osnovno_zdravstveno VALUES(786888532,'nositelj','Hrvatska',102);
INSERT INTO osig_pac VALUES(102, TO_DATE('2026-01-07','YYYY-MM-DD'),TO_DATE('2055-12-31','YYYY-MM-DD'),786888532);
INSERT INTO dopunsko_osiguranje VALUES(551671601,'Croatia');
INSERT INTO dop_pac VALUES(102, TO_DATE('2019-06-29','YYYY-MM-DD'), TO_DATE('2020-06-27','YYYY-MM-DD'),102, 551671601);

INSERT INTO anamneza VALUES (225,'Bolno mokrenje,povi�ena temp.',TO_DATE('2019-02-16','YYYY-MM-DD'),102);
INSERT INTO dijag_anamneza VALUES(265,225,'N15.1');
INSERT INTO pretraga VALUES(190,'Krv','KKS',TO_DATE('2019-02-17','YYYY-MM-DD'),265);


INSERT INTO pacijent VALUES(103,'Margareta','Grana','F',TO_DATE('2002-05-21','YYYY-MM-DD'), 'Srijemska 71,Osijek','0953201232',4);
INSERT INTO osnovno_zdravstveno VALUES(730981123,'nositelj','Hrvatska',103);
INSERT INTO osig_pac VALUES(103, TO_DATE('2027-05-15','YYYY-MM-DD'),TO_DATE('2057-05-07','YYYY-MM-DD'),730981123);
INSERT INTO dopunsko_osiguranje VALUES(990638967,'Generali');
INSERT INTO dop_pac VALUES(103, TO_DATE('2020-02-11','YYYY-MM-DD'), TO_DATE('2021-02-09','YYYY-MM-DD'),103, 990638967);

INSERT INTO anamneza VALUES (226,'Mu�nina,povra�anje,bol u trbuhu',TO_DATE('2019-06-07','YYYY-MM-DD'),103);
INSERT INTO dijag_anamneza VALUES(266,226,'K35.9');
INSERT INTO pretraga VALUES(191,'Feces',null,TO_DATE('2019-06-07','YYYY-MM-DD'),266);
INSERT INTO anamneza VALUES (227,'Mu�nina,povra�anje,bol u trbuhu',TO_DATE('2019-06-01','YYYY-MM-DD'),103);
INSERT INTO dijag_anamneza VALUES(267,227,'K35.9');
INSERT INTO pretraga VALUES(192,'Feces',null,TO_DATE('2019-06-03','YYYY-MM-DD'),267);


INSERT INTO pacijent VALUES(104,'Marija','Dobro�evi�','F',TO_DATE('1977-10-18','YYYY-MM-DD'), 'Zagreba�ka 1,Osijek','0987581680',2);
INSERT INTO osnovno_zdravstveno VALUES(238783477,'nositelj','Hrvatska',104);
INSERT INTO osig_pac VALUES(104, TO_DATE('2002-10-12','YYYY-MM-DD'),TO_DATE('2032-10-04','YYYY-MM-DD'),238783477);
INSERT INTO dopunsko_osiguranje VALUES(601135794,'Croatia');
INSERT INTO dop_pac VALUES(104, TO_DATE('2019-05-02','YYYY-MM-DD'), TO_DATE('2020-04-30','YYYY-MM-DD'),104, 601135794);

INSERT INTO anamneza VALUES (228,'Rane u ustima,proljev,vru�ica',TO_DATE('2019-11-09','YYYY-MM-DD'),104);
INSERT INTO dijag_anamneza VALUES(268,228,'K50.0');
INSERT INTO uputnice VALUES(101,'A2','Gastroenterologija','prvi pregled',268);
INSERT INTO pretraga VALUES(193,'Feces',null,TO_DATE('2019-11-10','YYYY-MM-DD'),268);
INSERT INTO anamneza VALUES (229,'Crveilo ko�e,bol i uko�enost zglobova',TO_DATE('2019-07-24','YYYY-MM-DD'),104);
INSERT INTO dijag_anamneza VALUES(269,229,'M00.0');


INSERT INTO pacijent VALUES(105,'Klara','Bo�i�evi�','F',TO_DATE('1945-07-26','YYYY-MM-DD'), 'Srijemska 79,Osijek','0996219354',3);
INSERT INTO osnovno_zdravstveno VALUES(465144039,'nositelj','Hrvatska',105);
INSERT INTO osig_pac VALUES(105, TO_DATE('2010-07-10','YYYY-MM-DD'),TO_DATE('2040-07-02','YYYY-MM-DD'),465144039);
INSERT INTO dopunsko_osiguranje VALUES(594554987,'Allianz');
INSERT INTO dop_pac VALUES(105, TO_DATE('2019-12-26','YYYY-MM-DD'), TO_DATE('2020-12-24','YYYY-MM-DD'),105, 594554987);

INSERT INTO anamneza VALUES (230,'Sumnja na tetanus',TO_DATE('2020-05-02','YYYY-MM-DD'),105);
INSERT INTO dijag_anamneza VALUES(270,230,'Z23.5');
INSERT INTO cijepljenje VALUES(50,1,270);


INSERT INTO pacijent VALUES(106,'Luka','Budak','M',TO_DATE('1948-01-08','YYYY-MM-DD'), 'Wilsonova 96,Osijek','0953080612',1);
INSERT INTO osnovno_zdravstveno VALUES(317101155,'nositelj','Hrvatska',106);
INSERT INTO osig_pac VALUES(106, TO_DATE('2012-12-22','YYYY-MM-DD'),TO_DATE('2042-12-15','YYYY-MM-DD'),317101155);
INSERT INTO dopunsko_osiguranje VALUES(336536171,'Allianz');
INSERT INTO dop_pac VALUES(106, TO_DATE('2020-03-07','YYYY-MM-DD'), TO_DATE('2021-03-06','YYYY-MM-DD'),106, 336536171);

INSERT INTO anamneza VALUES (231,'Osjetljivost i oteknu�e sinusa',TO_DATE('2019-06-25','YYYY-MM-DD'),106);
INSERT INTO dijag_anamneza VALUES(271,231,'J01.2');
INSERT INTO terapija VALUES(150,'3x dnevno',271,1014);
INSERT INTO pretraga VALUES(194,'Krv','BIOKEMIJA',TO_DATE('2019-06-27','YYYY-MM-DD'),271);


INSERT INTO pacijent VALUES(107,'Tomislav','Duman�i�','M',TO_DATE('1978-07-08','YYYY-MM-DD'), 'Drinska 81,Osijek','0913683901',2);
INSERT INTO osnovno_zdravstveno VALUES(250317643,'nositelj','Hrvatska',107);
INSERT INTO osig_pac VALUES(107, TO_DATE('2003-07-02','YYYY-MM-DD'),TO_DATE('2033-06-24','YYYY-MM-DD'),250317643);
INSERT INTO dopunsko_osiguranje VALUES(337397114,'Allianz');
INSERT INTO dop_pac VALUES(107, TO_DATE('2019-12-18','YYYY-MM-DD'), TO_DATE('2020-12-16','YYYY-MM-DD'),107, 337397114);

INSERT INTO anamneza VALUES (232,'Povisen tlak',TO_DATE('2020-02-13','YYYY-MM-DD'),107);
INSERT INTO dijag_anamneza VALUES(272,232,'I10');
INSERT INTO terapija VALUES(151,'1,0,0',272,1018);


INSERT INTO pacijent VALUES(108,'Ivan','Pervan','M',TO_DATE('1991-01-26','YYYY-MM-DD'), 'Zagreba�ka 6,Osijek','0993873533',1);
INSERT INTO osnovno_zdravstveno VALUES(527147959,'nositelj','Hrvatska',108);
INSERT INTO osig_pac VALUES(108, TO_DATE('2016-01-20','YYYY-MM-DD'),TO_DATE('2046-01-12','YYYY-MM-DD'),527147959);
INSERT INTO dopunsko_osiguranje VALUES(6250820,'HZZO');
INSERT INTO dop_pac VALUES(108, TO_DATE('2019-08-04','YYYY-MM-DD'), TO_DATE('2020-08-02','YYYY-MM-DD'),108, 6250820);

INSERT INTO anamneza VALUES (233,'Povisen tlak',TO_DATE('2019-08-08','YYYY-MM-DD'),108);
INSERT INTO dijag_anamneza VALUES(273,233,'I10');
INSERT INTO terapija VALUES(152,'1,0,0',273,1026);


INSERT INTO pacijent VALUES(109,'Margareta','Trconi�','F',TO_DATE('1998-08-21','YYYY-MM-DD'), 'Mlinska 50,Osijek','0953215658',4);
INSERT INTO osnovno_zdravstveno VALUES(983763374,'nositelj','Hrvatska',109);
INSERT INTO osig_pac VALUES(109, TO_DATE('2023-08-15','YYYY-MM-DD'),TO_DATE('2053-08-07','YYYY-MM-DD'),983763374);
INSERT INTO dopunsko_osiguranje VALUES(192822845,'Croatia');
INSERT INTO dop_pac VALUES(109, TO_DATE('2019-03-09','YYYY-MM-DD'), TO_DATE('2020-03-07','YYYY-MM-DD'),109, 192822845);

INSERT INTO anamneza VALUES (234,'Nemogu�nost mokrenja',TO_DATE('2020-04-05','YYYY-MM-DD'),109);
INSERT INTO dijag_anamneza VALUES(274,234,'N21.0');
INSERT INTO pretraga VALUES(195,'Krv','BIOKEMIJA',TO_DATE('2020-04-07','YYYY-MM-DD'),274);


INSERT INTO pacijent VALUES(110,'Zvonimir','Vakanjac','M',TO_DATE('1967-09-03','YYYY-MM-DD'), 'Europska avenija 45,Osijek','0998768993',3);
INSERT INTO osnovno_zdravstveno VALUES(437787810,'nositelj','Hrvatska',110);
INSERT INTO osig_pac VALUES(110, TO_DATE('1992-08-27','YYYY-MM-DD'),TO_DATE('2022-08-20','YYYY-MM-DD'),437787810);
INSERT INTO dopunsko_osiguranje VALUES(326633984,'Generali');
INSERT INTO dop_pac VALUES(110, TO_DATE('2020-01-20','YYYY-MM-DD'), TO_DATE('2021-01-18','YYYY-MM-DD'),110, 326633984);

INSERT INTO anamneza VALUES (235,'Nemo�,gubitak teka,visoka temp.',TO_DATE('2020-04-28','YYYY-MM-DD'),110);
INSERT INTO dijag_anamneza VALUES(275,235,'J10.0');
INSERT INTO bolovanja VALUES(2050, TO_DATE('2020-04-28','YYYY-MM-DD'),TO_DATE('2020-04-29','YYYY-MM-DD'),'A0 bolest',275);
INSERT INTO uputnice VALUES(102,'A2','Pulmologija','prvi pregled',275);
INSERT INTO pretraga VALUES(196,'Krv','BIOKEMIJA',TO_DATE('2020-04-30','YYYY-MM-DD'),275);


INSERT INTO pacijent VALUES(111,'Vida','Medi�','M',TO_DATE('1997-05-25','YYYY-MM-DD'), 'Drinska 27,Osijek','0957325651',1);
INSERT INTO osnovno_zdravstveno VALUES(779291627,'nositelj','Hrvatska',111);
INSERT INTO osig_pac VALUES(111, TO_DATE('2022-05-19','YYYY-MM-DD'),TO_DATE('2052-05-11','YYYY-MM-DD'),779291627);
INSERT INTO dopunsko_osiguranje VALUES(4078136,'HZZO');
INSERT INTO dop_pac VALUES(111, TO_DATE('2019-12-28','YYYY-MM-DD'), TO_DATE('2020-12-26','YYYY-MM-DD'),111, 4078136);

INSERT INTO anamneza VALUES (236,'Bolno mokrenje,povi�ena temp.',TO_DATE('2019-01-20','YYYY-MM-DD'),111);
INSERT INTO dijag_anamneza VALUES(276,236,'N15.1');
INSERT INTO pretraga VALUES(197,'Krv','BIOKEMIJA',TO_DATE('2019-01-23','YYYY-MM-DD'),276);


INSERT INTO pacijent VALUES(112,'Robert','Vargi�','M',TO_DATE('1950-12-22','YYYY-MM-DD'), 'Zagreba�ka 94,Osijek','0918558763',4);
INSERT INTO osnovno_zdravstveno VALUES(987356954,'nositelj','Hrvatska',112);
INSERT INTO osig_pac VALUES(112, TO_DATE('2015-12-06','YYYY-MM-DD'),TO_DATE('2045-11-28','YYYY-MM-DD'),987356954);
INSERT INTO dopunsko_osiguranje VALUES(867545194,'Croatia');
INSERT INTO dop_pac VALUES(112, TO_DATE('2019-05-16','YYYY-MM-DD'), TO_DATE('2020-05-14','YYYY-MM-DD'),112, 867545194);

INSERT INTO anamneza VALUES (237,'Mu�nina,povra�anje,bol u trbuhu',TO_DATE('2019-10-22','YYYY-MM-DD'),112);
INSERT INTO dijag_anamneza VALUES(277,237,'K35.9');
INSERT INTO uputnice VALUES(103,'A2','Gastroenterologija','prvi pregled',277);
INSERT INTO pretraga VALUES(198,'Feces',null,TO_DATE('2019-10-23','YYYY-MM-DD'),277);
INSERT INTO anamneza VALUES (238,'Osjetljivost i oteknu�e sinusa',TO_DATE('2019-10-16','YYYY-MM-DD'),112);
INSERT INTO dijag_anamneza VALUES(278,238,'J01.2');
INSERT INTO terapija VALUES(153,'0,0,1',278,1003);
INSERT INTO pretraga VALUES(199,'Krv','BIOKEMIJA',TO_DATE('2019-10-21','YYYY-MM-DD'),278);


INSERT INTO pacijent VALUES(113,'Bruno','Tompa','M',TO_DATE('1960-03-02','YYYY-MM-DD'), 'Mlinska 7,Osijek','0918071110',4);
INSERT INTO osnovno_zdravstveno VALUES(789525730,'nositelj','Hrvatska',113);
INSERT INTO osig_pac VALUES(113, TO_DATE('1985-02-24','YYYY-MM-DD'),TO_DATE('2015-02-17','YYYY-MM-DD'),789525730);
INSERT INTO dopunsko_osiguranje VALUES(278731055,'Generali');
INSERT INTO dop_pac VALUES(113, TO_DATE('2019-05-12','YYYY-MM-DD'), TO_DATE('2020-05-10','YYYY-MM-DD'),113, 278731055);

INSERT INTO anamneza VALUES (239,'Bolno mokrenje',TO_DATE('2019-10-19','YYYY-MM-DD'),113);
INSERT INTO dijag_anamneza VALUES(279,239,'N30');
INSERT INTO bolovanja VALUES(2051, TO_DATE('2019-10-19','YYYY-MM-DD'),TO_DATE('2019-10-22','YYYY-MM-DD'),'A0 bolest',279);
INSERT INTO pretraga VALUES(200,'Krv','KKS',TO_DATE('2019-10-24','YYYY-MM-DD'),279);


INSERT INTO pacijent VALUES(114,'Roko','Salic','M',TO_DATE('1985-10-26','YYYY-MM-DD'), 'Wilsonova 130,Osijek','0993926695',2);
INSERT INTO osnovno_zdravstveno VALUES(150642762,'nositelj','Hrvatska',114);
INSERT INTO osig_pac VALUES(114, TO_DATE('2010-10-20','YYYY-MM-DD'),TO_DATE('2040-10-12','YYYY-MM-DD'),150642762);
INSERT INTO dopunsko_osiguranje VALUES(447661752,'Allianz');
INSERT INTO dop_pac VALUES(114, TO_DATE('2020-02-14','YYYY-MM-DD'), TO_DATE('2021-02-12','YYYY-MM-DD'),114, 447661752);

INSERT INTO anamneza VALUES (240,'Povisen tlak',TO_DATE('2019-11-24','YYYY-MM-DD'),114);
INSERT INTO dijag_anamneza VALUES(280,240,'I10');
INSERT INTO uputnice VALUES(104,'A2','Nefrologija','prvi pregled',280);
INSERT INTO uputnice VALUES(105,'A2','Kardiologija','prvi pregled',280);


INSERT INTO pacijent VALUES(115,'Hrvoje','Radman','M',TO_DATE('2005-02-05','YYYY-MM-DD'), 'Vukovarska 29,Osijek','0999721708',1);
INSERT INTO osnovno_zdravstveno VALUES(758892083,'�lan obitelji','Hrvatska',115);
INSERT INTO osig_pac VALUES(115, TO_DATE('2005-02-05','YYYY-MM-DD'),TO_DATE('2035-01-29','YYYY-MM-DD'),758892083);
INSERT INTO dopunsko_osiguranje VALUES(649866647,'Generali');
INSERT INTO dop_pac VALUES(115, TO_DATE('2020-05-05','YYYY-MM-DD'), TO_DATE('2021-05-04','YYYY-MM-DD'),115, 649866647);

INSERT INTO anamneza VALUES (241,'Mu�nina,povra�anje,bol u trbuhu',TO_DATE('2020-04-08','YYYY-MM-DD'),115);
INSERT INTO dijag_anamneza VALUES(281,241,'K35.9');
INSERT INTO pretraga VALUES(201,'Krv','KKS',TO_DATE('2020-04-09','YYYY-MM-DD'),281);


INSERT INTO pacijent VALUES(116,'Slaven','Balog','M',TO_DATE('1979-03-21','YYYY-MM-DD'), 'Wilsonova 69,Osijek','0994858600',1);
INSERT INTO osnovno_zdravstveno VALUES(134967311,'nositelj','Hrvatska',116);
INSERT INTO osig_pac VALUES(116, TO_DATE('2004-03-14','YYYY-MM-DD'),TO_DATE('2034-03-07','YYYY-MM-DD'),134967311);
INSERT INTO dopunsko_osiguranje VALUES(5153957,'HZZO');
INSERT INTO dop_pac VALUES(116, TO_DATE('2019-09-05','YYYY-MM-DD'), TO_DATE('2020-09-03','YYYY-MM-DD'),116, 5153957);

INSERT INTO anamneza VALUES (242,'Vezikule na ko�i sa gnojem',TO_DATE('2020-03-25','YYYY-MM-DD'),116);
INSERT INTO dijag_anamneza VALUES(282,242,'L08.0');
INSERT INTO bolovanja VALUES(2052, TO_DATE('2020-03-25','YYYY-MM-DD'),TO_DATE('2020-03-31','YYYY-MM-DD'),'A0 bolest',282);
INSERT INTO pretraga VALUES(202,'Krv','KKS',TO_DATE('2020-03-27','YYYY-MM-DD'),282);


INSERT INTO pacijent VALUES(117,'Ante','Neve��anin','M',TO_DATE('1986-01-26','YYYY-MM-DD'), 'Wilsonova 81,Osijek','0954719928',2);
INSERT INTO osnovno_zdravstveno VALUES(609348178,'nositelj','Hrvatska',117);
INSERT INTO osig_pac VALUES(117, TO_DATE('2011-01-20','YYYY-MM-DD'),TO_DATE('2041-01-12','YYYY-MM-DD'),609348178);
INSERT INTO dopunsko_osiguranje VALUES(488401814,'Allianz');
INSERT INTO dop_pac VALUES(117, TO_DATE('2020-03-27','YYYY-MM-DD'), TO_DATE('2021-03-26','YYYY-MM-DD'),117, 488401814);

INSERT INTO anamneza VALUES (243,'Mu�nina,povra�anje,bol u trbuhu',TO_DATE('2019-03-23','YYYY-MM-DD'),117);
INSERT INTO dijag_anamneza VALUES(283,243,'K35.9');
INSERT INTO bolovanja VALUES(2053, TO_DATE('2019-03-23','YYYY-MM-DD'),TO_DATE('2019-03-27','YYYY-MM-DD'),'A0 bolest',283);
INSERT INTO uputnice VALUES(106,'A2','Gastroenterologija','prvi pregled',283);
INSERT INTO pretraga VALUES(203,'Krv','KKS',TO_DATE('2019-03-24','YYYY-MM-DD'),283);


INSERT INTO pacijent VALUES(118,'Boris','Lasovi�','M',TO_DATE('2010-07-02','YYYY-MM-DD'), '�upanijska 99,Osijek','0957078606',1);
INSERT INTO osnovno_zdravstveno VALUES(194596910,'�lan obitelji','Hrvatska',118);
INSERT INTO osig_pac VALUES(118, TO_DATE('2010-07-02','YYYY-MM-DD'),TO_DATE('2040-06-24','YYYY-MM-DD'),194596910);
INSERT INTO dopunsko_osiguranje VALUES(745462849,'Allianz');
INSERT INTO dop_pac VALUES(118, TO_DATE('2019-08-19','YYYY-MM-DD'), TO_DATE('2020-08-17','YYYY-MM-DD'),118, 745462849);

INSERT INTO anamneza VALUES (244,'Rane u ustima,proljev,vru�ica',TO_DATE('2019-12-27','YYYY-MM-DD'),118);
INSERT INTO dijag_anamneza VALUES(284,244,'K50.0');
INSERT INTO uputnice VALUES(107,'A2','Gastroenterologija','prvi pregled',284);
INSERT INTO pretraga VALUES(204,'Feces',null,TO_DATE('2019-12-28','YYYY-MM-DD'),284);
INSERT INTO anamneza VALUES (245,'Rane u ustima,proljev,vru�ica',TO_DATE('2020-03-19','YYYY-MM-DD'),118);
INSERT INTO dijag_anamneza VALUES(285,245,'K50.0');
INSERT INTO pretraga VALUES(205,'Krv','DKS',TO_DATE('2020-03-21','YYYY-MM-DD'),285);


INSERT INTO pacijent VALUES(119,'Leonarda','Nago','F',TO_DATE('1987-12-14','YYYY-MM-DD'), 'Mlinska 40,Osijek','0919958905',1);
INSERT INTO osnovno_zdravstveno VALUES(809007670,'nositelj','Hrvatska',119);
INSERT INTO osig_pac VALUES(119, TO_DATE('2012-12-07','YYYY-MM-DD'),TO_DATE('2042-11-30','YYYY-MM-DD'),809007670);
INSERT INTO dopunsko_osiguranje VALUES(333533291,'Croatia');
INSERT INTO dop_pac VALUES(119, TO_DATE('2019-07-01','YYYY-MM-DD'), TO_DATE('2020-06-29','YYYY-MM-DD'),119, 333533291);

INSERT INTO anamneza VALUES (246,'Grlobolja,uve�ani krajnici',TO_DATE('2019-06-26','YYYY-MM-DD'),119);
INSERT INTO dijag_anamneza VALUES(286,246,'J02');
INSERT INTO terapija VALUES(154,'3x dnevno',286,1006);
INSERT INTO bolovanja VALUES(2054, TO_DATE('2019-06-26','YYYY-MM-DD'),TO_DATE('2019-07-01','YYYY-MM-DD'),'A0 bolest',286);
INSERT INTO pretraga VALUES(206,'Krv','DKS',TO_DATE('2019-06-27','YYYY-MM-DD'),286);


INSERT INTO pacijent VALUES(120,'Boris','Nago','M',TO_DATE('1942-09-08','YYYY-MM-DD'), 'Mlinska 113,Osijek','0999002470',4);
INSERT INTO osnovno_zdravstveno VALUES(233108165,'nositelj','Hrvatska',120);
INSERT INTO osig_pac VALUES(120, TO_DATE('2007-08-23','YYYY-MM-DD'),TO_DATE('2037-08-15','YYYY-MM-DD'),233108165);
INSERT INTO dopunsko_osiguranje VALUES(883565852,'Allianz');
INSERT INTO dop_pac VALUES(120, TO_DATE('2019-07-08','YYYY-MM-DD'), TO_DATE('2020-07-06','YYYY-MM-DD'),120, 883565852);

INSERT INTO anamneza VALUES (247,'Sumnja na tetanus',TO_DATE('2019-11-14','YYYY-MM-DD'),120);
INSERT INTO dijag_anamneza VALUES(287,247,'Z23.5');
INSERT INTO cijepljenje VALUES(51,1,287);


INSERT INTO pacijent VALUES(121,'Bruno','Bu�i�','M',TO_DATE('2008-11-03','YYYY-MM-DD'), 'Europska avenija 49,Osijek','0915167303',2);
INSERT INTO osnovno_zdravstveno VALUES(273220519,'�lan obitelji','Hrvatska',121);
INSERT INTO osig_pac VALUES(121, TO_DATE('2008-11-03','YYYY-MM-DD'),TO_DATE('2038-10-27','YYYY-MM-DD'),273220519);
INSERT INTO dopunsko_osiguranje VALUES(641077294,'Allianz');
INSERT INTO dop_pac VALUES(121, TO_DATE('2019-09-05','YYYY-MM-DD'), TO_DATE('2020-09-03','YYYY-MM-DD'),121, 641077294);

INSERT INTO anamneza VALUES (248,'Povi�ena temp.,hipoglikemija',TO_DATE('2020-05-25','YYYY-MM-DD'),121);
INSERT INTO dijag_anamneza VALUES(288,248,'C22.0');
INSERT INTO uputnice VALUES(108,'A2','Onkologija','prvi pregled',288);
INSERT INTO anamneza VALUES (249,'Rane u ustima,proljev,vru�ica',TO_DATE('2020-03-29','YYYY-MM-DD'),121);
INSERT INTO dijag_anamneza VALUES(289,249,'K50.0');
INSERT INTO pretraga VALUES(207,'Krv','DKS',TO_DATE('2020-03-30','YYYY-MM-DD'),289);


INSERT INTO pacijent VALUES(122,'Ivica','Lenard','M',TO_DATE('1962-02-28','YYYY-MM-DD'), 'Mlinska 101,Osijek','0917253338',2);
INSERT INTO osnovno_zdravstveno VALUES(806996275,'nositelj','Hrvatska',122);
INSERT INTO osig_pac VALUES(122, TO_DATE('1987-02-22','YYYY-MM-DD'),TO_DATE('2017-02-14','YYYY-MM-DD'),806996275);
INSERT INTO dopunsko_osiguranje VALUES(135205688,'Croatia');
INSERT INTO dop_pac VALUES(122, TO_DATE('2019-08-23','YYYY-MM-DD'), TO_DATE('2020-08-21','YYYY-MM-DD'),122, 135205688);

INSERT INTO anamneza VALUES (250,'Bolno mokrenje',TO_DATE('2019-12-01','YYYY-MM-DD'),122);
INSERT INTO dijag_anamneza VALUES(290,250,'N30');
INSERT INTO bolovanja VALUES(2055, TO_DATE('2019-12-01','YYYY-MM-DD'),TO_DATE('2019-12-03','YYYY-MM-DD'),'A0 bolest',290);
INSERT INTO pretraga VALUES(208,'Krv','DKS',TO_DATE('2019-12-03','YYYY-MM-DD'),290);


INSERT INTO pacijent VALUES(123,'Karlo','Pataky','M',TO_DATE('1994-05-20','YYYY-MM-DD'), 'Drinska 72,Osijek','0982105480',1);
INSERT INTO osnovno_zdravstveno VALUES(368326977,'nositelj','Hrvatska',123);
INSERT INTO osig_pac VALUES(123, TO_DATE('2019-05-14','YYYY-MM-DD'),TO_DATE('2049-05-06','YYYY-MM-DD'),368326977);
INSERT INTO dopunsko_osiguranje VALUES(842683441,'Generali');
INSERT INTO dop_pac VALUES(123, TO_DATE('2019-03-17','YYYY-MM-DD'), TO_DATE('2020-03-15','YYYY-MM-DD'),123, 842683441);

INSERT INTO anamneza VALUES (251,'Vezikule na ko�i sa gnojem',TO_DATE('2020-04-28','YYYY-MM-DD'),123);
INSERT INTO dijag_anamneza VALUES(291,251,'L08.0');
INSERT INTO anamneza VALUES (252,'Rane u ustima,proljev,vru�ica',TO_DATE('2019-02-11','YYYY-MM-DD'),123);
INSERT INTO dijag_anamneza VALUES(292,252,'K50.0');
INSERT INTO pretraga VALUES(209,'Krv','DKS',TO_DATE('2019-02-12','YYYY-MM-DD'),292);


INSERT INTO pacijent VALUES(124,'Tomislav','Biland�i�','M',TO_DATE('1971-08-29','YYYY-MM-DD'), '�upanijska 44,Osijek','0916158638',3);
INSERT INTO osnovno_zdravstveno VALUES(219471078,'nositelj','Hrvatska',124);
INSERT INTO osig_pac VALUES(124, TO_DATE('1996-08-22','YYYY-MM-DD'),TO_DATE('2026-08-15','YYYY-MM-DD'),219471078);
INSERT INTO dopunsko_osiguranje VALUES(442602737,'Croatia');
INSERT INTO dop_pac VALUES(124, TO_DATE('2020-03-28','YYYY-MM-DD'), TO_DATE('2021-03-27','YYYY-MM-DD'),124, 442602737);

INSERT INTO anamneza VALUES (253,'POvi�ena temp. plikovi i osip na ko�i',TO_DATE('2019-06-29','YYYY-MM-DD'),124);
INSERT INTO dijag_anamneza VALUES(293,253,'B01');
INSERT INTO terapija VALUES(155,'0,0,1',293,1003);
INSERT INTO bolovanja VALUES(2056, TO_DATE('2019-06-29','YYYY-MM-DD'),TO_DATE('2019-07-01','YYYY-MM-DD'),'A0 bolest',293);
INSERT INTO pretraga VALUES(210,'Krv','BIOKEMIJA',TO_DATE('2019-06-30','YYYY-MM-DD'),293);


INSERT INTO pacijent VALUES(125,'Boris','Andrlon','M',TO_DATE('2016-01-14','YYYY-MM-DD'), 'Drinska 94,Osijek','0998875850',2);
INSERT INTO osnovno_zdravstveno VALUES(403508560,'�lan obitelji','Hrvatska',125);
INSERT INTO osig_pac VALUES(125, TO_DATE('2016-01-14','YYYY-MM-DD'),TO_DATE('2046-01-06','YYYY-MM-DD'),403508560);
INSERT INTO dopunsko_osiguranje VALUES(981779654,'Generali');
INSERT INTO dop_pac VALUES(125, TO_DATE('2019-10-19','YYYY-MM-DD'), TO_DATE('2020-10-17','YYYY-MM-DD'),125, 981779654);

INSERT INTO anamneza VALUES (254,'Rane u ustima,proljev,vru�ica',TO_DATE('2020-04-16','YYYY-MM-DD'),125);
INSERT INTO dijag_anamneza VALUES(294,254,'K50.0');
INSERT INTO uputnice VALUES(109,'A2','Gastroenterologija','prvi pregled',294);
INSERT INTO pretraga VALUES(211,'Krv','DKS',TO_DATE('2020-04-18','YYYY-MM-DD'),294);


INSERT INTO pacijent VALUES(126,'Stanko','Bro�anac','M',TO_DATE('1952-03-06','YYYY-MM-DD'), 'Europska avenija 79,Osijek','0985600198',3);
INSERT INTO osnovno_zdravstveno VALUES(468573232,'nositelj','Hrvatska',126);
INSERT INTO osig_pac VALUES(126, TO_DATE('2017-02-18','YYYY-MM-DD'),TO_DATE('2047-02-11','YYYY-MM-DD'),468573232);
INSERT INTO dopunsko_osiguranje VALUES(826930017,'Allianz');
INSERT INTO dop_pac VALUES(126, TO_DATE('2019-08-13','YYYY-MM-DD'), TO_DATE('2020-08-11','YYYY-MM-DD'),126, 826930017);

INSERT INTO anamneza VALUES (255,'Crveilo ko�e,bol i uko�enost zglobova',TO_DATE('2020-03-30','YYYY-MM-DD'),126);
INSERT INTO dijag_anamneza VALUES(295,255,'M00.0');
INSERT INTO anamneza VALUES (256,'Temp. normalna,crvenilo o�nih spojnica i ka�alj',TO_DATE('2019-03-28','YYYY-MM-DD'),126);
INSERT INTO dijag_anamneza VALUES(296,256,'Z23.7');
INSERT INTO cijepljenje VALUES(52,9,296);
INSERT INTO dijag_anamneza VALUES(297,256,'A37.0');


INSERT INTO pacijent VALUES(127,'Sa�a','Bajami�','M',TO_DATE('1951-05-02','YYYY-MM-DD'), 'Srijemska 32,Osijek','0994905054',4);
INSERT INTO osnovno_zdravstveno VALUES(597341419,'nositelj','Hrvatska',127);
INSERT INTO osig_pac VALUES(127, TO_DATE('2016-04-15','YYYY-MM-DD'),TO_DATE('2046-04-08','YYYY-MM-DD'),597341419);
INSERT INTO dopunsko_osiguranje VALUES(1218701,'HZZO');
INSERT INTO dop_pac VALUES(127, TO_DATE('2019-11-08','YYYY-MM-DD'), TO_DATE('2020-11-06','YYYY-MM-DD'),127, 1218701);

INSERT INTO anamneza VALUES (257,'POvi�ena temp. plikovi i osip na ko�i',TO_DATE('2019-05-10','YYYY-MM-DD'),127);
INSERT INTO dijag_anamneza VALUES(298,257,'B01');
INSERT INTO terapija VALUES(156,'0,0,1',298,1014);
INSERT INTO pretraga VALUES(212,'Krv','BIOKEMIJA',TO_DATE('2019-05-12','YYYY-MM-DD'),298);


INSERT INTO pacijent VALUES(128,'Hrvoje','Gavran','M',TO_DATE('1943-11-08','YYYY-MM-DD'), 'Vratni�ka 82,Osijek','0996966473',1);
INSERT INTO osnovno_zdravstveno VALUES(857418091,'nositelj','Hrvatska',128);
INSERT INTO osig_pac VALUES(128, TO_DATE('2008-10-22','YYYY-MM-DD'),TO_DATE('2038-10-15','YYYY-MM-DD'),857418091);
INSERT INTO dopunsko_osiguranje VALUES(302167384,'Generali');
INSERT INTO dop_pac VALUES(128, TO_DATE('2019-09-23','YYYY-MM-DD'), TO_DATE('2020-09-21','YYYY-MM-DD'),128, 302167384);

INSERT INTO anamneza VALUES (258,'Povisen tlak',TO_DATE('2019-09-15','YYYY-MM-DD'),128);
INSERT INTO dijag_anamneza VALUES(299,258,'I10');
INSERT INTO terapija VALUES(157,'1,0,0',299,1026);


INSERT INTO pacijent VALUES(129,'�eljka','Sanseovi�','F',TO_DATE('1970-05-25','YYYY-MM-DD'), 'Europska avenija 38,Osijek','0989717090',1);
INSERT INTO osnovno_zdravstveno VALUES(960088632,'nositelj','Hrvatska',129);
INSERT INTO osig_pac VALUES(129, TO_DATE('1995-05-19','YYYY-MM-DD'),TO_DATE('2025-05-11','YYYY-MM-DD'),960088632);
INSERT INTO dopunsko_osiguranje VALUES(192693571,'Croatia');
INSERT INTO dop_pac VALUES(129, TO_DATE('2020-01-26','YYYY-MM-DD'), TO_DATE('2021-01-24','YYYY-MM-DD'),129, 192693571);

INSERT INTO anamneza VALUES (259,'Sumnja na rak bubrega',TO_DATE('2020-05-20','YYYY-MM-DD'),129);
INSERT INTO dijag_anamneza VALUES(300,259,'C64.0');
INSERT INTO terapija VALUES(158,'1,0,1',300,1021);
INSERT INTO anamneza VALUES (260,'Nemogu�nost mokrenja',TO_DATE('2019-08-11','YYYY-MM-DD'),129);
INSERT INTO dijag_anamneza VALUES(301,260,'N21.0');
INSERT INTO bolovanja VALUES(2057, TO_DATE('2019-08-11','YYYY-MM-DD'),TO_DATE('2019-08-16','YYYY-MM-DD'),'A0 bolest',301);
INSERT INTO pretraga VALUES(213,'Krv','KKS',TO_DATE('2019-08-13','YYYY-MM-DD'),301);


INSERT INTO pacijent VALUES(130,'Igor','Sajber','M',TO_DATE('1961-08-18','YYYY-MM-DD'), 'Vratni�ka 142,Osijek','0987864500',4);
INSERT INTO osnovno_zdravstveno VALUES(459832683,'nositelj','Hrvatska',130);
INSERT INTO osig_pac VALUES(130, TO_DATE('1986-08-12','YYYY-MM-DD'),TO_DATE('2016-08-04','YYYY-MM-DD'),459832683);
INSERT INTO dopunsko_osiguranje VALUES(121325629,'Generali');
INSERT INTO dop_pac VALUES(130, TO_DATE('2019-03-13','YYYY-MM-DD'), TO_DATE('2020-03-11','YYYY-MM-DD'),130, 121325629);

INSERT INTO anamneza VALUES (261,'Mu�nina,povra�anje,bol u trbuhu',TO_DATE('2019-09-19','YYYY-MM-DD'),130);
INSERT INTO dijag_anamneza VALUES(302,261,'K35.9');
INSERT INTO uputnice VALUES(110,'A2','Gastroenterologija','prvi pregled',302);
INSERT INTO pretraga VALUES(214,'Feces',null,TO_DATE('2019-09-20','YYYY-MM-DD'),302);
INSERT INTO anamneza VALUES (262,'Crveni,ljuskav osip na tjemenu',TO_DATE('2019-11-18','YYYY-MM-DD'),130);
INSERT INTO dijag_anamneza VALUES(303,262,'L21.0');
INSERT INTO bolovanja VALUES(2058, TO_DATE('2019-11-18','YYYY-MM-DD'),TO_DATE('2019-11-23','YYYY-MM-DD'),'A0 bolest',303);
INSERT INTO pretraga VALUES(215,'Krv','DKS',TO_DATE('2019-11-19','YYYY-MM-DD'),303);


INSERT INTO pacijent VALUES(131,'Ivana','Slivka','F',TO_DATE('1986-03-12','YYYY-MM-DD'), 'Vukovarska 45,Osijek','0983680912',3);
INSERT INTO osnovno_zdravstveno VALUES(917893327,'nositelj','Hrvatska',131);
INSERT INTO osig_pac VALUES(131, TO_DATE('2011-03-06','YYYY-MM-DD'),TO_DATE('2041-02-26','YYYY-MM-DD'),917893327);
INSERT INTO dopunsko_osiguranje VALUES(4717350,'HZZO');
INSERT INTO dop_pac VALUES(131, TO_DATE('2020-02-10','YYYY-MM-DD'), TO_DATE('2021-02-08','YYYY-MM-DD'),131, 4717350);

INSERT INTO anamneza VALUES (263,'Crveilo ko�e,bol i uko�enost zglobova',TO_DATE('2019-04-07','YYYY-MM-DD'),131);
INSERT INTO dijag_anamneza VALUES(304,263,'M00.0');
INSERT INTO anamneza VALUES (264,'Crveilo ko�e,bol i uko�enost zglobova',TO_DATE('2020-01-21','YYYY-MM-DD'),131);
INSERT INTO dijag_anamneza VALUES(305,264,'M00.0');


INSERT INTO pacijent VALUES(132,'Hrvoje','Ehman','M',TO_DATE('2017-03-26','YYYY-MM-DD'), 'Ilirska 27,Osijek','0987594847',2);
INSERT INTO osnovno_zdravstveno VALUES(474444352,'�lan obitelji','Hrvatska',132);
INSERT INTO osig_pac VALUES(132, TO_DATE('2017-03-26','YYYY-MM-DD'),TO_DATE('2047-03-19','YYYY-MM-DD'),474444352);
INSERT INTO dopunsko_osiguranje VALUES(176047152,'Generali');
INSERT INTO dop_pac VALUES(132, TO_DATE('2020-06-23','YYYY-MM-DD'), TO_DATE('2021-06-22','YYYY-MM-DD'),132, 176047152);

INSERT INTO anamneza VALUES (265,'Nemo�,gubitak teka,visoka temp.',TO_DATE('2019-11-09','YYYY-MM-DD'),132);
INSERT INTO dijag_anamneza VALUES(306,265,'J10.0');
INSERT INTO anamneza VALUES (266,'Bolno mokrenje,povi�ena temp.',TO_DATE('2020-05-02','YYYY-MM-DD'),132);
INSERT INTO dijag_anamneza VALUES(307,266,'N15.1');


INSERT INTO pacijent VALUES(133,'Dorian','Vugrek','M',TO_DATE('1958-10-30','YYYY-MM-DD'), '�upanijska 51,Osijek','0991612840',1);
INSERT INTO osnovno_zdravstveno VALUES(144560845,'nositelj','Hrvatska',133);
INSERT INTO osig_pac VALUES(133, TO_DATE('1983-10-24','YYYY-MM-DD'),TO_DATE('2013-10-16','YYYY-MM-DD'),144560845);
INSERT INTO dopunsko_osiguranje VALUES(1753080,'HZZO');
INSERT INTO dop_pac VALUES(133, TO_DATE('2019-10-11','YYYY-MM-DD'), TO_DATE('2020-10-09','YYYY-MM-DD'),133, 1753080);

INSERT INTO anamneza VALUES (267,'Povi�ena temp.,hipoglikemija',TO_DATE('2019-06-24','YYYY-MM-DD'),133);
INSERT INTO dijag_anamneza VALUES(308,267,'C22.0');
INSERT INTO anamneza VALUES (268,'Crveni,ljuskav osip na tjemenu',TO_DATE('2020-02-14','YYYY-MM-DD'),133);
INSERT INTO dijag_anamneza VALUES(309,268,'L21.0');


INSERT INTO pacijent VALUES(134,'Lana','Juri�','F',TO_DATE('1971-05-11','YYYY-MM-DD'), 'Wilsonova 135,Osijek','0917213102',1);
INSERT INTO osnovno_zdravstveno VALUES(902680210,'nositelj','Hrvatska',134);
INSERT INTO osig_pac VALUES(134, TO_DATE('1996-05-04','YYYY-MM-DD'),TO_DATE('2026-04-27','YYYY-MM-DD'),902680210);
INSERT INTO dopunsko_osiguranje VALUES(738620743,'Croatia');
INSERT INTO dop_pac VALUES(134, TO_DATE('2019-08-03','YYYY-MM-DD'), TO_DATE('2020-08-01','YYYY-MM-DD'),134, 738620743);

INSERT INTO anamneza VALUES (269,'Rane u ustima,proljev,vru�ica',TO_DATE('2019-11-23','YYYY-MM-DD'),134);
INSERT INTO dijag_anamneza VALUES(310,269,'K50.0');
INSERT INTO pretraga VALUES(216,'Feces',null,TO_DATE('2019-11-24','YYYY-MM-DD'),310);
INSERT INTO anamneza VALUES (270,'Povisen tlak',TO_DATE('2020-01-02','YYYY-MM-DD'),134);
INSERT INTO dijag_anamneza VALUES(311,270,'I10');
INSERT INTO uputnice VALUES(111,'A2','Nefrologija','prvi pregled',311);
INSERT INTO uputnice VALUES(112,'A2','Kardiologija','prvi pregled',311);


INSERT INTO pacijent VALUES(135,'Laura','Mati�','F',TO_DATE('2001-03-21','YYYY-MM-DD'), 'Vratni�ka 126,Osijek','0957691651',1);
INSERT INTO osnovno_zdravstveno VALUES(406090792,'nositelj','Hrvatska',135);
INSERT INTO osig_pac VALUES(135, TO_DATE('2026-03-15','YYYY-MM-DD'),TO_DATE('2056-03-07','YYYY-MM-DD'),406090792);
INSERT INTO dopunsko_osiguranje VALUES(201030038,'Allianz');
INSERT INTO dop_pac VALUES(135, TO_DATE('2019-10-09','YYYY-MM-DD'), TO_DATE('2020-10-07','YYYY-MM-DD'),135, 201030038);

INSERT INTO anamneza VALUES (271,'Crveni,ljuskav osip na tjemenu',TO_DATE('2020-05-14','YYYY-MM-DD'),135);
INSERT INTO dijag_anamneza VALUES(312,271,'L21.0');
INSERT INTO pretraga VALUES(217,'Krv','BIOKEMIJA',TO_DATE('2020-05-15','YYYY-MM-DD'),312);


INSERT INTO pacijent VALUES(136,'Dajana','Duspara','F',TO_DATE('1993-10-10','YYYY-MM-DD'), '�upanijska 54,Osijek','0953825446',3);
INSERT INTO osnovno_zdravstveno VALUES(610899182,'nositelj','Hrvatska',136);
INSERT INTO osig_pac VALUES(136, TO_DATE('2018-10-04','YYYY-MM-DD'),TO_DATE('2048-09-26','YYYY-MM-DD'),610899182);
INSERT INTO dopunsko_osiguranje VALUES(390777029,'Allianz');
INSERT INTO dop_pac VALUES(136, TO_DATE('2019-03-24','YYYY-MM-DD'), TO_DATE('2020-03-22','YYYY-MM-DD'),136, 390777029);

INSERT INTO anamneza VALUES (272,'Nemo�,gubitak teka,visoka temp.',TO_DATE('2019-02-07','YYYY-MM-DD'),136);
INSERT INTO dijag_anamneza VALUES(313,272,'J10.0');
INSERT INTO terapija VALUES(159,'1x dnevno',313,1003);
INSERT INTO uputnice VALUES(113,'A2','Pulmologija','prvi pregled',313);
INSERT INTO pretraga VALUES(218,'Krv','DKS',TO_DATE('2019-02-09','YYYY-MM-DD'),313);


INSERT INTO pacijent VALUES(137,'Matej','Pfaf','M',TO_DATE('1987-03-28','YYYY-MM-DD'), 'Mlinska 8,Osijek','0913852688',2);
INSERT INTO osnovno_zdravstveno VALUES(128357920,'nositelj','Hrvatska',137);
INSERT INTO osig_pac VALUES(137, TO_DATE('2012-03-21','YYYY-MM-DD'),TO_DATE('2042-03-14','YYYY-MM-DD'),128357920);
INSERT INTO dopunsko_osiguranje VALUES(1023699,'HZZO');
INSERT INTO dop_pac VALUES(137, TO_DATE('2020-01-12','YYYY-MM-DD'), TO_DATE('2021-01-10','YYYY-MM-DD'),137, 1023699);

INSERT INTO anamneza VALUES (273,'Intezivan ka�alj, ote�ano disanje',TO_DATE('2019-12-13','YYYY-MM-DD'),137);
INSERT INTO dijag_anamneza VALUES(314,273,'J06.0');
INSERT INTO terapija VALUES(160,'1,0,1',314,1003);
INSERT INTO uputnice VALUES(114,'A2','Otorinolaringologija','prvi pregled',314);
INSERT INTO anamneza VALUES (274,'Temp. normalna,crvenilo o�nih spojnica i ka�alj',TO_DATE('2019-08-08','YYYY-MM-DD'),137);
INSERT INTO dijag_anamneza VALUES(315,274,'Z23.7');
INSERT INTO cijepljenje VALUES(53,9,315);
INSERT INTO dijag_anamneza VALUES(316,274,'A37.0');


INSERT INTO pacijent VALUES(138,'Dominik','�ivalj','M',TO_DATE('1950-11-04','YYYY-MM-DD'), 'Ilirska 114,Osijek','0956053792',2);
INSERT INTO osnovno_zdravstveno VALUES(371123370,'nositelj','Hrvatska',138);
INSERT INTO osig_pac VALUES(138, TO_DATE('2015-10-19','YYYY-MM-DD'),TO_DATE('2045-10-11','YYYY-MM-DD'),371123370);
INSERT INTO dopunsko_osiguranje VALUES(219328186,'Generali');
INSERT INTO dop_pac VALUES(138, TO_DATE('2020-02-23','YYYY-MM-DD'), TO_DATE('2021-02-21','YYYY-MM-DD'),138, 219328186);

INSERT INTO anamneza VALUES (275,'Povi�ena temp.,hipoglikemija',TO_DATE('2019-10-17','YYYY-MM-DD'),138);
INSERT INTO dijag_anamneza VALUES(317,275,'C22.0');
INSERT INTO anamneza VALUES (276,'POvi�ena temp. plikovi i osip na ko�i',TO_DATE('2019-09-08','YYYY-MM-DD'),138);
INSERT INTO dijag_anamneza VALUES(318,276,'B01');
INSERT INTO terapija VALUES(161,'1,0,1',318,1003);
INSERT INTO pretraga VALUES(219,'Krv','DKS',TO_DATE('2019-09-09','YYYY-MM-DD'),318);


INSERT INTO pacijent VALUES(139,'Antonio','�u�ov�ek','M',TO_DATE('1950-10-03','YYYY-MM-DD'), 'Vukovarska 122,Osijek','0985274942',1);
INSERT INTO osnovno_zdravstveno VALUES(878093325,'nositelj','Hrvatska',139);
INSERT INTO osig_pac VALUES(139, TO_DATE('2015-09-17','YYYY-MM-DD'),TO_DATE('2045-09-09','YYYY-MM-DD'),878093325);
INSERT INTO dopunsko_osiguranje VALUES(594334699,'Allianz');
INSERT INTO dop_pac VALUES(139, TO_DATE('2019-05-11','YYYY-MM-DD'), TO_DATE('2020-05-09','YYYY-MM-DD'),139, 594334699);

INSERT INTO anamneza VALUES (277,'Intezivan ka�alj, ote�ano disanje',TO_DATE('2019-10-23','YYYY-MM-DD'),139);
INSERT INTO dijag_anamneza VALUES(319,277,'J06.0');
INSERT INTO terapija VALUES(162,'0,0,1',319,1003);
INSERT INTO uputnice VALUES(115,'A2','Otorinolaringologija','prvi pregled',319);
INSERT INTO anamneza VALUES (278,'Sumnja na rak bubrega',TO_DATE('2020-04-30','YYYY-MM-DD'),139);
INSERT INTO dijag_anamneza VALUES(320,278,'C64.0');
INSERT INTO pretraga VALUES(220,'Krv','DKS',TO_DATE('2020-05-04','YYYY-MM-DD'),320);


INSERT INTO pacijent VALUES(140,'Jan','Luki�','M',TO_DATE('1947-05-18','YYYY-MM-DD'), 'Europska avenija 146,Osijek','0985600674',2);
INSERT INTO osnovno_zdravstveno VALUES(529621381,'nositelj','Hrvatska',140);
INSERT INTO osig_pac VALUES(140, TO_DATE('2012-05-01','YYYY-MM-DD'),TO_DATE('2042-04-24','YYYY-MM-DD'),529621381);
INSERT INTO dopunsko_osiguranje VALUES(487165329,'Generali');
INSERT INTO dop_pac VALUES(140, TO_DATE('2019-07-03','YYYY-MM-DD'), TO_DATE('2020-07-01','YYYY-MM-DD'),140, 487165329);

INSERT INTO anamneza VALUES (279,'Grlobolja,uve�ani krajnici',TO_DATE('2020-03-03','YYYY-MM-DD'),140);
INSERT INTO dijag_anamneza VALUES(321,279,'J02');
INSERT INTO terapija VALUES(163,'2x dnevno',321,1006);
INSERT INTO uputnice VALUES(116,'A2','Otorinolaringologija','prvi pregled',321);
INSERT INTO anamneza VALUES (280,'Intezivan ka�alj, ote�ano disanje',TO_DATE('2019-04-10','YYYY-MM-DD'),140);
INSERT INTO dijag_anamneza VALUES(322,280,'J06.0');


INSERT INTO pacijent VALUES(141,'Ivan','Bartolovi�','M',TO_DATE('1956-09-05','YYYY-MM-DD'), 'Srijemska 43,Osijek','0998228564',4);
INSERT INTO osnovno_zdravstveno VALUES(678714038,'nositelj','Hrvatska',141);
INSERT INTO osig_pac VALUES(141, TO_DATE('1981-08-30','YYYY-MM-DD'),TO_DATE('2011-08-23','YYYY-MM-DD'),678714038);
INSERT INTO dopunsko_osiguranje VALUES(750154486,'Allianz');
INSERT INTO dop_pac VALUES(141, TO_DATE('2019-06-03','YYYY-MM-DD'), TO_DATE('2020-06-01','YYYY-MM-DD'),141, 750154486);

INSERT INTO anamneza VALUES (281,'Mu�nina,povra�anje,bol u trbuhu',TO_DATE('2019-04-30','YYYY-MM-DD'),141);
INSERT INTO dijag_anamneza VALUES(323,281,'K35.9');
INSERT INTO pretraga VALUES(221,'Feces',null,TO_DATE('2019-05-02','YYYY-MM-DD'),323);
INSERT INTO anamneza VALUES (282,'Temp. normalna,crvenilo o�nih spojnica i ka�alj',TO_DATE('2020-03-09','YYYY-MM-DD'),141);
INSERT INTO dijag_anamneza VALUES(324,282,'Z23.7');
INSERT INTO cijepljenje VALUES(54,9,324);
INSERT INTO dijag_anamneza VALUES(325,282,'A37.0');


INSERT INTO pacijent VALUES(142,'Davor','Majeti�','M',TO_DATE('1983-12-24','YYYY-MM-DD'), 'Kozja�ka 132,Osijek','0918500981',1);
INSERT INTO osnovno_zdravstveno VALUES(660861593,'nositelj','Hrvatska',142);
INSERT INTO osig_pac VALUES(142, TO_DATE('2008-12-17','YYYY-MM-DD'),TO_DATE('2038-12-10','YYYY-MM-DD'),660861593);
INSERT INTO dopunsko_osiguranje VALUES(348237452,'Allianz');
INSERT INTO dop_pac VALUES(142, TO_DATE('2020-02-13','YYYY-MM-DD'), TO_DATE('2021-02-11','YYYY-MM-DD'),142, 348237452);

INSERT INTO anamneza VALUES (283,'Nemo�,gubitak teka,visoka temp.',TO_DATE('2019-12-09','YYYY-MM-DD'),142);
INSERT INTO dijag_anamneza VALUES(326,283,'J10.0');
INSERT INTO bolovanja VALUES(2059, TO_DATE('2019-12-09','YYYY-MM-DD'),TO_DATE('2019-12-10','YYYY-MM-DD'),'A0 bolest',326);
INSERT INTO pretraga VALUES(222,'Krv','KKS',TO_DATE('2019-12-11','YYYY-MM-DD'),326);


INSERT INTO pacijent VALUES(143,'Iva','�krbi�','F',TO_DATE('1983-06-19','YYYY-MM-DD'), 'Europska avenija 45,Osijek','0954629009',1);
INSERT INTO osnovno_zdravstveno VALUES(893663825,'nositelj','Hrvatska',143);
INSERT INTO osig_pac VALUES(143, TO_DATE('2008-06-12','YYYY-MM-DD'),TO_DATE('2038-06-05','YYYY-MM-DD'),893663825);
INSERT INTO dopunsko_osiguranje VALUES(889615773,'Croatia');
INSERT INTO dop_pac VALUES(143, TO_DATE('2019-05-11','YYYY-MM-DD'), TO_DATE('2020-05-09','YYYY-MM-DD'),143, 889615773);

INSERT INTO anamneza VALUES (284,'Nemo�,gubitak teka,visoka temp.',TO_DATE('2019-03-13','YYYY-MM-DD'),143);
INSERT INTO dijag_anamneza VALUES(327,284,'J10.0');
INSERT INTO terapija VALUES(164,'3x dnevno',327,1003);
INSERT INTO anamneza VALUES (285,'Nemo�,gubitak teka,visoka temp.',TO_DATE('2019-11-11','YYYY-MM-DD'),143);
INSERT INTO dijag_anamneza VALUES(328,285,'J10.0');
INSERT INTO uputnice VALUES(117,'A2','Pulmologija','prvi pregled',328);


INSERT INTO pacijent VALUES(144,'Anja','Nikitovi�','F',TO_DATE('1977-04-13','YYYY-MM-DD'), 'Vratni�ka 61,Osijek','0996706383',2);
INSERT INTO osnovno_zdravstveno VALUES(942616365,'nositelj','Hrvatska',144);
INSERT INTO osig_pac VALUES(144, TO_DATE('2002-04-07','YYYY-MM-DD'),TO_DATE('2032-03-30','YYYY-MM-DD'),942616365);
INSERT INTO dopunsko_osiguranje VALUES(355902011,'Allianz');
INSERT INTO dop_pac VALUES(144, TO_DATE('2019-10-26','YYYY-MM-DD'), TO_DATE('2020-10-24','YYYY-MM-DD'),144, 355902011);

INSERT INTO anamneza VALUES (286,'Sumnja na rak bubrega',TO_DATE('2019-05-17','YYYY-MM-DD'),144);
INSERT INTO dijag_anamneza VALUES(329,286,'C64.0');
INSERT INTO bolovanja VALUES(2060, TO_DATE('2019-05-17','YYYY-MM-DD'),TO_DATE('2019-05-20','YYYY-MM-DD'),'A0 bolest',329);
INSERT INTO terapija VALUES(165,'1,0,1',329,1021);
INSERT INTO pretraga VALUES(223,'Krv','BIOKEMIJA',TO_DATE('2019-05-18','YYYY-MM-DD'),329);


INSERT INTO pacijent VALUES(145,'Patrik','Omazi�','M',TO_DATE('1983-11-25','YYYY-MM-DD'), 'Kozja�ka 8,Osijek','0993493241',2);
INSERT INTO osnovno_zdravstveno VALUES(582950664,'nositelj','Hrvatska',145);
INSERT INTO osig_pac VALUES(145, TO_DATE('2008-11-18','YYYY-MM-DD'),TO_DATE('2038-11-11','YYYY-MM-DD'),582950664);
INSERT INTO dopunsko_osiguranje VALUES(759525971,'Allianz');
INSERT INTO dop_pac VALUES(145, TO_DATE('2019-08-18','YYYY-MM-DD'), TO_DATE('2020-08-16','YYYY-MM-DD'),145, 759525971);

INSERT INTO anamneza VALUES (287,'Nemo�,gubitak teka,visoka temp.',TO_DATE('2019-05-19','YYYY-MM-DD'),145);
INSERT INTO dijag_anamneza VALUES(330,287,'J10.0');
INSERT INTO terapija VALUES(166,'0,0,1',330,1006);
INSERT INTO bolovanja VALUES(2061, TO_DATE('2019-05-19','YYYY-MM-DD'),TO_DATE('2019-05-21','YYYY-MM-DD'),'A0 bolest',330);
INSERT INTO pretraga VALUES(224,'Krv','BIOKEMIJA',TO_DATE('2019-05-21','YYYY-MM-DD'),330);


INSERT INTO pacijent VALUES(146,'Fran','Luki�','M',TO_DATE('2009-03-19','YYYY-MM-DD'), 'Ilirska 39,Osijek','0914344247',2);
INSERT INTO osnovno_zdravstveno VALUES(503465110,'�lan obitelji','Hrvatska',146);
INSERT INTO osig_pac VALUES(146, TO_DATE('2009-03-19','YYYY-MM-DD'),TO_DATE('2039-03-12','YYYY-MM-DD'),503465110);
INSERT INTO dopunsko_osiguranje VALUES(180190410,'Croatia');
INSERT INTO dop_pac VALUES(146, TO_DATE('2020-03-03','YYYY-MM-DD'), TO_DATE('2021-03-02','YYYY-MM-DD'),146, 180190410);

INSERT INTO anamneza VALUES (288,'Povi�ena temp.,hipoglikemija',TO_DATE('2019-03-30','YYYY-MM-DD'),146);
INSERT INTO dijag_anamneza VALUES(331,288,'C22.0');
INSERT INTO anamneza VALUES (289,'Rane u ustima,proljev,vru�ica',TO_DATE('2019-08-29','YYYY-MM-DD'),146);
INSERT INTO dijag_anamneza VALUES(332,289,'K50.0');
INSERT INTO pretraga VALUES(225,'Feces',null,TO_DATE('2019-08-30','YYYY-MM-DD'),332);


INSERT INTO pacijent VALUES(147,'Ana','Medi�','F',TO_DATE('1962-09-25','YYYY-MM-DD'), 'Vratni�ka 70,Osijek','0981326382',4);
INSERT INTO osnovno_zdravstveno VALUES(908871383,'nositelj','Hrvatska',147);
INSERT INTO osig_pac VALUES(147, TO_DATE('1987-09-19','YYYY-MM-DD'),TO_DATE('2017-09-11','YYYY-MM-DD'),908871383);
INSERT INTO dopunsko_osiguranje VALUES(5563841,'HZZO');
INSERT INTO dop_pac VALUES(147, TO_DATE('2020-05-24','YYYY-MM-DD'), TO_DATE('2021-05-23','YYYY-MM-DD'),147, 5563841);

INSERT INTO anamneza VALUES (290,'Bolno mokrenje,povi�ena temp.',TO_DATE('2020-05-17','YYYY-MM-DD'),147);
INSERT INTO dijag_anamneza VALUES(333,290,'N15.1');
INSERT INTO bolovanja VALUES(2062, TO_DATE('2020-05-17','YYYY-MM-DD'),TO_DATE('2020-05-19','YYYY-MM-DD'),'A0 bolest',333);
INSERT INTO pretraga VALUES(226,'Krv','DKS',TO_DATE('2020-05-18','YYYY-MM-DD'),333);


INSERT INTO pacijent VALUES(148,'Zdenko','Turni�ki','M',TO_DATE('1944-03-16','YYYY-MM-DD'), 'Mlinska 115,Osijek','0991072553',2);
INSERT INTO osnovno_zdravstveno VALUES(885851298,'nositelj','Hrvatska',148);
INSERT INTO osig_pac VALUES(148, TO_DATE('2009-02-28','YYYY-MM-DD'),TO_DATE('2039-02-21','YYYY-MM-DD'),885851298);
INSERT INTO dopunsko_osiguranje VALUES(729833867,'Allianz');
INSERT INTO dop_pac VALUES(148, TO_DATE('2020-02-29','YYYY-MM-DD'), TO_DATE('2021-02-27','YYYY-MM-DD'),148, 729833867);

INSERT INTO anamneza VALUES (291,'Nemogu�nost mokrenja',TO_DATE('2020-01-17','YYYY-MM-DD'),148);
INSERT INTO dijag_anamneza VALUES(334,291,'N21.0');
INSERT INTO pretraga VALUES(227,'Krv','DKS',TO_DATE('2020-01-18','YYYY-MM-DD'),334);


INSERT INTO pacijent VALUES(149,'Iva','Mata�i�','F',TO_DATE('1956-12-14','YYYY-MM-DD'), 'Mlinska 130,Osijek','0958131836',1);
INSERT INTO osnovno_zdravstveno VALUES(575879946,'nositelj','Hrvatska',149);
INSERT INTO osig_pac VALUES(149, TO_DATE('1981-12-08','YYYY-MM-DD'),TO_DATE('2011-12-01','YYYY-MM-DD'),575879946);
INSERT INTO dopunsko_osiguranje VALUES(360498831,'Allianz');
INSERT INTO dop_pac VALUES(149, TO_DATE('2019-05-30','YYYY-MM-DD'), TO_DATE('2020-05-28','YYYY-MM-DD'),149, 360498831);

INSERT INTO anamneza VALUES (292,'Grlobolja,uve�ani krajnici',TO_DATE('2019-12-30','YYYY-MM-DD'),149);
INSERT INTO dijag_anamneza VALUES(335,292,'J02');
INSERT INTO terapija VALUES(167,'0,0,1',335,1014);
INSERT INTO anamneza VALUES (293,'Povi�ena temp.,hipoglikemija',TO_DATE('2019-07-15','YYYY-MM-DD'),149);
INSERT INTO dijag_anamneza VALUES(336,293,'C22.0');


INSERT INTO pacijent VALUES(150,'Ivan','Izvezi�','M',TO_DATE('2013-10-06','YYYY-MM-DD'), 'Zagreba�ka 5,Osijek','0958178418',2);
INSERT INTO osnovno_zdravstveno VALUES(225067715,'�lan obitelji','Hrvatska',150);
INSERT INTO osig_pac VALUES(150, TO_DATE('2013-10-06','YYYY-MM-DD'),TO_DATE('2043-09-29','YYYY-MM-DD'),225067715);
INSERT INTO dopunsko_osiguranje VALUES(393827178,'Croatia');
INSERT INTO dop_pac VALUES(150, TO_DATE('2020-03-21','YYYY-MM-DD'), TO_DATE('2021-03-20','YYYY-MM-DD'),150, 393827178);

INSERT INTO anamneza VALUES (294,'Intezivan ka�alj, ote�ano disanje',TO_DATE('2019-01-15','YYYY-MM-DD'),150);
INSERT INTO dijag_anamneza VALUES(337,294,'J06.0');
INSERT INTO anamneza VALUES (295,'Osjetljivost i oteknu�e sinusa',TO_DATE('2019-11-15','YYYY-MM-DD'),150);
INSERT INTO dijag_anamneza VALUES(338,295,'J01.2');
INSERT INTO terapija VALUES(168,'1,0,1',338,1006);
INSERT INTO uputnice VALUES(118,'A2','Otorinolaringologija','prvi pregled',338);


INSERT INTO pacijent VALUES(151,'Tonka','Kova�evi�','F',TO_DATE('1997-10-25','YYYY-MM-DD'), 'Vratni�ka 138,Osijek','0954473051',2);
INSERT INTO osnovno_zdravstveno VALUES(198877619,'nositelj','Hrvatska',151);
INSERT INTO osig_pac VALUES(151, TO_DATE('2022-10-19','YYYY-MM-DD'),TO_DATE('2052-10-11','YYYY-MM-DD'),198877619);
INSERT INTO dopunsko_osiguranje VALUES(284493274,'Generali');
INSERT INTO dop_pac VALUES(151, TO_DATE('2019-09-22','YYYY-MM-DD'), TO_DATE('2020-09-20','YYYY-MM-DD'),151, 284493274);

INSERT INTO anamneza VALUES (296,'POvi�ena temp. plikovi i osip na ko�i',TO_DATE('2019-02-02','YYYY-MM-DD'),151);
INSERT INTO dijag_anamneza VALUES(339,296,'B01');
INSERT INTO uputnice VALUES(119,'A2','Infektologija','prvi pregled',339);
INSERT INTO anamneza VALUES (297,'Tvrda ko�a,gubitak dlake',TO_DATE('2019-01-21','YYYY-MM-DD'),151);
INSERT INTO dijag_anamneza VALUES(340,297,'M34.0');
INSERT INTO pretraga VALUES(228,'Krv','BIOKEMIJA',TO_DATE('2019-01-23','YYYY-MM-DD'),340);


INSERT INTO pacijent VALUES(152,'Hrvoje','Paukovi�','M',TO_DATE('1958-08-01','YYYY-MM-DD'), '�upanijska 27,Osijek','0994463118',1);
INSERT INTO osnovno_zdravstveno VALUES(874957272,'nositelj','Hrvatska',152);
INSERT INTO osig_pac VALUES(152, TO_DATE('1983-07-26','YYYY-MM-DD'),TO_DATE('2013-07-18','YYYY-MM-DD'),874957272);
INSERT INTO dopunsko_osiguranje VALUES(4309792,'HZZO');
INSERT INTO dop_pac VALUES(152, TO_DATE('2019-08-14','YYYY-MM-DD'), TO_DATE('2020-08-12','YYYY-MM-DD'),152, 4309792);

INSERT INTO anamneza VALUES (298,'Tvrda ko�a,gubitak dlake',TO_DATE('2019-03-13','YYYY-MM-DD'),152);
INSERT INTO dijag_anamneza VALUES(341,298,'M34.0');
INSERT INTO anamneza VALUES (299,'Crveni,ljuskav osip na tjemenu',TO_DATE('2019-02-24','YYYY-MM-DD'),152);
INSERT INTO dijag_anamneza VALUES(342,299,'L21.0');
INSERT INTO bolovanja VALUES(2063, TO_DATE('2019-02-24','YYYY-MM-DD'),TO_DATE('2019-03-04','YYYY-MM-DD'),'A0 bolest',342);
INSERT INTO pretraga VALUES(229,'Krv','KKS',TO_DATE('2019-02-25','YYYY-MM-DD'),342);


INSERT INTO pacijent VALUES(153,'Jasmina','Pacek','F',TO_DATE('2016-03-26','YYYY-MM-DD'), '�upanijska 93,Osijek','0914092318',1);
INSERT INTO osnovno_zdravstveno VALUES(989521525,'�lan obitelji','Hrvatska',153);
INSERT INTO osig_pac VALUES(153, TO_DATE('2016-03-26','YYYY-MM-DD'),TO_DATE('2046-03-19','YYYY-MM-DD'),989521525);
INSERT INTO dopunsko_osiguranje VALUES(432128031,'Allianz');
INSERT INTO dop_pac VALUES(153, TO_DATE('2019-12-20','YYYY-MM-DD'), TO_DATE('2020-12-18','YYYY-MM-DD'),153, 432128031);

INSERT INTO anamneza VALUES (300,'Grlobolja,uve�ani krajnici',TO_DATE('2019-11-07','YYYY-MM-DD'),153);
INSERT INTO dijag_anamneza VALUES(343,300,'J02');
INSERT INTO terapija VALUES(169,'0,0,1',343,1006);
INSERT INTO anamneza VALUES (301,'Nemogu�nost mokrenja',TO_DATE('2019-12-22','YYYY-MM-DD'),153);
INSERT INTO dijag_anamneza VALUES(344,301,'N21.0');
INSERT INTO pretraga VALUES(230,'Urin',null,TO_DATE('2019-12-23','YYYY-MM-DD'),344);


INSERT INTO pacijent VALUES(154,'Bojan','Karanovi�','M',TO_DATE('1989-04-23','YYYY-MM-DD'), 'Vukovarska 8,Osijek','0995937680',3);
INSERT INTO osnovno_zdravstveno VALUES(439765444,'nositelj','Hrvatska',154);
INSERT INTO osig_pac VALUES(154, TO_DATE('2014-04-17','YYYY-MM-DD'),TO_DATE('2044-04-09','YYYY-MM-DD'),439765444);
INSERT INTO dopunsko_osiguranje VALUES(605157278,'Allianz');
INSERT INTO dop_pac VALUES(154, TO_DATE('2019-03-11','YYYY-MM-DD'), TO_DATE('2020-03-09','YYYY-MM-DD'),154, 605157278);

INSERT INTO anamneza VALUES (303,'Mu�nina,povra�anje,bol u trbuhu',TO_DATE('2019-05-09','YYYY-MM-DD'),154);
INSERT INTO dijag_anamneza VALUES(346,303,'K35.9');
INSERT INTO bolovanja VALUES(2064, TO_DATE('2019-05-09','YYYY-MM-DD'),TO_DATE('2019-05-18','YYYY-MM-DD'),'A0 bolest',346);
INSERT INTO pretraga VALUES(231,'Krv','DKS',TO_DATE('2019-05-10','YYYY-MM-DD'),346);


INSERT INTO pacijent VALUES(155,'David','�ajn','M',TO_DATE('1967-07-27','YYYY-MM-DD'), 'Ilirska 68,Osijek','0985822368',2);
INSERT INTO osnovno_zdravstveno VALUES(798105987,'nositelj','Hrvatska',155);
INSERT INTO osig_pac VALUES(155, TO_DATE('1992-07-20','YYYY-MM-DD'),TO_DATE('2022-07-13','YYYY-MM-DD'),798105987);
INSERT INTO dopunsko_osiguranje VALUES(768978616,'Generali');
INSERT INTO dop_pac VALUES(155, TO_DATE('2020-01-12','YYYY-MM-DD'), TO_DATE('2021-01-10','YYYY-MM-DD'),155, 768978616);

INSERT INTO anamneza VALUES (304,'Crveni,ljuskav osip na tjemenu',TO_DATE('2019-06-11','YYYY-MM-DD'),155);
INSERT INTO dijag_anamneza VALUES(347,304,'L21.0');
INSERT INTO anamneza VALUES (305,'Grlobolja,uve�ani krajnici',TO_DATE('2019-01-26','YYYY-MM-DD'),155);
INSERT INTO dijag_anamneza VALUES(348,305,'J02');
INSERT INTO terapija VALUES(170,'1,0,1',348,1003);
INSERT INTO bolovanja VALUES(2065, TO_DATE('2019-01-26','YYYY-MM-DD'),TO_DATE('2019-02-04','YYYY-MM-DD'),'A0 bolest',348);
INSERT INTO pretraga VALUES(232,'Krv','DKS',TO_DATE('2019-01-27','YYYY-MM-DD'),348);


INSERT INTO pacijent VALUES(156,'Grgur','Krajina','M',TO_DATE('2002-03-27','YYYY-MM-DD'), 'Zagreba�ka 67,Osijek','0996817529',2);
INSERT INTO osnovno_zdravstveno VALUES(947144916,'nositelj','Hrvatska',156);
INSERT INTO osig_pac VALUES(156, TO_DATE('2027-03-21','YYYY-MM-DD'),TO_DATE('2057-03-13','YYYY-MM-DD'),947144916);
INSERT INTO dopunsko_osiguranje VALUES(782503270,'Croatia');
INSERT INTO dop_pac VALUES(156, TO_DATE('2019-02-22','YYYY-MM-DD'), TO_DATE('2020-02-21','YYYY-MM-DD'),156, 782503270);

INSERT INTO anamneza VALUES (306,'Sumnja na rak bubrega',TO_DATE('2019-04-05','YYYY-MM-DD'),156);
INSERT INTO dijag_anamneza VALUES(349,306,'C64.0');
INSERT INTO anamneza VALUES (307,'Nemo�,gubitak teka,visoka temp.',TO_DATE('2020-04-09','YYYY-MM-DD'),156);
INSERT INTO dijag_anamneza VALUES(350,307,'J10.0');
INSERT INTO terapija VALUES(171,'0,0,1',350,1003);
INSERT INTO pretraga VALUES(233,'Krv','BIOKEMIJA',TO_DATE('2020-04-10','YYYY-MM-DD'),350);


INSERT INTO pacijent VALUES(157,'Jasenko','Alpeza','M',TO_DATE('1982-05-04','YYYY-MM-DD'), 'Europska avenija 140,Osijek','0985043700',1);
INSERT INTO osnovno_zdravstveno VALUES(775322438,'nositelj','Hrvatska',157);
INSERT INTO osig_pac VALUES(157, TO_DATE('2007-04-28','YYYY-MM-DD'),TO_DATE('2037-04-20','YYYY-MM-DD'),775322438);
INSERT INTO dopunsko_osiguranje VALUES(781959185,'Generali');
INSERT INTO dop_pac VALUES(157, TO_DATE('2020-05-26','YYYY-MM-DD'), TO_DATE('2021-05-25','YYYY-MM-DD'),157, 781959185);

INSERT INTO anamneza VALUES (308,'Crveilo ko�e,bol i uko�enost zglobova',TO_DATE('2019-04-16','YYYY-MM-DD'),157);
INSERT INTO dijag_anamneza VALUES(351,308,'M00.0');
INSERT INTO bolovanja VALUES(2066, TO_DATE('2019-04-16','YYYY-MM-DD'),TO_DATE('2019-04-18','YYYY-MM-DD'),'A0 bolest',351);
INSERT INTO pretraga VALUES(234,'Krv','DKS',TO_DATE('2019-04-17','YYYY-MM-DD'),351);


INSERT INTO pacijent VALUES(158,'Davor','Ivankovi�','M',TO_DATE('1971-04-02','YYYY-MM-DD'), 'Zagreba�ka 130,Osijek','0993803006',2);
INSERT INTO osnovno_zdravstveno VALUES(994085525,'nositelj','Hrvatska',158);
INSERT INTO osig_pac VALUES(158, TO_DATE('1996-03-26','YYYY-MM-DD'),TO_DATE('2026-03-19','YYYY-MM-DD'),994085525);
INSERT INTO dopunsko_osiguranje VALUES(700970502,'Croatia');
INSERT INTO dop_pac VALUES(158, TO_DATE('2020-01-26','YYYY-MM-DD'), TO_DATE('2021-01-24','YYYY-MM-DD'),158, 700970502);

INSERT INTO anamneza VALUES (309,'Sumnja na rak bubrega',TO_DATE('2019-10-01','YYYY-MM-DD'),158);
INSERT INTO dijag_anamneza VALUES(352,309,'C64.0');
INSERT INTO anamneza VALUES (310,'Povisen tlak',TO_DATE('2019-09-05','YYYY-MM-DD'),158);
INSERT INTO dijag_anamneza VALUES(353,310,'I10');
INSERT INTO terapija VALUES(172,'1,0,0',353,1026);


INSERT INTO pacijent VALUES(159,'Lucija','Kurtovi�','F',TO_DATE('2002-12-22','YYYY-MM-DD'), 'Wilsonova 6,Osijek','0984731815',3);
INSERT INTO osnovno_zdravstveno VALUES(856181818,'�lan obitelji','Hrvatska',159);
INSERT INTO osig_pac VALUES(159, TO_DATE('2002-12-22','YYYY-MM-DD'),TO_DATE('2032-12-14','YYYY-MM-DD'),856181818);
INSERT INTO dopunsko_osiguranje VALUES(7363039,'HZZO');
INSERT INTO dop_pac VALUES(159, TO_DATE('2019-07-28','YYYY-MM-DD'), TO_DATE('2020-07-26','YYYY-MM-DD'),159, 7363039);

INSERT INTO anamneza VALUES (311,'Sumnja na rak bubrega',TO_DATE('2020-03-27','YYYY-MM-DD'),159);
INSERT INTO dijag_anamneza VALUES(354,311,'C64.0');
INSERT INTO terapija VALUES(173,'3x dnevno',354,1021);
INSERT INTO uputnice VALUES(120,'A2','Onkologija','prvi pregled',354);
INSERT INTO anamneza VALUES (312,'POvi�ena temp. plikovi i osip na ko�i',TO_DATE('2019-09-06','YYYY-MM-DD'),159);
INSERT INTO dijag_anamneza VALUES(355,312,'B01');
INSERT INTO pretraga VALUES(235,'Krv','DKS',TO_DATE('2019-09-09','YYYY-MM-DD'),355);


INSERT INTO pacijent VALUES(160,'Helena','Ladi�','F',TO_DATE('2015-02-20','YYYY-MM-DD'), 'Vukovarska 147,Osijek','0912670945',3);
INSERT INTO osnovno_zdravstveno VALUES(499656393,'�lan obitelji','Hrvatska',160);
INSERT INTO osig_pac VALUES(160, TO_DATE('2015-02-20','YYYY-MM-DD'),TO_DATE('2045-02-12','YYYY-MM-DD'),499656393);
INSERT INTO dopunsko_osiguranje VALUES(335420580,'Croatia');
INSERT INTO dop_pac VALUES(160, TO_DATE('2020-04-26','YYYY-MM-DD'), TO_DATE('2021-04-25','YYYY-MM-DD'),160, 335420580);

INSERT INTO anamneza VALUES (313,'Mu�nina,povra�anje,bol u trbuhu',TO_DATE('2019-03-27','YYYY-MM-DD'),160);
INSERT INTO dijag_anamneza VALUES(356,313,'K35.9');
INSERT INTO pretraga VALUES(236,'Feces',null,TO_DATE('2019-03-28','YYYY-MM-DD'),356);
INSERT INTO anamneza VALUES (314,'Vezikule na ko�i sa gnojem',TO_DATE('2020-04-24','YYYY-MM-DD'),160);
INSERT INTO dijag_anamneza VALUES(357,314,'L08.0');


INSERT INTO pacijent VALUES(161,'Andrija','Katana','M',TO_DATE('1986-12-01','YYYY-MM-DD'), 'Drinska 37,Osijek','0996359051',2);
INSERT INTO osnovno_zdravstveno VALUES(534444511,'nositelj','Hrvatska',161);
INSERT INTO osig_pac VALUES(161, TO_DATE('2011-11-25','YYYY-MM-DD'),TO_DATE('2041-11-17','YYYY-MM-DD'),534444511);
INSERT INTO dopunsko_osiguranje VALUES(239842299,'Croatia');
INSERT INTO dop_pac VALUES(161, TO_DATE('2019-10-14','YYYY-MM-DD'), TO_DATE('2020-10-12','YYYY-MM-DD'),161, 239842299);

INSERT INTO anamneza VALUES (315,'Mu�nina,povra�anje,bol u trbuhu',TO_DATE('2020-01-09','YYYY-MM-DD'),161);
INSERT INTO dijag_anamneza VALUES(358,315,'K35.9');
INSERT INTO uputnice VALUES(121,'A2','Gastroenterologija','prvi pregled',358);
INSERT INTO pretraga VALUES(237,'Feces',null,TO_DATE('2020-01-11','YYYY-MM-DD'),358);
INSERT INTO anamneza VALUES (316,'Vezikule na ko�i sa gnojem',TO_DATE('2019-08-07','YYYY-MM-DD'),161);
INSERT INTO dijag_anamneza VALUES(359,316,'L08.0');


INSERT INTO pacijent VALUES(162,'Petar','Vrki�','M',TO_DATE('1958-10-02','YYYY-MM-DD'), 'Vukovarska 15,Osijek','0956342526',3);
INSERT INTO osnovno_zdravstveno VALUES(339224385,'nositelj','Hrvatska',162);
INSERT INTO osig_pac VALUES(162, TO_DATE('1983-09-26','YYYY-MM-DD'),TO_DATE('2013-09-18','YYYY-MM-DD'),339224385);
INSERT INTO dopunsko_osiguranje VALUES(207916735,'Croatia');
INSERT INTO dop_pac VALUES(162, TO_DATE('2019-05-01','YYYY-MM-DD'), TO_DATE('2020-04-29','YYYY-MM-DD'),162, 207916735);

INSERT INTO anamneza VALUES (317,'Mu�nina,povra�anje,bol u trbuhu',TO_DATE('2019-06-03','YYYY-MM-DD'),162);
INSERT INTO dijag_anamneza VALUES(360,317,'K35.9');
INSERT INTO pretraga VALUES(238,'Feces',null,TO_DATE('2019-06-04','YYYY-MM-DD'),360);
INSERT INTO anamneza VALUES (318,'Povisen tlak',TO_DATE('2020-01-08','YYYY-MM-DD'),162);
INSERT INTO dijag_anamneza VALUES(361,318,'I10');
INSERT INTO terapija VALUES(174,'1,0,0',361,1008);


INSERT INTO pacijent VALUES(163,'Vedran','Ba�i�','M',TO_DATE('1979-04-28','YYYY-MM-DD'), 'Srijemska 143,Osijek','0996402109',2);
INSERT INTO osnovno_zdravstveno VALUES(587212190,'nositelj','Hrvatska',163);
INSERT INTO osig_pac VALUES(163, TO_DATE('2004-04-21','YYYY-MM-DD'),TO_DATE('2034-04-14','YYYY-MM-DD'),587212190);
INSERT INTO dopunsko_osiguranje VALUES(717849052,'Croatia');
INSERT INTO dop_pac VALUES(163, TO_DATE('2020-03-29','YYYY-MM-DD'), TO_DATE('2021-03-28','YYYY-MM-DD'),163, 717849052);

INSERT INTO anamneza VALUES (319,'Povisen tlak',TO_DATE('2020-03-29','YYYY-MM-DD'),163);
INSERT INTO dijag_anamneza VALUES(362,319,'I10');
INSERT INTO terapija VALUES(175,'1,0,0',362,1018);


INSERT INTO pacijent VALUES(164,'Marko','Varga','M',TO_DATE('1994-10-05','YYYY-MM-DD'), '�upanijska 136,Osijek','0986949457',4);
INSERT INTO osnovno_zdravstveno VALUES(363852859,'nositelj','Hrvatska',164);
INSERT INTO osig_pac VALUES(164, TO_DATE('2019-09-29','YYYY-MM-DD'),TO_DATE('2049-09-21','YYYY-MM-DD'),363852859);
INSERT INTO dopunsko_osiguranje VALUES(2269640,'HZZO');
INSERT INTO dop_pac VALUES(164, TO_DATE('2019-11-06','YYYY-MM-DD'), TO_DATE('2020-11-04','YYYY-MM-DD'),164, 2269640);

INSERT INTO anamneza VALUES (320,'Sumnja na rak bubrega',TO_DATE('2019-01-29','YYYY-MM-DD'),164);
INSERT INTO dijag_anamneza VALUES(363,320,'C64.0');
INSERT INTO pretraga VALUES(239,'Krv','DKS',TO_DATE('2019-01-31','YYYY-MM-DD'),363);


INSERT INTO pacijent VALUES(165,'Anja','Vitasovi�','F',TO_DATE('1945-01-19','YYYY-MM-DD'), '�upanijska 123,Osijek','0983059083',4);
INSERT INTO osnovno_zdravstveno VALUES(928293001,'nositelj','Hrvatska',165);
INSERT INTO osig_pac VALUES(165, TO_DATE('2010-01-03','YYYY-MM-DD'),TO_DATE('2039-12-27','YYYY-MM-DD'),928293001);
INSERT INTO dopunsko_osiguranje VALUES(533899393,'Croatia');
INSERT INTO dop_pac VALUES(165, TO_DATE('2019-02-25','YYYY-MM-DD'), TO_DATE('2020-02-24','YYYY-MM-DD'),165, 533899393);

INSERT INTO anamneza VALUES (321,'Sumnja na tetanus',TO_DATE('2020-01-19','YYYY-MM-DD'),165);
INSERT INTO dijag_anamneza VALUES(364,321,'Z23.5');
INSERT INTO cijepljenje VALUES(55,1,364);


INSERT INTO pacijent VALUES(166,'Marko','Drventi�','M',TO_DATE('1979-07-30','YYYY-MM-DD'), 'Wilsonova 42,Osijek','0996118037',3);
INSERT INTO osnovno_zdravstveno VALUES(522426450,'nositelj','Hrvatska',166);
INSERT INTO osig_pac VALUES(166, TO_DATE('2004-07-23','YYYY-MM-DD'),TO_DATE('2034-07-16','YYYY-MM-DD'),522426450);
INSERT INTO dopunsko_osiguranje VALUES(583620014,'Croatia');
INSERT INTO dop_pac VALUES(166, TO_DATE('2019-08-18','YYYY-MM-DD'), TO_DATE('2020-08-16','YYYY-MM-DD'),166, 583620014);

INSERT INTO anamneza VALUES (322,'Nemogu�nost mokrenja',TO_DATE('2019-12-30','YYYY-MM-DD'),166);
INSERT INTO dijag_anamneza VALUES(365,322,'N21.0');
INSERT INTO bolovanja VALUES(2067, TO_DATE('2019-12-30','YYYY-MM-DD'),TO_DATE('2020-01-07','YYYY-MM-DD'),'A0 bolest',365);
INSERT INTO uputnice VALUES(122,'A2','Neurologija','prvi pregled',365);
INSERT INTO pretraga VALUES(240,'Krv','KKS',TO_DATE('2020-01-05','YYYY-MM-DD'),365);


INSERT INTO pacijent VALUES(167,'Enis','Brki�','M',TO_DATE('2007-05-27','YYYY-MM-DD'), 'Vratni�ka 143,Osijek','0918386041',4);
INSERT INTO osnovno_zdravstveno VALUES(874890853,'�lan obitelji','Hrvatska',167);
INSERT INTO osig_pac VALUES(167, TO_DATE('2007-05-27','YYYY-MM-DD'),TO_DATE('2037-05-19','YYYY-MM-DD'),874890853);
INSERT INTO dopunsko_osiguranje VALUES(906417657,'Croatia');
INSERT INTO dop_pac VALUES(167, TO_DATE('2019-07-15','YYYY-MM-DD'), TO_DATE('2020-07-13','YYYY-MM-DD'),167, 906417657);

INSERT INTO anamneza VALUES (323,'Nemogu�nost mokrenja',TO_DATE('2019-12-25','YYYY-MM-DD'),167);
INSERT INTO dijag_anamneza VALUES(366,323,'N21.0');
INSERT INTO pretraga VALUES(241,'Urin',null,TO_DATE('2019-12-26','YYYY-MM-DD'),366);
INSERT INTO terapija VALUES(176,'0,0,1',366,1007);
INSERT INTO anamneza VALUES (325,'Intezivan ka�alj, ote�ano disanje',TO_DATE('2019-02-10','YYYY-MM-DD'),167);
INSERT INTO dijag_anamneza VALUES(368,325,'J06.0');


INSERT INTO pacijent VALUES(168,'Nikola','Bo�i�','M',TO_DATE('1947-07-08','YYYY-MM-DD'), 'Vratni�ka 122,Osijek','0953449676',3);
INSERT INTO osnovno_zdravstveno VALUES(171915882,'nositelj','Hrvatska',168);
INSERT INTO osig_pac VALUES(168, TO_DATE('2012-06-21','YYYY-MM-DD'),TO_DATE('2042-06-14','YYYY-MM-DD'),171915882);
INSERT INTO dopunsko_osiguranje VALUES(2658031,'HZZO');
INSERT INTO dop_pac VALUES(168, TO_DATE('2019-03-09','YYYY-MM-DD'), TO_DATE('2020-03-07','YYYY-MM-DD'),168, 2658031);

INSERT INTO anamneza VALUES (326,'Povisen tlak',TO_DATE('2019-10-07','YYYY-MM-DD'),168);
INSERT INTO dijag_anamneza VALUES(369,326,'I10');
INSERT INTO terapija VALUES(177,'1,0,0',369,1008);


INSERT INTO pacijent VALUES(169,'Marko','Kunac','M',TO_DATE('1944-06-01','YYYY-MM-DD'), 'Srijemska 97,Osijek','0992378172',2);
INSERT INTO osnovno_zdravstveno VALUES(132005652,'nositelj','Hrvatska',169);
INSERT INTO osig_pac VALUES(169, TO_DATE('2009-05-16','YYYY-MM-DD'),TO_DATE('2039-05-09','YYYY-MM-DD'),132005652);
INSERT INTO dopunsko_osiguranje VALUES(4928204,'HZZO');
INSERT INTO dop_pac VALUES(169, TO_DATE('2019-10-20','YYYY-MM-DD'), TO_DATE('2020-10-18','YYYY-MM-DD'),169, 4928204);

INSERT INTO anamneza VALUES (327,'POvi�ena temp. plikovi i osip na ko�i',TO_DATE('2020-06-09','YYYY-MM-DD'),169);
INSERT INTO dijag_anamneza VALUES(370,327,'B01');
INSERT INTO uputnice VALUES(123,'A2','Infektologija','prvi pregled',370);
INSERT INTO pretraga VALUES(242,'Krv','KKS',TO_DATE('2020-06-11','YYYY-MM-DD'),370);


INSERT INTO pacijent VALUES(170,'Andrej','Stipeti�','M',TO_DATE('2003-07-16','YYYY-MM-DD'), 'Drinska 33,Osijek','0983860620',1);
INSERT INTO osnovno_zdravstveno VALUES(154034666,'�lan obitelji','Hrvatska',170);
INSERT INTO osig_pac VALUES(170, TO_DATE('2003-07-16','YYYY-MM-DD'),TO_DATE('2033-07-08','YYYY-MM-DD'),154034666);
INSERT INTO dopunsko_osiguranje VALUES(973889242,'Allianz');
INSERT INTO dop_pac VALUES(170, TO_DATE('2020-03-13','YYYY-MM-DD'), TO_DATE('2021-03-12','YYYY-MM-DD'),170, 973889242);

INSERT INTO anamneza VALUES (328,'Crveilo ko�e,bol i uko�enost zglobova',TO_DATE('2019-06-04','YYYY-MM-DD'),170);
INSERT INTO dijag_anamneza VALUES(371,328,'M00.0');
INSERT INTO anamneza VALUES (329,'Tvrda ko�a,gubitak dlake',TO_DATE('2020-05-14','YYYY-MM-DD'),170);
INSERT INTO dijag_anamneza VALUES(372,329,'M34.0');
INSERT INTO pretraga VALUES(243,'Krv','DKS',TO_DATE('2020-05-15','YYYY-MM-DD'),372);


INSERT INTO pacijent VALUES(171,'Stefan','�pani�','M',TO_DATE('1953-02-20','YYYY-MM-DD'), 'Vukovarska 104,Osijek','0912751894',3);
INSERT INTO osnovno_zdravstveno VALUES(344754043,'nositelj','Hrvatska',171);
INSERT INTO osig_pac VALUES(171, TO_DATE('2018-02-04','YYYY-MM-DD'),TO_DATE('2048-01-28','YYYY-MM-DD'),344754043);
INSERT INTO dopunsko_osiguranje VALUES(870384745,'Generali');
INSERT INTO dop_pac VALUES(171, TO_DATE('2019-10-19','YYYY-MM-DD'), TO_DATE('2020-10-17','YYYY-MM-DD'),171, 870384745);

INSERT INTO anamneza VALUES (330,'Sumnja na rak bubrega',TO_DATE('2019-01-02','YYYY-MM-DD'),171);
INSERT INTO dijag_anamneza VALUES(373,330,'C64.0');
INSERT INTO terapija VALUES(178,'2x dnevno',373,1021);
INSERT INTO anamneza VALUES (331,'Nemogu�nost mokrenja',TO_DATE('2020-06-24','YYYY-MM-DD'),171);
INSERT INTO dijag_anamneza VALUES(374,331,'N21.0');
INSERT INTO pretraga VALUES(244,'Krv','BIOKEMIJA',TO_DATE('2020-06-27','YYYY-MM-DD'),374);


INSERT INTO pacijent VALUES(172,'Marko','Filipovi�','M',TO_DATE('2011-10-28','YYYY-MM-DD'), 'Kozja�ka 4,Osijek','0956444406',2);
INSERT INTO osnovno_zdravstveno VALUES(378028292,'�lan obitelji','Hrvatska',172);
INSERT INTO osig_pac VALUES(172, TO_DATE('2011-10-28','YYYY-MM-DD'),TO_DATE('2041-10-20','YYYY-MM-DD'),378028292);
INSERT INTO dopunsko_osiguranje VALUES(371285362,'Croatia');
INSERT INTO dop_pac VALUES(172, TO_DATE('2019-05-04','YYYY-MM-DD'), TO_DATE('2020-05-02','YYYY-MM-DD'),172, 371285362);

INSERT INTO anamneza VALUES (332,'Rane u ustima,proljev,vru�ica',TO_DATE('2020-02-15','YYYY-MM-DD'),172);
INSERT INTO dijag_anamneza VALUES(375,332,'K50.0');
INSERT INTO pretraga VALUES(245,'Feces',null,TO_DATE('2020-02-16','YYYY-MM-DD'),375);
INSERT INTO anamneza VALUES (333,'Sumnja na rak bubrega',TO_DATE('2019-10-27','YYYY-MM-DD'),172);
INSERT INTO dijag_anamneza VALUES(376,333,'C64.0');
INSERT INTO terapija VALUES(179,'3x dnevno',376,1021);
INSERT INTO pretraga VALUES(246,'Krv','BIOKEMIJA',TO_DATE('2019-10-28','YYYY-MM-DD'),376);


INSERT INTO pacijent VALUES(173,'Patricia','�ori�','F',TO_DATE('1990-06-18','YYYY-MM-DD'), 'Vratni�ka 1,Osijek','0984929302',2);
INSERT INTO osnovno_zdravstveno VALUES(388309331,'nositelj','Hrvatska',173);
INSERT INTO osig_pac VALUES(173, TO_DATE('2015-06-12','YYYY-MM-DD'),TO_DATE('2045-06-04','YYYY-MM-DD'),388309331);
INSERT INTO dopunsko_osiguranje VALUES(879496199,'Allianz');
INSERT INTO dop_pac VALUES(173, TO_DATE('2019-12-07','YYYY-MM-DD'), TO_DATE('2020-12-05','YYYY-MM-DD'),173, 879496199);

INSERT INTO anamneza VALUES (334,'Nemo�,gubitak teka,visoka temp.',TO_DATE('2019-01-31','YYYY-MM-DD'),173);
INSERT INTO dijag_anamneza VALUES(377,334,'J10.0');
INSERT INTO terapija VALUES(180,'3x dnevno',377,1006);
INSERT INTO uputnice VALUES(124,'A2','Pulmologija','prvi pregled',377);
INSERT INTO pretraga VALUES(247,'Krv','DKS',TO_DATE('2019-02-04','YYYY-MM-DD'),377);


INSERT INTO pacijent VALUES(174,'Lorena','Spreitzer','F',TO_DATE('1994-10-12','YYYY-MM-DD'), 'Vukovarska 39,Osijek','0918310804',3);
INSERT INTO osnovno_zdravstveno VALUES(353781819,'nositelj','Hrvatska',174);
INSERT INTO osig_pac VALUES(174, TO_DATE('2019-10-06','YYYY-MM-DD'),TO_DATE('2049-09-28','YYYY-MM-DD'),353781819);
INSERT INTO dopunsko_osiguranje VALUES(746104667,'Croatia');
INSERT INTO dop_pac VALUES(174, TO_DATE('2019-09-26','YYYY-MM-DD'), TO_DATE('2020-09-24','YYYY-MM-DD'),174, 746104667);

INSERT INTO anamneza VALUES (335,'Bolno mokrenje,povi�ena temp.',TO_DATE('2019-06-01','YYYY-MM-DD'),174);
INSERT INTO dijag_anamneza VALUES(378,335,'N15.1');
INSERT INTO pretraga VALUES(248,'Krv','BIOKEMIJA',TO_DATE('2019-06-03','YYYY-MM-DD'),378);


INSERT INTO pacijent VALUES(175,'Matija','Paris','F',TO_DATE('1987-11-22','YYYY-MM-DD'), 'Vukovarska 67,Osijek','0912343503',3);
INSERT INTO osnovno_zdravstveno VALUES(543997851,'nositelj','Hrvatska',175);
INSERT INTO osig_pac VALUES(175, TO_DATE('2012-11-15','YYYY-MM-DD'),TO_DATE('2042-11-08','YYYY-MM-DD'),543997851);
INSERT INTO dopunsko_osiguranje VALUES(664228804,'Allianz');
INSERT INTO dop_pac VALUES(175, TO_DATE('2020-03-09','YYYY-MM-DD'), TO_DATE('2021-03-08','YYYY-MM-DD'),175, 664228804);

INSERT INTO anamneza VALUES (336,'Grlobolja,uve�ani krajnici',TO_DATE('2019-07-25','YYYY-MM-DD'),175);
INSERT INTO dijag_anamneza VALUES(379,336,'J02');
INSERT INTO uputnice VALUES(125,'A2','Otorinolaringologija','prvi pregled',379);
INSERT INTO anamneza VALUES (337,'Osjetljivost i oteknu�e sinusa',TO_DATE('2019-12-27','YYYY-MM-DD'),175);
INSERT INTO dijag_anamneza VALUES(380,337,'J01.2');
INSERT INTO terapija VALUES(181,'3x dnevno',380,1006);
INSERT INTO bolovanja VALUES(2068, TO_DATE('2019-12-27','YYYY-MM-DD'),TO_DATE('2020-01-04','YYYY-MM-DD'),'A0 bolest',380);
INSERT INTO pretraga VALUES(249,'Krv','KKS',TO_DATE('2019-12-29','YYYY-MM-DD'),380);


INSERT INTO pacijent VALUES(176,'Agneza','Rukavina','F',TO_DATE('1963-10-30','YYYY-MM-DD'), 'Wilsonova 1,Osijek','0914014824',2);
INSERT INTO osnovno_zdravstveno VALUES(727380383,'nositelj','Hrvatska',176);
INSERT INTO osig_pac VALUES(176, TO_DATE('1988-10-23','YYYY-MM-DD'),TO_DATE('2018-10-16','YYYY-MM-DD'),727380383);
INSERT INTO dopunsko_osiguranje VALUES(450938003,'Generali');
INSERT INTO dop_pac VALUES(176, TO_DATE('2019-04-27','YYYY-MM-DD'), TO_DATE('2020-04-25','YYYY-MM-DD'),176, 450938003);

INSERT INTO anamneza VALUES (338,'Tvrda ko�a,gubitak dlake',TO_DATE('2019-04-08','YYYY-MM-DD'),176);
INSERT INTO dijag_anamneza VALUES(381,338,'M34.0');
INSERT INTO bolovanja VALUES(2069, TO_DATE('2019-04-08','YYYY-MM-DD'),TO_DATE('2019-04-17','YYYY-MM-DD'),'A0 bolest',381);
INSERT INTO pretraga VALUES(250,'Krv','DKS',TO_DATE('2019-04-09','YYYY-MM-DD'),381);


INSERT INTO pacijent VALUES(177,'Marija','Kati�','F',TO_DATE('1954-01-13','YYYY-MM-DD'), 'Vratni�ka 30,Osijek','0994561565',1);
INSERT INTO osnovno_zdravstveno VALUES(297999777,'nositelj','Hrvatska',177);
INSERT INTO osig_pac VALUES(177, TO_DATE('2018-12-28','YYYY-MM-DD'),TO_DATE('2048-12-20','YYYY-MM-DD'),297999777);
INSERT INTO dopunsko_osiguranje VALUES(782659842,'Allianz');
INSERT INTO dop_pac VALUES(177, TO_DATE('2019-08-08','YYYY-MM-DD'), TO_DATE('2020-08-06','YYYY-MM-DD'),177, 782659842);

INSERT INTO anamneza VALUES (339,'Tvrda ko�a,gubitak dlake',TO_DATE('2020-03-16','YYYY-MM-DD'),177);
INSERT INTO dijag_anamneza VALUES(382,339,'M34.0');
INSERT INTO anamneza VALUES (340,'Povisen tlak',TO_DATE('2019-09-06','YYYY-MM-DD'),177);
INSERT INTO dijag_anamneza VALUES(383,340,'I10');
INSERT INTO uputnice VALUES(126,'A2','Nefrologija','prvi pregled',383);
INSERT INTO uputnice VALUES(127,'A2','Kardiologija','prvi pregled',383);























INSERT INTO pacijent VALUES(178,'Petar','Vrki�','M',TO_DATE('1986-01-20','YYYY-MM-DD'), 'Kozja�ka 112,Osijek','0918970379',3);
INSERT INTO osnovno_zdravstveno VALUES(953488431,'nositelj','Hrvatska',178);
INSERT INTO osig_pac VALUES(178, TO_DATE('2011-01-14','YYYY-MM-DD'),TO_DATE('2041-01-06','YYYY-MM-DD'),953488431);
INSERT INTO dopunsko_osiguranje VALUES(420658189,'Croatia');
INSERT INTO dop_pac VALUES(178, TO_DATE('2020-03-18','YYYY-MM-DD'), TO_DATE('2021-03-17','YYYY-MM-DD'),178, 420658189);

INSERT INTO anamneza VALUES (341,'Nemogu�nost mokrenja',TO_DATE('2019-02-09','YYYY-MM-DD'),178);
INSERT INTO dijag_anamneza VALUES(384,341,'N21.0');
INSERT INTO uputnice VALUES(128,'A2','Neurologija','prvi pregled',384);
INSERT INTO pretraga VALUES(251,'Urin',null,TO_DATE('2019-02-11','YYYY-MM-DD'),384);
INSERT INTO terapija VALUES(182,'3x dnevno',384,1007);
INSERT INTO anamneza VALUES (343,'Mu�nina,povra�anje,bol u trbuhu',TO_DATE('2020-03-05','YYYY-MM-DD'),178);
INSERT INTO dijag_anamneza VALUES(386,343,'K35.9');
INSERT INTO bolovanja VALUES(2070, TO_DATE('2020-03-05','YYYY-MM-DD'),TO_DATE('2020-03-08','YYYY-MM-DD'),'A0 bolest',386);
INSERT INTO pretraga VALUES(252,'Krv','BIOKEMIJA',TO_DATE('2020-03-06','YYYY-MM-DD'),386);


INSERT INTO pacijent VALUES(179,'Ivana','Bogdanovi�','F',TO_DATE('1960-06-07','YYYY-MM-DD'), 'Europska avenija 62,Osijek','0954779214',1);
INSERT INTO osnovno_zdravstveno VALUES(846064192,'nositelj','Hrvatska',179);
INSERT INTO osig_pac VALUES(179, TO_DATE('1985-06-01','YYYY-MM-DD'),TO_DATE('2015-05-25','YYYY-MM-DD'),846064192);
INSERT INTO dopunsko_osiguranje VALUES(2522936,'HZZO');
INSERT INTO dop_pac VALUES(179, TO_DATE('2019-09-26','YYYY-MM-DD'), TO_DATE('2020-09-24','YYYY-MM-DD'),179, 2522936);

INSERT INTO anamneza VALUES (344,'Bolno mokrenje,povi�ena temp.',TO_DATE('2019-11-16','YYYY-MM-DD'),179);
INSERT INTO dijag_anamneza VALUES(387,344,'N15.1');
INSERT INTO bolovanja VALUES(2071, TO_DATE('2019-11-16','YYYY-MM-DD'),TO_DATE('2019-11-25','YYYY-MM-DD'),'A0 bolest',387);
INSERT INTO pretraga VALUES(253,'Krv','KKS',TO_DATE('2019-11-17','YYYY-MM-DD'),387);


INSERT INTO pacijent VALUES(180,'Lana','Gavrilovi�','F',TO_DATE('1981-02-12','YYYY-MM-DD'), 'Zagreba�ka 10,Osijek','0951531031',2);
INSERT INTO osnovno_zdravstveno VALUES(233386704,'nositelj','Hrvatska',180);
INSERT INTO osig_pac VALUES(180, TO_DATE('2006-02-06','YYYY-MM-DD'),TO_DATE('2036-01-30','YYYY-MM-DD'),233386704);
INSERT INTO dopunsko_osiguranje VALUES(724273710,'Allianz');
INSERT INTO dop_pac VALUES(180, TO_DATE('2019-05-10','YYYY-MM-DD'), TO_DATE('2020-05-08','YYYY-MM-DD'),180, 724273710);

INSERT INTO anamneza VALUES (345,'Sumnja na tetanus',TO_DATE('2020-04-13','YYYY-MM-DD'),180);
INSERT INTO dijag_anamneza VALUES(388,345,'Z23.5');
INSERT INTO cijepljenje VALUES(56,1,388);


INSERT INTO pacijent VALUES(181,'Ena','�ulin','F',TO_DATE('1975-12-10','YYYY-MM-DD'), 'Ilirska 60,Osijek','0912671869',4);
INSERT INTO osnovno_zdravstveno VALUES(235809921,'nositelj','Hrvatska',181);
INSERT INTO osig_pac VALUES(181, TO_DATE('2000-12-03','YYYY-MM-DD'),TO_DATE('2030-11-26','YYYY-MM-DD'),235809921);
INSERT INTO dopunsko_osiguranje VALUES(632041934,'Croatia');
INSERT INTO dop_pac VALUES(181, TO_DATE('2019-03-20','YYYY-MM-DD'), TO_DATE('2020-03-18','YYYY-MM-DD'),181, 632041934);

INSERT INTO anamneza VALUES (346,'Grlobolja,uve�ani krajnici',TO_DATE('2019-06-04','YYYY-MM-DD'),181);
INSERT INTO dijag_anamneza VALUES(389,346,'J02');
INSERT INTO bolovanja VALUES(2072, TO_DATE('2019-06-04','YYYY-MM-DD'),TO_DATE('2019-06-05','YYYY-MM-DD'),'A0 bolest',389);
INSERT INTO uputnice VALUES(129,'A2','Otorinolaringologija','prvi pregled',389);
INSERT INTO pretraga VALUES(254,'Krv','DKS',TO_DATE('2019-06-07','YYYY-MM-DD'),389);


INSERT INTO pacijent VALUES(182,'Matea','Vila','F',TO_DATE('1993-01-21','YYYY-MM-DD'), 'Kozja�ka 53,Osijek','0987303962',2);
INSERT INTO osnovno_zdravstveno VALUES(388205142,'nositelj','Hrvatska',182);
INSERT INTO osig_pac VALUES(182, TO_DATE('2018-01-15','YYYY-MM-DD'),TO_DATE('2048-01-08','YYYY-MM-DD'),388205142);
INSERT INTO dopunsko_osiguranje VALUES(843777173,'Croatia');
INSERT INTO dop_pac VALUES(182, TO_DATE('2019-05-19','YYYY-MM-DD'), TO_DATE('2020-05-17','YYYY-MM-DD'),182, 843777173);

INSERT INTO anamneza VALUES (347,'Sumnja na rak bubrega',TO_DATE('2019-12-06','YYYY-MM-DD'),182);
INSERT INTO dijag_anamneza VALUES(390,347,'C64.0');
INSERT INTO terapija VALUES(183,'0,0,1',390,1021);
INSERT INTO pretraga VALUES(255,'Krv','DKS',TO_DATE('2019-12-09','YYYY-MM-DD'),390);


INSERT INTO pacijent VALUES(183,'Iva','Kljaju�','F',TO_DATE('1986-03-22','YYYY-MM-DD'), 'Wilsonova 59,Osijek','0998286335',3);
INSERT INTO osnovno_zdravstveno VALUES(973794476,'nositelj','Hrvatska',183);
INSERT INTO osig_pac VALUES(183, TO_DATE('2011-03-16','YYYY-MM-DD'),TO_DATE('2041-03-08','YYYY-MM-DD'),973794476);
INSERT INTO dopunsko_osiguranje VALUES(496025988,'Generali');
INSERT INTO dop_pac VALUES(183, TO_DATE('2019-10-11','YYYY-MM-DD'), TO_DATE('2020-10-09','YYYY-MM-DD'),183, 496025988);

INSERT INTO anamneza VALUES (348,'Rane u ustima,proljev,vru�ica',TO_DATE('2019-01-11','YYYY-MM-DD'),183);
INSERT INTO dijag_anamneza VALUES(391,348,'K50.0');
INSERT INTO pretraga VALUES(256,'Feces',null,TO_DATE('2019-01-12','YYYY-MM-DD'),391);
INSERT INTO anamneza VALUES (349,'Nemogu�nost mokrenja',TO_DATE('2019-06-09','YYYY-MM-DD'),183);
INSERT INTO dijag_anamneza VALUES(392,349,'N21.0');
INSERT INTO uputnice VALUES(130,'A2','Neurologija','prvi pregled',392);
INSERT INTO pretraga VALUES(257,'Urin',null,TO_DATE('2019-06-10','YYYY-MM-DD'),392);
INSERT INTO terapija VALUES(184,'2x dnevno',392,1020);


INSERT INTO pacijent VALUES(184,'Ema','Bise','F',TO_DATE('1994-09-27','YYYY-MM-DD'), 'Mlinska 92,Osijek','0953308931',4);
INSERT INTO osnovno_zdravstveno VALUES(656384083,'nositelj','Hrvatska',184);
INSERT INTO osig_pac VALUES(184, TO_DATE('2019-09-21','YYYY-MM-DD'),TO_DATE('2049-09-13','YYYY-MM-DD'),656384083);
INSERT INTO dopunsko_osiguranje VALUES(606906919,'Generali');
INSERT INTO dop_pac VALUES(184, TO_DATE('2019-11-27','YYYY-MM-DD'), TO_DATE('2020-11-25','YYYY-MM-DD'),184, 606906919);

INSERT INTO anamneza VALUES (351,'Bolno mokrenje',TO_DATE('2019-06-21','YYYY-MM-DD'),184);
INSERT INTO dijag_anamneza VALUES(394,351,'N30');
INSERT INTO uputnice VALUES(131,'A2','Neurologija','prvi pregled',394);
INSERT INTO pretraga VALUES(258,'Urin',null,TO_DATE('2019-06-22','YYYY-MM-DD'),394);
INSERT INTO terapija VALUES(185,'3x dnevno',394,1007);
INSERT INTO anamneza VALUES (353,'Rane u ustima,proljev,vru�ica',TO_DATE('2020-01-20','YYYY-MM-DD'),184);
INSERT INTO dijag_anamneza VALUES(396,353,'K50.0');
INSERT INTO pretraga VALUES(259,'Krv','BIOKEMIJA',TO_DATE('2020-01-22','YYYY-MM-DD'),396);


INSERT INTO pacijent VALUES(185,'Luka','Vili�','M',TO_DATE('1983-01-02','YYYY-MM-DD'), 'Drinska 141,Osijek','0914663679',3);
INSERT INTO osnovno_zdravstveno VALUES(689799673,'nositelj','Hrvatska',185);
INSERT INTO osig_pac VALUES(185, TO_DATE('2007-12-27','YYYY-MM-DD'),TO_DATE('2037-12-19','YYYY-MM-DD'),689799673);
INSERT INTO dopunsko_osiguranje VALUES(824974554,'Croatia');
INSERT INTO dop_pac VALUES(185, TO_DATE('2019-05-21','YYYY-MM-DD'), TO_DATE('2020-05-19','YYYY-MM-DD'),185, 824974554);

INSERT INTO anamneza VALUES (354,'Povisen tlak',TO_DATE('2019-06-03','YYYY-MM-DD'),185);
INSERT INTO dijag_anamneza VALUES(397,354,'I10');
INSERT INTO terapija VALUES(186,'1,0,0',397,1026);


INSERT INTO pacijent VALUES(186,'Marko','Jagustin','M',TO_DATE('2016-12-04','YYYY-MM-DD'), 'Kozja�ka 55,Osijek','0983153491',2);
INSERT INTO osnovno_zdravstveno VALUES(824658077,'�lan obitelji','Hrvatska',186);
INSERT INTO osig_pac VALUES(186, TO_DATE('2016-12-04','YYYY-MM-DD'),TO_DATE('2046-11-27','YYYY-MM-DD'),824658077);
INSERT INTO dopunsko_osiguranje VALUES(905509784,'Croatia');
INSERT INTO dop_pac VALUES(186, TO_DATE('2019-05-31','YYYY-MM-DD'), TO_DATE('2020-05-29','YYYY-MM-DD'),186, 905509784);

INSERT INTO anamneza VALUES (355,'POvi�ena temp. plikovi i osip na ko�i',TO_DATE('2020-04-13','YYYY-MM-DD'),186);
INSERT INTO dijag_anamneza VALUES(398,355,'B01');
INSERT INTO uputnice VALUES(132,'A2','Infektologija','prvi pregled',398);
INSERT INTO pretraga VALUES(260,'Krv','KKS',TO_DATE('2020-04-14','YYYY-MM-DD'),398);


INSERT INTO pacijent VALUES(187,'Adrian','Bu�ek','M',TO_DATE('1981-08-11','YYYY-MM-DD'), 'Europska avenija 144,Osijek','0914892056',3);
INSERT INTO osnovno_zdravstveno VALUES(532550931,'nositelj','Hrvatska',187);
INSERT INTO osig_pac VALUES(187, TO_DATE('2006-08-05','YYYY-MM-DD'),TO_DATE('2036-07-28','YYYY-MM-DD'),532550931);
INSERT INTO dopunsko_osiguranje VALUES(243012840,'Allianz');
INSERT INTO dop_pac VALUES(187, TO_DATE('2019-12-13','YYYY-MM-DD'), TO_DATE('2020-12-11','YYYY-MM-DD'),187, 243012840);

INSERT INTO anamneza VALUES (356,'Mu�nina,povra�anje,bol u trbuhu',TO_DATE('2020-05-16','YYYY-MM-DD'),187);
INSERT INTO dijag_anamneza VALUES(399,356,'K35.9');
INSERT INTO pretraga VALUES(261,'Feces',null,TO_DATE('2020-05-17','YYYY-MM-DD'),399);
INSERT INTO anamneza VALUES (357,'Nemo�,gubitak teka,visoka temp.',TO_DATE('2019-09-05','YYYY-MM-DD'),187);
INSERT INTO dijag_anamneza VALUES(400,357,'J10.0');
INSERT INTO terapija VALUES(187,'2x dnevno',400,1003);


INSERT INTO pacijent VALUES(188,'Maroje','Ragu�','M',TO_DATE('2003-09-20','YYYY-MM-DD'), 'Drinska 113,Osijek','0992361456',3);
INSERT INTO osnovno_zdravstveno VALUES(438205307,'�lan obitelji','Hrvatska',188);
INSERT INTO osig_pac VALUES(188, TO_DATE('2003-09-20','YYYY-MM-DD'),TO_DATE('2033-09-12','YYYY-MM-DD'),438205307);
INSERT INTO dopunsko_osiguranje VALUES(541275101,'Generali');
INSERT INTO dop_pac VALUES(188, TO_DATE('2020-01-29','YYYY-MM-DD'), TO_DATE('2021-01-27','YYYY-MM-DD'),188, 541275101);

INSERT INTO anamneza VALUES (358,'Vezikule na ko�i sa gnojem',TO_DATE('2019-08-03','YYYY-MM-DD'),188);
INSERT INTO dijag_anamneza VALUES(401,358,'L08.0');
INSERT INTO pretraga VALUES(262,'Krv','DKS',TO_DATE('2019-08-04','YYYY-MM-DD'),401);


INSERT INTO pacijent VALUES(189,'Hrvoje','Brod','M',TO_DATE('1988-10-08','YYYY-MM-DD'), 'Wilsonova 44,Osijek','0956042407',4);
INSERT INTO osnovno_zdravstveno VALUES(852792152,'nositelj','Hrvatska',189);
INSERT INTO osig_pac VALUES(189, TO_DATE('2013-10-02','YYYY-MM-DD'),TO_DATE('2043-09-25','YYYY-MM-DD'),852792152);
INSERT INTO dopunsko_osiguranje VALUES(630950740,'Croatia');
INSERT INTO dop_pac VALUES(189, TO_DATE('2019-12-12','YYYY-MM-DD'), TO_DATE('2020-12-10','YYYY-MM-DD'),189, 630950740);

INSERT INTO anamneza VALUES (359,'Povi�ena temp.,hipoglikemija',TO_DATE('2019-10-20','YYYY-MM-DD'),189);
INSERT INTO dijag_anamneza VALUES(402,359,'C22.0');
INSERT INTO anamneza VALUES (360,'Rane u ustima,proljev,vru�ica',TO_DATE('2020-02-15','YYYY-MM-DD'),189);
INSERT INTO dijag_anamneza VALUES(403,360,'K50.0');
INSERT INTO bolovanja VALUES(2073, TO_DATE('2020-02-15','YYYY-MM-DD'),TO_DATE('2020-02-20','YYYY-MM-DD'),'A0 bolest',403);
INSERT INTO pretraga VALUES(263,'Krv','BIOKEMIJA',TO_DATE('2020-02-16','YYYY-MM-DD'),403);


INSERT INTO pacijent VALUES(190,'�eljko','Sablji�','M',TO_DATE('1976-04-05','YYYY-MM-DD'), '�upanijska 44,Osijek','0958015982',2);
INSERT INTO osnovno_zdravstveno VALUES(789812223,'nositelj','Hrvatska',190);
INSERT INTO osig_pac VALUES(190, TO_DATE('2001-03-30','YYYY-MM-DD'),TO_DATE('2031-03-23','YYYY-MM-DD'),789812223);
INSERT INTO dopunsko_osiguranje VALUES(886271353,'Generali');
INSERT INTO dop_pac VALUES(190, TO_DATE('2020-04-22','YYYY-MM-DD'), TO_DATE('2021-04-21','YYYY-MM-DD'),190, 886271353);

INSERT INTO anamneza VALUES (361,'Crveni,ljuskav osip na tjemenu',TO_DATE('2020-03-23','YYYY-MM-DD'),190);
INSERT INTO dijag_anamneza VALUES(404,361,'L21.0');
INSERT INTO anamneza VALUES (362,'Nemo�,gubitak teka,visoka temp.',TO_DATE('2020-03-02','YYYY-MM-DD'),190);
INSERT INTO dijag_anamneza VALUES(405,362,'J10.0');
INSERT INTO bolovanja VALUES(2074, TO_DATE('2020-03-02','YYYY-MM-DD'),TO_DATE('2020-03-10','YYYY-MM-DD'),'A0 bolest',405);
INSERT INTO uputnice VALUES(133,'A2','Pulmologija','prvi pregled',405);
INSERT INTO pretraga VALUES(264,'Krv','BIOKEMIJA',TO_DATE('2020-03-04','YYYY-MM-DD'),405);


INSERT INTO pacijent VALUES(191,'Bruno','Borozni','M',TO_DATE('1944-11-04','YYYY-MM-DD'), 'Drinska 132,Osijek','0915123366',2);
INSERT INTO osnovno_zdravstveno VALUES(713045685,'nositelj','Hrvatska',191);
INSERT INTO osig_pac VALUES(191, TO_DATE('2009-10-19','YYYY-MM-DD'),TO_DATE('2039-10-12','YYYY-MM-DD'),713045685);
INSERT INTO dopunsko_osiguranje VALUES(453870640,'Generali');
INSERT INTO dop_pac VALUES(191, TO_DATE('2020-04-27','YYYY-MM-DD'), TO_DATE('2021-04-26','YYYY-MM-DD'),191, 453870640);

INSERT INTO anamneza VALUES (363,'Tvrda ko�a,gubitak dlake',TO_DATE('2020-04-26','YYYY-MM-DD'),191);
INSERT INTO dijag_anamneza VALUES(406,363,'M34.0');
INSERT INTO anamneza VALUES (364,'Nemo�,gubitak teka,visoka temp.',TO_DATE('2019-08-26','YYYY-MM-DD'),191);
INSERT INTO dijag_anamneza VALUES(407,364,'J10.0');
INSERT INTO terapija VALUES(188,'1,0,1',407,1006);
INSERT INTO pretraga VALUES(265,'Krv','BIOKEMIJA',TO_DATE('2019-08-27','YYYY-MM-DD'),407);


INSERT INTO pacijent VALUES(192,'Paula','Herek','F',TO_DATE('1952-08-16','YYYY-MM-DD'), 'Vukovarska 46,Osijek','0989860462',4);
INSERT INTO osnovno_zdravstveno VALUES(956670801,'nositelj','Hrvatska',192);
INSERT INTO osig_pac VALUES(192, TO_DATE('2017-07-31','YYYY-MM-DD'),TO_DATE('2047-07-24','YYYY-MM-DD'),956670801);
INSERT INTO dopunsko_osiguranje VALUES(362219976,'Croatia');
INSERT INTO dop_pac VALUES(192, TO_DATE('2019-11-09','YYYY-MM-DD'), TO_DATE('2020-11-07','YYYY-MM-DD'),192, 362219976);

INSERT INTO anamneza VALUES (365,'Intezivan ka�alj, ote�ano disanje',TO_DATE('2019-02-12','YYYY-MM-DD'),192);
INSERT INTO dijag_anamneza VALUES(408,365,'J06.0');
INSERT INTO anamneza VALUES (366,'Nemo�,gubitak teka,visoka temp.',TO_DATE('2020-06-18','YYYY-MM-DD'),192);
INSERT INTO dijag_anamneza VALUES(409,366,'J10.0');
INSERT INTO terapija VALUES(189,'3x dnevno',409,1006);


INSERT INTO pacijent VALUES(193,'Borna','Duvnjak','M',TO_DATE('1972-07-05','YYYY-MM-DD'), '�upanijska 18,Osijek','0996260518',3);
INSERT INTO osnovno_zdravstveno VALUES(662265522,'nositelj','Hrvatska',193);
INSERT INTO osig_pac VALUES(193, TO_DATE('1997-06-29','YYYY-MM-DD'),TO_DATE('2027-06-22','YYYY-MM-DD'),662265522);
INSERT INTO dopunsko_osiguranje VALUES(509721270,'Allianz');
INSERT INTO dop_pac VALUES(193, TO_DATE('2019-09-26','YYYY-MM-DD'), TO_DATE('2020-09-24','YYYY-MM-DD'),193, 509721270);

INSERT INTO anamneza VALUES (367,'Osjetljivost i oteknu�e sinusa',TO_DATE('2020-03-22','YYYY-MM-DD'),193);
INSERT INTO dijag_anamneza VALUES(410,367,'J01.2');
INSERT INTO anamneza VALUES (368,'POvi�ena temp. plikovi i osip na ko�i',TO_DATE('2020-06-09','YYYY-MM-DD'),193);
INSERT INTO dijag_anamneza VALUES(411,368,'B01');
INSERT INTO terapija VALUES(190,'3x dnevno',411,1006);


INSERT INTO pacijent VALUES(194,'Nevena','Jovi�i�','F',TO_DATE('1991-10-22','YYYY-MM-DD'), 'Kozja�ka 61,Osijek','0959961176',2);
INSERT INTO osnovno_zdravstveno VALUES(429112098,'nositelj','Hrvatska',194);
INSERT INTO osig_pac VALUES(194, TO_DATE('2016-10-15','YYYY-MM-DD'),TO_DATE('2046-10-08','YYYY-MM-DD'),429112098);
INSERT INTO dopunsko_osiguranje VALUES(5265434,'HZZO');
INSERT INTO dop_pac VALUES(194, TO_DATE('2019-11-30','YYYY-MM-DD'), TO_DATE('2020-11-28','YYYY-MM-DD'),194, 5265434);

INSERT INTO anamneza VALUES (369,'Grlobolja,uve�ani krajnici',TO_DATE('2019-06-12','YYYY-MM-DD'),194);
INSERT INTO dijag_anamneza VALUES(412,369,'J02');
INSERT INTO uputnice VALUES(134,'A2','Otorinolaringologija','prvi pregled',412);
INSERT INTO pretraga VALUES(266,'Krv','DKS',TO_DATE('2019-06-13','YYYY-MM-DD'),412);


INSERT INTO pacijent VALUES(195,'Bruno','�osi�','M',TO_DATE('1958-04-14','YYYY-MM-DD'), 'Srijemska 72,Osijek','0956363141',2);
INSERT INTO osnovno_zdravstveno VALUES(360625054,'nositelj','Hrvatska',195);
INSERT INTO osig_pac VALUES(195, TO_DATE('1983-04-08','YYYY-MM-DD'),TO_DATE('2013-03-31','YYYY-MM-DD'),360625054);
INSERT INTO dopunsko_osiguranje VALUES(1444037,'HZZO');
INSERT INTO dop_pac VALUES(195, TO_DATE('2020-04-02','YYYY-MM-DD'), TO_DATE('2021-04-01','YYYY-MM-DD'),195, 1444037);

INSERT INTO anamneza VALUES (370,'Sumnja na tetanus',TO_DATE('2019-01-18','YYYY-MM-DD'),195);
INSERT INTO dijag_anamneza VALUES(413,370,'Z23.5');
INSERT INTO cijepljenje VALUES(57,1,413);


INSERT INTO pacijent VALUES(196,'Josip','Aladrovi�','M',TO_DATE('2017-12-11','YYYY-MM-DD'), 'Srijemska 82,Osijek','0985722665',1);
INSERT INTO osnovno_zdravstveno VALUES(313549295,'�lan obitelji','Hrvatska',196);
INSERT INTO osig_pac VALUES(196, TO_DATE('2017-12-11','YYYY-MM-DD'),TO_DATE('2047-12-04','YYYY-MM-DD'),313549295);
INSERT INTO dopunsko_osiguranje VALUES(787663227,'Allianz');
INSERT INTO dop_pac VALUES(196, TO_DATE('2020-05-01','YYYY-MM-DD'), TO_DATE('2021-04-30','YYYY-MM-DD'),196, 787663227);

INSERT INTO anamneza VALUES (371,'kontrola i cijepljenje',TO_DATE('2018-04-10','YYYY-MM-DD'),196);
INSERT INTO dijag_anamneza VALUES(414,371,'Z27.2');
INSERT INTO dijag_anamneza VALUES(415,371,'Z24.0');
INSERT INTO dijag_anamneza VALUES(416,371,'Z24.6');
INSERT INTO cijepljenje VALUES(58,2,414);
INSERT INTO cijepljenje VALUES(59,3,415);
INSERT INTO cijepljenje VALUES(60,4,416);


INSERT INTO pacijent VALUES(197,'Davor','Milakovi�','M',TO_DATE('1997-01-18','YYYY-MM-DD'), 'Vukovarska 56,Osijek','0988201751',4);
INSERT INTO osnovno_zdravstveno VALUES(234872332,'nositelj','Hrvatska',197);
INSERT INTO osig_pac VALUES(197, TO_DATE('2022-01-12','YYYY-MM-DD'),TO_DATE('2052-01-05','YYYY-MM-DD'),234872332);
INSERT INTO dopunsko_osiguranje VALUES(822005144,'Generali');
INSERT INTO dop_pac VALUES(197, TO_DATE('2019-03-16','YYYY-MM-DD'), TO_DATE('2020-03-14','YYYY-MM-DD'),197, 822005144);

INSERT INTO anamneza VALUES (372,'Mu�nina,povra�anje,bol u trbuhu',TO_DATE('2019-08-07','YYYY-MM-DD'),197);
INSERT INTO dijag_anamneza VALUES(417,372,'K35.9');
INSERT INTO uputnice VALUES(135,'A2','Gastroenterologija','prvi pregled',417);
INSERT INTO pretraga VALUES(267,'Krv','KKS',TO_DATE('2019-08-09','YYYY-MM-DD'),417);


INSERT INTO pacijent VALUES(198,'Kre�imir','Kraljevi�','M',TO_DATE('1975-11-18','YYYY-MM-DD'), 'Srijemska 41,Osijek','0987633541',4);
INSERT INTO osnovno_zdravstveno VALUES(437606357,'nositelj','Hrvatska',198);
INSERT INTO osig_pac VALUES(198, TO_DATE('2000-11-11','YYYY-MM-DD'),TO_DATE('2030-11-04','YYYY-MM-DD'),437606357);
INSERT INTO dopunsko_osiguranje VALUES(915455389,'Croatia');
INSERT INTO dop_pac VALUES(198, TO_DATE('2019-05-14','YYYY-MM-DD'), TO_DATE('2020-05-12','YYYY-MM-DD'),198, 915455389);

INSERT INTO anamneza VALUES (373,'Temp. normalna,crvenilo o�nih spojnica i ka�alj',TO_DATE('2019-04-04','YYYY-MM-DD'),198);
INSERT INTO dijag_anamneza VALUES(418,373,'Z23.7');
INSERT INTO cijepljenje VALUES(61,9,418);
INSERT INTO dijag_anamneza VALUES(419,373,'A37.0');


INSERT INTO pacijent VALUES(199,'Branimir','Popovi�','M',TO_DATE('1961-05-20','YYYY-MM-DD'), 'Srijemska 40,Osijek','0986433217',2);
INSERT INTO osnovno_zdravstveno VALUES(356735119,'nositelj','Hrvatska',199);
INSERT INTO osig_pac VALUES(199, TO_DATE('1986-05-14','YYYY-MM-DD'),TO_DATE('2016-05-06','YYYY-MM-DD'),356735119);
INSERT INTO dopunsko_osiguranje VALUES(7689830,'HZZO');
INSERT INTO dop_pac VALUES(199, TO_DATE('2020-05-03','YYYY-MM-DD'), TO_DATE('2021-05-02','YYYY-MM-DD'),199, 7689830);

INSERT INTO anamneza VALUES (374,'Povisen tlak',TO_DATE('2019-11-01','YYYY-MM-DD'),199);
INSERT INTO dijag_anamneza VALUES(420,374,'I10');
INSERT INTO terapija VALUES(191,'1,0,0',420,1026);


INSERT INTO pacijent VALUES(200,'Ivana','Gnjidi�','F',TO_DATE('1995-02-21','YYYY-MM-DD'), 'Europska avenija 43,Osijek','0958205032',4);
INSERT INTO osnovno_zdravstveno VALUES(686811694,'nositelj','Hrvatska',200);
INSERT INTO osig_pac VALUES(200, TO_DATE('2020-02-15','YYYY-MM-DD'),TO_DATE('2050-02-07','YYYY-MM-DD'),686811694);
INSERT INTO dopunsko_osiguranje VALUES(230995787,'Allianz');
INSERT INTO dop_pac VALUES(200, TO_DATE('2019-03-28','YYYY-MM-DD'), TO_DATE('2020-03-26','YYYY-MM-DD'),200, 230995787);

INSERT INTO anamneza VALUES (375,'Mu�nina,povra�anje,bol u trbuhu',TO_DATE('2019-10-24','YYYY-MM-DD'),200);
INSERT INTO dijag_anamneza VALUES(421,375,'K35.9');
INSERT INTO pretraga VALUES(268,'Krv','KKS',TO_DATE('2019-10-26','YYYY-MM-DD'),421);


INSERT INTO pacijent VALUES(201,'Dino','Magu�i�','M',TO_DATE('1993-11-06','YYYY-MM-DD'), 'Kozja�ka 2,Osijek','0992079597',1);
INSERT INTO osnovno_zdravstveno VALUES(884566368,'nositelj','Hrvatska',201);
INSERT INTO osig_pac VALUES(201, TO_DATE('2018-10-31','YYYY-MM-DD'),TO_DATE('2048-10-23','YYYY-MM-DD'),884566368);
INSERT INTO dopunsko_osiguranje VALUES(144481402,'Allianz');
INSERT INTO dop_pac VALUES(201, TO_DATE('2019-09-20','YYYY-MM-DD'), TO_DATE('2020-09-18','YYYY-MM-DD'),201, 144481402);

INSERT INTO anamneza VALUES (376,'Temp. normalna,crvenilo o�nih spojnica i ka�alj',TO_DATE('2019-06-06','YYYY-MM-DD'),201);
INSERT INTO dijag_anamneza VALUES(422,376,'Z23.7');
INSERT INTO cijepljenje VALUES(62,9,422);
INSERT INTO dijag_anamneza VALUES(423,376,'A37.0');


INSERT INTO pacijent VALUES(202,'Lea','Kne�evi�','F',TO_DATE('1989-02-28','YYYY-MM-DD'), 'Srijemska 104,Osijek','0957545490',1);
INSERT INTO osnovno_zdravstveno VALUES(688754823,'nositelj','Hrvatska',202);
INSERT INTO osig_pac VALUES(202, TO_DATE('2014-02-22','YYYY-MM-DD'),TO_DATE('2044-02-15','YYYY-MM-DD'),688754823);
INSERT INTO dopunsko_osiguranje VALUES(3398051,'HZZO');
INSERT INTO dop_pac VALUES(202, TO_DATE('2019-10-11','YYYY-MM-DD'), TO_DATE('2020-10-09','YYYY-MM-DD'),202, 3398051);

INSERT INTO anamneza VALUES (377,'Osjetljivost i oteknu�e sinusa',TO_DATE('2020-02-21','YYYY-MM-DD'),202);
INSERT INTO dijag_anamneza VALUES(424,377,'J01.2');
INSERT INTO terapija VALUES(192,'1x dnevno',424,1003);
INSERT INTO bolovanja VALUES(2075, TO_DATE('2020-02-21','YYYY-MM-DD'),TO_DATE('2020-02-29','YYYY-MM-DD'),'A0 bolest',424);
INSERT INTO pretraga VALUES(269,'Krv','BIOKEMIJA',TO_DATE('2020-02-22','YYYY-MM-DD'),424);


INSERT INTO pacijent VALUES(203,'Nora','Grevinger','F',TO_DATE('1969-02-10','YYYY-MM-DD'), 'Vratni�ka 102,Osijek','0994542735',4);
INSERT INTO osnovno_zdravstveno VALUES(995659561,'nositelj','Hrvatska',203);
INSERT INTO osig_pac VALUES(203, TO_DATE('1994-02-04','YYYY-MM-DD'),TO_DATE('2024-01-28','YYYY-MM-DD'),995659561);
INSERT INTO dopunsko_osiguranje VALUES(678912231,'Allianz');
INSERT INTO dop_pac VALUES(203, TO_DATE('2019-12-27','YYYY-MM-DD'), TO_DATE('2020-12-25','YYYY-MM-DD'),203, 678912231);

INSERT INTO anamneza VALUES (378,'Grlobolja,uve�ani krajnici',TO_DATE('2019-04-12','YYYY-MM-DD'),203);
INSERT INTO dijag_anamneza VALUES(425,378,'J02');
INSERT INTO terapija VALUES(193,'1x dnevno',425,1003);
INSERT INTO anamneza VALUES (379,'Osjetljivost i oteknu�e sinusa',TO_DATE('2019-11-01','YYYY-MM-DD'),203);
INSERT INTO dijag_anamneza VALUES(426,379,'J01.2');


INSERT INTO pacijent VALUES(204,'Mario','Peri�','M',TO_DATE('1989-03-14','YYYY-MM-DD'), 'Srijemska 8,Osijek','0985718299',4);
INSERT INTO osnovno_zdravstveno VALUES(987628755,'nositelj','Hrvatska',204);
INSERT INTO osig_pac VALUES(204, TO_DATE('2014-03-08','YYYY-MM-DD'),TO_DATE('2044-02-29','YYYY-MM-DD'),987628755);
INSERT INTO dopunsko_osiguranje VALUES(2225136,'HZZO');
INSERT INTO dop_pac VALUES(204, TO_DATE('2019-08-18','YYYY-MM-DD'), TO_DATE('2020-08-16','YYYY-MM-DD'),204, 2225136);

INSERT INTO anamneza VALUES (380,'Grlobolja,uve�ani krajnici',TO_DATE('2020-06-26','YYYY-MM-DD'),204);
INSERT INTO dijag_anamneza VALUES(427,380,'J02');
INSERT INTO anamneza VALUES (381,'Vezikule na ko�i sa gnojem',TO_DATE('2019-10-19','YYYY-MM-DD'),204);
INSERT INTO dijag_anamneza VALUES(428,381,'L08.0');


INSERT INTO pacijent VALUES(205,'Sanja','Opa�ak','F',TO_DATE('1942-02-25','YYYY-MM-DD'), 'Zagreba�ka 120,Osijek','0953351674',1);
INSERT INTO osnovno_zdravstveno VALUES(430443208,'nositelj','Hrvatska',205);
INSERT INTO osig_pac VALUES(205, TO_DATE('2007-02-09','YYYY-MM-DD'),TO_DATE('2037-02-01','YYYY-MM-DD'),430443208);
INSERT INTO dopunsko_osiguranje VALUES(475306480,'Generali');
INSERT INTO dop_pac VALUES(205, TO_DATE('2019-10-26','YYYY-MM-DD'), TO_DATE('2020-10-24','YYYY-MM-DD'),205, 475306480);

INSERT INTO anamneza VALUES (382,'Bolno mokrenje,povi�ena temp.',TO_DATE('2019-12-06','YYYY-MM-DD'),205);
INSERT INTO dijag_anamneza VALUES(429,382,'N15.1');
INSERT INTO anamneza VALUES (383,'Sumnja na rak bubrega',TO_DATE('2019-01-24','YYYY-MM-DD'),205);
INSERT INTO dijag_anamneza VALUES(430,383,'C64.0');
INSERT INTO terapija VALUES(194,'0,0,1',430,1021);
INSERT INTO pretraga VALUES(270,'Krv','KKS',TO_DATE('2019-01-28','YYYY-MM-DD'),430);


INSERT INTO pacijent VALUES(206,'Irena','Fi�er','F',TO_DATE('1946-04-25','YYYY-MM-DD'), 'Mlinska 48,Osijek','0918329028',2);
INSERT INTO osnovno_zdravstveno VALUES(950609338,'nositelj','Hrvatska',206);
INSERT INTO osig_pac VALUES(206, TO_DATE('2011-04-09','YYYY-MM-DD'),TO_DATE('2041-04-01','YYYY-MM-DD'),950609338);
INSERT INTO dopunsko_osiguranje VALUES(3462905,'HZZO');
INSERT INTO dop_pac VALUES(206, TO_DATE('2020-05-21','YYYY-MM-DD'), TO_DATE('2021-05-20','YYYY-MM-DD'),206, 3462905);

INSERT INTO anamneza VALUES (384,'Mu�nina,povra�anje,bol u trbuhu',TO_DATE('2019-08-31','YYYY-MM-DD'),206);
INSERT INTO dijag_anamneza VALUES(431,384,'K35.9');
INSERT INTO pretraga VALUES(271,'Krv','DKS',TO_DATE('2019-09-03','YYYY-MM-DD'),431);


INSERT INTO pacijent VALUES(207,'Juraj','�unec','M',TO_DATE('1951-08-29','YYYY-MM-DD'), 'Ilirska 4,Osijek','0916498574',4);
INSERT INTO osnovno_zdravstveno VALUES(929160402,'nositelj','Hrvatska',207);
INSERT INTO osig_pac VALUES(207, TO_DATE('2016-08-12','YYYY-MM-DD'),TO_DATE('2046-08-05','YYYY-MM-DD'),929160402);
INSERT INTO dopunsko_osiguranje VALUES(476958274,'Croatia');
INSERT INTO dop_pac VALUES(207, TO_DATE('2019-04-18','YYYY-MM-DD'), TO_DATE('2020-04-16','YYYY-MM-DD'),207, 476958274);

INSERT INTO anamneza VALUES (385,'Bolno mokrenje',TO_DATE('2019-09-02','YYYY-MM-DD'),207);
INSERT INTO dijag_anamneza VALUES(432,385,'N30');
INSERT INTO uputnice VALUES(136,'A2','Neurologija','prvi pregled',432);
INSERT INTO pretraga VALUES(272,'Urin',null,TO_DATE('2019-09-03','YYYY-MM-DD'),432);
INSERT INTO anamneza VALUES (387,'Povisen tlak',TO_DATE('2020-03-28','YYYY-MM-DD'),207);
INSERT INTO dijag_anamneza VALUES(434,387,'I10');
INSERT INTO terapija VALUES(195,'1,0,0',434,1018);


INSERT INTO pacijent VALUES(208,'Ru�a','Ba�i�','F',TO_DATE('1963-06-22','YYYY-MM-DD'), 'Zagreba�ka 128,Osijek','0956025177',3);
INSERT INTO osnovno_zdravstveno VALUES(989100840,'nositelj','Hrvatska',208);
INSERT INTO osig_pac VALUES(208, TO_DATE('1988-06-15','YYYY-MM-DD'),TO_DATE('2018-06-08','YYYY-MM-DD'),989100840);
INSERT INTO dopunsko_osiguranje VALUES(4775458,'HZZO');
INSERT INTO dop_pac VALUES(208, TO_DATE('2019-08-01','YYYY-MM-DD'), TO_DATE('2020-07-30','YYYY-MM-DD'),208, 4775458);

INSERT INTO anamneza VALUES (388,'Sumnja na rak bubrega',TO_DATE('2019-11-13','YYYY-MM-DD'),208);
INSERT INTO dijag_anamneza VALUES(435,388,'C64.0');
INSERT INTO anamneza VALUES (389,'Intezivan ka�alj, ote�ano disanje',TO_DATE('2019-05-07','YYYY-MM-DD'),208);
INSERT INTO dijag_anamneza VALUES(436,389,'J06.0');
INSERT INTO terapija VALUES(196,'3x dnevno',436,1014);
INSERT INTO bolovanja VALUES(2076, TO_DATE('2019-05-07','YYYY-MM-DD'),TO_DATE('2019-05-11','YYYY-MM-DD'),'A0 bolest',436);
INSERT INTO uputnice VALUES(137,'A2','Otorinolaringologija','prvi pregled',436);
INSERT INTO pretraga VALUES(273,'Krv','KKS',TO_DATE('2019-05-08','YYYY-MM-DD'),436);


INSERT INTO pacijent VALUES(209,'Petra','Kocei�','F',TO_DATE('1997-09-24','YYYY-MM-DD'), 'Wilsonova 101,Osijek','0915615039',4);
INSERT INTO osnovno_zdravstveno VALUES(862046151,'nositelj','Hrvatska',209);
INSERT INTO osig_pac VALUES(209, TO_DATE('2022-09-18','YYYY-MM-DD'),TO_DATE('2052-09-10','YYYY-MM-DD'),862046151);
INSERT INTO dopunsko_osiguranje VALUES(194502487,'Generali');
INSERT INTO dop_pac VALUES(209, TO_DATE('2019-07-05','YYYY-MM-DD'), TO_DATE('2020-07-03','YYYY-MM-DD'),209, 194502487);

INSERT INTO anamneza VALUES (390,'Tvrda ko�a,gubitak dlake',TO_DATE('2020-05-15','YYYY-MM-DD'),209);
INSERT INTO dijag_anamneza VALUES(437,390,'M34.0');
INSERT INTO anamneza VALUES (391,'Sumnja na rak bubrega',TO_DATE('2019-05-22','YYYY-MM-DD'),209);
INSERT INTO dijag_anamneza VALUES(438,391,'C64.0');
INSERT INTO pretraga VALUES(274,'Krv','DKS',TO_DATE('2019-05-23','YYYY-MM-DD'),438);


INSERT INTO pacijent VALUES(210,'Valentina','Vitasovi�','F',TO_DATE('1950-11-16','YYYY-MM-DD'), 'Mlinska 75,Osijek','0989390669',2);
INSERT INTO osnovno_zdravstveno VALUES(798175015,'nositelj','Hrvatska',210);
INSERT INTO osig_pac VALUES(210, TO_DATE('2015-10-31','YYYY-MM-DD'),TO_DATE('2045-10-23','YYYY-MM-DD'),798175015);
INSERT INTO dopunsko_osiguranje VALUES(5770411,'HZZO');
INSERT INTO dop_pac VALUES(210, TO_DATE('2020-03-18','YYYY-MM-DD'), TO_DATE('2021-03-17','YYYY-MM-DD'),210, 5770411);

INSERT INTO anamneza VALUES (392,'Sumnja na tetanus',TO_DATE('2019-08-18','YYYY-MM-DD'),210);
INSERT INTO dijag_anamneza VALUES(439,392,'Z23.5');
INSERT INTO cijepljenje VALUES(63,1,439);


INSERT INTO pacijent VALUES(211,'Zara','Bakovi�','F',TO_DATE('2016-01-01','YYYY-MM-DD'), 'Srijemska 54,Osijek','0954782526',3);
INSERT INTO osnovno_zdravstveno VALUES(505793799,'�lan obitelji','Hrvatska',211);
INSERT INTO osig_pac VALUES(211, TO_DATE('2016-01-01','YYYY-MM-DD'),TO_DATE('2045-12-24','YYYY-MM-DD'),505793799);
INSERT INTO dopunsko_osiguranje VALUES(6486525,'HZZO');
INSERT INTO dop_pac VALUES(211, TO_DATE('2019-02-22','YYYY-MM-DD'), TO_DATE('2020-02-21','YYYY-MM-DD'),211, 6486525);

INSERT INTO anamneza VALUES (393,'Tvrda ko�a,gubitak dlake',TO_DATE('2020-05-19','YYYY-MM-DD'),211);
INSERT INTO dijag_anamneza VALUES(440,393,'M34.0');
INSERT INTO anamneza VALUES (394,'Povi�ena temp.,hipoglikemija',TO_DATE('2019-01-07','YYYY-MM-DD'),211);
INSERT INTO dijag_anamneza VALUES(441,394,'C22.0');


INSERT INTO pacijent VALUES(212,'Fran','Tisaj','M',TO_DATE('1997-01-01','YYYY-MM-DD'), '�upanijska 7,Osijek','0953377109',2);
INSERT INTO osnovno_zdravstveno VALUES(778722431,'nositelj','Hrvatska',212);
INSERT INTO osig_pac VALUES(212, TO_DATE('2021-12-26','YYYY-MM-DD'),TO_DATE('2051-12-19','YYYY-MM-DD'),778722431);
INSERT INTO dopunsko_osiguranje VALUES(631701443,'Generali');
INSERT INTO dop_pac VALUES(212, TO_DATE('2020-04-15','YYYY-MM-DD'), TO_DATE('2021-04-14','YYYY-MM-DD'),212, 631701443);

INSERT INTO anamneza VALUES (395,'Sumnja na rak bubrega',TO_DATE('2019-08-11','YYYY-MM-DD'),212);
INSERT INTO dijag_anamneza VALUES(442,395,'C64.0');
INSERT INTO terapija VALUES(197,'1,0,1',442,1021);
INSERT INTO pretraga VALUES(275,'Krv','DKS',TO_DATE('2019-08-13','YYYY-MM-DD'),442);


INSERT INTO pacijent VALUES(213,'Filip','Filo�evi�','M',TO_DATE('2006-09-14','YYYY-MM-DD'), 'Europska avenija 62,Osijek','0991047923',2);
INSERT INTO osnovno_zdravstveno VALUES(865588357,'�lan obitelji','Hrvatska',213);
INSERT INTO osig_pac VALUES(213, TO_DATE('2006-09-14','YYYY-MM-DD'),TO_DATE('2036-09-06','YYYY-MM-DD'),865588357);
INSERT INTO dopunsko_osiguranje VALUES(1080966,'HZZO');
INSERT INTO dop_pac VALUES(213, TO_DATE('2019-08-30','YYYY-MM-DD'), TO_DATE('2020-08-28','YYYY-MM-DD'),213, 1080966);

INSERT INTO anamneza VALUES (396,'Vezikule na ko�i sa gnojem',TO_DATE('2019-04-30','YYYY-MM-DD'),213);
INSERT INTO dijag_anamneza VALUES(443,396,'L08.0');
INSERT INTO anamneza VALUES (397,'Crveni,ljuskav osip na tjemenu',TO_DATE('2019-09-16','YYYY-MM-DD'),213);
INSERT INTO dijag_anamneza VALUES(444,397,'L21.0');


INSERT INTO pacijent VALUES(214,'Luka','Tisaj','M',TO_DATE('1986-03-07','YYYY-MM-DD'), 'Ilirska 105,Osijek','0983407509',3);
INSERT INTO osnovno_zdravstveno VALUES(169798285,'nositelj','Hrvatska',214);
INSERT INTO osig_pac VALUES(214, TO_DATE('2011-03-01','YYYY-MM-DD'),TO_DATE('2041-02-21','YYYY-MM-DD'),169798285);
INSERT INTO dopunsko_osiguranje VALUES(520013285,'Croatia');
INSERT INTO dop_pac VALUES(214, TO_DATE('2019-04-21','YYYY-MM-DD'), TO_DATE('2020-04-19','YYYY-MM-DD'),214, 520013285);

INSERT INTO anamneza VALUES (398,'POvi�ena temp. plikovi i osip na ko�i',TO_DATE('2020-06-23','YYYY-MM-DD'),214);
INSERT INTO dijag_anamneza VALUES(445,398,'B01');
INSERT INTO terapija VALUES(198,'1,0,1',445,1003);
INSERT INTO bolovanja VALUES(2077, TO_DATE('2020-06-23','YYYY-MM-DD'),TO_DATE('2020-06-27','YYYY-MM-DD'),'A0 bolest',445);
INSERT INTO pretraga VALUES(276,'Krv','KKS',TO_DATE('2020-06-25','YYYY-MM-DD'),445);


INSERT INTO pacijent VALUES(215,'Leonarda','Pavlin','F',TO_DATE('2000-06-05','YYYY-MM-DD'), '�upanijska 22,Osijek','0957417319',3);
INSERT INTO osnovno_zdravstveno VALUES(821232870,'nositelj','Hrvatska',215);
INSERT INTO osig_pac VALUES(215, TO_DATE('2025-05-30','YYYY-MM-DD'),TO_DATE('2055-05-23','YYYY-MM-DD'),821232870);
INSERT INTO dopunsko_osiguranje VALUES(856681334,'Generali');
INSERT INTO dop_pac VALUES(215, TO_DATE('2019-07-02','YYYY-MM-DD'), TO_DATE('2020-06-30','YYYY-MM-DD'),215, 856681334);

INSERT INTO anamneza VALUES (399,'Rane u ustima,proljev,vru�ica',TO_DATE('2020-03-19','YYYY-MM-DD'),215);
INSERT INTO dijag_anamneza VALUES(446,399,'K50.0');
INSERT INTO uputnice VALUES(138,'A2','Gastroenterologija','prvi pregled',446);
INSERT INTO pretraga VALUES(277,'Krv','KKS',TO_DATE('2020-03-22','YYYY-MM-DD'),446);


INSERT INTO pacijent VALUES(216,'Paola','Horvat','F',TO_DATE('1957-11-23','YYYY-MM-DD'), 'Vukovarska 96,Osijek','0952271453',3);
INSERT INTO osnovno_zdravstveno VALUES(523527906,'nositelj','Hrvatska',216);
INSERT INTO osig_pac VALUES(216, TO_DATE('1982-11-17','YYYY-MM-DD'),TO_DATE('2012-11-09','YYYY-MM-DD'),523527906);
INSERT INTO dopunsko_osiguranje VALUES(673236365,'Generali');
INSERT INTO dop_pac VALUES(216, TO_DATE('2020-06-16','YYYY-MM-DD'), TO_DATE('2021-06-15','YYYY-MM-DD'),216, 673236365);

INSERT INTO anamneza VALUES (400,'POvi�ena temp. plikovi i osip na ko�i',TO_DATE('2019-12-06','YYYY-MM-DD'),216);
INSERT INTO dijag_anamneza VALUES(447,400,'B01');
INSERT INTO terapija VALUES(199,'2x dnevno',447,1003);
INSERT INTO bolovanja VALUES(2078, TO_DATE('2019-12-06','YYYY-MM-DD'),TO_DATE('2019-12-13','YYYY-MM-DD'),'A0 bolest',447);
INSERT INTO pretraga VALUES(278,'Krv','BIOKEMIJA',TO_DATE('2019-12-07','YYYY-MM-DD'),447);


INSERT INTO pacijent VALUES(217,'Til','Ocvirk','M',TO_DATE('1974-02-27','YYYY-MM-DD'), 'Kozja�ka 81,Osijek','0985639922',2);
INSERT INTO osnovno_zdravstveno VALUES(694920542,'nositelj','Hrvatska',217);
INSERT INTO osig_pac VALUES(217, TO_DATE('1999-02-21','YYYY-MM-DD'),TO_DATE('2029-02-13','YYYY-MM-DD'),694920542);
INSERT INTO dopunsko_osiguranje VALUES(2604265,'HZZO');
INSERT INTO dop_pac VALUES(217, TO_DATE('2019-04-03','YYYY-MM-DD'), TO_DATE('2020-04-01','YYYY-MM-DD'),217, 2604265);

INSERT INTO anamneza VALUES (401,'Sumnja na rak bubrega',TO_DATE('2019-05-04','YYYY-MM-DD'),217);
INSERT INTO dijag_anamneza VALUES(448,401,'C64.0');
INSERT INTO terapija VALUES(200,'0,0,1',448,1021);
INSERT INTO anamneza VALUES (402,'Povisen tlak',TO_DATE('2019-02-24','YYYY-MM-DD'),217);
INSERT INTO dijag_anamneza VALUES(449,402,'I10');
INSERT INTO terapija VALUES(201,'1,0,0',449,1008);


INSERT INTO pacijent VALUES(218,'Andrej','Vi�tica','M',TO_DATE('2017-01-04','YYYY-MM-DD'), 'Vratni�ka 130,Osijek','0911296423',4);
INSERT INTO osnovno_zdravstveno VALUES(538865760,'�lan obitelji','Hrvatska',218);
INSERT INTO osig_pac VALUES(218, TO_DATE('2017-01-04','YYYY-MM-DD'),TO_DATE('2046-12-28','YYYY-MM-DD'),538865760);
INSERT INTO dopunsko_osiguranje VALUES(5499166,'HZZO');
INSERT INTO dop_pac VALUES(218, TO_DATE('2019-10-04','YYYY-MM-DD'), TO_DATE('2020-10-02','YYYY-MM-DD'),218, 5499166);

INSERT INTO anamneza VALUES (403,'Nemo�,gubitak teka,visoka temp.',TO_DATE('2020-04-03','YYYY-MM-DD'),218);
INSERT INTO dijag_anamneza VALUES(450,403,'J10.0');
INSERT INTO anamneza VALUES (404,'Temp. normalna,crvenilo o�nih spojnica i ka�alj',TO_DATE('2019-07-14','YYYY-MM-DD'),218);
INSERT INTO dijag_anamneza VALUES(451,404,'Z23.7');
INSERT INTO cijepljenje VALUES(64,9,451);
INSERT INTO dijag_anamneza VALUES(452,404,'A37.0');


INSERT INTO pacijent VALUES(219,'Borna','Kati�','M',TO_DATE('1996-10-04','YYYY-MM-DD'), 'Wilsonova 20,Osijek','0987811371',2);
INSERT INTO osnovno_zdravstveno VALUES(550485231,'nositelj','Hrvatska',219);
INSERT INTO osig_pac VALUES(219, TO_DATE('2021-09-28','YYYY-MM-DD'),TO_DATE('2051-09-21','YYYY-MM-DD'),550485231);
INSERT INTO dopunsko_osiguranje VALUES(239498984,'Allianz');
INSERT INTO dop_pac VALUES(219, TO_DATE('2019-09-18','YYYY-MM-DD'), TO_DATE('2020-09-16','YYYY-MM-DD'),219, 239498984);

INSERT INTO anamneza VALUES (405,'Nemogu�nost mokrenja',TO_DATE('2019-12-19','YYYY-MM-DD'),219);
INSERT INTO dijag_anamneza VALUES(453,405,'N21.0');
INSERT INTO uputnice VALUES(139,'A2','Neurologija','prvi pregled',453);
INSERT INTO pretraga VALUES(279,'Krv','DKS',TO_DATE('2019-12-20','YYYY-MM-DD'),453);


INSERT INTO pacijent VALUES(220,'�eljka','�aban','F',TO_DATE('1985-08-19','YYYY-MM-DD'), 'Ilirska 37,Osijek','0998420707',3);
INSERT INTO osnovno_zdravstveno VALUES(260431987,'nositelj','Hrvatska',220);
INSERT INTO osig_pac VALUES(220, TO_DATE('2010-08-13','YYYY-MM-DD'),TO_DATE('2040-08-05','YYYY-MM-DD'),260431987);
INSERT INTO dopunsko_osiguranje VALUES(447804555,'Croatia');
INSERT INTO dop_pac VALUES(220, TO_DATE('2020-04-23','YYYY-MM-DD'), TO_DATE('2021-04-22','YYYY-MM-DD'),220, 447804555);

INSERT INTO anamneza VALUES (406,'Sumnja na rak bubrega',TO_DATE('2020-01-30','YYYY-MM-DD'),220);
INSERT INTO dijag_anamneza VALUES(454,406,'C64.0');
INSERT INTO terapija VALUES(202,'3x dnevno',454,1021);
INSERT INTO anamneza VALUES (407,'Intezivan ka�alj, ote�ano disanje',TO_DATE('2019-06-21','YYYY-MM-DD'),220);
INSERT INTO dijag_anamneza VALUES(455,407,'J06.0');


INSERT INTO pacijent VALUES(221,'Sonja','�kevin','F',TO_DATE('2011-09-27','YYYY-MM-DD'), 'Vukovarska 99,Osijek','0992207798',2);
INSERT INTO osnovno_zdravstveno VALUES(552352344,'�lan obitelji','Hrvatska',221);
INSERT INTO osig_pac VALUES(221, TO_DATE('2011-09-27','YYYY-MM-DD'),TO_DATE('2041-09-19','YYYY-MM-DD'),552352344);
INSERT INTO dopunsko_osiguranje VALUES(852234819,'Generali');
INSERT INTO dop_pac VALUES(221, TO_DATE('2019-07-26','YYYY-MM-DD'), TO_DATE('2020-07-24','YYYY-MM-DD'),221, 852234819);

INSERT INTO anamneza VALUES (408,'Rane u ustima,proljev,vru�ica',TO_DATE('2019-09-17','YYYY-MM-DD'),221);
INSERT INTO dijag_anamneza VALUES(456,408,'K50.0');
INSERT INTO uputnice VALUES(140,'A2','Gastroenterologija','prvi pregled',456);
INSERT INTO pretraga VALUES(280,'Feces',null,TO_DATE('2019-09-18','YYYY-MM-DD'),456);
INSERT INTO anamneza VALUES (409,'Intezivan ka�alj, ote�ano disanje',TO_DATE('2019-05-24','YYYY-MM-DD'),221);
INSERT INTO dijag_anamneza VALUES(457,409,'J06.0');


INSERT INTO pacijent VALUES(222,'Branko','Toki�','M',TO_DATE('1947-04-27','YYYY-MM-DD'), 'Europska avenija 52,Osijek','0998343138',3);
INSERT INTO osnovno_zdravstveno VALUES(637712927,'nositelj','Hrvatska',222);
INSERT INTO osig_pac VALUES(222, TO_DATE('2012-04-10','YYYY-MM-DD'),TO_DATE('2042-04-03','YYYY-MM-DD'),637712927);
INSERT INTO dopunsko_osiguranje VALUES(602602679,'Allianz');
INSERT INTO dop_pac VALUES(222, TO_DATE('2020-06-27','YYYY-MM-DD'), TO_DATE('2021-06-26','YYYY-MM-DD'),222, 602602679);

INSERT INTO anamneza VALUES (410,'Crveni,ljuskav osip na tjemenu',TO_DATE('2019-12-06','YYYY-MM-DD'),222);
INSERT INTO dijag_anamneza VALUES(458,410,'L21.0');
INSERT INTO uputnice VALUES(141,'A2','Dermatologija','prvi pregled',458);
INSERT INTO pretraga VALUES(281,'Krv','KKS',TO_DATE('2019-12-07','YYYY-MM-DD'),458);


INSERT INTO pacijent VALUES(223,'Ivana','Srn','F',TO_DATE('1999-11-02','YYYY-MM-DD'), 'Vratni�ka 6,Osijek','0954156064',3);
INSERT INTO osnovno_zdravstveno VALUES(309411323,'nositelj','Hrvatska',223);
INSERT INTO osig_pac VALUES(223, TO_DATE('2024-10-26','YYYY-MM-DD'),TO_DATE('2054-10-19','YYYY-MM-DD'),309411323);
INSERT INTO dopunsko_osiguranje VALUES(1910686,'HZZO');
INSERT INTO dop_pac VALUES(223, TO_DATE('2019-10-25','YYYY-MM-DD'), TO_DATE('2020-10-23','YYYY-MM-DD'),223, 1910686);

INSERT INTO anamneza VALUES (411,'Intezivan ka�alj, ote�ano disanje',TO_DATE('2019-10-17','YYYY-MM-DD'),223);
INSERT INTO dijag_anamneza VALUES(459,411,'J06.0');
INSERT INTO anamneza VALUES (412,'Bolno mokrenje',TO_DATE('2019-08-08','YYYY-MM-DD'),223);
INSERT INTO dijag_anamneza VALUES(460,412,'N30');
INSERT INTO pretraga VALUES(282,'Krv','DKS',TO_DATE('2019-08-09','YYYY-MM-DD'),460);


INSERT INTO pacijent VALUES(224,'Silvio','An�eli�','M',TO_DATE('1982-11-05','YYYY-MM-DD'), 'Drinska 133,Osijek','0997033472',4);
INSERT INTO osnovno_zdravstveno VALUES(269290919,'nositelj','Hrvatska',224);
INSERT INTO osig_pac VALUES(224, TO_DATE('2007-10-30','YYYY-MM-DD'),TO_DATE('2037-10-22','YYYY-MM-DD'),269290919);
INSERT INTO dopunsko_osiguranje VALUES(532998411,'Croatia');
INSERT INTO dop_pac VALUES(224, TO_DATE('2020-05-08','YYYY-MM-DD'), TO_DATE('2021-05-07','YYYY-MM-DD'),224, 532998411);

INSERT INTO anamneza VALUES (413,'Intezivan ka�alj, ote�ano disanje',TO_DATE('2020-06-20','YYYY-MM-DD'),224);
INSERT INTO dijag_anamneza VALUES(461,413,'J06.0');
INSERT INTO terapija VALUES(203,'1,0,1',461,1006);
INSERT INTO anamneza VALUES (414,'Osjetljivost i oteknu�e sinusa',TO_DATE('2019-12-28','YYYY-MM-DD'),224);
INSERT INTO dijag_anamneza VALUES(462,414,'J01.2');


INSERT INTO pacijent VALUES(225,'Laura','Lukasovi�','F',TO_DATE('2008-08-02','YYYY-MM-DD'), 'Vukovarska 136,Osijek','0997414934',4);
INSERT INTO osnovno_zdravstveno VALUES(462150860,'�lan obitelji','Hrvatska',225);
INSERT INTO osig_pac VALUES(225, TO_DATE('2008-08-02','YYYY-MM-DD'),TO_DATE('2038-07-26','YYYY-MM-DD'),462150860);
INSERT INTO dopunsko_osiguranje VALUES(1290841,'HZZO');
INSERT INTO dop_pac VALUES(225, TO_DATE('2020-05-01','YYYY-MM-DD'), TO_DATE('2021-04-30','YYYY-MM-DD'),225, 1290841);

INSERT INTO anamneza VALUES (415,'Sumnja na rak bubrega',TO_DATE('2020-05-19','YYYY-MM-DD'),225);
INSERT INTO dijag_anamneza VALUES(463,415,'C64.0');
INSERT INTO terapija VALUES(204,'1,0,1',463,1021);
INSERT INTO anamneza VALUES (416,'Povi�ena temp.,hipoglikemija',TO_DATE('2019-06-01','YYYY-MM-DD'),225);
INSERT INTO dijag_anamneza VALUES(464,416,'C22.0');
INSERT INTO pretraga VALUES(283,'Krv','DKS',TO_DATE('2019-06-02','YYYY-MM-DD'),464);


INSERT INTO pacijent VALUES(226,'Vinko','Medi�','M',TO_DATE('1978-03-05','YYYY-MM-DD'), 'Kozja�ka 135,Osijek','0951327103',2);
INSERT INTO osnovno_zdravstveno VALUES(643377028,'nositelj','Hrvatska',226);
INSERT INTO osig_pac VALUES(226, TO_DATE('2003-02-27','YYYY-MM-DD'),TO_DATE('2033-02-19','YYYY-MM-DD'),643377028);
INSERT INTO dopunsko_osiguranje VALUES(7896005,'HZZO');
INSERT INTO dop_pac VALUES(226, TO_DATE('2020-03-17','YYYY-MM-DD'), TO_DATE('2021-03-16','YYYY-MM-DD'),226, 7896005);

INSERT INTO anamneza VALUES (417,'Crveni,ljuskav osip na tjemenu',TO_DATE('2019-06-26','YYYY-MM-DD'),226);
INSERT INTO dijag_anamneza VALUES(465,417,'L21.0');
INSERT INTO bolovanja VALUES(2079, TO_DATE('2019-06-26','YYYY-MM-DD'),TO_DATE('2019-07-03','YYYY-MM-DD'),'A0 bolest',465);
INSERT INTO pretraga VALUES(284,'Krv','DKS',TO_DATE('2019-06-27','YYYY-MM-DD'),465);


INSERT INTO pacijent VALUES(227,'Fran','Andri�','M',TO_DATE('1973-11-30','YYYY-MM-DD'), 'Ilirska 120,Osijek','0952584034',4);
INSERT INTO osnovno_zdravstveno VALUES(954515831,'nositelj','Hrvatska',227);
INSERT INTO osig_pac VALUES(227, TO_DATE('1998-11-24','YYYY-MM-DD'),TO_DATE('2028-11-16','YYYY-MM-DD'),954515831);
INSERT INTO dopunsko_osiguranje VALUES(510434296,'Allianz');
INSERT INTO dop_pac VALUES(227, TO_DATE('2019-10-19','YYYY-MM-DD'), TO_DATE('2020-10-17','YYYY-MM-DD'),227, 510434296);

INSERT INTO anamneza VALUES (418,'POvi�ena temp. plikovi i osip na ko�i',TO_DATE('2019-01-12','YYYY-MM-DD'),227);
INSERT INTO dijag_anamneza VALUES(466,418,'B01');
INSERT INTO anamneza VALUES (419,'Sumnja na rak bubrega',TO_DATE('2019-05-07','YYYY-MM-DD'),227);
INSERT INTO dijag_anamneza VALUES(467,419,'C64.0');
INSERT INTO bolovanja VALUES(2080, TO_DATE('2019-05-07','YYYY-MM-DD'),TO_DATE('2019-05-13','YYYY-MM-DD'),'A0 bolest',467);
INSERT INTO terapija VALUES(205,'2x dnevno',467,1021);
INSERT INTO pretraga VALUES(285,'Krv','KKS',TO_DATE('2019-05-08','YYYY-MM-DD'),467);


INSERT INTO pacijent VALUES(228,'Matej','Lon�ari�','M',TO_DATE('1978-01-06','YYYY-MM-DD'), 'Zagreba�ka 46,Osijek','0952754247',4);
INSERT INTO osnovno_zdravstveno VALUES(762829885,'nositelj','Hrvatska',228);
INSERT INTO osig_pac VALUES(228, TO_DATE('2002-12-31','YYYY-MM-DD'),TO_DATE('2032-12-23','YYYY-MM-DD'),762829885);
INSERT INTO dopunsko_osiguranje VALUES(748075749,'Croatia');
INSERT INTO dop_pac VALUES(228, TO_DATE('2020-03-01','YYYY-MM-DD'), TO_DATE('2021-02-28','YYYY-MM-DD'),228, 748075749);

INSERT INTO anamneza VALUES (420,'Mu�nina,povra�anje,bol u trbuhu',TO_DATE('2020-02-25','YYYY-MM-DD'),228);
INSERT INTO dijag_anamneza VALUES(468,420,'K35.9');
INSERT INTO uputnice VALUES(142,'A2','Gastroenterologija','prvi pregled',468);
INSERT INTO pretraga VALUES(286,'Feces',null,TO_DATE('2020-02-26','YYYY-MM-DD'),468);
INSERT INTO anamneza VALUES (421,'Crveilo ko�e,bol i uko�enost zglobova',TO_DATE('2020-06-01','YYYY-MM-DD'),228);
INSERT INTO dijag_anamneza VALUES(469,421,'M00.0');


INSERT INTO pacijent VALUES(229,'Ana','Cvitkovi�','F',TO_DATE('1992-12-15','YYYY-MM-DD'), 'Srijemska 57,Osijek','0997378816',1);
INSERT INTO osnovno_zdravstveno VALUES(367268586,'nositelj','Hrvatska',229);
INSERT INTO osig_pac VALUES(229, TO_DATE('2017-12-09','YYYY-MM-DD'),TO_DATE('2047-12-02','YYYY-MM-DD'),367268586);
INSERT INTO dopunsko_osiguranje VALUES(337349566,'Generali');
INSERT INTO dop_pac VALUES(229, TO_DATE('2019-02-21','YYYY-MM-DD'), TO_DATE('2020-02-20','YYYY-MM-DD'),229, 337349566);

INSERT INTO anamneza VALUES (422,'Crveni,ljuskav osip na tjemenu',TO_DATE('2019-08-13','YYYY-MM-DD'),229);
INSERT INTO dijag_anamneza VALUES(470,422,'L21.0');
INSERT INTO anamneza VALUES (423,'Crveilo ko�e,bol i uko�enost zglobova',TO_DATE('2019-01-28','YYYY-MM-DD'),229);
INSERT INTO dijag_anamneza VALUES(471,423,'M00.0');
INSERT INTO pretraga VALUES(287,'Krv','BIOKEMIJA',TO_DATE('2019-01-29','YYYY-MM-DD'),471);


INSERT INTO pacijent VALUES(230,'Sofija','Grgi�','F',TO_DATE('1974-07-13','YYYY-MM-DD'), 'Drinska 92,Osijek','0955423993',2);
INSERT INTO osnovno_zdravstveno VALUES(936711001,'nositelj','Hrvatska',230);
INSERT INTO osig_pac VALUES(230, TO_DATE('1999-07-07','YYYY-MM-DD'),TO_DATE('2029-06-29','YYYY-MM-DD'),936711001);
INSERT INTO dopunsko_osiguranje VALUES(686431577,'Allianz');
INSERT INTO dop_pac VALUES(230, TO_DATE('2020-03-09','YYYY-MM-DD'), TO_DATE('2021-03-08','YYYY-MM-DD'),230, 686431577);

INSERT INTO anamneza VALUES (424,'Grlobolja,uve�ani krajnici',TO_DATE('2019-05-29','YYYY-MM-DD'),230);
INSERT INTO dijag_anamneza VALUES(472,424,'J02');
INSERT INTO terapija VALUES(206,'1,0,1',472,1014);
INSERT INTO bolovanja VALUES(2081, TO_DATE('2019-05-29','YYYY-MM-DD'),TO_DATE('2019-06-05','YYYY-MM-DD'),'A0 bolest',472);
INSERT INTO pretraga VALUES(288,'Krv','KKS',TO_DATE('2019-05-30','YYYY-MM-DD'),472);


INSERT INTO pacijent VALUES(231,'Marina','Bolfan','F',TO_DATE('1993-12-16','YYYY-MM-DD'), 'Europska avenija 72,Osijek','0983370102',1);
INSERT INTO osnovno_zdravstveno VALUES(453966238,'nositelj','Hrvatska',231);
INSERT INTO osig_pac VALUES(231, TO_DATE('2018-12-10','YYYY-MM-DD'),TO_DATE('2048-12-02','YYYY-MM-DD'),453966238);
INSERT INTO dopunsko_osiguranje VALUES(995605134,'Croatia');
INSERT INTO dop_pac VALUES(231, TO_DATE('2020-02-02','YYYY-MM-DD'), TO_DATE('2021-01-31','YYYY-MM-DD'),231, 995605134);

INSERT INTO anamneza VALUES (425,'Bolno mokrenje',TO_DATE('2020-04-01','YYYY-MM-DD'),231);
INSERT INTO dijag_anamneza VALUES(473,425,'N30');
INSERT INTO uputnice VALUES(143,'A2','Neurologija','prvi pregled',473);
INSERT INTO pretraga VALUES(289,'Urin',null,TO_DATE('2020-04-02','YYYY-MM-DD'),473);
INSERT INTO anamneza VALUES (427,'Bolno mokrenje,povi�ena temp.',TO_DATE('2020-04-19','YYYY-MM-DD'),231);
INSERT INTO dijag_anamneza VALUES(475,427,'N15.1');


INSERT INTO pacijent VALUES(232,'Antonio','Antunovi�','M',TO_DATE('1969-03-03','YYYY-MM-DD'), 'Wilsonova 5,Osijek','0989467598',1);
INSERT INTO osnovno_zdravstveno VALUES(622816482,'nositelj','Hrvatska',232);
INSERT INTO osig_pac VALUES(232, TO_DATE('1994-02-25','YYYY-MM-DD'),TO_DATE('2024-02-18','YYYY-MM-DD'),622816482);
INSERT INTO dopunsko_osiguranje VALUES(950107094,'Allianz');
INSERT INTO dop_pac VALUES(232, TO_DATE('2020-05-28','YYYY-MM-DD'), TO_DATE('2021-05-27','YYYY-MM-DD'),232, 950107094);

INSERT INTO anamneza VALUES (428,'Intezivan ka�alj, ote�ano disanje',TO_DATE('2019-09-26','YYYY-MM-DD'),232);
INSERT INTO dijag_anamneza VALUES(476,428,'J06.0');
INSERT INTO bolovanja VALUES(2082, TO_DATE('2019-09-26','YYYY-MM-DD'),TO_DATE('2019-10-04','YYYY-MM-DD'),'A0 bolest',476);
INSERT INTO uputnice VALUES(144,'A2','Otorinolaringologija','prvi pregled',476);
INSERT INTO pretraga VALUES(290,'Krv','KKS',TO_DATE('2019-09-28','YYYY-MM-DD'),476);


INSERT INTO pacijent VALUES(233,'Marin','Valin�i�','M',TO_DATE('1946-03-07','YYYY-MM-DD'), 'Zagreba�ka 121,Osijek','0912144121',3);
INSERT INTO osnovno_zdravstveno VALUES(533014572,'nositelj','Hrvatska',233);
INSERT INTO osig_pac VALUES(233, TO_DATE('2011-02-19','YYYY-MM-DD'),TO_DATE('2041-02-11','YYYY-MM-DD'),533014572);
INSERT INTO dopunsko_osiguranje VALUES(452224533,'Generali');
INSERT INTO dop_pac VALUES(233, TO_DATE('2020-01-05','YYYY-MM-DD'), TO_DATE('2021-01-03','YYYY-MM-DD'),233, 452224533);

INSERT INTO anamneza VALUES (429,'Bolno mokrenje,povi�ena temp.',TO_DATE('2020-05-18','YYYY-MM-DD'),233);
INSERT INTO dijag_anamneza VALUES(477,429,'N15.1');
INSERT INTO anamneza VALUES (430,'Povisen tlak',TO_DATE('2019-12-10','YYYY-MM-DD'),233);
INSERT INTO dijag_anamneza VALUES(478,430,'I10');
INSERT INTO terapija VALUES(207,'1,0,0',478,1018);


INSERT INTO pacijent VALUES(234,'Juro','Vidi�','M',TO_DATE('1985-05-24','YYYY-MM-DD'), 'Zagreba�ka 30,Osijek','0956212031',3);
INSERT INTO osnovno_zdravstveno VALUES(225263546,'nositelj','Hrvatska',234);
INSERT INTO osig_pac VALUES(234, TO_DATE('2010-05-18','YYYY-MM-DD'),TO_DATE('2040-05-10','YYYY-MM-DD'),225263546);
INSERT INTO dopunsko_osiguranje VALUES(5720933,'HZZO');
INSERT INTO dop_pac VALUES(234, TO_DATE('2020-06-09','YYYY-MM-DD'), TO_DATE('2021-06-08','YYYY-MM-DD'),234, 5720933);

INSERT INTO anamneza VALUES (431,'Intezivan ka�alj, ote�ano disanje',TO_DATE('2019-02-09','YYYY-MM-DD'),234);
INSERT INTO dijag_anamneza VALUES(479,431,'J06.0');
INSERT INTO terapija VALUES(208,'3x dnevno',479,1006);
INSERT INTO bolovanja VALUES(2083, TO_DATE('2019-02-09','YYYY-MM-DD'),TO_DATE('2019-02-12','YYYY-MM-DD'),'A0 bolest',479);
INSERT INTO pretraga VALUES(291,'Krv','DKS',TO_DATE('2019-02-10','YYYY-MM-DD'),479);


INSERT INTO pacijent VALUES(235,'Mihaela','Lubina','F',TO_DATE('1984-12-18','YYYY-MM-DD'), 'Zagreba�ka 12,Osijek','0913045614',2);
INSERT INTO osnovno_zdravstveno VALUES(445025257,'nositelj','Hrvatska',235);
INSERT INTO osig_pac VALUES(235, TO_DATE('2009-12-12','YYYY-MM-DD'),TO_DATE('2039-12-05','YYYY-MM-DD'),445025257);
INSERT INTO dopunsko_osiguranje VALUES(154011465,'Croatia');
INSERT INTO dop_pac VALUES(235, TO_DATE('2020-04-05','YYYY-MM-DD'), TO_DATE('2021-04-04','YYYY-MM-DD'),235, 154011465);

INSERT INTO anamneza VALUES (432,'Crveilo ko�e,bol i uko�enost zglobova',TO_DATE('2020-05-18','YYYY-MM-DD'),235);
INSERT INTO dijag_anamneza VALUES(480,432,'M00.0');
INSERT INTO anamneza VALUES (433,'Bolno mokrenje,povi�ena temp.',TO_DATE('2020-06-23','YYYY-MM-DD'),235);
INSERT INTO dijag_anamneza VALUES(481,433,'N15.1');


INSERT INTO pacijent VALUES(236,'Ana','Elkaz','F',TO_DATE('1983-04-20','YYYY-MM-DD'), 'Srijemska 62,Osijek','0912119162',1);
INSERT INTO osnovno_zdravstveno VALUES(157042905,'nositelj','Hrvatska',236);
INSERT INTO osig_pac VALUES(236, TO_DATE('2008-04-13','YYYY-MM-DD'),TO_DATE('2038-04-06','YYYY-MM-DD'),157042905);
INSERT INTO dopunsko_osiguranje VALUES(4991456,'HZZO');
INSERT INTO dop_pac VALUES(236, TO_DATE('2020-01-11','YYYY-MM-DD'), TO_DATE('2021-01-09','YYYY-MM-DD'),236, 4991456);

INSERT INTO anamneza VALUES (434,'Crveilo ko�e,bol i uko�enost zglobova',TO_DATE('2019-03-11','YYYY-MM-DD'),236);
INSERT INTO dijag_anamneza VALUES(482,434,'M00.0');
INSERT INTO bolovanja VALUES(2084, TO_DATE('2019-03-11','YYYY-MM-DD'),TO_DATE('2019-03-18','YYYY-MM-DD'),'A0 bolest',482);
INSERT INTO pretraga VALUES(292,'Krv','BIOKEMIJA',TO_DATE('2019-03-12','YYYY-MM-DD'),482);


INSERT INTO pacijent VALUES(237,'Darko','Hodak','M',TO_DATE('1970-06-23','YYYY-MM-DD'), 'Kozja�ka 29,Osijek','0917335042',2);
INSERT INTO osnovno_zdravstveno VALUES(687345944,'nositelj','Hrvatska',237);
INSERT INTO osig_pac VALUES(237, TO_DATE('1995-06-17','YYYY-MM-DD'),TO_DATE('2025-06-09','YYYY-MM-DD'),687345944);
INSERT INTO dopunsko_osiguranje VALUES(192232939,'Croatia');
INSERT INTO dop_pac VALUES(237, TO_DATE('2020-02-26','YYYY-MM-DD'), TO_DATE('2021-02-24','YYYY-MM-DD'),237, 192232939);

INSERT INTO anamneza VALUES (435,'Osjetljivost i oteknu�e sinusa',TO_DATE('2019-06-22','YYYY-MM-DD'),237);
INSERT INTO dijag_anamneza VALUES(483,435,'J01.2');
INSERT INTO terapija VALUES(209,'0,0,1',483,1003);
INSERT INTO uputnice VALUES(145,'A2','Otorinolaringologija','prvi pregled',483);
INSERT INTO anamneza VALUES (436,'Mu�nina,povra�anje,bol u trbuhu',TO_DATE('2019-07-26','YYYY-MM-DD'),237);
INSERT INTO dijag_anamneza VALUES(484,436,'K35.9');
INSERT INTO bolovanja VALUES(2085, TO_DATE('2019-07-26','YYYY-MM-DD'),TO_DATE('2019-08-03','YYYY-MM-DD'),'A0 bolest',484);
INSERT INTO pretraga VALUES(293,'Krv','DKS',TO_DATE('2019-07-27','YYYY-MM-DD'),484);


INSERT INTO pacijent VALUES(238,'Filip','Lovri�','M',TO_DATE('1971-08-07','YYYY-MM-DD'), 'Vratni�ka 111,Osijek','0982132266',1);
INSERT INTO osnovno_zdravstveno VALUES(682005908,'nositelj','Hrvatska',238);
INSERT INTO osig_pac VALUES(238, TO_DATE('1996-07-31','YYYY-MM-DD'),TO_DATE('2026-07-24','YYYY-MM-DD'),682005908);
INSERT INTO dopunsko_osiguranje VALUES(578421296,'Allianz');
INSERT INTO dop_pac VALUES(238, TO_DATE('2019-07-29','YYYY-MM-DD'), TO_DATE('2020-07-27','YYYY-MM-DD'),238, 578421296);

INSERT INTO anamneza VALUES (437,'Intezivan ka�alj, ote�ano disanje',TO_DATE('2020-06-28','YYYY-MM-DD'),238);
INSERT INTO dijag_anamneza VALUES(485,437,'J06.0');
INSERT INTO bolovanja VALUES(2086, TO_DATE('2020-06-28','YYYY-MM-DD'),TO_DATE('2020-07-03','YYYY-MM-DD'),'A0 bolest',485);
INSERT INTO pretraga VALUES(294,'Krv','KKS',TO_DATE('2020-06-29','YYYY-MM-DD'),485);


INSERT INTO pacijent VALUES(239,'Kruno','Kokot','M',TO_DATE('2014-08-16','YYYY-MM-DD'), 'Wilsonova 97,Osijek','0996936920',4);
INSERT INTO osnovno_zdravstveno VALUES(137886148,'�lan obitelji','Hrvatska',239);
INSERT INTO osig_pac VALUES(239, TO_DATE('2014-08-16','YYYY-MM-DD'),TO_DATE('2044-08-08','YYYY-MM-DD'),137886148);
INSERT INTO dopunsko_osiguranje VALUES(968808931,'Allianz');
INSERT INTO dop_pac VALUES(239, TO_DATE('2020-03-06','YYYY-MM-DD'), TO_DATE('2021-03-05','YYYY-MM-DD'),239, 968808931);

INSERT INTO anamneza VALUES (438,'Temp. normalna,crvenilo o�nih spojnica i ka�alj',TO_DATE('2020-06-02','YYYY-MM-DD'),239);
INSERT INTO dijag_anamneza VALUES(486,438,'Z23.7');
INSERT INTO cijepljenje VALUES(65,9,486);
INSERT INTO dijag_anamneza VALUES(487,438,'A37.0');









INSERT INTO pacijent VALUES(240,'Josipa','Sabljo','F',TO_DATE('2003-07-20','YYYY-MM-DD'), '�upanijska 1,Osijek','0957310314',3);
INSERT INTO osnovno_zdravstveno VALUES(274906562,'�lan obitelji','Hrvatska',240);
INSERT INTO osig_pac VALUES(240, TO_DATE('2003-07-20','YYYY-MM-DD'),TO_DATE('2033-07-12','YYYY-MM-DD'),274906562);
INSERT INTO dopunsko_osiguranje VALUES(290669335,'Allianz');
INSERT INTO dop_pac VALUES(240, TO_DATE('2019-04-29','YYYY-MM-DD'), TO_DATE('2020-04-27','YYYY-MM-DD'),240, 290669335);

INSERT INTO anamneza VALUES (439,'Temp. normalna,crvenilo o�nih spojnica i ka�alj',TO_DATE('2019-10-02','YYYY-MM-DD'),240);
INSERT INTO dijag_anamneza VALUES(488,439,'Z23.7');
INSERT INTO cijepljenje VALUES(66,9,488);
INSERT INTO dijag_anamneza VALUES(489,439,'A37.0');


INSERT INTO pacijent VALUES(241,'Ena','Suli�','F',TO_DATE('2010-08-26','YYYY-MM-DD'), 'Zagreba�ka 131,Osijek','0917140358',3);
INSERT INTO osnovno_zdravstveno VALUES(835782511,'�lan obitelji','Hrvatska',241);
INSERT INTO osig_pac VALUES(241, TO_DATE('2010-08-26','YYYY-MM-DD'),TO_DATE('2040-08-18','YYYY-MM-DD'),835782511);
INSERT INTO dopunsko_osiguranje VALUES(560170018,'Croatia');
INSERT INTO dop_pac VALUES(241, TO_DATE('2019-05-29','YYYY-MM-DD'), TO_DATE('2020-05-27','YYYY-MM-DD'),241, 560170018);

INSERT INTO anamneza VALUES (440,'Bolno mokrenje,povi�ena temp.',TO_DATE('2019-05-31','YYYY-MM-DD'),241);
INSERT INTO dijag_anamneza VALUES(490,440,'N15.1');
INSERT INTO anamneza VALUES (441,'POvi�ena temp. plikovi i osip na ko�i',TO_DATE('2020-06-06','YYYY-MM-DD'),241);
INSERT INTO dijag_anamneza VALUES(491,441,'B01');
INSERT INTO terapija VALUES(210,'3x dnevno',491,1003);
INSERT INTO uputnice VALUES(146,'A2','Infektologija','prvi pregled',491);
INSERT INTO pretraga VALUES(295,'Krv','KKS',TO_DATE('2020-06-07','YYYY-MM-DD'),491);


INSERT INTO pacijent VALUES(242,'Lora','Aladrovi�','F',TO_DATE('2002-10-18','YYYY-MM-DD'), 'Europska avenija 17,Osijek','0998899267',4);
INSERT INTO osnovno_zdravstveno VALUES(271489347,'�lan obitelji','Hrvatska',242);
INSERT INTO osig_pac VALUES(242, TO_DATE('2002-10-18','YYYY-MM-DD'),TO_DATE('2032-10-10','YYYY-MM-DD'),271489347);
INSERT INTO dopunsko_osiguranje VALUES(990055786,'Croatia');
INSERT INTO dop_pac VALUES(242, TO_DATE('2019-10-14','YYYY-MM-DD'), TO_DATE('2020-10-12','YYYY-MM-DD'),242, 990055786);

INSERT INTO anamneza VALUES (442,'Intezivan ka�alj, ote�ano disanje',TO_DATE('2019-12-28','YYYY-MM-DD'),242);
INSERT INTO dijag_anamneza VALUES(492,442,'J06.0');
INSERT INTO anamneza VALUES (443,'Rane u ustima,proljev,vru�ica',TO_DATE('2019-08-05','YYYY-MM-DD'),242);
INSERT INTO dijag_anamneza VALUES(493,443,'K50.0');
INSERT INTO pretraga VALUES(296,'Krv','KKS',TO_DATE('2019-08-06','YYYY-MM-DD'),493);


INSERT INTO pacijent VALUES(243,'Barbara','Medved','F',TO_DATE('1997-05-02','YYYY-MM-DD'), 'Wilsonova 111,Osijek','0955865609',1);
INSERT INTO osnovno_zdravstveno VALUES(241229944,'nositelj','Hrvatska',243);
INSERT INTO osig_pac VALUES(243, TO_DATE('2022-04-26','YYYY-MM-DD'),TO_DATE('2052-04-18','YYYY-MM-DD'),241229944);
INSERT INTO dopunsko_osiguranje VALUES(584602327,'Croatia');
INSERT INTO dop_pac VALUES(243, TO_DATE('2019-09-01','YYYY-MM-DD'), TO_DATE('2020-08-30','YYYY-MM-DD'),243, 584602327);

INSERT INTO anamneza VALUES (444,'Intezivan ka�alj, ote�ano disanje',TO_DATE('2019-01-12','YYYY-MM-DD'),243);
INSERT INTO dijag_anamneza VALUES(494,444,'J06.0');
INSERT INTO terapija VALUES(211,'1x dnevno',494,1003);
INSERT INTO anamneza VALUES (445,'Crveilo ko�e,bol i uko�enost zglobova',TO_DATE('2020-06-21','YYYY-MM-DD'),243);
INSERT INTO dijag_anamneza VALUES(495,445,'M00.0');
INSERT INTO pretraga VALUES(297,'Krv','BIOKEMIJA',TO_DATE('2020-06-22','YYYY-MM-DD'),495);


INSERT INTO pacijent VALUES(244,'Ria','Vukas','F',TO_DATE('2017-07-23','YYYY-MM-DD'), 'Mlinska 115,Osijek','0914511474',2);
INSERT INTO osnovno_zdravstveno VALUES(384495866,'�lan obitelji','Hrvatska',244);
INSERT INTO osig_pac VALUES(244, TO_DATE('2017-07-23','YYYY-MM-DD'),TO_DATE('2047-07-16','YYYY-MM-DD'),384495866);
INSERT INTO dopunsko_osiguranje VALUES(793705526,'Allianz');
INSERT INTO dop_pac VALUES(244, TO_DATE('2020-04-13','YYYY-MM-DD'), TO_DATE('2021-04-12','YYYY-MM-DD'),244, 793705526);

INSERT INTO anamneza VALUES (446,'kontrola i cijepljenje',TO_DATE('2017-11-20','YYYY-MM-DD'),244);
INSERT INTO dijag_anamneza VALUES(496,446,'Z27.2');
INSERT INTO dijag_anamneza VALUES(497,446,'Z24.0');
INSERT INTO dijag_anamneza VALUES(498,446,'Z24.6');
INSERT INTO cijepljenje VALUES(67,2,496);
INSERT INTO cijepljenje VALUES(68,3,497);
INSERT INTO cijepljenje VALUES(69,4,498);
INSERT INTO anamneza VALUES (447,'Grlobolja,uve�ani krajnici',TO_DATE('2019-06-10','YYYY-MM-DD'),244);
INSERT INTO dijag_anamneza VALUES(499,447,'J02');
INSERT INTO pretraga VALUES(298,'Krv','KKS',TO_DATE('2019-06-13','YYYY-MM-DD'),499);


INSERT INTO pacijent VALUES(245,'Laura','Herek','F',TO_DATE('1988-12-25','YYYY-MM-DD'), '�upanijska 119,Osijek','0992908036',4);
INSERT INTO osnovno_zdravstveno VALUES(267067029,'nositelj','Hrvatska',245);
INSERT INTO osig_pac VALUES(245, TO_DATE('2013-12-19','YYYY-MM-DD'),TO_DATE('2043-12-12','YYYY-MM-DD'),267067029);
INSERT INTO dopunsko_osiguranje VALUES(275366351,'Allianz');
INSERT INTO dop_pac VALUES(245, TO_DATE('2020-02-20','YYYY-MM-DD'), TO_DATE('2021-02-18','YYYY-MM-DD'),245, 275366351);

INSERT INTO anamneza VALUES (448,'Mu�nina,povra�anje,bol u trbuhu',TO_DATE('2019-10-29','YYYY-MM-DD'),245);
INSERT INTO dijag_anamneza VALUES(500,448,'K35.9');
INSERT INTO bolovanja VALUES(2087, TO_DATE('2019-10-29','YYYY-MM-DD'),TO_DATE('2019-11-03','YYYY-MM-DD'),'A0 bolest',500);
INSERT INTO pretraga VALUES(299,'Krv','BIOKEMIJA',TO_DATE('2019-10-30','YYYY-MM-DD'),500);


INSERT INTO pacijent VALUES(246,'Sven','Latinovi�','M',TO_DATE('2012-07-11','YYYY-MM-DD'), 'Wilsonova 77,Osijek','0986803672',3);
INSERT INTO osnovno_zdravstveno VALUES(559594864,'�lan obitelji','Hrvatska',246);
INSERT INTO osig_pac VALUES(246, TO_DATE('2012-07-11','YYYY-MM-DD'),TO_DATE('2042-07-04','YYYY-MM-DD'),559594864);
INSERT INTO dopunsko_osiguranje VALUES(496963603,'Allianz');
INSERT INTO dop_pac VALUES(246, TO_DATE('2019-05-23','YYYY-MM-DD'), TO_DATE('2020-05-21','YYYY-MM-DD'),246, 496963603);

INSERT INTO anamneza VALUES (449,'Mu�nina,povra�anje,bol u trbuhu',TO_DATE('2019-10-01','YYYY-MM-DD'),246);
INSERT INTO dijag_anamneza VALUES(501,449,'K35.9');
INSERT INTO pretraga VALUES(300,'Feces',null,TO_DATE('2019-10-02','YYYY-MM-DD'),501);
INSERT INTO anamneza VALUES (450,'Vezikule na ko�i sa gnojem',TO_DATE('2019-09-22','YYYY-MM-DD'),246);
INSERT INTO dijag_anamneza VALUES(502,450,'L08.0');
INSERT INTO uputnice VALUES(147,'A2','Dermatologija','prvi pregled',502);
INSERT INTO pretraga VALUES(301,'Krv','DKS',TO_DATE('2019-09-23','YYYY-MM-DD'),502);


INSERT INTO pacijent VALUES(247,'Damir','Vidovi�','M',TO_DATE('1959-12-26','YYYY-MM-DD'), '�upanijska 54,Osijek','0958016170',3);
INSERT INTO osnovno_zdravstveno VALUES(315415335,'nositelj','Hrvatska',247);
INSERT INTO osig_pac VALUES(247, TO_DATE('1984-12-19','YYYY-MM-DD'),TO_DATE('2014-12-12','YYYY-MM-DD'),315415335);
INSERT INTO dopunsko_osiguranje VALUES(377166438,'Generali');
INSERT INTO dop_pac VALUES(247, TO_DATE('2019-03-10','YYYY-MM-DD'), TO_DATE('2020-03-08','YYYY-MM-DD'),247, 377166438);

INSERT INTO anamneza VALUES (451,'Vezikule na ko�i sa gnojem',TO_DATE('2019-11-19','YYYY-MM-DD'),247);
INSERT INTO dijag_anamneza VALUES(503,451,'L08.0');
INSERT INTO anamneza VALUES (452,'Crveni,ljuskav osip na tjemenu',TO_DATE('2020-05-31','YYYY-MM-DD'),247);
INSERT INTO dijag_anamneza VALUES(504,452,'L21.0');
INSERT INTO uputnice VALUES(148,'A2','Dermatologija','prvi pregled',504);


INSERT INTO pacijent VALUES(248,'Toni','Opa�ak','M',TO_DATE('1974-07-10','YYYY-MM-DD'), 'Zagreba�ka 24,Osijek','0999014898',3);
INSERT INTO osnovno_zdravstveno VALUES(280662295,'nositelj','Hrvatska',248);
INSERT INTO osig_pac VALUES(248, TO_DATE('1999-07-04','YYYY-MM-DD'),TO_DATE('2029-06-26','YYYY-MM-DD'),280662295);
INSERT INTO dopunsko_osiguranje VALUES(260723167,'Croatia');
INSERT INTO dop_pac VALUES(248, TO_DATE('2019-07-15','YYYY-MM-DD'), TO_DATE('2020-07-13','YYYY-MM-DD'),248, 260723167);

INSERT INTO anamneza VALUES (453,'Tvrda ko�a,gubitak dlake',TO_DATE('2019-04-25','YYYY-MM-DD'),248);
INSERT INTO dijag_anamneza VALUES(505,453,'M34.0');
INSERT INTO anamneza VALUES (454,'POvi�ena temp. plikovi i osip na ko�i',TO_DATE('2020-02-25','YYYY-MM-DD'),248);
INSERT INTO dijag_anamneza VALUES(506,454,'B01');
INSERT INTO bolovanja VALUES(2088, TO_DATE('2020-02-25','YYYY-MM-DD'),TO_DATE('2020-02-27','YYYY-MM-DD'),'A0 bolest',506);
INSERT INTO pretraga VALUES(302,'Krv','DKS',TO_DATE('2020-02-26','YYYY-MM-DD'),506);


INSERT INTO pacijent VALUES(249,'Leo','Rebu�','M',TO_DATE('1982-11-24','YYYY-MM-DD'), 'Zagreba�ka 101,Osijek','0986326837',1);
INSERT INTO osnovno_zdravstveno VALUES(403834587,'nositelj','Hrvatska',249);
INSERT INTO osig_pac VALUES(249, TO_DATE('2007-11-18','YYYY-MM-DD'),TO_DATE('2037-11-10','YYYY-MM-DD'),403834587);
INSERT INTO dopunsko_osiguranje VALUES(405764380,'Generali');
INSERT INTO dop_pac VALUES(249, TO_DATE('2020-04-22','YYYY-MM-DD'), TO_DATE('2021-04-21','YYYY-MM-DD'),249, 405764380);

INSERT INTO anamneza VALUES (455,'Rane u ustima,proljev,vru�ica',TO_DATE('2019-09-08','YYYY-MM-DD'),249);
INSERT INTO dijag_anamneza VALUES(507,455,'K50.0');
INSERT INTO pretraga VALUES(303,'Feces',null,TO_DATE('2019-09-09','YYYY-MM-DD'),507);
INSERT INTO anamneza VALUES (456,'POvi�ena temp. plikovi i osip na ko�i',TO_DATE('2019-02-20','YYYY-MM-DD'),249);
INSERT INTO dijag_anamneza VALUES(508,456,'B01');
INSERT INTO terapija VALUES(212,'3x dnevno',508,1003);
INSERT INTO bolovanja VALUES(2089, TO_DATE('2019-02-20','YYYY-MM-DD'),TO_DATE('2019-02-28','YYYY-MM-DD'),'A0 bolest',508);
INSERT INTO pretraga VALUES(304,'Krv','DKS',TO_DATE('2019-02-21','YYYY-MM-DD'),508);


INSERT INTO pacijent VALUES(250,'Bruno','Gro�i�','M',TO_DATE('1998-03-25','YYYY-MM-DD'), 'Vratni�ka 141,Osijek','0916425750',2);
INSERT INTO osnovno_zdravstveno VALUES(219781803,'nositelj','Hrvatska',250);
INSERT INTO osig_pac VALUES(250, TO_DATE('2023-03-19','YYYY-MM-DD'),TO_DATE('2053-03-11','YYYY-MM-DD'),219781803);
INSERT INTO dopunsko_osiguranje VALUES(214371481,'Generali');
INSERT INTO dop_pac VALUES(250, TO_DATE('2020-03-14','YYYY-MM-DD'), TO_DATE('2021-03-13','YYYY-MM-DD'),250, 214371481);

INSERT INTO anamneza VALUES (457,'Vezikule na ko�i sa gnojem',TO_DATE('2019-09-12','YYYY-MM-DD'),250);
INSERT INTO dijag_anamneza VALUES(509,457,'L08.0');
INSERT INTO anamneza VALUES (458,'Crveilo ko�e,bol i uko�enost zglobova',TO_DATE('2019-08-14','YYYY-MM-DD'),250);
INSERT INTO dijag_anamneza VALUES(510,458,'M00.0');
INSERT INTO pretraga VALUES(305,'Krv','BIOKEMIJA',TO_DATE('2019-08-15','YYYY-MM-DD'),510);


INSERT INTO pacijent VALUES(251,'Ileana','Rosanda','F',TO_DATE('1987-07-11','YYYY-MM-DD'), '�upanijska 23,Osijek','0911323145',2);
INSERT INTO osnovno_zdravstveno VALUES(931142890,'nositelj','Hrvatska',251);
INSERT INTO osig_pac VALUES(251, TO_DATE('2012-07-04','YYYY-MM-DD'),TO_DATE('2042-06-27','YYYY-MM-DD'),931142890);
INSERT INTO dopunsko_osiguranje VALUES(945686864,'Allianz');
INSERT INTO dop_pac VALUES(251, TO_DATE('2019-03-09','YYYY-MM-DD'), TO_DATE('2020-03-07','YYYY-MM-DD'),251, 945686864);

INSERT INTO anamneza VALUES (459,'POvi�ena temp. plikovi i osip na ko�i',TO_DATE('2020-04-20','YYYY-MM-DD'),251);
INSERT INTO dijag_anamneza VALUES(511,459,'B01');
INSERT INTO bolovanja VALUES(2090, TO_DATE('2020-04-20','YYYY-MM-DD'),TO_DATE('2020-04-28','YYYY-MM-DD'),'A0 bolest',511);
INSERT INTO uputnice VALUES(149,'A2','Infektologija','prvi pregled',511);
INSERT INTO pretraga VALUES(306,'Krv','KKS',TO_DATE('2020-04-30','YYYY-MM-DD'),511);


INSERT INTO pacijent VALUES(252,'Tina','Goreta','F',TO_DATE('2011-03-06','YYYY-MM-DD'), 'Zagreba�ka 143,Osijek','0913781159',4);
INSERT INTO osnovno_zdravstveno VALUES(322192867,'�lan obitelji','Hrvatska',252);
INSERT INTO osig_pac VALUES(252, TO_DATE('2011-03-06','YYYY-MM-DD'),TO_DATE('2041-02-26','YYYY-MM-DD'),322192867);
INSERT INTO dopunsko_osiguranje VALUES(515167259,'Allianz');
INSERT INTO dop_pac VALUES(252, TO_DATE('2019-06-01','YYYY-MM-DD'), TO_DATE('2020-05-30','YYYY-MM-DD'),252, 515167259);

INSERT INTO anamneza VALUES (460,'Rane u ustima,proljev,vru�ica',TO_DATE('2020-03-27','YYYY-MM-DD'),252);
INSERT INTO dijag_anamneza VALUES(512,460,'K50.0');
INSERT INTO uputnice VALUES(150,'A2','Gastroenterologija','prvi pregled',512);
INSERT INTO pretraga VALUES(307,'Krv','KKS',TO_DATE('2020-03-28','YYYY-MM-DD'),512);


INSERT INTO pacijent VALUES(253,'Filip','Zadravec','M',TO_DATE('1958-02-11','YYYY-MM-DD'), 'Zagreba�ka 44,Osijek','0987131383',3);
INSERT INTO osnovno_zdravstveno VALUES(755725952,'nositelj','Hrvatska',253);
INSERT INTO osig_pac VALUES(253, TO_DATE('1983-02-05','YYYY-MM-DD'),TO_DATE('2013-01-28','YYYY-MM-DD'),755725952);
INSERT INTO dopunsko_osiguranje VALUES(209897047,'Generali');
INSERT INTO dop_pac VALUES(253, TO_DATE('2019-12-08','YYYY-MM-DD'), TO_DATE('2020-12-06','YYYY-MM-DD'),253, 209897047);

INSERT INTO anamneza VALUES (461,'Nemogu�nost mokrenja',TO_DATE('2019-01-20','YYYY-MM-DD'),253);
INSERT INTO dijag_anamneza VALUES(513,461,'N21.0');
INSERT INTO pretraga VALUES(308,'Urin',null,TO_DATE('2019-01-21','YYYY-MM-DD'),513);
INSERT INTO terapija VALUES(213,'1,0,1',513,1007);
INSERT INTO anamneza VALUES (463,'Povisen tlak',TO_DATE('2019-06-20','YYYY-MM-DD'),253);
INSERT INTO dijag_anamneza VALUES(515,463,'I10');
INSERT INTO terapija VALUES(214,'1,0,0',515,1018);


INSERT INTO pacijent VALUES(254,'Marko','Rebi�','M',TO_DATE('1942-05-24','YYYY-MM-DD'), 'Drinska 48,Osijek','0954358579',1);
INSERT INTO osnovno_zdravstveno VALUES(772353799,'nositelj','Hrvatska',254);
INSERT INTO osig_pac VALUES(254, TO_DATE('2007-05-08','YYYY-MM-DD'),TO_DATE('2037-04-30','YYYY-MM-DD'),772353799);
INSERT INTO dopunsko_osiguranje VALUES(786674729,'Generali');
INSERT INTO dop_pac VALUES(254, TO_DATE('2019-12-16','YYYY-MM-DD'), TO_DATE('2020-12-14','YYYY-MM-DD'),254, 786674729);

INSERT INTO anamneza VALUES (464,'Nemogu�nost mokrenja',TO_DATE('2019-12-25','YYYY-MM-DD'),254);
INSERT INTO dijag_anamneza VALUES(516,464,'N21.0');
INSERT INTO uputnice VALUES(151,'A2','Neurologija','prvi pregled',516);
INSERT INTO pretraga VALUES(309,'Krv','KKS',TO_DATE('2019-12-26','YYYY-MM-DD'),516);


INSERT INTO pacijent VALUES(255,'Ante','�ivkovi�','M',TO_DATE('1963-12-28','YYYY-MM-DD'), '�upanijska 108,Osijek','0959051205',4);
INSERT INTO osnovno_zdravstveno VALUES(289282371,'nositelj','Hrvatska',255);
INSERT INTO osig_pac VALUES(255, TO_DATE('1988-12-21','YYYY-MM-DD'),TO_DATE('2018-12-14','YYYY-MM-DD'),289282371);
INSERT INTO dopunsko_osiguranje VALUES(4732093,'HZZO');
INSERT INTO dop_pac VALUES(255, TO_DATE('2019-11-22','YYYY-MM-DD'), TO_DATE('2020-11-20','YYYY-MM-DD'),255, 4732093);

INSERT INTO anamneza VALUES (465,'Sumnja na tetanus',TO_DATE('2019-12-30','YYYY-MM-DD'),255);
INSERT INTO dijag_anamneza VALUES(517,465,'Z23.5');
INSERT INTO cijepljenje VALUES(70,1,517);


INSERT INTO pacijent VALUES(256,'Milan','Orli�','M',TO_DATE('2017-09-29','YYYY-MM-DD'), 'Zagreba�ka 72,Osijek','0996137999',1);
INSERT INTO osnovno_zdravstveno VALUES(875891695,'�lan obitelji','Hrvatska',256);
INSERT INTO osig_pac VALUES(256, TO_DATE('2017-09-29','YYYY-MM-DD'),TO_DATE('2047-09-22','YYYY-MM-DD'),875891695);
INSERT INTO dopunsko_osiguranje VALUES(499902691,'Generali');
INSERT INTO dop_pac VALUES(256, TO_DATE('2020-01-02','YYYY-MM-DD'), TO_DATE('2020-12-31','YYYY-MM-DD'),256, 499902691);

INSERT INTO anamneza VALUES (466,'kontrola i cijepljenje',TO_DATE('2018-01-27','YYYY-MM-DD'),256);
INSERT INTO dijag_anamneza VALUES(518,466,'Z27.2');
INSERT INTO dijag_anamneza VALUES(519,466,'Z24.0');
INSERT INTO dijag_anamneza VALUES(520,466,'Z24.6');
INSERT INTO cijepljenje VALUES(71,2,518);
INSERT INTO cijepljenje VALUES(72,3,519);
INSERT INTO cijepljenje VALUES(73,4,520);


INSERT INTO pacijent VALUES(257,'Laura','Bijeli�','F',TO_DATE('2015-03-29','YYYY-MM-DD'), 'Vratni�ka 93,Osijek','0914375027',1);
INSERT INTO osnovno_zdravstveno VALUES(801460868,'�lan obitelji','Hrvatska',257);
INSERT INTO osig_pac VALUES(257, TO_DATE('2015-03-29','YYYY-MM-DD'),TO_DATE('2045-03-21','YYYY-MM-DD'),801460868);
INSERT INTO dopunsko_osiguranje VALUES(200343418,'Croatia');
INSERT INTO dop_pac VALUES(257, TO_DATE('2019-05-11','YYYY-MM-DD'), TO_DATE('2020-05-09','YYYY-MM-DD'),257, 200343418);

INSERT INTO anamneza VALUES (467,'Nemogu�nost mokrenja',TO_DATE('2019-12-24','YYYY-MM-DD'),257);
INSERT INTO dijag_anamneza VALUES(521,467,'N21.0');
INSERT INTO pretraga VALUES(310,'Urin',null,TO_DATE('2019-12-25','YYYY-MM-DD'),521);
INSERT INTO anamneza VALUES (469,'Povi�ena temp.,hipoglikemija',TO_DATE('2020-06-21','YYYY-MM-DD'),257);
INSERT INTO dijag_anamneza VALUES(523,469,'C22.0');
INSERT INTO terapija VALUES(215,'1x dnevno',523,1021);
INSERT INTO uputnice VALUES(152,'A2','Onkologija','prvi pregled',523);
INSERT INTO pretraga VALUES(311,'Krv','KKS',TO_DATE('2020-06-23','YYYY-MM-DD'),523);


INSERT INTO pacijent VALUES(258,'Mislav','Karlovi�','M',TO_DATE('1978-01-23','YYYY-MM-DD'), 'Europska avenija 2,Osijek','0956963783',4);
INSERT INTO osnovno_zdravstveno VALUES(915880015,'nositelj','Hrvatska',258);
INSERT INTO osig_pac VALUES(258, TO_DATE('2003-01-17','YYYY-MM-DD'),TO_DATE('2033-01-09','YYYY-MM-DD'),915880015);
INSERT INTO dopunsko_osiguranje VALUES(990711235,'Generali');
INSERT INTO dop_pac VALUES(258, TO_DATE('2020-03-02','YYYY-MM-DD'), TO_DATE('2021-03-01','YYYY-MM-DD'),258, 990711235);

INSERT INTO anamneza VALUES (470,'Mu�nina,povra�anje,bol u trbuhu',TO_DATE('2020-06-13','YYYY-MM-DD'),258);
INSERT INTO dijag_anamneza VALUES(524,470,'K35.9');
INSERT INTO pretraga VALUES(312,'Feces',null,TO_DATE('2020-06-14','YYYY-MM-DD'),524);
INSERT INTO anamneza VALUES (471,'Osjetljivost i oteknu�e sinusa',TO_DATE('2019-10-08','YYYY-MM-DD'),258);
INSERT INTO dijag_anamneza VALUES(525,471,'J01.2');
INSERT INTO bolovanja VALUES(2091, TO_DATE('2019-10-08','YYYY-MM-DD'),TO_DATE('2019-10-11','YYYY-MM-DD'),'A0 bolest',525);
INSERT INTO pretraga VALUES(313,'Krv','KKS',TO_DATE('2019-10-09','YYYY-MM-DD'),525);


INSERT INTO pacijent VALUES(259,'Zdenko','Horjan','M',TO_DATE('1959-11-25','YYYY-MM-DD'), '�upanijska 25,Osijek','0987538735',4);
INSERT INTO osnovno_zdravstveno VALUES(139047183,'nositelj','Hrvatska',259);
INSERT INTO osig_pac VALUES(259, TO_DATE('1984-11-18','YYYY-MM-DD'),TO_DATE('2014-11-11','YYYY-MM-DD'),139047183);
INSERT INTO dopunsko_osiguranje VALUES(326324209,'Croatia');
INSERT INTO dop_pac VALUES(259, TO_DATE('2019-03-16','YYYY-MM-DD'), TO_DATE('2020-03-14','YYYY-MM-DD'),259, 326324209);

INSERT INTO anamneza VALUES (472,'POvi�ena temp. plikovi i osip na ko�i',TO_DATE('2020-03-15','YYYY-MM-DD'),259);
INSERT INTO dijag_anamneza VALUES(526,472,'B01');
INSERT INTO terapija VALUES(216,'0,0,1',526,1006);
INSERT INTO uputnice VALUES(153,'A2','Infektologija','prvi pregled',526);
INSERT INTO anamneza VALUES (473,'Nemogu�nost mokrenja',TO_DATE('2019-07-25','YYYY-MM-DD'),259);
INSERT INTO dijag_anamneza VALUES(527,473,'N21.0');
INSERT INTO bolovanja VALUES(2092, TO_DATE('2019-07-25','YYYY-MM-DD'),TO_DATE('2019-07-29','YYYY-MM-DD'),'A0 bolest',527);
INSERT INTO pretraga VALUES(314,'Krv','BIOKEMIJA',TO_DATE('2019-07-28','YYYY-MM-DD'),527);


INSERT INTO pacijent VALUES(260,'Zvonko','Cubri�','M',TO_DATE('1970-09-11','YYYY-MM-DD'), 'Srijemska 8,Osijek','0916141988',1);
INSERT INTO osnovno_zdravstveno VALUES(195861684,'nositelj','Hrvatska',260);
INSERT INTO osig_pac VALUES(260, TO_DATE('1995-09-05','YYYY-MM-DD'),TO_DATE('2025-08-28','YYYY-MM-DD'),195861684);
INSERT INTO dopunsko_osiguranje VALUES(479729037,'Generali');
INSERT INTO dop_pac VALUES(260, TO_DATE('2019-08-07','YYYY-MM-DD'), TO_DATE('2020-08-05','YYYY-MM-DD'),260, 479729037);

INSERT INTO anamneza VALUES (474,'Nemo�,gubitak teka,visoka temp.',TO_DATE('2019-01-24','YYYY-MM-DD'),260);
INSERT INTO dijag_anamneza VALUES(528,474,'J10.0');
INSERT INTO terapija VALUES(217,'0,0,1',528,1014);
INSERT INTO anamneza VALUES (475,'Temp. normalna,crvenilo o�nih spojnica i ka�alj',TO_DATE('2020-03-18','YYYY-MM-DD'),260);
INSERT INTO dijag_anamneza VALUES(529,475,'Z23.7');
INSERT INTO cijepljenje VALUES(74,9,529);
INSERT INTO dijag_anamneza VALUES(530,475,'A37.0');


INSERT INTO pacijent VALUES(261,'Leonarda','Grguri�','F',TO_DATE('1995-03-02','YYYY-MM-DD'), 'Vratni�ka 89,Osijek','0918042900',3);
INSERT INTO osnovno_zdravstveno VALUES(536181430,'nositelj','Hrvatska',261);
INSERT INTO osig_pac VALUES(261, TO_DATE('2020-02-24','YYYY-MM-DD'),TO_DATE('2050-02-16','YYYY-MM-DD'),536181430);
INSERT INTO dopunsko_osiguranje VALUES(563668154,'Croatia');
INSERT INTO dop_pac VALUES(261, TO_DATE('2019-02-24','YYYY-MM-DD'), TO_DATE('2020-02-23','YYYY-MM-DD'),261, 563668154);

INSERT INTO anamneza VALUES (476,'Povisen tlak',TO_DATE('2019-08-15','YYYY-MM-DD'),261);
INSERT INTO dijag_anamneza VALUES(531,476,'I10');
INSERT INTO uputnice VALUES(154,'A2','Nefrologija','prvi pregled',531);
INSERT INTO uputnice VALUES(155,'A2','Kardiologija','prvi pregled',531);


INSERT INTO pacijent VALUES(262,'Larisa','Hude','F',TO_DATE('1994-02-17','YYYY-MM-DD'), 'Kozja�ka 13,Osijek','0955446818',2);
INSERT INTO osnovno_zdravstveno VALUES(139905422,'nositelj','Hrvatska',262);
INSERT INTO osig_pac VALUES(262, TO_DATE('2019-02-11','YYYY-MM-DD'),TO_DATE('2049-02-03','YYYY-MM-DD'),139905422);
INSERT INTO dopunsko_osiguranje VALUES(1256490,'HZZO');
INSERT INTO dop_pac VALUES(262, TO_DATE('2019-12-06','YYYY-MM-DD'), TO_DATE('2020-12-04','YYYY-MM-DD'),262, 1256490);

INSERT INTO anamneza VALUES (477,'Temp. normalna,crvenilo o�nih spojnica i ka�alj',TO_DATE('2020-01-18','YYYY-MM-DD'),262);
INSERT INTO dijag_anamneza VALUES(532,477,'Z23.7');
INSERT INTO cijepljenje VALUES(75,9,532);
INSERT INTO dijag_anamneza VALUES(533,477,'A37.0');


INSERT INTO pacijent VALUES(263,'Dina','Magi�','F',TO_DATE('1964-09-24','YYYY-MM-DD'), 'Wilsonova 38,Osijek','0955887017',3);
INSERT INTO osnovno_zdravstveno VALUES(651628830,'nositelj','Hrvatska',263);
INSERT INTO osig_pac VALUES(263, TO_DATE('1989-09-18','YYYY-MM-DD'),TO_DATE('2019-09-11','YYYY-MM-DD'),651628830);
INSERT INTO dopunsko_osiguranje VALUES(4939219,'HZZO');
INSERT INTO dop_pac VALUES(263, TO_DATE('2019-11-15','YYYY-MM-DD'), TO_DATE('2020-11-13','YYYY-MM-DD'),263, 4939219);

INSERT INTO anamneza VALUES (478,'Crveilo ko�e,bol i uko�enost zglobova',TO_DATE('2020-03-09','YYYY-MM-DD'),263);
INSERT INTO dijag_anamneza VALUES(534,478,'M00.0');
INSERT INTO uputnice VALUES(156,'A2','Ortopedija','prvi pregled',534);
INSERT INTO anamneza VALUES (479,'Sumnja na rak bubrega',TO_DATE('2019-07-10','YYYY-MM-DD'),263);
INSERT INTO dijag_anamneza VALUES(535,479,'C64.0');
INSERT INTO bolovanja VALUES(2093, TO_DATE('2019-07-10','YYYY-MM-DD'),TO_DATE('2019-07-17','YYYY-MM-DD'),'A0 bolest',535);
INSERT INTO pretraga VALUES(315,'Krv','BIOKEMIJA',TO_DATE('2019-07-11','YYYY-MM-DD'),535);


INSERT INTO pacijent VALUES(264,'Danijela','Mato�evi�','F',TO_DATE('1991-04-04','YYYY-MM-DD'), 'Vratni�ka 24,Osijek','0983080641',4);
INSERT INTO osnovno_zdravstveno VALUES(378220481,'nositelj','Hrvatska',264);
INSERT INTO osig_pac VALUES(264, TO_DATE('2016-03-28','YYYY-MM-DD'),TO_DATE('2046-03-21','YYYY-MM-DD'),378220481);
INSERT INTO dopunsko_osiguranje VALUES(928425897,'Generali');
INSERT INTO dop_pac VALUES(264, TO_DATE('2019-10-05','YYYY-MM-DD'), TO_DATE('2020-10-03','YYYY-MM-DD'),264, 928425897);

INSERT INTO anamneza VALUES (480,'Bolno mokrenje,povi�ena temp.',TO_DATE('2019-09-16','YYYY-MM-DD'),264);
INSERT INTO dijag_anamneza VALUES(536,480,'N15.1');
INSERT INTO anamneza VALUES (481,'Grlobolja,uve�ani krajnici',TO_DATE('2020-05-22','YYYY-MM-DD'),264);
INSERT INTO dijag_anamneza VALUES(537,481,'J02');


INSERT INTO pacijent VALUES(265,'Barbara','Kova�','F',TO_DATE('1981-04-19','YYYY-MM-DD'), 'Srijemska 62,Osijek','0993975509',1);
INSERT INTO osnovno_zdravstveno VALUES(846300114,'nositelj','Hrvatska',265);
INSERT INTO osig_pac VALUES(265, TO_DATE('2006-04-13','YYYY-MM-DD'),TO_DATE('2036-04-05','YYYY-MM-DD'),846300114);
INSERT INTO dopunsko_osiguranje VALUES(402967600,'Generali');
INSERT INTO dop_pac VALUES(265, TO_DATE('2019-09-16','YYYY-MM-DD'), TO_DATE('2020-09-14','YYYY-MM-DD'),265, 402967600);

INSERT INTO anamneza VALUES (482,'Sumnja na rak bubrega',TO_DATE('2020-06-06','YYYY-MM-DD'),265);
INSERT INTO dijag_anamneza VALUES(538,482,'C64.0');
INSERT INTO anamneza VALUES (483,'Povi�ena temp.,hipoglikemija',TO_DATE('2020-06-16','YYYY-MM-DD'),265);
INSERT INTO dijag_anamneza VALUES(539,483,'C22.0');
INSERT INTO bolovanja VALUES(2094, TO_DATE('2020-06-16','YYYY-MM-DD'),TO_DATE('2020-06-19','YYYY-MM-DD'),'A0 bolest',539);
INSERT INTO terapija VALUES(218,'3x dnevno',539,1021);
INSERT INTO pretraga VALUES(316,'Krv','BIOKEMIJA',TO_DATE('2020-06-18','YYYY-MM-DD'),539);


INSERT INTO pacijent VALUES(266,'Leon','Na�','M',TO_DATE('1945-06-27','YYYY-MM-DD'), 'Srijemska 40,Osijek','0986709693',1);
INSERT INTO osnovno_zdravstveno VALUES(848072629,'nositelj','Hrvatska',266);
INSERT INTO osig_pac VALUES(266, TO_DATE('2010-06-11','YYYY-MM-DD'),TO_DATE('2040-06-03','YYYY-MM-DD'),848072629);
INSERT INTO dopunsko_osiguranje VALUES(563068569,'Generali');
INSERT INTO dop_pac VALUES(266, TO_DATE('2019-03-23','YYYY-MM-DD'), TO_DATE('2020-03-21','YYYY-MM-DD'),266, 563068569);

INSERT INTO anamneza VALUES (484,'Povisen tlak',TO_DATE('2019-03-19','YYYY-MM-DD'),266);
INSERT INTO dijag_anamneza VALUES(540,484,'I10');
INSERT INTO terapija VALUES(219,'1,0,0',540,1008);


INSERT INTO pacijent VALUES(267,'Leo','Ivanek','M',TO_DATE('1971-05-18','YYYY-MM-DD'), 'Srijemska 147,Osijek','0957495646',4);
INSERT INTO osnovno_zdravstveno VALUES(662774435,'nositelj','Hrvatska',267);
INSERT INTO osig_pac VALUES(267, TO_DATE('1996-05-11','YYYY-MM-DD'),TO_DATE('2026-05-04','YYYY-MM-DD'),662774435);
INSERT INTO dopunsko_osiguranje VALUES(895591141,'Allianz');
INSERT INTO dop_pac VALUES(267, TO_DATE('2019-06-05','YYYY-MM-DD'), TO_DATE('2020-06-03','YYYY-MM-DD'),267, 895591141);

INSERT INTO anamneza VALUES (485,'Tvrda ko�a,gubitak dlake',TO_DATE('2020-04-25','YYYY-MM-DD'),267);
INSERT INTO dijag_anamneza VALUES(541,485,'M34.0');
INSERT INTO anamneza VALUES (486,'Bolno mokrenje,povi�ena temp.',TO_DATE('2019-07-19','YYYY-MM-DD'),267);
INSERT INTO dijag_anamneza VALUES(542,486,'N15.1');
INSERT INTO bolovanja VALUES(2095, TO_DATE('2019-07-19','YYYY-MM-DD'),TO_DATE('2019-07-28','YYYY-MM-DD'),'A0 bolest',542);
INSERT INTO pretraga VALUES(317,'Krv','KKS',TO_DATE('2019-07-25','YYYY-MM-DD'),542);


INSERT INTO pacijent VALUES(268,'Lucija','Balatinac','F',TO_DATE('2016-08-05','YYYY-MM-DD'), 'Drinska 142,Osijek','0985069374',3);
INSERT INTO osnovno_zdravstveno VALUES(409412120,'�lan obitelji','Hrvatska',268);
INSERT INTO osig_pac VALUES(268, TO_DATE('2016-08-05','YYYY-MM-DD'),TO_DATE('2046-07-29','YYYY-MM-DD'),409412120);
INSERT INTO dopunsko_osiguranje VALUES(635049735,'Allianz');
INSERT INTO dop_pac VALUES(268, TO_DATE('2019-06-24','YYYY-MM-DD'), TO_DATE('2020-06-22','YYYY-MM-DD'),268, 635049735);

INSERT INTO anamneza VALUES (487,'Bolno mokrenje,povi�ena temp.',TO_DATE('2020-03-15','YYYY-MM-DD'),268);
INSERT INTO dijag_anamneza VALUES(543,487,'N15.1');
INSERT INTO anamneza VALUES (488,'Nemo�,gubitak teka,visoka temp.',TO_DATE('2019-11-24','YYYY-MM-DD'),268);
INSERT INTO dijag_anamneza VALUES(544,488,'J10.0');


INSERT INTO pacijent VALUES(269,'Domagoj','Sabo','M',TO_DATE('1944-10-28','YYYY-MM-DD'), 'Zagreba�ka 22,Osijek','0959296076',2);
INSERT INTO osnovno_zdravstveno VALUES(473149106,'nositelj','Hrvatska',269);
INSERT INTO osig_pac VALUES(269, TO_DATE('2009-10-12','YYYY-MM-DD'),TO_DATE('2039-10-05','YYYY-MM-DD'),473149106);
INSERT INTO dopunsko_osiguranje VALUES(305594971,'Allianz');
INSERT INTO dop_pac VALUES(269, TO_DATE('2019-07-19','YYYY-MM-DD'), TO_DATE('2020-07-17','YYYY-MM-DD'),269, 305594971);

INSERT INTO anamneza VALUES (489,'Mu�nina,povra�anje,bol u trbuhu',TO_DATE('2020-03-22','YYYY-MM-DD'),269);
INSERT INTO dijag_anamneza VALUES(545,489,'K35.9');
INSERT INTO pretraga VALUES(318,'Feces',null,TO_DATE('2020-03-23','YYYY-MM-DD'),545);
INSERT INTO anamneza VALUES (490,'Temp. normalna,crvenilo o�nih spojnica i ka�alj',TO_DATE('2020-06-19','YYYY-MM-DD'),269);
INSERT INTO dijag_anamneza VALUES(546,490,'Z23.7');
INSERT INTO cijepljenje VALUES(76,9,546);
INSERT INTO dijag_anamneza VALUES(547,490,'A37.0');


INSERT INTO pacijent VALUES(270,'Zrinka','�abi�','F',TO_DATE('2004-07-25','YYYY-MM-DD'), 'Zagreba�ka 81,Osijek','0987665635',3);
INSERT INTO osnovno_zdravstveno VALUES(804181500,'�lan obitelji','Hrvatska',270);
INSERT INTO osig_pac VALUES(270, TO_DATE('2004-07-25','YYYY-MM-DD'),TO_DATE('2034-07-18','YYYY-MM-DD'),804181500);
INSERT INTO dopunsko_osiguranje VALUES(5174471,'HZZO');
INSERT INTO dop_pac VALUES(270, TO_DATE('2019-06-06','YYYY-MM-DD'), TO_DATE('2020-06-04','YYYY-MM-DD'),270, 5174471);

INSERT INTO anamneza VALUES (491,'Grlobolja,uve�ani krajnici',TO_DATE('2020-03-26','YYYY-MM-DD'),270);
INSERT INTO dijag_anamneza VALUES(548,491,'J02');
INSERT INTO terapija VALUES(220,'1x dnevno',548,1014);
INSERT INTO pretraga VALUES(319,'Krv','DKS',TO_DATE('2020-03-28','YYYY-MM-DD'),548);


INSERT INTO pacijent VALUES(271,'Dora','Sabi�','F',TO_DATE('1996-05-30','YYYY-MM-DD'), 'Zagreba�ka 127,Osijek','0996587433',3);
INSERT INTO osnovno_zdravstveno VALUES(402830251,'nositelj','Hrvatska',271);
INSERT INTO osig_pac VALUES(271, TO_DATE('2021-05-24','YYYY-MM-DD'),TO_DATE('2051-05-17','YYYY-MM-DD'),402830251);
INSERT INTO dopunsko_osiguranje VALUES(680925163,'Generali');
INSERT INTO dop_pac VALUES(271, TO_DATE('2019-12-06','YYYY-MM-DD'), TO_DATE('2020-12-04','YYYY-MM-DD'),271, 680925163);

INSERT INTO anamneza VALUES (492,'Temp. normalna,crvenilo o�nih spojnica i ka�alj',TO_DATE('2019-10-03','YYYY-MM-DD'),271);
INSERT INTO dijag_anamneza VALUES(549,492,'Z23.7');
INSERT INTO cijepljenje VALUES(77,9,549);
INSERT INTO dijag_anamneza VALUES(550,492,'A37.0');


INSERT INTO pacijent VALUES(272,'Ivana','Lerinc','F',TO_DATE('2008-11-20','YYYY-MM-DD'), 'Ilirska 23,Osijek','0919669253',2);
INSERT INTO osnovno_zdravstveno VALUES(989551537,'�lan obitelji','Hrvatska',272);
INSERT INTO osig_pac VALUES(272, TO_DATE('2008-11-20','YYYY-MM-DD'),TO_DATE('2038-11-13','YYYY-MM-DD'),989551537);
INSERT INTO dopunsko_osiguranje VALUES(476056242,'Allianz');
INSERT INTO dop_pac VALUES(272, TO_DATE('2020-03-18','YYYY-MM-DD'), TO_DATE('2021-03-17','YYYY-MM-DD'),272, 476056242);

INSERT INTO anamneza VALUES (493,'Crveni,ljuskav osip na tjemenu',TO_DATE('2019-12-05','YYYY-MM-DD'),272);
INSERT INTO dijag_anamneza VALUES(551,493,'L21.0');
INSERT INTO anamneza VALUES (494,'POvi�ena temp. plikovi i osip na ko�i',TO_DATE('2019-03-28','YYYY-MM-DD'),272);
INSERT INTO dijag_anamneza VALUES(552,494,'B01');
INSERT INTO terapija VALUES(221,'0,0,1',552,1003);


INSERT INTO pacijent VALUES(273,'Nina','Krnjai�','F',TO_DATE('1951-04-27','YYYY-MM-DD'), 'Vukovarska 137,Osijek','0912441858',1);
INSERT INTO osnovno_zdravstveno VALUES(682733510,'nositelj','Hrvatska',273);
INSERT INTO osig_pac VALUES(273, TO_DATE('2016-04-10','YYYY-MM-DD'),TO_DATE('2046-04-03','YYYY-MM-DD'),682733510);
INSERT INTO dopunsko_osiguranje VALUES(846767991,'Generali');
INSERT INTO dop_pac VALUES(273, TO_DATE('2019-07-23','YYYY-MM-DD'), TO_DATE('2020-07-21','YYYY-MM-DD'),273, 846767991);

INSERT INTO anamneza VALUES (495,'Osjetljivost i oteknu�e sinusa',TO_DATE('2019-06-22','YYYY-MM-DD'),273);
INSERT INTO dijag_anamneza VALUES(553,495,'J01.2');
INSERT INTO terapija VALUES(222,'1,0,1',553,1014);
INSERT INTO anamneza VALUES (496,'Crveni,ljuskav osip na tjemenu',TO_DATE('2020-05-06','YYYY-MM-DD'),273);
INSERT INTO dijag_anamneza VALUES(554,496,'L21.0');


INSERT INTO pacijent VALUES(274,'Matej','Zrinski','M',TO_DATE('1997-08-20','YYYY-MM-DD'), 'Mlinska 24,Osijek','0983179644',4);
INSERT INTO osnovno_zdravstveno VALUES(762174589,'nositelj','Hrvatska',274);
INSERT INTO osig_pac VALUES(274, TO_DATE('2022-08-14','YYYY-MM-DD'),TO_DATE('2052-08-06','YYYY-MM-DD'),762174589);
INSERT INTO dopunsko_osiguranje VALUES(161923213,'Generali');
INSERT INTO dop_pac VALUES(274, TO_DATE('2020-03-10','YYYY-MM-DD'), TO_DATE('2021-03-09','YYYY-MM-DD'),274, 161923213);

INSERT INTO anamneza VALUES (497,'Sumnja na rak bubrega',TO_DATE('2019-11-25','YYYY-MM-DD'),274);
INSERT INTO dijag_anamneza VALUES(555,497,'C64.0');
INSERT INTO terapija VALUES(223,'3x dnevno',555,1021);
INSERT INTO anamneza VALUES (498,'Nemogu�nost mokrenja',TO_DATE('2020-03-12','YYYY-MM-DD'),274);
INSERT INTO dijag_anamneza VALUES(556,498,'N21.0');
INSERT INTO uputnice VALUES(157,'A2','Neurologija','prvi pregled',556);
INSERT INTO pretraga VALUES(320,'Krv','DKS',TO_DATE('2020-03-13','YYYY-MM-DD'),556);


INSERT INTO pacijent VALUES(275,'Nika','Rimaj','F',TO_DATE('2018-10-24','YYYY-MM-DD'), 'Drinska 18,Osijek','0952134127',4);
INSERT INTO osnovno_zdravstveno VALUES(675548184,'�lan obitelji','Hrvatska',275);
INSERT INTO osig_pac VALUES(275, TO_DATE('2018-10-24','YYYY-MM-DD'),TO_DATE('2048-10-16','YYYY-MM-DD'),675548184);
INSERT INTO dopunsko_osiguranje VALUES(636430686,'Croatia');
INSERT INTO dop_pac VALUES(275, TO_DATE('2019-07-29','YYYY-MM-DD'), TO_DATE('2020-07-27','YYYY-MM-DD'),275, 636430686);

INSERT INTO anamneza VALUES (499,'kontrola i cijepljenje',TO_DATE('2019-02-21','YYYY-MM-DD'),275);
INSERT INTO dijag_anamneza VALUES(557,499,'Z27.2');
INSERT INTO dijag_anamneza VALUES(558,499,'Z24.0');
INSERT INTO dijag_anamneza VALUES(559,499,'Z24.6');
INSERT INTO cijepljenje VALUES(78,2,557);
INSERT INTO cijepljenje VALUES(79,3,558);
INSERT INTO cijepljenje VALUES(80,4,559);


INSERT INTO pacijent VALUES(276,'Kristijan','Hrbi�','M',TO_DATE('1945-05-18','YYYY-MM-DD'), 'Srijemska 70,Osijek','0993322655',4);
INSERT INTO osnovno_zdravstveno VALUES(241455804,'nositelj','Hrvatska',276);
INSERT INTO osig_pac VALUES(276, TO_DATE('2010-05-02','YYYY-MM-DD'),TO_DATE('2040-04-24','YYYY-MM-DD'),241455804);
INSERT INTO dopunsko_osiguranje VALUES(894005293,'Allianz');
INSERT INTO dop_pac VALUES(276, TO_DATE('2020-01-04','YYYY-MM-DD'), TO_DATE('2021-01-02','YYYY-MM-DD'),276, 894005293);

INSERT INTO anamneza VALUES (500,'Nemogu�nost mokrenja',TO_DATE('2019-01-04','YYYY-MM-DD'),276);
INSERT INTO dijag_anamneza VALUES(560,500,'N21.0');
INSERT INTO pretraga VALUES(321,'Urin',null,TO_DATE('2019-01-05','YYYY-MM-DD'),560);
INSERT INTO terapija VALUES(224,'3x dnevno',560,1020);
INSERT INTO anamneza VALUES (502,'Grlobolja,uve�ani krajnici',TO_DATE('2019-05-31','YYYY-MM-DD'),276);
INSERT INTO dijag_anamneza VALUES(562,502,'J02');
INSERT INTO uputnice VALUES(158,'A2','Otorinolaringologija','prvi pregled',562);
INSERT INTO pretraga VALUES(322,'Krv','DKS',TO_DATE('2019-06-03','YYYY-MM-DD'),562);


INSERT INTO pacijent VALUES(277,'Timon','Rabi','M',TO_DATE('1964-07-17','YYYY-MM-DD'), '�upanijska 104,Osijek','0992145223',4);
INSERT INTO osnovno_zdravstveno VALUES(981334822,'nositelj','Hrvatska',277);
INSERT INTO osig_pac VALUES(277, TO_DATE('1989-07-11','YYYY-MM-DD'),TO_DATE('2019-07-04','YYYY-MM-DD'),981334822);
INSERT INTO dopunsko_osiguranje VALUES(154107996,'Croatia');
INSERT INTO dop_pac VALUES(277, TO_DATE('2020-02-02','YYYY-MM-DD'), TO_DATE('2021-01-31','YYYY-MM-DD'),277, 154107996);

INSERT INTO anamneza VALUES (503,'Crveilo ko�e,bol i uko�enost zglobova',TO_DATE('2019-12-05','YYYY-MM-DD'),277);
INSERT INTO dijag_anamneza VALUES(563,503,'M00.0');
INSERT INTO anamneza VALUES (504,'Povi�ena temp.,hipoglikemija',TO_DATE('2020-03-24','YYYY-MM-DD'),277);
INSERT INTO dijag_anamneza VALUES(564,504,'C22.0');
INSERT INTO terapija VALUES(225,'1x dnevno',564,1021);


INSERT INTO pacijent VALUES(278,'Domagoj','�tibi','M',TO_DATE('1991-01-10','YYYY-MM-DD'), 'Srijemska 125,Osijek','0984145086',4);
INSERT INTO osnovno_zdravstveno VALUES(945802049,'nositelj','Hrvatska',278);
INSERT INTO osig_pac VALUES(278, TO_DATE('2016-01-04','YYYY-MM-DD'),TO_DATE('2045-12-27','YYYY-MM-DD'),945802049);
INSERT INTO dopunsko_osiguranje VALUES(8524790,'HZZO');
INSERT INTO dop_pac VALUES(278, TO_DATE('2019-11-07','YYYY-MM-DD'), TO_DATE('2020-11-05','YYYY-MM-DD'),278, 8524790);

INSERT INTO anamneza VALUES (505,'Crveilo ko�e,bol i uko�enost zglobova',TO_DATE('2020-03-27','YYYY-MM-DD'),278);
INSERT INTO dijag_anamneza VALUES(565,505,'M00.0');
INSERT INTO pretraga VALUES(323,'Krv','BIOKEMIJA',TO_DATE('2020-03-28','YYYY-MM-DD'),565);


INSERT INTO pacijent VALUES(279,'Matko','Andabaka','M',TO_DATE('1958-04-09','YYYY-MM-DD'), 'Ilirska 40,Osijek','0958079236',4);
INSERT INTO osnovno_zdravstveno VALUES(135424224,'nositelj','Hrvatska',279);
INSERT INTO osig_pac VALUES(279, TO_DATE('1983-04-03','YYYY-MM-DD'),TO_DATE('2013-03-26','YYYY-MM-DD'),135424224);
INSERT INTO dopunsko_osiguranje VALUES(366023189,'Generali');
INSERT INTO dop_pac VALUES(279, TO_DATE('2020-06-10','YYYY-MM-DD'), TO_DATE('2021-06-09','YYYY-MM-DD'),279, 366023189);

INSERT INTO anamneza VALUES (506,'Bolno mokrenje,povi�ena temp.',TO_DATE('2019-07-23','YYYY-MM-DD'),279);
INSERT INTO dijag_anamneza VALUES(566,506,'N15.1');
INSERT INTO bolovanja VALUES(2096, TO_DATE('2019-07-23','YYYY-MM-DD'),TO_DATE('2019-07-24','YYYY-MM-DD'),'A0 bolest',566);
INSERT INTO uputnice VALUES(159,'A2','Neurologija','prvi pregled',566);
INSERT INTO pretraga VALUES(324,'Krv','BIOKEMIJA',TO_DATE('2019-07-24','YYYY-MM-DD'),566);


INSERT INTO pacijent VALUES(280,'Melisa','Su�ec','F',TO_DATE('1995-02-02','YYYY-MM-DD'), 'Kozja�ka 84,Osijek','0911041214',3);
INSERT INTO osnovno_zdravstveno VALUES(990306638,'nositelj','Hrvatska',280);
INSERT INTO osig_pac VALUES(280, TO_DATE('2020-01-27','YYYY-MM-DD'),TO_DATE('2050-01-19','YYYY-MM-DD'),990306638);
INSERT INTO dopunsko_osiguranje VALUES(502487846,'Allianz');
INSERT INTO dop_pac VALUES(280, TO_DATE('2019-04-10','YYYY-MM-DD'), TO_DATE('2020-04-08','YYYY-MM-DD'),280, 502487846);

INSERT INTO anamneza VALUES (507,'Temp. normalna,crvenilo o�nih spojnica i ka�alj',TO_DATE('2019-03-31','YYYY-MM-DD'),280);
INSERT INTO dijag_anamneza VALUES(567,507,'Z23.7');
INSERT INTO cijepljenje VALUES(81,9,567);
INSERT INTO dijag_anamneza VALUES(568,507,'A37.0');


INSERT INTO pacijent VALUES(281,'Nada','Sikra','F',TO_DATE('2018-08-17','YYYY-MM-DD'), 'Wilsonova 61,Osijek','0997916180',1);
INSERT INTO osnovno_zdravstveno VALUES(788353680,'�lan obitelji','Hrvatska',281);
INSERT INTO osig_pac VALUES(281, TO_DATE('2018-08-17','YYYY-MM-DD'),TO_DATE('2048-08-09','YYYY-MM-DD'),788353680);
INSERT INTO dopunsko_osiguranje VALUES(850790993,'Croatia');
INSERT INTO dop_pac VALUES(281, TO_DATE('2020-04-12','YYYY-MM-DD'), TO_DATE('2021-04-11','YYYY-MM-DD'),281, 850790993);

INSERT INTO anamneza VALUES (508,'kontrola i cijepljenje',TO_DATE('2018-12-15','YYYY-MM-DD'),281);
INSERT INTO dijag_anamneza VALUES(569,508,'Z27.2');
INSERT INTO dijag_anamneza VALUES(570,508,'Z24.0');
INSERT INTO dijag_anamneza VALUES(571,508,'Z24.6');
INSERT INTO cijepljenje VALUES(82,2,569);
INSERT INTO cijepljenje VALUES(83,3,570);
INSERT INTO cijepljenje VALUES(84,4,571);

---(ordinacija1)---
select *
from pretraga;
INSERT INTO anamneza VALUES (1,'Ka�lje,svrbe ju o�i i ote�ano di�e','10/04/2018',17);
INSERT INTO dijag_anamneza VALUES(1,1,'J45.0');
INSERT INTO terapija VALUES(1,'1,0,0',1,1001);
INSERT INTO anamneza VALUES (2,'Crvenilo na ko�i,svrab te vodeni mjehuri�i na rukama','20/01/2019',17);
INSERT INTO dijag_anamneza VALUES(2,2,'B02');
INSERT INTO terapija VALUES(2,'2',2,1002);
INSERT INTO pretraga VALUES(100,'Krv','BIOKEMIJA','22/01/2019',2);
INSERT INTO anamneza VALUES (003,'Trazi pregled za voza�ku dozvolu','10/05/2019',17);
INSERT INTO dijag_anamneza VALUES(3,3,'Z04.2');
INSERT INTO anamneza VALUES (4,'Stala nogom na ekser','15/08/2018',17);
INSERT INTO dijag_anamneza VALUES(4,4,'Z23.5');
INSERT INTO cijepljenje VALUES(1,1,4);
----
INSERT INTO anamneza VALUES (5,'Proljev,gr�evi i neugoda u �eludcu','10/02/2015',16);
INSERT INTO dijag_anamneza VALUES(5,5,'A09');
INSERT INTO terapija VALUES(3,'2x1 dnevno',5,1003);
INSERT INTO terapija VALUES(4,'3x1 dnevno',5,1004);
INSERT INTO uputnice VALUES(1,'C2','Hitna medicina','pregled i obrada',5);
INSERT INTO bolovanja VALUES(2000,'10/02/2015','15/02/2015','A0 bolest',5);
INSERT INTO anamneza VALUES (6,'Lupanje srca,vrtoglavica','26/04/2018',16);
INSERT INTO dijag_anamneza VALUES(6,6,'F41.2');
INSERT INTO terapija VALUES(5,'1x1 dnevno',6,1005);
INSERT INTO uputnice VALUES(2,'A2','Psihijatrija','kontrolni pregled',6);
INSERT INTO pretraga VALUES(101,'Krv','PV','29/04/2018',6);
----
INSERT INTO anamneza VALUES (7,'Grlo crveno,visoka temepratura i pove�anje limfnih �vorova','10/03/2019',18);
INSERT INTO dijag_anamneza VALUES(7,7,'J03.9');
INSERT INTO terapija VALUES(6,'2x1 dnevno',7,1006);
INSERT INTO bolovanja VALUES(2001,'10/03/2019','17/03/2019','A0 bolest',7);
INSERT INTO anamneza VALUES (8,'Slabost,vrtoglavica','20/08/2019',18);
INSERT INTO dijag_anamneza VALUES(8,8,'D50');
INSERT INTO pretraga VALUES(102,'Krv','KKS','21/08/2019',8);
INSERT INTO anamneza VALUES (36,'Dijete mu je bolesno','11/02/2020',18);
INSERT INTO dijag_anamneza VALUES(43,36,'J03.9');
INSERT INTO bolovanja VALUES(2012,'11/02/2020','15/02/2020','F2 bolest',43);
---
INSERT INTO anamneza VALUES (9,'pregled djeteta','10/12/2018',19);
INSERT INTO dijag_anamneza VALUES(9,9,'Z00.1');
INSERT INTO anamneza VALUES (10,'kontrola i cijepljenje','20/02/2019',19);
INSERT INTO dijag_anamneza VALUES(10,10,'Z27.2');
INSERT INTO dijag_anamneza VALUES(11,10,'Z24.0');
INSERT INTO dijag_anamneza VALUES(12,10,'Z24.6');
INSERT INTO cijepljenje VALUES(2,2,10);
INSERT INTO cijepljenje VALUES(3,3,11);
INSERT INTO cijepljenje VALUES(4,4,12);
----
INSERT INTO anamneza VALUES (11,'Osje�a bol pri mokrenju par dana','25/10/2019',20);
INSERT INTO dijag_anamneza VALUES(13,11,'R30');
INSERT INTO terapija VALUES(8,'1x1 dnevno',13,1007);
INSERT INTO pretraga(pretragaID,naziv_pretrage,datum,dijag_anamnezaID) VALUES(103,'Urin','26/10/2019',13);
INSERT INTO anamneza VALUES (12,'Bolovi u ramenu ,slaba pokretljivost','11/09/2019',20);
INSERT INTO dijag_anamneza VALUES(14,12,'M75');
INSERT INTO terapija VALUES(9,'3x1 dnevno',14,1008);
INSERT INTO bolovanja VALUES(2003,'11/09/2019','13/09/2019','A0 bolest',14);
INSERT INTO uputnice VALUES(3,'A2','Radiologija','RTG ramena',14);
----
INSERT INTO anamneza VALUES (13,'Stala nogom na ekser','10/04/2018',21);
INSERT INTO dijag_anamneza VALUES(15,13,'Z23.5');
INSERT INTO cijepljenje VALUES(5,1,15);
INSERT INTO anamneza VALUES (14,'Visok tlak ,glavobolja','18/02/2020',21);
INSERT INTO dijag_anamneza VALUES(16,14,'I10');
INSERT INTO terapija VALUES(10,'1,0,0',16,1008);
INSERT INTO pretraga VALUES(104,'Krv','PV','20/02/2020',16);
INSERT INTO pretraga VALUES(325,'Krv','KKS','28/02/2020',16);
-----
INSERT INTO anamneza VALUES (15,'U�estala glavobolja popra�ena povra�anjem ponekad','25/01/2020',22);
INSERT INTO dijag_anamneza VALUES(17,15,'R51');
INSERT INTO terapija VALUES(11,'1x1 dnevno',17,1010);
INSERT INTO uputnice VALUES(4,'A3','Neurologija','pregled i obrada',17);
INSERT INTO bolovanja VALUES(2004,'25/01/2020','28/01/2020','A0 bolest',17);
INSERT INTO pretraga VALUES(105,'Krv','KKS','26/01/2020',17);
INSERT INTO anamneza VALUES (16,'Visok tlak ,glavobolja,trnci u lijevoj ruci','11/05/2020',22);
INSERT INTO dijag_anamneza VALUES(18,16,'I10');
INSERT INTO uputnice VALUES(5,'C2','Hitna medicina','pregled i obrada',18);
INSERT INTO pretraga VALUES(326,'Krv','KKS','24/05/2020',18);
----
INSERT INTO anamneza VALUES (17,'Trvda,krvava stolica te jako rijetka','10/01/2020',23);
INSERT INTO dijag_anamneza VALUES(19,17,'R51');
INSERT INTO uputnice VALUES(6,'A3','Interni prijem','kolonoskopija',19);
INSERT INTO pretraga(pretragaID,naziv_pretrage,datum,dijag_anamnezaID) VALUES(106,'Feces','12/01/2020',19);
INSERT INTO anamneza VALUES (18,'Visok tlak,ne pomazu stari lijekovi','11/05/2020',23);
INSERT INTO dijag_anamneza VALUES(20,18,'I10');
INSERT INTO terapija VALUES(12,'1,0,0',20,1008);
INSERT INTO pretraga VALUES(107,'Krv','KKS','13/05/2020',20);
INSERT INTO pretraga VALUES(108,'Krv','SE','13/05/2020',20);
----
INSERT INTO anamneza VALUES (19,'Lupanje srca,vrtoglavica te bolno mokrenje','10/04/2020',24);
INSERT INTO dijag_anamneza VALUES(21,19,'F41.2');
INSERT INTO dijag_anamneza VALUES(22,19,'R30');
INSERT INTO bolovanja VALUES(2005,'10/04/2020','12/04/2020','A0 bolest',21);
INSERT INTO terapija VALUES(13,'1x1 dnevno',21,1005);
INSERT INTO uputnice VALUES(7,'A2','Psihijatrija','pregled i obrada',21);
INSERT INTO pretraga (pretragaID,naziv_pretrage,datum,dijag_anamnezaID) VALUES(109,'Urin','11/04/2020',22);
-----
INSERT INTO anamneza VALUES (20,'Grlo izrazito crveno,suhi ka�alj','10/07/2015',25);
INSERT INTO dijag_anamneza VALUES(23,19,'J03.0');
INSERT INTO terapija VALUES(14,'1,0,1',23,1011);
----
INSERT INTO anamneza VALUES (21,'Te�ko di�e,gnojni iscijedak','10/04/2018',26);
INSERT INTO dijag_anamneza VALUES(24,21,'J01');
INSERT INTO terapija VALUES(15,'1x1 dnevno',24,1013);
INSERT INTO bolovanja VALUES(2006,'10/04/2020','15/04/2020','A0 bolest',24);
INSERT INTO uputnice VALUES(8,'A2','Otorinolaringologija','pregled i obrada',24);
INSERT INTO anamneza VALUES (22,'U�estale glavobolje te grlobolja','23/10/2019',26);
INSERT INTO dijag_anamneza VALUES(25,22,'J01');
INSERT INTO dijag_anamneza VALUES(26,22,'J03.0');
INSERT INTO pretraga VALUES(327,'Krv','KKS','29/10/2019',26);
INSERT INTO bolovanja VALUES(2007,'23/10/2019','26/10/2019','A0 bolest',25);
INSERT INTO terapija VALUES(16,'1x1 dnevno',26,1012);
INSERT INTO uputnice VALUES(9,'D2','Algologija','pregled i obrada',25);
-----
INSERT INTO anamneza VALUES (23,'Tvrd stomak, andominalni bolovi','09/04/2018',27);
INSERT INTO dijag_anamneza VALUES(27,23,'K40');
INSERT INTO terapija VALUES(17,'2x1 dnevno',27,1014);
INSERT INTO bolovanja VALUES(2008,'09/03/2018','15/03/2018','A0 bolest',27);
INSERT INTO uputnice VALUES(10,'B2','Gastroenterologija','operativni zahvat',27);
INSERT INTO anamneza VALUES (24,'Bol u �eludcu,mu�nina','10/12/2018',27);
INSERT INTO dijag_anamneza VALUES(28,24,'B98');
INSERT INTO pretraga (pretragaID,naziv_pretrage,datum,dijag_anamnezaID) VALUES(110,'Feces','11/12/2018',28);
INSERT INTO anamneza VALUES (31,'Dijete mu je bolesno','20/04/2020',27);
INSERT INTO dijag_anamneza VALUES(35,31,'J03.9');
INSERT INTO bolovanja VALUES(2011,'20/04/2020','26/04/2020','F1 bolest',35);
-----
INSERT INTO anamneza VALUES (25,'Veruke u podru�ju noge','29/03/2017',28);
INSERT INTO dijag_anamneza VALUES(29,25,'B07');
INSERT INTO cijepljenje VALUES(6, 3,29);
INSERT INTO uputnice VALUES(11,'D2','Pedijatrija','spaljivanje bradavica',29);
INSERT INTO anamneza VALUES (26,'Vari�ele','02/02/2019',28);
INSERT INTO dijag_anamneza VALUES(30,26,'B01');
INSERT INTO terapija VALUES(18,'3x1 dnevno',30,1015);
---
INSERT INTO anamneza VALUES (27,'Arterijska hipertenzija,tahikardija i znojenje(gornji trbuh)','30/03/2016',29);
INSERT INTO dijag_anamneza VALUES(31,27,'C47.1');
INSERT INTO bolovanja VALUES(2009,'31/03/2016','05/04/2016','A0 bolest',31);
INSERT INTO pretraga VALUES(111,'Krv','BIOKEMIJA','02/04/2016',31);
INSERT INTO pretraga VALUES(112,'Krv','KKS','02/04/2016',31);
INSERT INTO uputnice VALUES(12,'A2','Endokrinologija','pregled',31);
INSERT INTO anamneza VALUES (28,'Kontrola','10/04/2016',29);
INSERT INTO dijag_anamneza VALUES(32,28,'C47.1');
INSERT INTO uputnice VALUES(13,'B2','Endokrinologija','operativni zahvat',32);
----
INSERT INTO anamneza VALUES (29,'Svrab,crvenilo ko�e na rukama i le�ima','30/03/2019',30);
INSERT INTO dijag_anamneza VALUES(33,29,'L20');
INSERT INTO terapija VALUES(19,'3x1 dnevno',33,1016);
INSERT INTO uputnice VALUES(100,'A2','Dermatologija','pregled',33);
INSERT INTO anamneza VALUES (30,'Rana uzrokvana �eljezom,ote�ano hodanje','03/08/2016',30);
INSERT INTO dijag_anamneza VALUES(34,30,'Z23.5');
INSERT INTO bolovanja VALUES(2010,'04/08/2016','07/08/2016','A0 bolest',34);
INSERT INTO cijepljenje VALUES(7,1,34);
----
INSERT INTO anamneza VALUES (32,'Zamagljen vid,dvoslike','26/06/2019',46);
INSERT INTO dijag_anamneza VALUES(36,32,'H25');
INSERT INTO dijag_anamneza VALUES(37,32,'H40.0');
INSERT INTO uputnice VALUES(14,'A1','Oftamologija','prvi pregled',36);
INSERT INTO uputnice VALUES(15,'A2','Kabinet za glaukom','pregled',37);
INSERT INTO anamneza VALUES (33,'Slabost,vrtoglavica,puno spava','01/04/2020',46);
INSERT INTO dijag_anamneza VALUES(38,33,'D50');
INSERT INTO pretraga VALUES(113,'Krv','BIOKEMIJA','03/04/2020',38);
INSERT INTO pretraga VALUES(114,'Krv','KKS','03/04/2020',38);
---
INSERT INTO anamneza VALUES (34,'Otok i crvenilo na tonzilama,temp. 38.1','30/03/2019',47);
INSERT INTO dijag_anamneza VALUES(39,34,'J02');
INSERT INTO terapija VALUES(20,'1,0,1',39,1017);
INSERT INTO anamneza VALUES (35,'kontrola i cijepljenje','20/02/2019',47);
INSERT INTO dijag_anamneza VALUES(40,35,'Z27.2');
INSERT INTO dijag_anamneza VALUES(41,35,'Z24.0');
INSERT INTO dijag_anamneza VALUES(42,35,'Z24.6');
INSERT INTO cijepljenje VALUES(8,2,40);
INSERT INTO cijepljenje VALUES(9,3,41);
INSERT INTO cijepljenje VALUES(10,4,42);
---
INSERT INTO anamneza VALUES (37,'Do�ao sa hitne,navodno pao sa bicikla,rana na ruci(10 �avova)','31/05/2020',54);
INSERT INTO dijag_anamneza VALUES(44,37,'T1.1');
INSERT INTO terapija VALUES(21,'2x1 dnevno',44,1008);
INSERT INTO bolovanja VALUES(2013,'01/06/2020','03/06/2020','A0 bolest',44);
INSERT INTO anamneza VALUES (38,'Rana uzrokvana �eljezom,ote�ano hodanje','03/07/2019',54);
INSERT INTO dijag_anamneza VALUES(45,38,'Z23.5');
INSERT INTO cijepljenje VALUES(12,1,45);
----
INSERT INTO anamneza VALUES (39,'bol u trbuhu,uve�ana palpabilna slezena','15/05/2020',53);
INSERT INTO dijag_anamneza VALUES(46,39,'R16.1');
INSERT INTO bolovanja VALUES(2014,'16/05/2020','17/05/2020','A0 bolest',46);
INSERT INTO pretraga VALUES(115,'Krv','DKS','16/05/2020',46);
INSERT INTO pretraga VALUES(116,'Krv','KKS','16/05/2020',46);
INSERT INTO pretraga VALUES(117,'Krv','HB1.C','16/05/2020',46);
INSERT INTO anamneza VALUES (40,'Hipertenzija','03/07/2019',53);
INSERT INTO dijag_anamneza VALUES(47,40,'I10');
INSERT INTO terapija VALUES(22,'1,0,0',47,1018);
----
INSERT INTO anamneza VALUES (41,'telefonski navodi u�estalo mokrenje malih koli�ina uz peckanje','15/05/2020',48);
INSERT INTO dijag_anamneza VALUES(48,41,'N30');
INSERT INTO terapija VALUES(23,'1,0,1',48,1020);
INSERT INTO pretraga VALUES(118,'Urin','DKS','16/05/2020',48);
----
INSERT INTO anamneza VALUES (42,'Ugrizao ga pas,povr�inska rana desne potk.','09/05/2020',55);
INSERT INTO dijag_anamneza VALUES(49,42,'W54');
INSERT INTO dijag_anamneza VALUES(50,42,'Z23.5');
INSERT INTO cijepljenje VALUES(13,1,50);
-----
INSERT INTO anamneza VALUES (43,'Citopunkcijski ustanovljena maligna tvroba,osoba depresivna','22/04/2020',52);
INSERT INTO dijag_anamneza VALUES(51,43,'C50');
INSERT INTO dijag_anamneza VALUES(52,43,'F32');
INSERT INTO terapija VALUES(24,'1,0,1',52,1021);
INSERT INTO pretraga VALUES(328,'Krv','KKS','30/04/2020',51);
INSERT INTO pretraga VALUES(329,'Krv','DKS','30/04/2020',51);
INSERT INTO uputnice VALUES(16,'A2','Onkologija','prvi pregled',51);
----
INSERT INTO anamneza VALUES (44,'Svrbe�,crvenilo i peckanje oka','01/04/2020',49);
INSERT INTO dijag_anamneza VALUES(53,44,'H10');
INSERT INTO terapija VALUES(25,'1,0,1',53,1022);
INSERT INTO terapija VALUES(26,'1,0,1',53,1023);

--
INSERT INTO anamneza VALUES (45,'Gnojni uretalni iscijedak,disurija,pozitivna na gonoreju','01/02/2020',50);
INSERT INTO dijag_anamneza VALUES(54,45,'H10');
INSERT INTO terapija VALUES(27,'1,0,1',54,1024);
INSERT INTO anamneza VALUES (46,'Svrbe�,crvenilo i peckanje oka','14/04/2020',50);
INSERT INTO dijag_anamneza VALUES(55,46,'H10');
INSERT INTO terapija VALUES(28,'1,0,1',55,1022);
INSERT INTO terapija VALUES(29,'1,0,1',55,1023);
---
INSERT INTO anamneza VALUES (47,'U�estale migrene','07/03/2020',51);
INSERT INTO dijag_anamneza VALUES(56,47,'G43.8');
INSERT INTO terapija VALUES(30,'1x1 dnevno(2)',56,1025);
INSERT INTO pretraga VALUES(330,'Krv','DKS','15/03/2020',56);
INSERT INTO uputnice VALUES(17,'A2','Algologija','Kontrolni pregled',56);
INSERT INTO uputnice VALUES(18,'D2','Kabinet za glaukom','akumpuktura',56);
INSERT INTO anamneza VALUES (48,'Pacijentica pala sa stabla,ozlijedila ruku','07/05/2019',51);
INSERT INTO dijag_anamneza VALUES(57,48,'W14');
INSERT INTO bolovanja VALUES(2015,'16/05/2020','17/05/2020','A0 bolest',57);
---
INSERT INTO anamneza VALUES (49,'Ka�lje,svrbe ga o�i i ote�ano di�e','28/04/2020',56);
INSERT INTO dijag_anamneza VALUES(58,49,'J45.0');
INSERT INTO terapija VALUES(31,'1,0,0',58,1001);
INSERT INTO anamneza VALUES (50,'Trazi pregled za voza�ku dozvolu','05/05/2019',56);
INSERT INTO dijag_anamneza VALUES(59,50,'Z04.2');
INSERT INTO anamneza VALUES (51,'Proljev,gr�evi i mu�nina','23/02/2020',56);
INSERT INTO dijag_anamneza VALUES(60,51,'A09');
INSERT INTO terapija VALUES(32,'2x1 dnevno',60,1003);
INSERT INTO terapija VALUES(33,'3x1 dnevno',60,1004);
INSERT INTO uputnice VALUES(20,'C2','Hitna medicina','pregled i obrada',60);
INSERT INTO anamneza VALUES (52,'Grlo crveno,visoka temepratura 39,5 i pove�anje limfnih �vorova','10/08/2019',56);
INSERT INTO dijag_anamneza VALUES(61,52,'J03.9');
INSERT INTO terapija VALUES(34,'3x1 dnevno',61,1006);
INSERT INTO bolovanja VALUES(2016,'10/08/2019','17/08/2019','A0 bolest',61);
----
INSERT INTO anamneza VALUES (53,'Ozljeda na radu,ubo se na �avao','15/03/2020',57);
INSERT INTO dijag_anamneza VALUES(62,53,'Z23.5');
INSERT INTO cijepljenje VALUES(14,1,62);
INSERT INTO anamneza VALUES (54,'Grlo crveno,visoka temepratura 39,5','10/06/2020',57);
INSERT INTO dijag_anamneza VALUES(63,54,'J15.1');
INSERT INTO terapija VALUES(35,'3x1 dnevno',63,1012);
INSERT INTO bolovanja VALUES(2017,'10/06/2020','17/06/2020','A0 bolest',63);
INSERT INTO anamneza VALUES (55,'Anksioznost','16/04/2020',57);
INSERT INTO dijag_anamneza VALUES(64,55,'F41.2');
INSERT INTO terapija VALUES(36,'1x1 dnevno',64,1005);
INSERT INTO uputnice VALUES(21,'A2','Psihijatrija','kontrolni pregled',64);
INSERT INTO pretraga VALUES(119,'Krv','KKS','18/04/2020',64);
INSERT INTO pretraga VALUES(120,'Krv','BIOKEMIJA','18/04/2020',64);
----
INSERT INTO anamneza VALUES (56,'Grlo crveno,visoka temepratura i pove�anje limfnih �vorova','10/05/2019',58);
INSERT INTO dijag_anamneza VALUES(65,56,'J03.9');
INSERT INTO terapija VALUES(37,'2x1 dnevno',65,1006);
INSERT INTO bolovanja VALUES(2018,'10/05/2019','17/05/2019','A0 bolest',65);
INSERT INTO anamneza VALUES (57,'Slabost,vrtoglavica','20/08/2019',58);
INSERT INTO dijag_anamneza VALUES(66,57,'D50');
INSERT INTO pretraga VALUES(121,'Krv','KKS','22/08/2019',66);
---
INSERT INTO anamneza VALUES (58,'pregled djeteta','10/12/2010',63);
INSERT INTO dijag_anamneza VALUES(67,58,'Z00.1');
INSERT INTO anamneza VALUES (59,'kontrola i cijepljenje','20/03/2010',63);
INSERT INTO dijag_anamneza VALUES(68,59,'Z27.2');
INSERT INTO dijag_anamneza VALUES(69,59,'Z24.0');
INSERT INTO dijag_anamneza VALUES(70,59,'Z24.6');
INSERT INTO cijepljenje VALUES(15,2,68);
INSERT INTO cijepljenje VALUES(16,3,69);
INSERT INTO cijepljenje VALUES(17,4,70);
INSERT INTO anamneza VALUES (73,'Grlo crveno,visoka temepratura','10/05/2019',63);
INSERT INTO dijag_anamneza VALUES(71,73,'J03.9');
INSERT INTO terapija VALUES(38,'2x1 dnevno',71,1012);
----
INSERT INTO anamneza VALUES (60,'Osje�a bol pri mokrenju par dana','25/04/2020',59);
INSERT INTO dijag_anamneza VALUES(72,60,'R30');
INSERT INTO terapija VALUES(39,'1x1 dnevno',72,1007);
INSERT INTO uputnice VALUES(22,'A2','Nefrologija','prvi pregled',72);
INSERT INTO pretraga(pretragaID,naziv_pretrage,datum,dijag_anamnezaID) VALUES(122,'Urin','26/04/2020',72);
INSERT INTO anamneza VALUES (61,'Bolovi u koljenu ,slaba pokretljivost','11/09/2019',59);
INSERT INTO dijag_anamneza VALUES(73,61,'S10');
INSERT INTO terapija VALUES(40,'3x1 dnevno',73,1008);
INSERT INTO uputnice VALUES(23,'A2','Radiologija','RTG koljena',73);
----
INSERT INTO anamneza VALUES (62,'Dijete mu je bolesno','09/05/2020',60);
INSERT INTO dijag_anamneza VALUES(74,62,'J03.9');
INSERT INTO bolovanja VALUES(2019,'09/05/2020','15/05/2020','F2 bolest',74);
INSERT INTO anamneza VALUES (63,'Stao nogom na �avao,otvorena rana na peti','10/04/2019',60);
INSERT INTO dijag_anamneza VALUES(75,63,'Z23.5');
INSERT INTO dijag_anamneza VALUES(76,63,'S91.3');
INSERT INTO cijepljenje VALUES(18,1,75);
INSERT INTO anamneza VALUES (64,'Visok tlak ,glavobolja','19/05/2020',60);
INSERT INTO dijag_anamneza VALUES(77,64,'I10');
INSERT INTO terapija VALUES(41,'1,0,0',77,1008);
INSERT INTO pretraga VALUES(123,'Krv','PV','20/05/2020',77);
INSERT INTO pretraga VALUES(124,'Krv','KKS','20/05/2020',77);
-----
INSERT INTO anamneza VALUES (65,'U�estala glavobolja popra�ena povra�anjem','20/03/2020',61);
INSERT INTO dijag_anamneza VALUES(78,65,'R51');
INSERT INTO terapija VALUES(42,'1x1 dnevno',78,1010);
INSERT INTO uputnice VALUES(24,'A3','Neurologija','prvi pregled',78);
INSERT INTO bolovanja VALUES(2020,'20/03/2020','24/03/2020','A0 bolest',78);
INSERT INTO pretraga VALUES(125,'Krv','KKS','21/03/2020',78);
INSERT INTO pretraga VALUES(126,'Krv','DKS','21/03/2020',78);
INSERT INTO anamneza VALUES (66,'Visok tlak ,glavobolja,trnci u lijevoj ruci','11/05/2020',61);
INSERT INTO dijag_anamneza VALUES(79,66,'I10');
INSERT INTO uputnice VALUES(25,'C2','Hitna medicina','pregled i obrada',79);
----
INSERT INTO anamneza VALUES (67,'Tvrda,rijetka,krvava stolica','20/03/2020',62);
INSERT INTO dijag_anamneza VALUES(80,67,'R51');
INSERT INTO uputnice VALUES(26,'A3','Interni prijem','kolonoskopija',80);
INSERT INTO uputnice VALUES(27,'A3','Interni prijem','gastroskopija',80);
INSERT INTO pretraga(pretragaID,naziv_pretrage,datum,dijag_anamnezaID) VALUES(127,'Feces','22/03/2020',80);
INSERT INTO anamneza VALUES (68,'Visok tlak,ne pomazu stari lijekovi','11/05/2020',62);
INSERT INTO dijag_anamneza VALUES(81,68,'I10');
INSERT INTO terapija VALUES(43,'1,0,0',81,1026);
INSERT INTO pretraga VALUES(128,'Krv','KKS','13/05/2020',81);
INSERT INTO pretraga VALUES(129,'Krv','SE','13/05/2020',81);
----
INSERT INTO anamneza VALUES (69,'Lupanje srca,vrtoglavica te bolno mokrenje','12/01/2020',64);
INSERT INTO dijag_anamneza VALUES(82,69,'F41.2');
INSERT INTO dijag_anamneza VALUES(83,69,'R30');
INSERT INTO bolovanja VALUES(2021,'12/01/2020','14/01/2020','A0 bolest',83);
INSERT INTO terapija VALUES(44,'1x1 dnevno',82,1010);
INSERT INTO uputnice VALUES(28,'A2','Psihijatrija','prvi pregled',82);
INSERT INTO pretraga (pretragaID,naziv_pretrage,datum,dijag_anamnezaID) VALUES(130,'Urin','13/01/2020',83);
INSERT INTO anamneza VALUES (70,'Visok tlak,promjena terapije','11/05/2020',64);
INSERT INTO dijag_anamneza VALUES(84,70,'I10');
INSERT INTO terapija VALUES(45,'1,0,0',84,1026);
-----
INSERT INTO anamneza VALUES (71,'Grlo izrazito crveno,suhi ka�alj','10/07/2020',65);
INSERT INTO dijag_anamneza VALUES(85,71,'J03.0');
INSERT INTO terapija VALUES(46,'1,0,1',85,1012);
INSERT INTO anamneza VALUES (72,'Trvda,krvava stolica','16/05/2020',65);
INSERT INTO dijag_anamneza VALUES(86,72,'R51');
INSERT INTO pretraga(pretragaID,naziv_pretrage,datum,dijag_anamnezaID) VALUES(131,'Feces','17/05/2020',86);


commit;

----
INSERT INTO anamneza VALUES (74,'Te�ko di�e,gnojni iscijedak','17/03/2020',66);
INSERT INTO dijag_anamneza VALUES(87,74,'J01');
INSERT INTO terapija VALUES(47,'1x1 dnevno',87,1013);
INSERT INTO bolovanja VALUES(2022,'17/03/2020','21/03/2020','A0 bolest',87);
INSERT INTO uputnice VALUES(29,'A2','Otorinolaringologija','prvi pregled',87);
INSERT INTO anamneza VALUES (75,'U�estale glavobolje,grlobolja','23/11/2019',66);
INSERT INTO dijag_anamneza VALUES(88,75,'J01');
INSERT INTO dijag_anamneza VALUES(89,75,'J03.0');
INSERT INTO bolovanja VALUES(2023,'23/11/2019','26/11/2019','A0 bolest',88);
INSERT INTO terapija VALUES(48,'2x1 dnevno',89,1012);
INSERT INTO uputnice VALUES(30,'D2','Algologija','akumpuktura',88);
----
INSERT INTO anamneza VALUES (76,'Dijete joj je bolesno','09/06/2020',67);
INSERT INTO dijag_anamneza VALUES(90,76,'J03.9');
INSERT INTO bolovanja VALUES(2024,'09/06/2020','13/06/2020','F1 bolest',90);
INSERT INTO anamneza VALUES (77,'Tvrd stomak, andominalni bolovi','09/04/2019',67);
INSERT INTO dijag_anamneza VALUES(91,77,'K40');
INSERT INTO terapija VALUES(49,'3x1 dnevno',91,1014);
INSERT INTO bolovanja VALUES(2025,'09/03/2019','15/03/2019','A0 bolest',91);
INSERT INTO uputnice VALUES(31,'B2','Gastroenterologija','prvi pregled',91);
INSERT INTO anamneza VALUES (78,'Bol u �eludcu,mu�nina','10/01/2020',67);
INSERT INTO dijag_anamneza VALUES(92,78,'B98');
INSERT INTO pretraga (pretragaID,naziv_pretrage,datum,dijag_anamnezaID) VALUES(132,'Feces','11/01/2020',92);
-----
INSERT INTO anamneza VALUES (79,'Vari�ele','02/04/2020',68);
INSERT INTO dijag_anamneza VALUES(93,79,'B01');
INSERT INTO terapija VALUES(50,'3x1 dnevno',93,1015);
INSERT INTO anamneza VALUES (80,'pregled djeteta','10/12/2018',68);
INSERT INTO dijag_anamneza VALUES(94,80,'Z00.1');
INSERT INTO anamneza VALUES (81,'kontrola i cijepljenje','20/02/2019',68);
INSERT INTO dijag_anamneza VALUES(95,81,'Z27.2');
INSERT INTO dijag_anamneza VALUES(96,81,'Z24.0');
INSERT INTO dijag_anamneza VALUES(97,81,'Z24.6');
INSERT INTO cijepljenje VALUES(19,2,95);
INSERT INTO cijepljenje VALUES(20,3,96);
INSERT INTO cijepljenje VALUES(21,4,97);
---
INSERT INTO anamneza VALUES (82,'Veruke u podru�ju noge','29/03/2017',69);
INSERT INTO dijag_anamneza VALUES(98,82,'B07');
INSERT INTO cijepljenje VALUES(22, 3,98);
INSERT INTO anamneza VALUES (83,'Arterijska hipertenzija,tahikardija i znojenje(gornji trbuh)','30/07/2016',69);
INSERT INTO dijag_anamneza VALUES(99,83,'C47.1');
INSERT INTO bolovanja VALUES(2026,'31/07/2016','05/08/2016','A0 bolest',99);
INSERT INTO pretraga VALUES(133,'Krv','BIOKEMIJA','01/08/2016',99);
INSERT INTO pretraga VALUES(134,'Krv','KKS','01/08/2016',99);
INSERT INTO uputnice VALUES(32,'A2','Endokrinologija','pregled',99);
INSERT INTO anamneza VALUES (84,'Kontrola','10/08/2016',69);
INSERT INTO dijag_anamneza VALUES(100,84,'C47.1');
INSERT INTO uputnice VALUES(33,'B2','Endokrinologija','operativni zahvat',100);





----
INSERT INTO anamneza VALUES (85,'Svrab,crvenilo ko�e na rukama i ltrbuhu','30/03/2019',70);
INSERT INTO dijag_anamneza VALUES(101,85,'L20');
INSERT INTO terapija VALUES(51,'2x1 dnevno',101,1016);
INSERT INTO uputnice VALUES(34,'A2','Dermatologija','prvi pregled',101);
INSERT INTO anamneza VALUES (86,'Rana uzrokvana �eljezom,ote�ano hodanje','03/05/2020',70);
INSERT INTO dijag_anamneza VALUES(102,86,'Z23.5');
INSERT INTO bolovanja VALUES(2027,'04/05/2020','07/05/2020','A0 bolest',102);
INSERT INTO cijepljenje VALUES(23,1,102);
INSERT INTO anamneza VALUES (87,'Arterijska hipertenzija,tahikardija','30/01/2020',70);
INSERT INTO dijag_anamneza VALUES(103,87,'C47.1');
INSERT INTO pretraga VALUES(135,'Krv','BIOKEMIJA','01/02/2020',103);
INSERT INTO pretraga VALUES(136,'Krv','KKS','01/02/2020',103);
INSERT INTO uputnice VALUES(35,'A2','Endokrinologija','prvi pregled',103);
----
INSERT INTO anamneza VALUES (88,'Zamagljen vid,dvoslike','26/02/2020',71);
INSERT INTO dijag_anamneza VALUES(104,88,'H25');
INSERT INTO dijag_anamneza VALUES(105,88,'H40.0');
INSERT INTO uputnice VALUES(36,'A1','Oftamologija','prvi pregled',104);
INSERT INTO uputnice VALUES(37,'A2','Kabinet za glaukom','pregled',105);
INSERT INTO anamneza VALUES (89,'Slabost,vrtoglavica,puno spava','01/02/2020',71);
INSERT INTO dijag_anamneza VALUES(106,89,'D50');
INSERT INTO pretraga VALUES(137,'Krv','BIOKEMIJA','03/02/2020',106);
INSERT INTO pretraga VALUES(138,'Krv','KKS','03/02/2020',106);
---
INSERT INTO anamneza VALUES (90,'Otok i crvenilo na tonzilama,temp. 38.1','30/03/2020',73);
INSERT INTO dijag_anamneza VALUES(107,90,'J02');
INSERT INTO terapija VALUES(52,'1,1,1',107,1017);
INSERT INTO anamneza VALUES (91,'kontrola i cijepljenje','24/02/2008',73);
INSERT INTO dijag_anamneza VALUES(108,91,'Z27.2');
INSERT INTO dijag_anamneza VALUES(109,91,'Z24.0');
INSERT INTO dijag_anamneza VALUES(110,91,'Z24.6');
INSERT INTO cijepljenje VALUES(24,2,108);
INSERT INTO cijepljenje VALUES(25,3,109);
INSERT INTO cijepljenje VALUES(26,4,110);
---
INSERT INTO anamneza VALUES (92,'Rana na ruci(15 �avova)','31/05/2020',72);
INSERT INTO dijag_anamneza VALUES(111,92,'T1.1');
INSERT INTO terapija VALUES(53,'3x1 dnevno',111,1008);
INSERT INTO bolovanja VALUES(2028,'01/06/2020','05/06/2020','A0 bolest',111);
INSERT INTO anamneza VALUES (93,'Rana uzrokvana �eljezom,ote�ano hodanje','03/09/2019',72);
INSERT INTO dijag_anamneza VALUES(112,93,'Z23.5');
INSERT INTO cijepljenje VALUES(27,1,112);

----
INSERT INTO anamneza VALUES (94,'bol u trbuhu,uve�ana slezena','15/03/2020',74);
INSERT INTO dijag_anamneza VALUES(113,94,'R16.1');
INSERT INTO bolovanja VALUES(2029,'16/03/2020','17/03/2020','A0 bolest',113);
INSERT INTO pretraga VALUES(139,'Krv','DKS','16/03/2020',113);
INSERT INTO pretraga VALUES(140,'Krv','KKS','16/03/2020',113);
INSERT INTO pretraga VALUES(141,'Krv','HB1.C','16/03/2020',113);
INSERT INTO anamneza VALUES (95,'Hipertenzija','03/07/2018',74);
INSERT INTO dijag_anamneza VALUES(114,95,'I10');
INSERT INTO terapija VALUES(54,'1,0,0',114,1018);
----
INSERT INTO anamneza VALUES (96,'telefonski navodi u�estalo mokrenje malih koli�ina uz peckanje','06/05/2020',48);
INSERT INTO dijag_anamneza VALUES(115,96,'N30');
INSERT INTO terapija VALUES(55,'1,0,0',115,1020);
INSERT INTO pretraga VALUES(142,'Urin','DKS','07/05/2020',115);
INSERT INTO anamneza VALUES (509,'Grlo izrazito crveno,suhi ka�alj','10/07/2020',56);
INSERT INTO dijag_anamneza VALUES(590,509,'J03.0');
INSERT INTO terapija VALUES(240,'1,0,1',590,1006);

commit;









----
INSERT INTO anamneza VALUES (97,'Ugrizao ju pas,povr�inska rana desne potk.','02/06/2020',76);
INSERT INTO dijag_anamneza VALUES(116,97,'W54');
INSERT INTO dijag_anamneza VALUES(117,97,'Z23.5');
INSERT INTO cijepljenje VALUES(28,1,117);
-----
INSERT INTO anamneza VALUES (98,'Citopunkcijski ustanovljena maligna tvroba,osoba depresivna','22/04/2020',79);
INSERT INTO dijag_anamneza VALUES(118,98,'C50');
INSERT INTO dijag_anamneza VALUES(119,98,'F32');
INSERT INTO terapija VALUES(56,'1,0,0',119,1021);
INSERT INTO uputnice VALUES(38,'A2','Onkologija','prvi pregled',118);
----
INSERT INTO anamneza VALUES (99,'Svrbe�,crvenilo i peckanje oka','01/3/2020',77);
INSERT INTO dijag_anamneza VALUES(120,99,'H10');
INSERT INTO terapija VALUES(57,'1,0,1',120,1022);
INSERT INTO terapija VALUES(58,'1,0,1',120,1023);
--
INSERT INTO anamneza VALUES (100,'Gnojni uretalni iscijedak,pozitivna na gonoreju','07/02/2018',80);
INSERT INTO dijag_anamneza VALUES(121,100,'H10');
INSERT INTO terapija VALUES(59,'1,0,1',121,1024);
INSERT INTO anamneza VALUES (101,'Svrbe�,crvenilo i peckanje oka','14/05/2020',80);
INSERT INTO dijag_anamneza VALUES(122,101,'H10');
INSERT INTO terapija VALUES(60,'1,0,1',122,1022);
INSERT INTO terapija VALUES(61,'1,0,1',122,1023);
---
INSERT INTO anamneza VALUES (102,'U�estale migrene','07/03/2020',78);
INSERT INTO dijag_anamneza VALUES(123,102,'G43.8');
INSERT INTO terapija VALUES(62,'1x1 dnevno(2)',123,1025);
INSERT INTO uputnice VALUES(39,'A2','Algologija','Kontrolni pregled',123);
INSERT INTO uputnice VALUES(40,'D2','Kabinet za glaukom','akumpuktura',123);
INSERT INTO anamneza VALUES (103,'Pacijent pao sa stabla,ozlijedila nogu','07/06/2019',78);
INSERT INTO dijag_anamneza VALUES(124,103,'W14');
INSERT INTO bolovanja VALUES(2030,'16/06/2020','17/06/2020','A0 bolest',124);

INSERT INTO anamneza VALUES (104,'Astma uzrokovana alergijom','15/06/2020',81);
INSERT INTO dijag_anamneza VALUES(125,104,'J45.0');
INSERT INTO terapija VALUES(63,'1,0,0',125,1001);
INSERT INTO anamneza VALUES (105,'Trazi pregled za voza�ku dozvolu te osje�a se slabo,neispavano','08/05/2020',81);
INSERT INTO dijag_anamneza VALUES(126,105,'Z04.2');
INSERT INTO dijag_anamneza VALUES(128,105,'D50');
INSERT INTO pretraga VALUES(143,'Krv','BIOKEMIJA','10/05/2020',128);
INSERT INTO pretraga VALUES(144,'Krv','KKS','10/05/2020',128);
INSERT INTO anamneza VALUES (107,'Grlo crveno,visoka temepratura 39,5','10/06/2020',81);
INSERT INTO dijag_anamneza VALUES(127,107,'J03.9');
INSERT INTO terapija VALUES(64,'2x1 dnevno',127,1003);
INSERT INTO bolovanja VALUES(2031,'10/06/2020','15/06/2020','A0 bolest',127);
----
INSERT INTO anamneza VALUES (108,'Proljev,gr�evi i mu�nina','13/02/2020',82);
INSERT INTO dijag_anamneza VALUES(129,108,'A09');
INSERT INTO terapija VALUES(65,'2x1 dnevno',129,1003);
INSERT INTO uputnice VALUES(42,'C2','Hitna medicina','pregled i obrada',129);
INSERT INTO anamneza VALUES (109,'Kontrola','23/02/2020',82);
INSERT INTO dijag_anamneza VALUES(130,109,'A09');
INSERT INTO uputnice VALUES(43,'A2','Gastroenterologija','kontrolni pregled',130);
INSERT INTO anamneza VALUES (110,'Ozljeda na radu,strah od tetanusa','15/04/2020',82);
INSERT INTO dijag_anamneza VALUES(131,110,'Z23.5');
INSERT INTO cijepljenje VALUES(29,1,131);
INSERT INTO anamneza VALUES (111,'Grlo crveno,visoka temepratura 39,5','09/06/2020',82);
INSERT INTO dijag_anamneza VALUES(132,111,'J15.1');
INSERT INTO terapija VALUES(66,'3x1 dnevno',132,1003);
INSERT INTO bolovanja VALUES(2032,'09/06/2020','13/06/2020','A0 bolest',132);

----
INSERT INTO anamneza VALUES (112,'pregled djeteta','20/01/2019',83);
INSERT INTO dijag_anamneza VALUES(133,112,'Z00.1');
INSERT INTO anamneza VALUES (113,'kontrola i cijepljenje','20/03/2019',83);
INSERT INTO dijag_anamneza VALUES(134,113,'Z27.2');
INSERT INTO dijag_anamneza VALUES(135,113,'Z24.0');
INSERT INTO dijag_anamneza VALUES(136,113,'Z24.6');
INSERT INTO cijepljenje VALUES(30,2,134);
INSERT INTO cijepljenje VALUES(31,3,135);
INSERT INTO cijepljenje VALUES(32,4,136);
INSERT INTO anamneza VALUES (114,'Grlo crveno,visoka temepratura','10/05/2019',83);
INSERT INTO dijag_anamneza VALUES(137,114,'J03.9');
INSERT INTO terapija VALUES(67,'2x1 dnevno',137,1012);
----
INSERT INTO anamneza VALUES (115,'Grlo crveno,visoka temepratura i pove�anje limfnih �vorova','10/05/2020',84);
INSERT INTO dijag_anamneza VALUES(138,115,'J03.9');
INSERT INTO terapija VALUES(68,'2x1 dnevno',138,1006);
INSERT INTO bolovanja VALUES(2033,'10/05/2020','17/05/2020','A0 bolest',138);
INSERT INTO anamneza VALUES (116,'Slabost,vrtoglavica','20/09/2019',84);
INSERT INTO dijag_anamneza VALUES(139,116,'D50');
INSERT INTO pretraga VALUES(145,'Krv','KKS','23/09/2019',139);
INSERT INTO anamneza VALUES (117,'Osje�a bol pri mokrenju par dana','25/04/2020',84);
INSERT INTO dijag_anamneza VALUES(146,117,'R30');
INSERT INTO terapija VALUES(69,'2x1 dnevno',146,1007);
INSERT INTO uputnice VALUES(44,'A2','Nefrologija','prvi pregled',146);
INSERT INTO pretraga(pretragaID,naziv_pretrage,datum,dijag_anamnezaID) VALUES(146,'Urin','26/04/2020',146);
INSERT INTO anamneza VALUES (118,'Bolovi u koljenu ,slaba pokretljivost','11/09/2019',84);
INSERT INTO dijag_anamneza VALUES(147,118,'S10');
INSERT INTO terapija VALUES(70,'3x1 dnevno',146,1008);
INSERT INTO uputnice VALUES(45,'A2','Radiologija','RTG koljena',146);

----
INSERT INTO anamneza VALUES (119,'Dijete mu je bolesno','09/03/2020',85);
INSERT INTO dijag_anamneza VALUES(148,119,'J03.9');
INSERT INTO bolovanja VALUES(2034,'09/03/2020','13/03/2020','F2 bolest',148);
INSERT INTO anamneza VALUES (120,'Stao nogom na �avao,otvorena rana na peti','10/06/2020',85);
INSERT INTO dijag_anamneza VALUES(149,120,'Z23.5');
INSERT INTO dijag_anamneza VALUES(150,120,'S91.3');
INSERT INTO cijepljenje VALUES(33,1,149);
INSERT INTO anamneza VALUES (121,'Visok tlak(promjena terapije) ,glavobolja','19/03/2020',85);
INSERT INTO dijag_anamneza VALUES(151,121,'I10');
INSERT INTO terapija VALUES(71,'1,0,0',151,1026);
INSERT INTO pretraga VALUES(147,'Krv','PV','22/03/2020',151);
INSERT INTO pretraga VALUES(148,'Krv','KKS','22/03/2020',151);
-----
INSERT INTO anamneza VALUES (122,'U�estala glavobolja i mu�nin','15/03/2020',86);
INSERT INTO dijag_anamneza VALUES(152,122,'R51');
INSERT INTO terapija VALUES(72,'2x1 dnevno',152,1010);
INSERT INTO uputnice VALUES(46,'A3','Neurologija','prvi pregled',152);
INSERT INTO bolovanja VALUES(2035,'10/03/2020','12/03/2020','A0 bolest',152);
INSERT INTO pretraga VALUES(149,'Krv','KKS','21/03/2020',152);
INSERT INTO pretraga VALUES(150,'Krv','DKS','21/03/2020',152);
INSERT INTO anamneza VALUES (123,'Anksioznost','28/05/2020',86);
INSERT INTO dijag_anamneza VALUES(153,123,'F41.2');
INSERT INTO terapija VALUES(73,'1x1 dnevno',153,1005);
INSERT INTO uputnice VALUES(47,'A2','Psihijatrija','kontrolni pregled',153);
INSERT INTO pretraga VALUES(151,'Krv','BIOKEMIJA','30/05/2020',153);
----
INSERT INTO anamneza VALUES (124,'Tvrda,rijetka,krvava stolica','20/03/2020',87);
INSERT INTO dijag_anamneza VALUES(154,124,'R51');
INSERT INTO uputnice VALUES(48,'A3','Interni prijem','prvi pregled',154);
INSERT INTO pretraga(pretragaID,naziv_pretrage,datum,dijag_anamnezaID) VALUES(152,'Feces','22/03/2020',154);
INSERT INTO anamneza VALUES (125,'Visok tlak,ne pomazu stari lijekovi','11/05/2020',87);
INSERT INTO dijag_anamneza VALUES(155,125,'I10');
INSERT INTO terapija VALUES(74,'1,0,0',155,1026);
INSERT INTO pretraga VALUES(153,'Krv','KKS','13/05/2020',155);
INSERT INTO pretraga VALUES(154,'Krv','SE','13/05/2020',155);
----
INSERT INTO anamneza VALUES (126,'Slabost,vrtoglavica te bolno mokrenje','12/01/2020',88);
INSERT INTO dijag_anamneza VALUES(156,126,'D50');
INSERT INTO pretraga VALUES(155,'Krv','BIOKEMIJA','13/01/2020',156);
INSERT INTO dijag_anamneza VALUES(157,126,'R30');
INSERT INTO terapija VALUES(75,'1x1 dnevno',157,1010);
INSERT INTO pretraga (pretragaID,naziv_pretrage,datum,dijag_anamnezaID) VALUES(156,'Urin','13/01/2020',157);
INSERT INTO anamneza VALUES (127,'Visok tlak','11/05/2020',88);
INSERT INTO dijag_anamneza VALUES(158,127,'I10');
INSERT INTO uputnice VALUES(49,'A2','Nefrologija','prvi pregled',158);
INSERT INTO uputnice VALUES(50,'A2','Kardiologija','prvi pregled',158);
-----
INSERT INTO anamneza VALUES (128,'Grlo izrazito crveno,suhi ka�alj','22/04/2020',89);
INSERT INTO dijag_anamneza VALUES(159,128,'J03.0');
INSERT INTO terapija VALUES(76,'1,0,1',159,1012);
INSERT INTO anamneza VALUES (129,'Trvda,krvava stolica','16/05/2020',89);
INSERT INTO dijag_anamneza VALUES(160,129,'R51');
INSERT INTO pretraga(pretragaID,naziv_pretrage,datum,dijag_anamnezaID) VALUES(157,'Feces','17/05/2020',160);
INSERT INTO anamneza VALUES (130,'Visok tlak(promjena terapije)','18/05/2020',89);
INSERT INTO dijag_anamneza VALUES(161,130,'I10');
INSERT INTO terapija VALUES(77,'1,0,0',161,1026);

----
INSERT INTO anamneza VALUES (131,'Te�ko di�e,plu�a rade normalno','12/05/2020',90);
INSERT INTO dijag_anamneza VALUES(162,131,'J01');
INSERT INTO terapija VALUES(78,'2x1 dnevno',162,1012);
INSERT INTO bolovanja VALUES(2036,'12/05/2020','14/05/2020','A0 bolest',162);
INSERT INTO uputnice VALUES(51,'A2','Otorinolaringologija','prvi pregled',162);
INSERT INTO anamneza VALUES (132,'U�estale glavobolje,grlobolja','23/10/2019',90);
INSERT INTO dijag_anamneza VALUES(163,132,'J01');
INSERT INTO dijag_anamneza VALUES(164,132,'J03.0');
INSERT INTO bolovanja VALUES(2037,'23/10/2019','27/10/2019','A0 bolest',164);
INSERT INTO terapija VALUES(79,'3x1 dnevno',164,1012);
INSERT INTO uputnice VALUES(52,'D2','Algologija','akumpuktura',163);
----
commit;

INSERT INTO anamneza VALUES (133,'Dijete joj je bolesno','13/06/2020',91);
INSERT INTO dijag_anamneza VALUES(165,133,'J03.9');
INSERT INTO bolovanja VALUES(2038,'13/06/2020','16/06/2020','F1 bolest',165);
INSERT INTO anamneza VALUES (134,'Tvrd stomak, andominalni bolovi','09/02/2018',91);
INSERT INTO dijag_anamneza VALUES(166,134,'K40');
INSERT INTO terapija VALUES(80,'2x1 dnevno',166,1014);
INSERT INTO bolovanja VALUES(2039,'09/02/2018','15/02/2018','A0 bolest',166);
INSERT INTO uputnice VALUES(53,'B2','Gastroenterologija','prvi pregled',166);
INSERT INTO anamneza VALUES (135,'Bol u �eludcu,mu�nina','18/01/2020',91);
INSERT INTO dijag_anamneza VALUES(167,135,'B98');
INSERT INTO pretraga (pretragaID,naziv_pretrage,datum,dijag_anamnezaID) VALUES(158,'Feces','19/01/2020',167);
-----
INSERT INTO anamneza VALUES (136,'Vari�ele','02/06/2020',93);
INSERT INTO dijag_anamneza VALUES(168,136,'B01');
INSERT INTO terapija VALUES(81,'3x1 dnevno',168,1015);
INSERT INTO anamneza VALUES (137,'pregled djeteta','10/12/2000',93);
INSERT INTO dijag_anamneza VALUES(169,137,'Z00.1');
INSERT INTO anamneza VALUES (138,'kontrola i cijepljenje','20/02/2000',93);
INSERT INTO dijag_anamneza VALUES(170,138,'Z27.2');
INSERT INTO dijag_anamneza VALUES(171,138,'Z24.0');
INSERT INTO dijag_anamneza VALUES(172,138,'Z24.6');
INSERT INTO cijepljenje VALUES(34,2,170);
INSERT INTO cijepljenje VALUES(35,3,171);
INSERT INTO cijepljenje VALUES(36,4,172);
---

INSERT INTO anamneza VALUES (139,'Veruke u podru�ju noge','21/03/2019',92);
INSERT INTO dijag_anamneza VALUES(263,139,'B07');
INSERT INTO cijepljenje VALUES(37, 3,263);
INSERT INTO anamneza VALUES (140,'Arterijska hipertenzija,tahikardija i znojenje','30/06/2020',92);
INSERT INTO dijag_anamneza VALUES(173,140,'C47.1');
INSERT INTO bolovanja VALUES(2040,'30/06/2020','02/07/2020','A0 bolest',173);
INSERT INTO pretraga VALUES(159,'Krv','BIOKEMIJA','03/07/2020',173);
INSERT INTO pretraga VALUES(160,'Krv','KKS','03/07/2020',173);
INSERT INTO uputnice VALUES(54,'A2','Endokrinologija','pregled',173);
INSERT INTO anamneza VALUES (141,'Kontrola','10/08/2016',92);
INSERT INTO dijag_anamneza VALUES(174,141,'C47.1');
INSERT INTO uputnice VALUES(55,'B2','Endokrinologija','operativni zahvat',174);

----
INSERT INTO anamneza VALUES (142,'Visok tlak ,glavobolja,trnci u lijevoj ruci','11/05/2020',94);
INSERT INTO dijag_anamneza VALUES(175,142,'I10');
INSERT INTO uputnice VALUES(56,'C2','Hitna medicina','pregled i obrada',175);
INSERT INTO anamneza VALUES (143,'Svrab,crvenilo ko�e na trbuhu','30/03/2019',94);
INSERT INTO dijag_anamneza VALUES(176,143,'L20');
INSERT INTO terapija VALUES(82,'3x1 dnevno',176,1016);
INSERT INTO uputnice VALUES(57,'A2','Dermatologija','prvi pregled',176);
INSERT INTO anamneza VALUES (144,'Rana uzrokvana �eljezom,ote�ano hodanje','10/05/2020',94);
INSERT INTO dijag_anamneza VALUES(177,144,'Z23.5');
INSERT INTO bolovanja VALUES(2041,'10/05/2020','11/05/2020','A0 bolest',177);
INSERT INTO cijepljenje VALUES(38,1,177);
----
INSERT INTO anamneza VALUES (145,'Arterijska hipertenzija','31/03/2020',95);
INSERT INTO dijag_anamneza VALUES(178,145,'C47.1');
INSERT INTO pretraga VALUES(161,'Krv','BIOKEMIJA','01/04/2020',178);
INSERT INTO pretraga VALUES(162,'Krv','KKS','01/04/2020',178);
INSERT INTO uputnice VALUES(58,'A2','Endokrinologija','prvi pregled',178);
INSERT INTO anamneza VALUES (146,'Zamagljen vid,dvoslike','26/02/2020',95);
INSERT INTO dijag_anamneza VALUES(179,146,'H25');
INSERT INTO dijag_anamneza VALUES(180,146,'H40.0');
INSERT INTO uputnice VALUES(78,'A1','Oftamologija','prvi pregled',179);
----
INSERT INTO anamneza VALUES (147,'Slabost,vrtoglavica,puno spava','01/02/2020',96);
INSERT INTO dijag_anamneza VALUES(181,147,'D50');
INSERT INTO pretraga VALUES(163,'Krv','BIOKEMIJA','03/02/2020',181);
INSERT INTO pretraga VALUES(164,'Krv','KKS','03/02/2020',181);
INSERT INTO anamneza VALUES (148,'Osje�a bol pri mokrenju par dana','20/11/2019',96);
INSERT INTO dijag_anamneza VALUES(182,148,'R30');
INSERT INTO terapija VALUES(83,'2x1 dnevno',182,1007);
INSERT INTO pretraga(pretragaID,naziv_pretrage,datum,dijag_anamnezaID) VALUES(165,'Urin','21/11/2019',182);
-----
INSERT INTO anamneza VALUES (149,'telefonski navodi u�estalo mokrenje malih koli�ina uz peckanje','06/04/2020',97);
INSERT INTO dijag_anamneza VALUES(183,149,'N30');
INSERT INTO terapija VALUES(84,'1,0,0',183,1020);
INSERT INTO pretraga VALUES(166,'Urin','DKS','07/04/2020',183);
INSERT INTO anamneza VALUES (150,'Svrbe�,crvenilo i peckanje oka','16/05/2020',97);
INSERT INTO dijag_anamneza VALUES(184,150,'H10');
INSERT INTO terapija VALUES(85,'1,1,1',184,1022);
INSERT INTO terapija VALUES(86,'1,0,1',184,1023);
-----
INSERT INTO anamneza VALUES (151,'Otok i crvenilo na tonzilama,temp. 38.1','30/03/2020',98);
INSERT INTO dijag_anamneza VALUES(185,151,'J02');
INSERT INTO terapija VALUES(87,'1,1,1',185,1017);
INSERT INTO anamneza VALUES (152,'kontrola i cijepljenje','24/02/2009',98);
INSERT INTO dijag_anamneza VALUES(186,152,'Z27.2');
INSERT INTO dijag_anamneza VALUES(187,152,'Z24.0');
INSERT INTO cijepljenje VALUES(39,2,186);
INSERT INTO cijepljenje VALUES(40,3,187);
----
INSERT INTO anamneza VALUES (153,'Zamagljen vid,dvoslike','26/02/2020',99);
INSERT INTO dijag_anamneza VALUES(188,153,'H40.0');
INSERT INTO uputnice VALUES(59,'A2','Kabinet za glaukom','pregled',188);
INSERT INTO anamneza VALUES (154,'Slabost,vrtoglavica,puno spava','01/04/2020',99);
INSERT INTO dijag_anamneza VALUES(189,154,'D50');
INSERT INTO pretraga VALUES(167,'Krv','BIOKEMIJA','03/04/2020',189);
---
INSERT INTO anamneza VALUES (155,'Pacijent pao sa stabla,ozlijedila nogu','07/06/2020',100);
INSERT INTO dijag_anamneza VALUES(200,155,'W14');
INSERT INTO bolovanja VALUES(2042,'16/06/2020','17/06/2020','A0 bolest',200);
INSERT INTO anamneza VALUES (156,'Rana uzrokvana �eljezom,ote�ano hodanje','03/09/2019',100);
INSERT INTO dijag_anamneza VALUES(190,156,'Z23.5');
INSERT INTO cijepljenje VALUES(41,1,190);
---
INSERT INTO anamneza VALUES (157,'U�estalo mokrenje malih koli�ina uz peckanje','06/06/2020',42);
INSERT INTO dijag_anamneza VALUES(191,157,'N30');
INSERT INTO terapija VALUES(88,'1,0,1',191,1020);
INSERT INTO pretraga VALUES(168,'Urin','DKS','07/6/2020',191);
INSERT INTO anamneza VALUES (158,'Rana na ruci(15 �avova)','31/05/2020',42);
INSERT INTO dijag_anamneza VALUES(192,158,'T1.1');
INSERT INTO terapija VALUES(89,'3x1 dnevno',192,1008);
----
INSERT INTO anamneza VALUES (159,'Hipertenzija','03/07/2018',43);
INSERT INTO dijag_anamneza VALUES(193,159,'I10');
INSERT INTO terapija VALUES(90,'1,0,0',193,1026);
-----
INSERT INTO anamneza VALUES (160,'Pacijent pao sa stabla,ozlijedila nogu i ruku','07/06/2019',41);
INSERT INTO dijag_anamneza VALUES(194,160,'W14');
INSERT INTO bolovanja VALUES(2043,'16/06/2020','17/06/2020','A0 bolest',194);
INSERT INTO anamneza VALUES (161,'Hipertenzija','03/07/2018',41);
INSERT INTO dijag_anamneza VALUES(195,161,'I10');
INSERT INTO terapija VALUES(91,'1,0,0',195,1018);
----


INSERT INTO anamneza VALUES (164,'Svrbe�,crvenilo i peckanje oka','01/3/2020',44);
INSERT INTO dijag_anamneza VALUES(196,164,'H10');
INSERT INTO terapija VALUES(92,'1,1,1',196,1023);
INSERT INTO anamneza VALUES (162,'bol u trbuhu,uve�ana slezena','15/03/2020',44);
INSERT INTO dijag_anamneza VALUES(197,162,'R16.1');
INSERT INTO pretraga VALUES(169,'Krv','DKS','16/03/2020',197);
INSERT INTO pretraga VALUES(170,'Krv','KKS','16/03/2020',197);

-----
INSERT INTO anamneza VALUES (163,'Citopunkcijski ustanovljena maligna tvroba na dojci','22/06/2020',45);
INSERT INTO dijag_anamneza VALUES(198,163,'C50');
INSERT INTO dijag_anamneza VALUES(199,163,'F32');
INSERT INTO terapija VALUES(93,'1,0,1',199,1021);
INSERT INTO uputnice VALUES(60,'A2','Onkologija','prvi pregled',198);
----
INSERT INTO anamneza VALUES (195,'Osje�a bol pri mokrenju','15/05/2020',1);
INSERT INTO dijag_anamneza VALUES(201,195,'R30');
INSERT INTO terapija VALUES(110,'3x1 dnevno',201,1007);
INSERT INTO uputnice VALUES(77,'A2','Nefrologija','prvi pregled',201);
INSERT INTO pretraga(pretragaID,naziv_pretrage,datum,dijag_anamnezaID) VALUES(171,'Urin','16/05/2020',201);
INSERT INTO anamneza VALUES (165,'Visok tlak(promjena terapije)','20/03/2020',1);
INSERT INTO dijag_anamneza VALUES(202,165,'I10');
INSERT INTO terapija VALUES(94,'1,0,0',202,1026);
INSERT INTO pretraga VALUES(172,'Krv','PV','21/03/2020',202);
INSERT INTO pretraga VALUES(173,'Krv','KKS','21/03/2020',202);
----

INSERT INTO anamneza VALUES (166,'Trazi pregled za voza�ku dozvolu ','08/04/2020',2);
INSERT INTO dijag_anamneza VALUES(203,166,'Z04.2');
INSERT INTO anamneza VALUES (167,'Grlo crveno,visoka temepratura 40,5','10/06/2020',2);
INSERT INTO dijag_anamneza VALUES(204,167,'J03.9');
INSERT INTO terapija VALUES(95,'2x1 dnevno',204,1003);
INSERT INTO anamneza VALUES (168,'Osje�a se slabo,neispavano ve� mjesec dana','09/05/2020',2);
INSERT INTO dijag_anamneza VALUES(205,168,'D50');
INSERT INTO pretraga VALUES(174,'Krv','BIOKEMIJA','10/05/2020',205);
INSERT INTO pretraga VALUES(175,'Krv','KKS','10/05/2020',205);
------
INSERT INTO anamneza VALUES (169,'Visok tlak(promjena terapije)','20/08/2019',3);
INSERT INTO dijag_anamneza VALUES(206,169,'I10');
INSERT INTO terapija VALUES(96,'1,0,0',206,1018);
INSERT INTO pretraga VALUES(184,'Krv','KKS','21/08/2019',206);
INSERT INTO anamneza VALUES (170,'Stao nogom na �avao,otvorena rana na peti','10/05/2020',3);
INSERT INTO dijag_anamneza VALUES(207,170,'Z23.5');
INSERT INTO cijepljenje VALUES(42,1,207);
INSERT INTO anamneza VALUES (171,'Kontrolni pregled tjedan dana poslije,stanje se pogorsalo','23/02/2020',3);
INSERT INTO dijag_anamneza VALUES(208,171,'A09');
INSERT INTO uputnice VALUES(61,'A2','Gastroenterologija','kontrolni pregled',208);
------
INSERT INTO anamneza VALUES (172,'pregled djeteta','30/01/2019',4);
INSERT INTO dijag_anamneza VALUES(209,172,'Z00.1');
INSERT INTO anamneza VALUES (173,'kontrola i cijepljenje','20/04/2019',4);
INSERT INTO dijag_anamneza VALUES(210,173,'Z27.2');
INSERT INTO dijag_anamneza VALUES(211,173,'Z24.0');
INSERT INTO dijag_anamneza VALUES(212,173,'Z24.6');
INSERT INTO cijepljenje VALUES(43,2,210);
INSERT INTO cijepljenje VALUES(44,3,211);
INSERT INTO cijepljenje VALUES(45,4,212);
INSERT INTO anamneza VALUES (174,'Vari�ele','01/02/2020',4);
INSERT INTO dijag_anamneza VALUES(213,174,'B01');
INSERT INTO terapija VALUES(97,'3x1 dnevno',213,1015);
-------
INSERT INTO anamneza VALUES (175,'Trazi pregled za voza�ku dozvolu te osje�a se slabo,neispavano','08/09/2019',5);
INSERT INTO dijag_anamneza VALUES(214,175,'Z04.2');
INSERT INTO anamneza VALUES (176,'Grlo crveno,visoka temepratura 39,5','19/06/2020',5);
INSERT INTO dijag_anamneza VALUES(215,176,'J03.9');
INSERT INTO terapija VALUES(98,'3x1 dnevno',215,1003);
INSERT INTO anamneza VALUES (177,'Grlo crveno,visoka temepratura i pove�anje limfnih �vorova','10/05/2019',5);
INSERT INTO dijag_anamneza VALUES(216,177,'J03.9');
INSERT INTO terapija VALUES(99,'2x1 dnevno',216,1006);
------
INSERT INTO anamneza VALUES (178,'Grlo crveno,visoka temepratura 38,5','10/06/2020',6);
INSERT INTO dijag_anamneza VALUES(217,178,'J03.9');
INSERT INTO terapija VALUES(100,'2x1 dnevno',217,1006);
INSERT INTO bolovanja VALUES(2044,'10/06/2020','15/06/2020','A0 bolest',217);
INSERT INTO anamneza VALUES (179,'Astma uzrokovana alergijom','23/04/2020',6);
INSERT INTO dijag_anamneza VALUES(218,179,'J45.0');
INSERT INTO anamneza VALUES (180,'Slabost,vrtoglavica','20/11/2019',6);
INSERT INTO dijag_anamneza VALUES(219,180,'D50');
INSERT INTO pretraga VALUES(176,'Krv','KKS','21/11/2019',219);
INSERT INTO anamneza VALUES (181,'Visok tlak(promjena terapije)','28/02/2020',6);
INSERT INTO dijag_anamneza VALUES(220,181,'I10');
INSERT INTO terapija VALUES(101,'1,0,0',220,1026);
----
INSERT INTO anamneza VALUES (182,'Visok tlak','28/02/2020',7);
INSERT INTO dijag_anamneza VALUES(221,182,'I10');
INSERT INTO terapija VALUES(102,'1,0,0',221,1018);
INSERT INTO anamneza VALUES (183,'Osje�a bol pri mokrenju','12/03/2020',7);
INSERT INTO dijag_anamneza VALUES(222,183,'R30');
INSERT INTO terapija VALUES(103,'3x1 dnevno',222,1007);
INSERT INTO uputnice VALUES(62,'A2','Nefrologija','prvi pregled',222);
INSERT INTO anamneza VALUES (184,'Anksioznost,mogu�a anemija','28/03/2020',7);
INSERT INTO dijag_anamneza VALUES(223,184,'F41.2');
INSERT INTO terapija VALUES(104,'1x1 dnevno',223,1005);
INSERT INTO uputnice VALUES(63,'A2','Psihijatrija','kontrolni pregled',223);
INSERT INTO pretraga VALUES(177,'Krv','BIOKEMIJA','30/03/2020',223);
-----
INSERT INTO anamneza VALUES (185,'Visok tlak(promjena terapije)','12/06/2020',8);
INSERT INTO dijag_anamneza VALUES(224,185,'I10');
INSERT INTO terapija VALUES(105,'1,0,0',224,1026);
INSERT INTO anamneza VALUES (186,'Visok tlak ,glavobolja,vrtoglavice,gubitak ravnote�e','01/06/2020',8);
INSERT INTO dijag_anamneza VALUES(225,186,'I10');
INSERT INTO uputnice VALUES(64,'C2','Hitna medicina','pregled i obrada',225);
INSERT INTO anamneza VALUES (187,'Zamagljen vid','26/09/2018',8);
INSERT INTO dijag_anamneza VALUES(226,187,'H25');
INSERT INTO uputnice VALUES(65,'A1','Oftamologija','prvi pregled',226);
-----
INSERT INTO anamneza VALUES (188,'Trazi pregled za voza�ku dozvolu','08/01/2020',9);
INSERT INTO dijag_anamneza VALUES(227,188,'Z04.2');
INSERT INTO anamneza VALUES (189,'Grlo crveno,visoka temepratura 39,5,uve�ani limfni �vorovi','10/06/2020',9);
INSERT INTO dijag_anamneza VALUES(228,189,'J03.9');
INSERT INTO terapija VALUES(106,'3x1 dnevno',228,1006);
INSERT INTO bolovanja VALUES(2045,'10/06/2020','15/06/2020','A0 bolest',228);
INSERT INTO anamneza VALUES (190,'Vari�ele','22/08/2019',9);
INSERT INTO dijag_anamneza VALUES(229,190,'B01');
INSERT INTO terapija VALUES(107,'3x1 dnevno',229,1015);
------
INSERT INTO anamneza VALUES (191,'Trazi pregled za voza�ku dozvolu','08/09/2018',10);
INSERT INTO dijag_anamneza VALUES(230,191,'Z04.2');
INSERT INTO anamneza VALUES (192,'Astma uzrokovana alergijom','15/04/2019',10);
INSERT INTO dijag_anamneza VALUES(231,192,'J45.0');
INSERT INTO terapija VALUES(108,'1,0,0',231,1001);
INSERT INTO anamneza VALUES (193,'Proljev,gr�evi i mu�nina','24/02/2020',10);
INSERT INTO dijag_anamneza VALUES(232,193,'A09');
INSERT INTO terapija VALUES(109,'2x1 dnevno',232,1003);
INSERT INTO uputnice VALUES(66,'C2','Hitna medicina','pregled i obrada',232);
INSERT INTO anamneza VALUES (194,'Slabost,vrtoglavica te bolno mokrenje','12/06/2020',10);
INSERT INTO dijag_anamneza VALUES(234,194,'D50');
INSERT INTO pretraga VALUES(178,'Krv','BIOKEMIJA','13/06/2020',234);
-------
INSERT INTO anamneza VALUES (196,'Ozljeda na radu,cijepljenje protiv tetanusa','15/06/2020',11);
INSERT INTO dijag_anamneza VALUES(235,196,'Z23.5');
INSERT INTO cijepljenje VALUES(46,1,235);
INSERT INTO anamneza VALUES (197,'Grlo crveno,visoka temepratura 40,5','11/05/2020',11);
INSERT INTO dijag_anamneza VALUES(236,197,'J15.1');
INSERT INTO terapija VALUES(131,'2x1 dnevno',236,1006);
INSERT INTO bolovanja VALUES(2046,'09/06/2020','13/06/2020','A0 bolest',236);
INSERT INTO anamneza VALUES (198,'Dijete mu je bolesno','09/04/2020',11);
INSERT INTO dijag_anamneza VALUES(262,198,'J03.9');
INSERT INTO bolovanja VALUES(2047,'09/04/2020','12/04/2020','F2 bolest',262);
-----
INSERT INTO anamneza VALUES (199,'Proljev i mu�nina','13/02/2020',12);
INSERT INTO dijag_anamneza VALUES(237,199,'A09');
INSERT INTO terapija VALUES(111,'2x1 dnevno',237,1003);
INSERT INTO uputnice VALUES(67,'C2','Hitna medicina','pregled i obrada',237);
INSERT INTO anamneza VALUES (200,'Kontrola','23/02/2020',12);
INSERT INTO dijag_anamneza VALUES(238,200,'A09');
INSERT INTO uputnice VALUES(68,'A2','Gastroenterologija','kontrolni pregled',238);
INSERT INTO anamneza VALUES (201,'Bolovi u koljenu ,slaba pokretljivost','11/06/2020',12);
INSERT INTO dijag_anamneza VALUES(239,201,'S10');
INSERT INTO terapija VALUES(112,'2x1 dnevno',239,1014);
INSERT INTO uputnice VALUES(69,'A2','Radiologija','RTG koljena',239);
-----
INSERT INTO anamneza VALUES (202,'Grlo crveno,visoka temepratura 39,5','19/05/2020',13);
INSERT INTO dijag_anamneza VALUES(240,202,'J03.9');
INSERT INTO terapija VALUES(113,'2x1 dnevno',240,1012);
INSERT INTO anamneza VALUES (203,'U�estala glavobolja','09/06/2020',13);
INSERT INTO dijag_anamneza VALUES(241,203,'R51');
INSERT INTO terapija VALUES(114,'2x1 dnevno',241,1010);
INSERT INTO uputnice VALUES(70,'A3','Neurologija','prvi pregled',241);
-----
INSERT INTO anamneza VALUES (204,'Hipertenzija','03/09/2019',14);
INSERT INTO dijag_anamneza VALUES(242,204,'I10');
INSERT INTO terapija VALUES(115,'1,0,0',242,1026);
INSERT INTO anamneza VALUES (205,'Zamagljen vid,dvoslike','14/04/2020',14);
INSERT INTO dijag_anamneza VALUES(243,205,'H40.0');
INSERT INTO uputnice VALUES(71,'A2','Kabinet za glaukom','pregled',243);
----
INSERT INTO anamneza VALUES (206,'Dijete mu je bolesno','12/05/2020',15);
INSERT INTO dijag_anamneza VALUES(244,206,'J03.9');
INSERT INTO bolovanja VALUES(2048,'12/05/2020','16/05/2020','F2 bolest',244);
INSERT INTO anamneza VALUES (207,'Otok i crvenilo na tonzilama,temp. 38.1','30/03/2020',15);
INSERT INTO dijag_anamneza VALUES(245,207,'J02');
INSERT INTO terapija VALUES(116,'1,1,1',245,1017);
-------
INSERT INTO anamneza VALUES (208,'Hipertenzija(promjena terapije)','13/04/2020',31);
INSERT INTO dijag_anamneza VALUES(246,208,'I10');
INSERT INTO terapija VALUES(117,'1,0,0',246,1018);
----
INSERT INTO anamneza VALUES (209,'Tvrd stomak, abdominalni bolovi','09/02/2018',32);
INSERT INTO dijag_anamneza VALUES(247,209,'K40');
INSERT INTO terapija VALUES(118,'2x1 dnevno',247,1014);
INSERT INTO uputnice VALUES(72,'B2','Gastroenterologija','prvi pregled',247);
-----
INSERT INTO anamneza VALUES (210,'Astma uzrokovana alergijom','12/04/2020',33);
INSERT INTO dijag_anamneza VALUES(248,210,'J45.0');
INSERT INTO terapija VALUES(119,'1,0,0',248,1001);
INSERT INTO anamneza VALUES (211,'Trazi pregled za voza�ku dozvolu','27/02/2020',33);
INSERT INTO dijag_anamneza VALUES(249,211,'Z04.2');
-------
INSERT INTO anamneza VALUES (212,'Grlo crveno,visoka temepratura','10/05/2019',34);
INSERT INTO dijag_anamneza VALUES(250,212,'J03.9');
INSERT INTO terapija VALUES(120,'2x1 dnevno',250,1017);
INSERT INTO anamneza VALUES (213,'Slabost,vrtoglavica te bolno mokrenje','19/03/2020',34);
INSERT INTO dijag_anamneza VALUES(251,213,'D50');
INSERT INTO pretraga VALUES(179,'Krv','BIOKEMIJA','20/03/2020',251);
INSERT INTO dijag_anamneza VALUES(252,213,'R30');
INSERT INTO terapija VALUES(121,'1x1 dnevno',252,1020);
INSERT INTO pretraga (pretragaID,naziv_pretrage,datum,dijag_anamnezaID) VALUES(180,'Urin','20/03/2020',252);
-----
INSERT INTO anamneza VALUES (214,'Osje�a bol pri mokrenju du�e vrijeme','21/02/2020',35);
INSERT INTO dijag_anamneza VALUES(253,214,'R30');
INSERT INTO terapija VALUES(122,'2x1 dnevno',253,1003);
INSERT INTO pretraga(pretragaID,naziv_pretrage,datum,dijag_anamnezaID) VALUES(181,'Urin','22/02/2020',253);
INSERT INTO anamneza VALUES (215,'Kontrola,nije do�lo do pobolj�anja','26/02/2020',35);
INSERT INTO dijag_anamneza VALUES(254,215,'R30');
INSERT INTO uputnice VALUES(73,'A2','Nefrologija','prvi pregled',254);
-----
INSERT INTO anamneza VALUES (216,'Anksioznost(promjena terapije),pacijent se �ali na umor i pospanost','21/05/2020',36);
INSERT INTO dijag_anamneza VALUES(255,216,'F41.2');
INSERT INTO terapija VALUES(123,'1x1 dnevno',255,1005);
INSERT INTO uputnice VALUES(74,'A2','Psihijatrija','kontrolni pregled',255);
INSERT INTO pretraga VALUES(182,'Krv','BIOKEMIJA','23/05/2020',255);
----
INSERT INTO anamneza VALUES (217,'Vari�ele','02/09/2019',37);
INSERT INTO dijag_anamneza VALUES(256,217,'B01');
INSERT INTO terapija VALUES(124,'3x1 dnevno',256,1015);
INSERT INTO terapija VALUES(125,'2x1 dnevno',256,1016);
---
INSERT INTO anamneza VALUES (218,'Svrbe� i peckanje oka','16/04/2020',38);
INSERT INTO dijag_anamneza VALUES(257,218,'H10');
INSERT INTO terapija VALUES(126,'1,1,1',257,1022);
INSERT INTO terapija VALUES(127,'1,0,1',257,1023);
INSERT INTO anamneza VALUES (219,'Hipertenzija,par dana ima tlak 150/100','03/07/2019',38);
INSERT INTO dijag_anamneza VALUES(258,219,'I10');
INSERT INTO terapija VALUES(128,'1,0,0',258,1026);
----
INSERT INTO anamneza VALUES (220,'Vari�ele','09/03/2020',39);
INSERT INTO dijag_anamneza VALUES(259,220,'B01');
INSERT INTO terapija VALUES(129,'3x1 dnevno',259,1015);
INSERT INTO terapija VALUES(130,'2x1 dnevno',259,1016);
--------
INSERT INTO anamneza VALUES (221,'Visok tlak','11/05/2020',40);
INSERT INTO dijag_anamneza VALUES(260,221,'I10');
INSERT INTO uputnice VALUES(75,'A2','Nefrologija','prvi pregled',260);
INSERT INTO uputnice VALUES(76,'A2','Kardiologija','prvi pregled',260);
INSERT INTO anamneza VALUES (222,'Slabost,vrtoglavica','30/04/2020',40);
INSERT INTO dijag_anamneza VALUES(261,222,'D50');
INSERT INTO pretraga VALUES(183,'Krv','KKS','02/05/2020',261);


COMMIT;
