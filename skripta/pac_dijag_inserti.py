#generira 180 pacijenata,dopunsko i zdravstveno za njih te generira unosa za anamneze,terapije,bolovanja,uputnice i pretrage svakog pacijenta
#samo što ih ispise bez spola
import random
from datetime import  timedelta,date
import datetime
from dateutil.relativedelta import relativedelta
#datumi za rođenja
start_date = datetime.date(1942, 1, 1)
end_date = datetime.date(2019, 1, 1)
time_between_dates = end_date - start_date
days_between_dates = time_between_dates.days
#datumi za anamneze(dolaske doktoru)
start_date2 = datetime.date(2019, 1, 1)
end_date2 = datetime.date(2020, 6, 30)
time_between_dates2 = end_date2 - start_date2
days_between_dates2 = time_between_dates2.days



f= open("pac_dijag.txt","w+")
l3=["Kozjačka","Drinska","Vukovarska","Županijska","Mlinska","Vratnička","Zagrebačka","Europska avenija","Wilsonova","Srijemska","Ilirska"]
l4=["091","098","095","099"]
l5=["HZZO","Generali","Allianz","Croatia"]
l22=["'Z23.7'","'Z23.5'"]
l1=["Bruno","Roko","Hrvoje","Slaven","Ante","Boris","Leonarda","Boris","Matej","Margareta","Marija","Klara","Luka","Tomislav","Ivan","Margareta","Zvonimir","Vida","Robert","Bruno","Ivica","Karlo","Tomislav","Boris","Stanko","Saša","Hrvoje","Željka","Igor","Ivana","Hrvoje","Dorian","Lana","Laura","Dajana","Matej","Dominik","Antonio","Jan","Ivan","Davor","Iva","Anja","Patrik","Fran","Ana","Zdenko","Iva","Ivan","Tonka","Hrvoje","Jasmina","Bojan","David","Grgur","Jasenko","Davor","Lucija","Helena","Andrija","Petar","Vedran","Marko","Anja","Marko","Enis","Nikola","Marko","Andrej","Stefan","Marko","Patricia","Lorena","Matija","Agneza","Marija","Petar","Ivana","Lana","Ena","Matea","Iva","Ema","Luka","Marko","Adrian","Maroje","Hrvoje","Željko","Bruno","Paula","Borna","Nevena","Bruno","Josip","Davor","Krešimir","Branimir","Ivana","Dino","Lea","Nora","Mario","Sanja","Irena","Juraj","Ruža","Petra","Valentina","Zara","Fran","Filip","Luka","Leonarda","Paola","Til","Andrej","Borna","Željka","Sonja","Branko","Ivana","Silvio","Laura","Vinko","Fran","Matej","Ana","Sofija","Marina","Antonio","Marin","Juro","Mihaela","Ana","Darko","Filip","Kruno","Josipa","Ena","Lora","Barbara","Ria","Laura","Sven","Damir","Toni","Leo","Bruno","Ileana","Tina","Filip","Marko","Ante","Milan","Laura","Mislav","Zdenko","Zvonko","Leonarda","Larisa","Dina","Danijela","Barbara","Leon","Leo","Lucija","Domagoj","Zrinka","Dora","Ivana","Nina","Matej","Nika","Kristijan","Timon","Domagoj","Matko","Melisa","Nada"]
l2=["Tompa","Salic","Radman","Balog","Nevešćanin","Lasović","Nago","Nago","Prpić","Grana","Dobrošević","Božićević","Budak","Dumančić","Pervan","Trconić","Vakanjac","Medić","Vargić","Bušić","Lenard","Pataky","Bilandžić","Andrlon","Brođanac","Bajamić","Gavran","Sanseović","Sajber","Slivka","Ehman","Vugrek","Jurić","Matić","Duspara","Pfaf","Živalj","Šušovček","Lukić","Bartolović","Majetić","Škrbić","Nikitović","Omazić","Lukić","Medić","Turniški","Matačić","Izvezić","Kovačević","Pauković","Pacek","Karanović","Šajn","Krajina","Alpeza","Ivanković","Kurtović","Ladić","Katana","Vrkić","Bašić","Varga","Vitasović","Drventić","Brkić","Božić","Kunac","Stipetić","Španić","Filipović","Čorić","Spreitzer","Paris","Rukavina","Katić","Vrkić","Bogdanović","Gavrilović","Čulin","Vila","Kljajuć","Bise","Vilić","Jagustin","Buček","Raguž","Brod","Sabljić","Borozni","Herek","Duvnjak","Jovičić","Čosić","Aladrović","Milaković","Kraljević","Popović","Gnjidić","Magušić","Knežević","Grevinger","Perić","Opačak","Fišer","Žunec","Bašić","Koceić","Vitasović","Baković","Tisaj","Filošević","Tisaj","Pavlin","Horvat","Ocvirk","Vištica","Katić","Šaban","Škevin","Tokić","Srn","Anđelić","Lukasović","Medić","Andrić","Lončarić","Cvitković","Grgić","Bolfan","Antunović","Valinčić","Vidić","Lubina","Elkaz","Hodak","Lovrić","Kokot","Sabljo","Sulić","Aladrović","Medved","Vukas","Herek","Latinović","Vidović","Opačak","Rebuš","Grošić","Rosanda","Goreta","Zadravec","Rebić","Živković","Orlić","Bijelić","Karlović","Horjan","Cubrić","Grgurić","Hude","Magić","Matošević","Kovač","Nađ","Ivanek","Balatinac","Sabo","Šabić","Sabić","Lerinc","Krnjaić","Zrinski","Rimaj","Hrbić","Rabi","Štibi","Andabaka","Sušec","Sikra"]
l33=["'Sumnja na hripavac'","'Sumnja na tetanus'"]
l44=["'A37.0'" ,"'J01.2'","'J06.0'","'J10.0'","'K35.9'","'K50.0'","'L08.0'","'L21.0'","'M00.0'","'M34.0'","'N15.1'","'N21.0'","'C22.0'","'C64.0'","'B01'","'N30'","'J02'","'I10'"]
l55=["'Temp. normalna,crvenilo očnih spojnica i kašalj'","'Osjetljivost i oteknuće sinusa'","'Intezivan kašalj, otežano disanje'","'Nemoć,gubitak teka,visoka temp.'","'Mučnina,povraćanje,bol u trbuhu'","'Rane u ustima,proljev,vrućica'","'Vezikule na koži sa gnojem'","'Crveni,ljuskav osip na tjemenu'","'Crveilo kože,bol i ukočenost zglobova'","'Tvrda koža,gubitak dlake'","'Bolno mokrenje,povišena temp.'","'Nemogućnost mokrenja'","'Povišena temp.,hipoglikemija'","'Sumnja na rak bubrega'","'POvišena temp. plikovi i osip na koži'","'Bolno mokrenje'","'Grlobolja,uvećani krajnici'","'Povisen tlak'"]
odjel=["'Infektologija'","'Otorinolaringologija'","'Otorinolaringologija'","'Pulmologija'","'Gastroenterologija'","'Gastroenterologija'","'Dermatologija'","'Dermatologija'","'Ortopedija'","'Ortopedija'","'Neurologija'","'Neurologija'","'Onkologija'","'Onkologija'","'Infektologija'" ,"'Neurologija'","'Otorinolaringologija'" ,"'Kardiologija'"]
doza=["'2x dnevno'","'3x dnevno'","'1x dnevno'","'1,0,1'","'0,0,1'"]
ant=[1003,1006,1014]
k=101
anam=225
dijag=265
cijep=50
terap=150
bolov=2050
uput=101
pret=190
tip=["'KKS'","'DKS'","'BIOKEMIJA'"]
res = ["'"+i +"','" +j+"',"  for i, j in zip(l1, l2)]
res2 =[zip(l22, l33)]
res1 = [list(a) for a in zip(l44, l55,odjel)]
br=0
import io
with io.open('pac_dijag.txt', "w", encoding="utf-8") as f:
    for i in res:   
        k=k+1
        lp=random.randrange(100000000,999999999)
        random_number_of_days = random.randrange(days_between_dates)
        random_date = start_date + datetime.timedelta(days=random_number_of_days)
        diff = relativedelta(date.today(), random_date).years
        osig=random.choice(l5)
        rand_br=random.randrange(1,500)
        rand_date = date.today() - datetime.timedelta(days=rand_br)
        if osig=="HZZO":
            br=random.randrange(1000000,9999999)
        else:
            br=random.randrange(100000000,999999999)
        if diff<18:
            p="'član obitelji'"
            m=random_date
            l=random_date + datetime.timedelta(days=18*365)
        elif diff>65:
            p="'nositelj'" 
            random_number_of_days = random.randrange(days_between_dates)
            m=random_date + datetime.timedelta(days=65*365) 
            l=m+datetime.timedelta(days=35*365) 
        else:
            p="'nositelj'" 
            random_number_of_days = random.randrange(days_between_dates)
            m=random_date + datetime.timedelta(days=25*365) 
            l=m+datetime.timedelta(days=45*365)  
        f.write("INSERT INTO pacijent VALUES("+str(k)+","+i+"  ,"+"TO_DATE('"+str(random_date)+"','YYYY-MM-DD'), '" +random.choice(l3)+" "+str(random.randrange(1,150))+",Osijek','"+random.choice(l4)+str(random.randrange(1000000,9999999))+  "',"+ str(random.randrange(1,5))+");"+"\n")
        f.write("INSERT INTO osnovno_zdravstveno VALUES("+str(lp)+","+ p + ",'Hrvatska'," + str(k)+");"+"\n")
        f.write("INSERT INTO osig_pac VALUES("+str(k)+", TO_DATE('"+str(m)+"','YYYY-MM-DD'),TO_DATE('"+str(m+datetime.timedelta(days=30*365))+"','YYYY-MM-DD'),"+str(lp)+");"+"\n")

        f.write("INSERT INTO dopunsko_osiguranje VALUES("+str(br)+",'"+ osig +"');"+"\n")
        f.write("INSERT INTO dop_pac VALUES("+str(k)+", TO_DATE('"+str(rand_date)+"','YYYY-MM-DD'), TO_DATE('"+str(rand_date+datetime.timedelta(days=364))+"','YYYY-MM-DD'),"+str(k)+", "+str(br)+");"+"\n")
        f.write("\n")
        for j in range(1,3):
            bol=random.choice(res1)
            random_number_of_days2 = random.randrange(days_between_dates2)
            random_date2 = start_date2 + datetime.timedelta(days=random_number_of_days2)
            diff2 = relativedelta(date.today(), random_date2).years
            if k%15==0 and diff>30:
                f.write("INSERT INTO anamneza VALUES ("+str(anam)+",'Sumnja na tetanus',TO_DATE('" + str(random_date2)+"','YYYY-MM-DD'),"+str(k)+");"+"\n")
                f.write("INSERT INTO dijag_anamneza VALUES("+str(dijag)+","+str(anam)+",'Z23.5');"+"\n")
                f.write("INSERT INTO cijepljenje VALUES("+str(cijep)+","+str(1)+","+str(dijag)+");"+"\n")
                anam=anam+1
                dijag=dijag+1
                cijep=cijep+1
                break
            if diff>19 and bol[0]=="'I10'":
                f.write("INSERT INTO anamneza VALUES ("+str(anam)+","+bol[1]+",TO_DATE('" + str(random_date2)+"','YYYY-MM-DD'),"+str(k)+");"+"\n")
                f.write("INSERT INTO dijag_anamneza VALUES("+str(dijag)+","+str(anam)+",'I10');"+"\n")
                lek=random.randrange(1,5)
                if lek==1:
                    tlak=1026
                    f.write("INSERT INTO terapija VALUES("+str(terap)+",'1,0,0',"+str(dijag)+","+str(tlak)+");"+"\n")
                    terap=terap+1
                elif lek==2:
                    tlak=1018
                    f.write("INSERT INTO terapija VALUES("+str(terap)+",'1,0,0',"+str(dijag)+","+str(tlak)+");"+"\n")
                    terap=terap+1
                elif lek==3:
                    tlak=1008
                    f.write("INSERT INTO terapija VALUES("+str(terap)+",'1,0,0',"+str(dijag)+","+str(tlak)+");"+"\n")
                    terap=terap+1
                else:
                    f.write("INSERT INTO uputnice VALUES("+str(uput)+",'A2','Nefrologija','prvi pregled',"+str(dijag)+");"+"\n")
                    uput=uput+1
                    f.write("INSERT INTO uputnice VALUES("+str(uput)+",'A2','Kardiologija','prvi pregled',"+str(dijag)+");"+"\n")
                    uput=uput+1
                dijag=dijag+1
                anam=anam+1
                break
            if diff<15 and bol[0]=="'I10'":
                bol=random.choice(res1)
            if diff<3:
                f.write("INSERT INTO anamneza VALUES ("+str(anam)+",'kontrola i cijepljenje',TO_DATE('" + str(random_date+datetime.timedelta(days=120))+"','YYYY-MM-DD'),"+str(k)+");"+"\n")
                f.write("INSERT INTO dijag_anamneza VALUES("+str(dijag)+","+str(anam)+",'Z27.2');"+"\n")
                dijag2=dijag+1
                f.write("INSERT INTO dijag_anamneza VALUES("+str(dijag2)+","+str(anam)+",'Z24.0');"+"\n")
                dijag3=dijag2+1
                f.write("INSERT INTO dijag_anamneza VALUES("+str(dijag3)+","+str(anam)+",'Z24.6');"+"\n")

                f.write("INSERT INTO cijepljenje VALUES("+str(cijep)+","+str(2)+","+str(dijag)+");"+"\n")
                cijep=cijep+1
                f.write("INSERT INTO cijepljenje VALUES("+str(cijep)+","+str(3)+","+str(dijag2)+");"+"\n")
                cijep=cijep+1
                f.write("INSERT INTO cijepljenje VALUES("+str(cijep)+","+str(4)+","+str(dijag3)+");"+"\n")
                dijag=dijag+3
                cijep=cijep+1
                anam=anam+1
                if bol[0]!="'J02'":
                    break
            if bol[0]=="'A37.0'":
                f.write("INSERT INTO anamneza VALUES ("+str(anam)+","+bol[1]+",TO_DATE('" + str(random_date2)+"','YYYY-MM-DD'),"+str(k)+");"+"\n")
                f.write("INSERT INTO dijag_anamneza VALUES("+str(dijag)+","+str(anam)+",'Z23.7');"+"\n")
                f.write("INSERT INTO cijepljenje VALUES("+str(cijep)+","+str(9)+","+str(dijag)+");"+"\n")
                cijep=cijep+1
                dijag=dijag+1
                f.write("INSERT INTO dijag_anamneza VALUES("+str(dijag)+","+str(anam)+","+bol[0]+");"+"\n")
                dijag=dijag+1
                anam=anam+1
                break
            br=br+1
            f.write("INSERT INTO anamneza VALUES ("+str(anam)+","+bol[1]+",TO_DATE('" + str(random_date2)+"','YYYY-MM-DD'),"+str(k)+");"+"\n")
            f.write("INSERT INTO dijag_anamneza VALUES("+str(dijag)+","+str(anam)+","+str(bol[0])+");"+"\n")
            if bol[2] in ["'Otorinolaringologija'","'Pulmologija'","'Infektologija'","'Pulmologija'"]:
                if br%2==0:
                    f.write("INSERT INTO terapija VALUES("+str(terap)+","+random.choice(doza)+","+str(dijag)+","+str(random.choice(ant))+");"+"\n")
                    terap=terap+1
            if diff>30 and diff<65 and br%3==0:
                    f.write("INSERT INTO bolovanja VALUES("+str(bolov)+", TO_DATE('"+str(random_date2)+"','YYYY-MM-DD'),TO_DATE('"+str(random_date2+datetime.timedelta(days=random.randrange(1,10)))+"','YYYY-MM-DD'),'A0 bolest',"+str(dijag)+");"+"\n") 
                    bolov=bolov+1
            if bol[0]=="'C22.0'" or bol[0]=="'C64.0'":
                 if br%2==0:
                    f.write("INSERT INTO terapija VALUES("+str(terap)+","+random.choice(doza)+","+str(dijag)+","+str(1021)+");"+"\n")
                    terap=terap+1
            if br%5==0:
                f.write("INSERT INTO uputnice VALUES("+str(uput)+",'A2',"+bol[2]+",'prvi pregled',"+str(dijag)+");"+"\n")
                uput=uput+1
            if br%3==0:
                vrsta="'Krv'"
                tip2=random.choice(tip)
                f.write("INSERT INTO pretraga VALUES("+str(pret)+","+vrsta+","+tip2+",TO_DATE('" + str(random_date2+datetime.timedelta(days=random.randrange(1,8)))+"','YYYY-MM-DD'),"+str(dijag)+");"+"\n")
                pret=pret+1
                dijag=dijag+1
                anam=anam+1
                break
            if bol[0]=="'N15.0'" or bol[0]=="'N21.0'" or bol[0]=="'N30'" :
                vrsta="'Urin'"
                tip2="null"
                f.write("INSERT INTO pretraga VALUES("+str(pret)+","+vrsta+","+tip2+",TO_DATE('" + str(random_date2+datetime.timedelta(days=1))+"','YYYY-MM-DD'),"+str(dijag)+");"+"\n")
                if br%2==0:
                    br2=random.randrange(1,3)
                    if br2==1:
                        te=1020
                    else:
                        te=1007
                    f.write("INSERT INTO terapija VALUES("+str(terap)+","+random.choice(doza)+","+str(dijag)+","+str(te)+");"+"\n")
                    terap=terap+1
                pret=pret+1
                dijag=dijag+1
                anam=anam+1
            if bol[0]=="'K35.9'" or bol[0]=="'K50.0'" :
                vrsta="'Feces'"
                tip2="null"
                f.write("INSERT INTO pretraga VALUES("+str(pret)+","+vrsta+","+tip2+",TO_DATE('" + str(random_date2+datetime.timedelta(days=1))+"','YYYY-MM-DD'),"+str(dijag)+");"+"\n")
                pret=pret+1
                dijag=dijag+1
                anam=anam+1
            else:
                dijag=dijag+1
                anam=anam+1
        f.write("\n")
        f.write("\n")

        
f.close()
