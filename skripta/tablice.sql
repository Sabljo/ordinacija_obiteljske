DROP TABLE bolovanja;
DROP TABLE uputnice;
DROP TABLE terapija;
DROP TABLE lijek;
DROP TABLE pretraga;
DROP TABLE cijepljenje;
DROP TABLE cjepivo;
DROP TABLE dijag_anamneza;
DROP TABLE anamneza;
DROP TABLE dijagnoza;
DROP TABLE osig_pac;
DROP TABLE dop_pac;
DROP TABLE dopunsko_osiguranje;
DROP TABLE osnovno_zdravstveno;
DROP TABLE pacijent;
DROP TABLE ordinacija;
DROP TABLE med_sestra;
DROP TABLE doktor;
DROP TABLE zaposlenik;
DROP TABLE radno_vrijeme;
CREATE TABLE radno_vrijeme(
      radno_vrijemeID INTEGER CONSTRAINT radno_vrijeme_pk PRIMARY KEY,
      neparni_datumi VARCHAR(15) NOT NULL,
      parni_datumi VARCHAR(15) NOT NULL,
      radna_subota VARCHAR(2) NOT NULL
);
CREATE TABLE zaposlenik(
    zaposlenikID INTEGER CONSTRAINT zaposlenik_pk PRIMARY KEY,
    ime VARCHAR(15) NOT NULL,
    prezime VARCHAR(15) NOT NULL,
    adresa VARCHAR(30) NOT NULL,
    kontakt VARCHAR(15) NOT NULL,
    placa NUMBER(7,2) NOT NULL,
    tip_zaposlenika VARCHAR(10) NOT NULL
);
CREATE TABLE doktor(
	doktorID INTEGER NOT NULL CONSTRAINT doktor_fk REFERENCES zaposlenik(zaposlenikID),
	IDdoktorata NUMBER(10) NOT NULL,
	PRIMARY KEY (doktorID)
);
CREATE TABLE med_sestra(
	med_sestraID INTEGER NOT NULL CONSTRAINT med_sestra_fk REFERENCES zaposlenik(zaposlenikID),
	broj_sanitarne_knjizice NUMBER(10) NOT NULL,
	PRIMARY KEY (med_sestraID)
);
CREATE TABLE ordinacija(
    ordinacijaID INTEGER CONSTRAINT ordinacija_pk PRIMARY KEY,
    adresa VARCHAR(35) NOT NULL,
    tel VARCHAR(15) NOT NULL,
    radno_vrijemeID INTEGER NOT NULL CONSTRAINT ord_fk_vrijeme REFERENCES radno_vrijeme(radno_vrijemeID),
    doktorID INTEGER NOT NULL CONSTRAINT ord_doktor_fk REFERENCES doktor(doktorID),
    med_sestraID INTEGER NOT NULL CONSTRAINT ord_med_sestra_fk REFERENCES med_sestra(med_sestraID)
);
ALTER TABLE ordinacija ADD CONSTRAINT ord_uniq1 UNIQUE(doktorID);
ALTER TABLE ordinacija ADD CONSTRAINT ord_uniq2 UNIQUE(med_sestraID);
CREATE TABLE pacijent(
    pacijentID NUMBER(38,0) CONSTRAINT pacijent_pk PRIMARY KEY,
    ime VARCHAR(15) NOT NULL,
    prezime VARCHAR(15) NOT NULL,
    spol VARCHAR(1) NOT NULL,
    datum_rodenja DATE NOT NULL,
    adresa VARCHAR(40) NOT NULL,
    telefon VARCHAR(15) NOT NULL,
    ordinacijaID INTEGER NOT NULL CONSTRAINT pac_fk_ord REFERENCES ordinacija(ordinacijaID)
);
CREATE TABLE dopunsko_osiguranje(
	broj_police INTEGER CONSTRAINT dop_pk PRIMARY KEY,
	kuca_osiguranja VARCHAR(20) NOT NULL
);
CREATE TABLE osnovno_zdravstveno(
	MBO  INTEGER CONSTRAINT zdrv_fk PRIMARY KEY,
	nositelj_osiguranja VARCHAR(15) NOT NULL,
	drzava_osiguranja VARCHAR(20) NOT NULL,
	pacijentID NUMBER(38,0) NOT NULL CONSTRAINT osig_pacijent_fk REFERENCES pacijent(pacijentID) ON DELETE CASCADE

);

ALTER TABLE osnovno_zdravstveno ADD CONSTRAINT zdrv_uniq UNIQUE(pacijentID);

CREATE TABLE osig_pac(
	osig_pac_ID INTEGER PRIMARY KEY,
	datum_pocetka DATE NOT NULL,
    datum_isteka DATE NOT NULL,
    MBO INTEGER NOT NULL CONSTRAINT osig_zdr_fk REFERENCES osnovno_zdravstveno(MBO)ON DELETE CASCADE

);
CREATE TABLE dop_pac(
	dop_pac_ID INTEGER PRIMARY KEY,
	datum_pocetka DATE NOT NULL,
    datum_isteka DATE NOT NULL,
	pacijentID NUMBER(38,0) NOT NULL CONSTRAINT fk_pac REFERENCES pacijent(pacijentID)ON DELETE CASCADE,
	broj_police INTEGER  NOT NULL CONSTRAINT fk_polica REFERENCES dopunsko_osiguranje(broj_police)ON DELETE CASCADE

);
CREATE TABLE anamneza (
    anamnezaID NUMBER(38,0) CONSTRAINT anamneza_pk PRIMARY KEY,
    razlog_dolaska VARCHAR(100) NOT NULL,
    datum DATE DEFAULT SYSDATE NOT NULL,
    pacijentID NUMBER(38,0) NOT NULL CONSTRAINT anam_fk_pacijent REFERENCES pacijent(pacijentID)
    ON DELETE CASCADE
);
CREATE TABLE dijagnoza (
     MKB VARCHAR(8) CONSTRAINT dijag_pk PRIMARY KEY,
     opis VARCHAR(100) NOT NULL
);
CREATE TABLE dijag_anamneza(
    dijag_anamnezaID NUMBER(38,0) CONSTRAINT dijag_anamneza_pk PRIMARY KEY,
    anamnezaID NUMBER(38,0) NOT NULL CONSTRAINT anam_fk REFERENCES anamneza(anamnezaID) ON DELETE CASCADE,
    MKB VARCHAR(5) NOT NULL CONSTRAINT dijag_fk REFERENCES dijagnoza(MKB) ON DELETE CASCADE
);
CREATE TABLE lijek(
    sifra_lijeka NUMBER(38,0) CONSTRAINT lijek_pk PRIMARY KEY,
    naziv VARCHAR(30) NOT NULL,
    proizvodjac VARCHAR(20) NOT NULL,
    cijena NUMBER(5,2) NOT NULL,
    doplata NUMBER(5,2)
);
CREATE TABLE terapija(
   terapijaID NUMBER(38,0) CONSTRAINT terapija_pk PRIMARY KEY,
   doza VARCHAR(15) NOT NULL,
   dijag_anamnezaID NUMBER(38,0) NOT NULL CONSTRAINT terapija_fk_dijag REFERENCES dijag_anamneza(dijag_anamnezaID) ON DELETE CASCADE,
   sifra_lijeka NUMBER(38,0) NOT NULL CONSTRAINT terapija_fk_lijek REFERENCES lijek(sifra_lijeka)
);

CREATE TABLE bolovanja(
      bolovanjeID NUMBER(38,0) CONSTRAINT bolovanje_pk PRIMARY KEY,
      datum_pocetka DATE NOT NULL,
      datum_isteka DATE NOT NULL,
      vrsta_bolovanja VARCHAR(15) NOT NULL,
      dijag_anamnezaID NUMBER(38,0) NOT NULL CONSTRAINT bolovanje_fk_dijag REFERENCES dijag_anamneza(dijag_anamnezaID)ON DELETE CASCADE
);
ALTER TABLE bolovanja ADD CONSTRAINT bol_uniq UNIQUE(dijag_anamnezaID);
CREATE TABLE uputnice(
      uputnicaID NUMBER(38,0) CONSTRAINT uputnica_pk PRIMARY KEY,
      tip VARCHAR(2) NOT NULL,
      odjel VARCHAR(20) NOT NULL,
      trazi_se VARCHAR(25) NOT NULL,
      dijag_anamnezaID NUMBER(38,0) NOT NULL CONSTRAINT uputnica_fk_dijag REFERENCES dijag_anamneza(dijag_anamnezaID) ON DELETE CASCADE
);
CREATE TABLE pretraga(
      pretragaID NUMBER(38,0) CONSTRAINT pretraga_pk PRIMARY KEY,
      naziv_pretrage VARCHAR(15) NOT NULL,
      vrsta_krvne VARCHAR(15),
      datum DATE NOT NULL,
      dijag_anamnezaID NUMBER(38,0) NOT NULL CONSTRAINT pretraga_fk_dijag REFERENCES dijag_anamneza(dijag_anamnezaID)ON DELETE CASCADE
);
CREATE TABLE cjepivo(
      cjepivoID INTEGER CONSTRAINT cjepivo_pk PRIMARY KEY,
      naziv_cjepiva VARCHAR(20) NOT NULL
);
CREATE TABLE cijepljenje(
      cijepljenjeID NUMBER(38,0) CONSTRAINT cijepljenje_pk PRIMARY KEY,
      cjepivoID INTEGER NOT NULL CONSTRAINT cijep_cjepivo REFERENCES cjepivo(cjepivoID) ON DELETE CASCADE,
      dijag_anamnezaID NUMBER(38,0) NOT NULL CONSTRAINT cijep_fk_dijag REFERENCES dijag_anamneza(dijag_anamnezaID)ON DELETE CASCADE
);
ALTER TABLE cijepljenje ADD CONSTRAINT cijep_uniq UNIQUE(dijag_anamnezaID);
COMMIT;

