--OKIDA�I--


--ROW-LEVEL OKIDA�I

--ne moze se upisati lijek �ija je cijena 0
CREATE OR REPLACE TRIGGER trig1
BEFORE INSERT ON lijek
FOR EACH ROW WHEN
(new.cijena=0)
BEGIN
raise_application_error(-20111,'Lijek kojem je cijena 0 ne mo�e se unijeti');
END trig1;
/

insert into lijek values(30,'Nolicin','Pliva',0,23);

--ne moze unijet radno vrijeme,tako da parnim i neparnim danima se radi u isto vrijeme
CREATE OR REPLACE TRIGGER trig2
BEFORE INSERT ON radno_vrijeme
FOR EACH ROW WHEN
(new.neparni_datumi=new.parni_datumi)
BEGIN
raise_application_error(-20111,'Ordinacija ne mo�e raditi parnim i neparnim datumima u isto vrijeme');
END trig2;
/
insert into radno_vrijeme values(6,'prijepodne','prijepodne','2.');

--Okida� koji prije unosa bri�e sve praznine u MKB-u i pretvara mala u velika slova 
CREATE OR REPLACE TRIGGER trig3
BEFORE INSERT ON dijagnoza
FOR EACH ROW
BEGIN
:new.MKB := UPPER(TRIM(:new.MKB));  
END trig3;
/
--
insert into dijagnoza values(' a01.3','Paratifus C');
select *
from dijagnoza;


--TABLE-LEVEL OKIDA�I

---Okida� koji ne dopu�ta brisanje pacijenata vikendom
CREATE OR REPLACE TRIGGER trig4
BEFORE DELETE ON pacijent
DECLARE
weekend_error EXCEPTION;
BEGIN
IF(TO_CHAR(SYSDATE,'DY') in ('NED','SUB')) THEN
    RAISE weekend_error;
END IF;
EXCEPTION
    WHEN weekend_error THEN
        raise_application_error(-20111,'Vikendom se ne mogu brisati pacijenti');
END trig4;
/

SELECT *
FROM pacijent;
DELETE FROM pacijent
WHERE ordinacijaID=4;

---------------------------------------------
---------------------------------------------
---------------------------------------------

--INDEKSI--

CREATE BITMAP INDEX ix_pac_spol
ON pacijent(spol);

CREATE INDEX ix_dijag_ana
ON dijag_anamneza(anamnezaID);


CREATE INDEX ix_anam_pac
ON anamneza(pacijentID);







