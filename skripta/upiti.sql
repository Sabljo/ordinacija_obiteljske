/*Upit koji vra�a ime i prezime pacijenta kojima je kao terapija propisan lijek sa najve�om
doplatom,ime i prezime doktora te naziv tog lijeka */
SELECT pacijent.ime || ' ' || pacijent.prezime as "Ime pacijenta",
z.ime || ' ' || z.prezime as "Ime doktora",lijek.naziv
FROM zaposlenik z INNER JOIN doktor ON doktorID=zaposlenikID INNER JOIN
ordinacija USING (doktorID) INNER JOIN pacijent USING (ordinacijaID)
INNER JOIN anamneza USING (pacijentID) INNER JOIN dijag_anamneza USING 
(anamnezaID) INNER JOIN terapija USING (dijag_anamnezaID) INNER JOIN
lijek USING (sifra_lijeka)
WHERE lijek.doplata =(SELECT MAX(lijek.doplata)FROM lijek)
ORDER BY z.ime,z.prezime,lijek.naziv;


/*Upit koji vra�a naziv ordinacije,ime lijeka koji je najvi�e propisan u toj ordinaciji te stupac u koji 
spremamo broj ,odnosno koliko je puta lijek propisan*/
SELECT 'Spec.ordinacija doktora/ice ' || z.ime || ' ' || z.prezime as "Naziv ordinacije",
naziv as "Naziv lijeka",br_lijeka as "Broj propisivanja"
FROM (SELECT COUNT(*) br_lijeka,ordinacijaID,lijek.naziv
    FROM zaposlenik z INNER JOIN doktor ON doktorID=zaposlenikID INNER JOIN
    ordinacija USING (doktorID) INNER JOIN pacijent USING (ordinacijaID)
    INNER JOIN anamneza USING (pacijentID) INNER JOIN dijag_anamneza USING 
    (anamnezaID) INNER JOIN terapija USING (dijag_anamnezaID) INNER JOIN
    lijek USING (sifra_lijeka)
    GROUP BY ordinacijaID,lijek.naziv)
    INNER JOIN ordinacija USING (ordinacijaID) INNER JOIN doktor USING(doktorID)
    INNER JOIN zaposlenik z ON doktorID=zaposlenikID
WHERE (br_lijeka,ordinacijaID) IN
 (SELECT MAX(br_lijeka),ordinacijaID
    FROM(
    SELECT COUNT(*) br_lijeka,ordinacijaID,lijek.naziv
    FROM ordinacija  INNER JOIN pacijent USING (ordinacijaID)
    INNER JOIN anamneza USING (pacijentID) INNER JOIN dijag_anamneza USING 
    (anamnezaID) INNER JOIN terapija USING (dijag_anamnezaID) INNER JOIN
    lijek USING (sifra_lijeka)
    GROUP BY ordinacijaID,lijek.naziv)
    GROUP BY ordinacijaID)
ORDER BY "Naziv ordinacije";

/*Upit koji vra�a nazive odjela na koje su pacijenti ordinacije Zvonimira Jelkovi�a upu�eni,
a pacijetni ordinacije Sandre Matkovi� u posljednjih godinu dana nisu */
(SELECT DISTINCT odjel as "Naziv odjela"
FROM zaposlenik z INNER JOIN doktor ON doktorID=zaposlenikID INNER JOIN
ordinacija USING (doktorID) INNER JOIN pacijent USING (ordinacijaID)
INNER JOIN anamneza USING (pacijentID) INNER JOIN dijag_anamneza USING 
(anamnezaID) INNER JOIN uputnice USING (dijag_anamnezaID)
WHERE z.ime='Zvonimir' and z.prezime='Jelkovi�' and SYSDATE - anamneza.datum <365)
MINUS 
(SELECT DISTINCT odjel as "Naziv odjela"
FROM zaposlenik z INNER JOIN doktor ON doktorID=zaposlenikID INNER JOIN
ordinacija USING (doktorID) INNER JOIN pacijent USING (ordinacijaID)
INNER JOIN anamneza USING (pacijentID) INNER JOIN dijag_anamneza USING 
(anamnezaID) INNER JOIN uputnice USING (dijag_anamnezaID)
WHERE z.ime='Sandra' and z.prezime='Matkovi�'and SYSDATE - anamneza.datum <365)
ORDER BY "Naziv odjela";


/*Upit koji vra�a naziv odjela takav da je  broj �ena  koje su 
upu�ene na taj odjel ve�i od 1 u ordinaciji Zvonimira Jelkovi�a*/
SELECT odjel as "Naziv odjela"
FROM zaposlenik z INNER JOIN doktor ON doktorID=zaposlenikID INNER JOIN
ordinacija USING (doktorID) INNER JOIN pacijent USING (ordinacijaID)
INNER JOIN anamneza USING (pacijentID) INNER JOIN dijag_anamneza USING 
(anamnezaID) INNER JOIN uputnice USING (dijag_anamnezaID)
where (z.ime='Zvonimir' and z.prezime='Jelkovi�' and spol='F')
GROUP BY odjel
HAVING COUNT(DISTINCT pacijentID)>=2;

/*Upit koji vra�a ime i prezime pacijenta cija je doktorica Antonija Leki� i ukupnu svotu novca
koji je platio za sve dolaske doktoru jer nema dopunsko(svaki oblik pregleda pla�a se 10 kuna)*/
SELECT pacijent.ime || ' ' || pacijent.prezime as "Ime pacijenta",ukupna_svota
FROM 
(SELECT pacijentID,SUM(CASE WHEN (SYSDATE - datum_isteka >0) THEN 10 ELSE 0 END) as ukupna_svota 
FROM zaposlenik z INNER JOIN doktor ON doktorID=zaposlenikID INNER JOIN
ordinacija USING (doktorID) INNER JOIN pacijent USING (ordinacijaID)
INNER JOIN anamneza USING (pacijentID)INNER JOIN dop_pac USING (pacijentID)
WHERE z.ime='Antonija' and z.prezime='Leki�'
GROUP BY pacijentID)
INNER JOIN pacijent USING (pacijentID)
WHERE ukupna_svota<>0;

/* Upit koji vra�a  ime i prezime pacijenta  kojima je najdu�e bolovanje
 trajalo du�e od prosje�nog trajanja bolovanja u toj ordinaciji te stupac max_trajanje_bolovanja
 i prosje�no trajanje bolovanja u ordinaciji */
SELECT p.ime || ' ' || p.prezime as "Ime pacijenta","max_trajanje_bolovanja",avg_br_dana as "prosje�no trajanje bolovanja u ordinaciji"
FROM (
(SELECT MAX(bolovanja.datum_isteka-bolovanja.datum_pocetka) as "max_trajanje_bolovanja",pacijentID
FROM  pacijent INNER JOIN anamneza USING (pacijentID) INNER JOIN dijag_anamneza USING 
(anamnezaID) INNER JOIN bolovanja USING (dijag_anamnezaID)
GROUP BY pacijentID)
INNER JOIN pacijent p USING(pacijentID))
INNER JOIN
(SELECT ROUND(AVG(bolovanja.datum_isteka-bolovanja.datum_pocetka)) avg_br_dana,ordinacijaID
FROM  pacijent p INNER JOIN anamneza USING (pacijentID) INNER JOIN dijag_anamneza USING 
(anamnezaID) INNER JOIN bolovanja USING (dijag_anamnezaID)
GROUP BY ordinacijaID) podupit 
USING (ordinacijaID)
WHERE "max_trajanje_bolovanja">avg_br_dana;

/*Ime i prezime pacijenta kojima je na nekom pregledu propisana terapija
,ali im nije odobreno bolovanje */
SELECT DISTINCT pacijent.ime || ' ' || pacijent.prezime as "Ime pacijenta",ordinacijaID
FROM ordinacija INNER JOIN pacijent USING (ordinacijaID)
    INNER JOIN anamneza USING (pacijentID) INNER JOIN dijag_anamneza USING 
    (anamnezaID) 
WHERE dijag_anamnezaID IN (SELECT dijag_anamnezaID FROM terapija) and
dijag_anamnezaID NOT IN (SELECT dijag_anamnezaID FROM bolovanja)
ORDER BY ordinacijaID;

/*Upit koji vra�a koliko prosje�no dana se �eka za va�enje krvi u nekoj ordinaciji */
SELECT 'Spec.ordinacija doktora/ice ' || z.ime || ' ' || z.prezime as "Naziv ordinacije",
br_dana as "Prosje�an broj dana �ekanja za va�enje krvi"
FROM 
(SELECT ROUND(AVG(BR)) br_dana,ordinacijaID
FROM (SELECT DISTINCT ordinacijaID,pacijentID,dijag_anamnezaID,pretraga.datum-anamneza.datum AS BR
FROM zaposlenik z INNER JOIN doktor ON doktorID=zaposlenikID INNER JOIN
ordinacija USING (doktorID) INNER JOIN pacijent USING (ordinacijaID)
INNER JOIN anamneza USING (pacijentID) INNER JOIN dijag_anamneza USING 
(anamnezaID) INNER JOIN pretraga USING (dijag_anamnezaID)
WHERE naziv_pretrage='Krv')
GROUP BY ordinacijaID)
INNER JOIN ordinacija USING (ordinacijaID) INNER JOIN doktor USING(doktorID)
INNER JOIN zaposlenik z ON doktorID=zaposlenikID
ORDER BY "Naziv ordinacije";












