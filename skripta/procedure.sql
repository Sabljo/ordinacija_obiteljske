--PROCEDURE ZA UNOS

/*Dana procedura sluzi za unos lijeka*/
CREATE SEQUENCE seq_lijek_sifra
START WITH 1027;
CREATE OR REPLACE PROCEDURE unos_lijeka (
	lijek_naziv IN lijek.naziv % TYPE,
	proizvodjac IN lijek.proizvodjac % TYPE,
    lijek_cijena IN lijek.cijena % TYPE,
    l_doplata IN lijek.doplata % TYPE :=null
) AS
	rez INTEGER;
    lijek_postoji EXCEPTION;
BEGIN
  SELECT COUNT(*) INTO rez 
  FROM lijek
  WHERE naziv = lijek_naziv;
  
  IF rez = 0  THEN
        INSERT INTO lijek 
        VALUES (seq_lijek_sifra.NEXTVAL, lijek_naziv,proizvodjac,lijek_cijena,l_doplata);
        COMMIT;
  ELSE
    raise lijek_postoji;
  END IF;
  
EXCEPTION
    WHEN  lijek_postoji then
    RAISE_APPLICATION_ERROR(-20001,'Taj lijek ve� se nalazi u tablici lijek');  
END unos_lijeka;
/
call unos_lijeka('Sumamed','Sandoz',27.44);
select *
from lijek;
------------------------------------------------------------------------
------------------------------------------------------------------------
------------------------------------------------------------------------
CREATE SEQUENCE seq_zap
START WITH 1;
/*Dana procedura sluzi za unos instance entiteta zaposlenika i podtipova doktor i med.sestra*/
CREATE OR REPLACE PROCEDURE unos_zaposlenika (
	z_ime IN zaposlenik.ime % TYPE,
	z_prezime IN zaposlenik.prezime % TYPE,
    z_adresa IN zaposlenik.adresa % TYPE,
    z_kontakt IN zaposlenik.kontakt % TYPE ,
    z_placa IN zaposlenik.placa % TYPE ,
    z_tip IN zaposlenik.tip_zaposlenika % TYPE ,
    z_id IN INTEGER
) AS
	rez1 INTEGER;
    rez2 INTEGER;
    i INTEGER;
    zap_tip EXCEPTION;
    zap_postoji EXCEPTION;
BEGIN
    IF z_tip<>'doktor' and  z_tip<>'med.sestra' THEN
        RAISE zap_tip;
    END IF;
    IF z_tip='doktor' THEN
        SELECT COUNT(*) INTO rez1 
        FROM doktor
        WHERE IDdoktorata=z_id;
        SELECT COUNT(*) INTO rez2 
        FROM zaposlenik
        WHERE ime=z_ime and prezime=z_prezime and kontakt=z_kontakt
        and placa=z_placa and tip_zaposlenika=z_tip and adresa=z_adresa;
        

        IF rez1=0 and rez2=0 THEN
            i:=seq_zap.NEXTVAL;
            INSERT INTO zaposlenik 
            VALUES (i, z_ime,z_prezime,z_adresa,z_kontakt,z_placa,z_tip);
            INSERT INTO doktor 
            VALUES (i,z_id);
            COMMIT;
        ELSE
            RAISE zap_postoji;
        END IF;
    END IF;
    IF z_tip='med.sestra' THEN
        SELECT COUNT(*) INTO rez1 
        FROM med_sestra
        WHERE broj_sanitarne_knjizice=z_id;
        
        SELECT COUNT(*) INTO rez2 
        FROM zaposlenik
        WHERE ime=z_ime and prezime=z_prezime and kontakt=z_kontakt
        and placa=z_placa and tip_zaposlenika=z_tip and adresa=z_adresa;
        
        IF rez1=0 and rez2=0 THEN
            i:=seq_zap.NEXTVAL;
            INSERT INTO zaposlenik 
            VALUES (i, z_ime,z_prezime,z_adresa,z_kontakt,z_placa,z_tip);
            INSERT INTO med_sestra
            VALUES (i,z_id);
            COMMIT;
        ELSE
            RAISE zap_postoji;
        END IF;
    END IF;

EXCEPTION
    WHEN  zap_tip THEN
    RAISE_APPLICATION_ERROR(-20001,'Taj tip zaposlenika ne postoji');  
    WHEN  zap_postoji THEN
    RAISE_APPLICATION_ERROR(-20001,'Osoba sa tim osobnim podacima je ve� une�ena u tablicu zaposlenik ili doktorat/br_san_knj ve� postoji');  
END unos_zaposlenika;
/
call unos_zaposlenika('Luka', 'Mari�', 'Ru�ina 10, Osijek', '0913603214',10000.00 ,'doktor',48735974);
---------------------------------------------------------------
---------------------------------------------------------------
---------------------------------------------------------------
---------------------------------------------------------------

--PROCEDURE ZA DOHVAT

SET SERVEROUTPUT ON SIZE 10000;
/*Ime i prezime pacijenta kojima je doktor onaj �to prima procedura te kojima je otvoreno bolovanje u posljednjih godinu dana*/
CREATE OR REPLACE PROCEDURE bol_u_godini_dana (
	d_ime IN zaposlenik.ime % TYPE,
    d_prezime IN zaposlenik.prezime % TYPE
    )
AS
    p_ime VARCHAR(20);
    p_prezime VARCHAR(20);
    c_counter INTEGER;
    p_cursor  SYS_REFCURSOR;
BEGIN
    SELECT COUNT(*) INTO c_counter
    FROM zaposlenik INNER JOIN doktor ON doktorID=zaposlenikID
    WHERE ime=d_ime and prezime=d_prezime;
    
    IF c_counter= 1 THEN 
        OPEN p_cursor FOR
            SELECT DISTINCT pacijent.ime,pacijent.prezime
            FROM zaposlenik z INNER JOIN doktor ON doktorID=zaposlenikID INNER JOIN
            ordinacija USING (doktorID) INNER JOIN pacijent USING (ordinacijaID)
            INNER JOIN anamneza USING (pacijentID) INNER JOIN dijag_anamneza USING 
            (anamnezaID) INNER JOIN bolovanja USING (dijag_anamnezaID)
            WHERE z.ime=d_ime and z.prezime=d_prezime and sysdate-bolovanja.datum_isteka<365
            ORDER BY pacijent.ime,pacijent.prezime ;
        DBMS_OUTPUT.PUT_LINE( 'IME  I PREZIME PACIJENTA');
        DBMS_OUTPUT.PUT_LINE( '------------------------');
        LOOP 
            FETCH p_cursor 
            INTO p_ime,p_prezime;
            EXIT WHEN p_cursor%NOTFOUND;
            DBMS_OUTPUT.PUT_LINE( p_ime || '  ' || p_prezime);
        END LOOP;
        CLOSE p_cursor;
    ELSE 
         DBMS_OUTPUT.PUT_LINE('Ne postoji navedeni doktor');
    END IF;
END bol_u_godini_dana;
/
call bol_u_godini_dana('Luka','Mari�');

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

/*Dana procedura vra�a imena i prezimena svih pacijenata i njihovih ordinacija 
koji su se cijeplili cjepivom �ije ime procedura prima kao argument*/
CREATE OR REPLACE PROCEDURE pacijent_cijepljenje (
	naziv_cjep IN cjepivo.naziv_cjepiva % TYPE) 
AS
    p_ime VARCHAR(20);
    p_prezime VARCHAR(20);
    ime_ordinacije VARCHAR(60);
    c_counter INTEGER;
    p_cursor  SYS_REFCURSOR;
BEGIN
    SELECT COUNT(*) INTO c_counter
    FROM cjepivo
    WHERE naziv_cjepiva=naziv_cjep;
    
    IF c_counter= 1 THEN 
        OPEN p_cursor FOR
            SELECT pacijent.ime , pacijent.prezime,'Spec.ordinacija doktora/ice ' || z.ime || ' ' || z.prezime
            FROM zaposlenik z INNER JOIN doktor ON doktorID=zaposlenikID INNER JOIN
            ordinacija USING (doktorID) INNER JOIN pacijent USING (ordinacijaID)
            INNER JOIN anamneza USING (pacijentID) INNER JOIN dijag_anamneza USING 
            (anamnezaID) INNER JOIN cijepljenje USING (dijag_anamnezaID) INNER JOIN
            cjepivo USING(cjepivoID)
            WHERE naziv_cjepiva=naziv_cjep
            ORDER BY z.ime,z.prezime;
        LOOP 
            FETCH p_cursor 
            INTO p_ime,p_prezime,ime_ordinacije;
            EXIT WHEN p_cursor%NOTFOUND;
            DBMS_OUTPUT.PUT_LINE(ime_ordinacije || '      ' || p_ime || '  ' || p_prezime);
        END LOOP;
        CLOSE p_cursor;
    ELSE 
         DBMS_OUTPUT.PUT_LINE('Navedeno cijepivo ne postoji');
    END IF;
END pacijent_cijepljenje;
/
call pacijent_cijepljenje('ANA-TE');

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------

--PROCEDURE ZA A�URIRANJE

/*Dana procedura a�urira cijenu i doplatu nekog lijeka*/
CREATE OR REPLACE PROCEDURE azuriranje_lijeka (
	lijek_naziv IN lijek.naziv % TYPE,
    lijek_cijena IN lijek.cijena % TYPE,
    l_doplata IN lijek.doplata % TYPE :=NULL
) AS
	rez INTEGER;
    lijek_ne_postoji EXCEPTION;
  
BEGIN
    SELECT COUNT(*) INTO rez 
    FROM lijek
    WHERE naziv = lijek_naziv;
    
    IF rez=0 THEN
        raise lijek_ne_postoji;
    END IF;
  
    IF rez = 1  THEN
            UPDATE lijek
            SET cijena=lijek_cijena,doplata=l_doplata
            WHERE naziv = lijek_naziv;
            COMMIT;
    END IF;
    EXCEPTION
        WHEN  lijek_ne_postoji then
            RAISE_APPLICATION_ERROR(-20001,'Taj lijek ne postoji u tablici lijek');  
END azuriranje_lijeka;
/
call azuriranje_lijeka ('Sumamed',22.89,null);
select *
from lijek;
------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------
SELECT * FROM pacijent;
CREATE OR REPLACE PROCEDURE azuriranje_pacijenta (
	pacijent_id IN pacijent.pacijentID % TYPE,
    p_ime IN pacijent.ime % TYPE,
    p_prezime IN pacijent.prezime % TYPE,
    nova_adresa IN pacijent.adresa % TYPE,
    novi_tel IN pacijent.telefon % TYPE,
    nova_ordinacija IN ordinacija.ordinacijaID % TYPE
) AS
	rez1 INTEGER;
    rez2 INTEGER;
    excep1  EXCEPTION;
    excep2 EXCEPTION;
BEGIN
  SELECT COUNT(*) INTO rez1
  FROM pacijent
  WHERE pacijentID=pacijent_id;
  
  SELECT COUNT(*) INTO rez2 
  FROM ordinacija
  WHERE ordinacijaID=nova_ordinacija;
  
  IF rez1=0 THEN
    RAISE excep1;
  END IF;
  
  IF rez2=0 THEN
    RAISE excep2;
  END IF;
  
  IF rez2 = 1  and rez1 = 1 THEN
        UPDATE pacijent
        SET ime=p_ime,prezime=p_prezime,ordinacijaID=nova_ordinacija,adresa=nova_adresa,telefon=novi_tel
        WHERE pacijentID = pacijent_id;
  END IF;
  COMMIT;
EXCEPTION
    WHEN excep1 THEN
    RAISE_APPLICATION_ERROR(-20001,'Taj pacijent ne postoji'); 
    WHEN excep2 THEN
    RAISE_APPLICATION_ERROR(-20001,'Ta ordinacija ne postoji'); 
END azuriranje_pacijenta;
/
call azuriranje_pacijenta(2,'Ana','�aravanja','Svijalska 7,�epin','099677851',4);
